#ifndef __BRU_DATA_H
#define __BRU_DATA_H


#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>

#define BRU_UI_LAYER_BRU_UI_LAYER_H_

#define BRU_PRESET_WATER_AMOUNT_MAX        (350.0f)	//(450.0f)
#define BRU_PRESET_WATER_AMOUNT_INF        (400.0f) //(500.0f)
#define BRU_PRESET_WATER_AMOUNT_MIN        (50.0f)
#define BRU_PRESET_WATER_AMOUNT_STEP       (50.0f)

#define BRU_PRESET_TEMPERATURE_MAX         (100.0f)
#define BRU_PRESET_TEMPERATURE_MIN         (40.0f)
#define BRU_PRESET_TEMPERATURE_STEP        (5.0f)

#define BRU_PRESET_TIME_MAX                (3600)
#define BRU_PRESET_TIME_MIN                (10)
#define BRU_PRESET_TIME_STEP1              (10)
#define BRU_PRESET_TIME_STEP2              (60)


#define BRU_DEFAULT_PRESET_ARR_SIZE        (7)          // min 5

#define BRU_HOT_WATER_DISPENSER_INDEX      (BRU_DEFAULT_PRESET_ARR_SIZE - 3)

#define BRU_ALARM_INDEX                    (BRU_DEFAULT_PRESET_ARR_SIZE - 2)

#define BRU_BLE_PRESET_IND                 (BRU_DEFAULT_PRESET_ARR_SIZE - 1)




typedef enum
{
    UI_ERROR_CODE_NONE = 0,
    UI_ERROR_CODE_01,                                   /*!<  */
    UI_ERROR_CODE_02,                                   /*!<  */
    UI_ERROR_CODE_03,                                   /*!<  */
    UI_ERROR_CODE_04,                                   /*!<  */
//		UI_ERROR_CODE_10 = 10,										// PV motor don't connected
//		UI_ERROR_CODE_11 = 11,										// PV swaped Pos_Lim_Switches
//		UI_ERROR_CODE_13 = 13,										// destroed reductor of PV	
//		//UI_ERROR_CODE_15 = 15,										// ??? PV defective/don't connected the limit switch OR bad setup of current limit level
//		UI_ERROR_CODE_16 = 16											// PV check nozzle (Close Switch on)
}bru_UI_Error_Code;

typedef enum
{
    MS_BLUETOOTH_OFF = 0,                                         /*!<  */
    MS_BLUETOOTH_ON = 1,
    MS_BLUETOOTH_END
}bru_MS_Bletooth_Sate;
typedef enum
{
    MS_ENGLISH = 0,                                         /*!<  */
    MS_FRENCH,                                              /*!<  */
    MS_DEUTCH,                                              /*!<  */
    MS_SPANISH,                                              /*!<  */
    MS_LANGUAGE_END
}bru_MS_Language;
typedef enum
{
    MS_IMPERIAL = 0,                                        /*!<  */
    MS_METRIC,                                               /*!<  */
    MS_UNITS_END
}bru_MS_Units;
typedef enum
{
    MS_RINSE_TO_CUP = 0,                                        /*!<  */
    MS_RINSE_TO_TRAY,                                           /*!<  */
    MS_RINSE_OFF,                                                /*!<  */
    MS_RINSE_CONFIG_END
}bru_MS_Rinse_Config;
typedef enum
{
    MS_FILTER_INSTALLED = 0,                                        /*!<  */
    MS_FILTER_NOT_INSTALLED,                                         /*!<  */
    MS_FILTER_SETUP_END
}bru_MS_Water_Filter_Setup;
typedef enum
{
    MS_FILTER_OK = 0,                                               /*!<  */
    MS_FILTER_REPLACE,                                               /*!<  */
    MS_FILTER_STATUS_END
}bru_MS_Water_Filter_Status;
typedef enum
{
    MS_MACHINE_OK = 0,                                              /*!<  */
    MS_MACHINE_CLEAN                                                /*!<  */
}bru_MS_Maintenance_Satus;

typedef enum{
	TIMEFORMAT_24,
	TIMEFORMAT_12,
}bru_TimeFormat;

typedef enum
{
	MS_BUZZER_OFF,
	MS_BUZZER_ON,
}Bru_Buzzer_State_Setup;

typedef struct bru_Preset_
{
    float                   temperature;

    uint32_t                brewingTime;

    float                   waterAmount;

} bru_Preset;

typedef struct bru_Machine_Setup_
{
    bru_MS_Bletooth_Sate             bluetoothState;

    bru_MS_Language                  language;

    bru_MS_Units                     units;

    bru_MS_Rinse_Config              rinseConfiguration;

    bru_MS_Water_Filter_Setup        filterSetup;

    bru_MS_Water_Filter_Status       filterStatus;
	
	  bru_TimeFormat timeformat;

    uint16_t                         rinseAmount;

    uint8_t                          redLed;

    uint8_t                          greenLed;

    uint8_t                          blueLed;

    uint8_t                          coldTeaPercentage;
		
		uint8_t 												 buzzerState;

}bru_Machine_Setup;

typedef struct bru_Preset_ *bru_Preset_Arr;
typedef struct bru_Machine_Setup_ * bru_Machine_Setup_Handle;
typedef struct bru_UI_Interface_ * bru_UI_Interface_Handle;

typedef struct bru_UI_Interface_
{
    bru_Preset_Arr               presetsArr;

    bru_Machine_Setup_Handle     hMachineSetup;

//    bru_UI_Error_Code            errorCoder;
    uint16_t 					           errorCode;

    uint32_t                     iCurrentTime;

    uint32_t                     iAlarmTime;

    uint16_t                     iCountdownBrewingTime;             //uint16_t                     iCountdownBrewingTime : 12;

    uint8_t                      iPresetCurrentIndex;               //uint8_t                      iPresetCurrentIndex : 4;

    uint8_t                      iPresetArrSize;                    //uint8_t                      iPresetArrSize : 4;

    uint8_t                      iWindowRecall;                     //uint8_t                      iWindowRecall : 2;

    uint8_t                      bLongPressedButtonState;           //uint8_t                      bLongPressedButtonState : 1;

    uint8_t                      bBrewingStartFlag;                 /* This flag is SET during running the brewing process and RESET before the Serving window */

    uint8_t                      bChamberWashingFlag;               /* This flag need for process execution chamber washing into the tray */

    uint8_t                      bBrewingProcessFlag;               /* This flag is SET during running the brewing process and RESET when stepping to the Home window */

    uint8_t                      bBrewingProcessResumeFlag;

    uint8_t                      bSelfTestingReadyFlag;

    uint8_t                      bNavigationLoopFlag;

    uint8_t                      bBrewingBluetoothPreset;
		
		uint8_t                      bMenuBackFlag;

    uint8_t 										 bMenuPresetFlag;
		
		uint8_t 										 bru_device_status; 
		
} bru_UI_Interface;

void bru_UI_Init(void);
bru_UI_Interface_Handle get_ui_interface(void);
#ifdef __cplusplus
           }
#endif
#endif

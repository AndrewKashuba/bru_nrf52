#include "bru_data.h"
#include "unix_time.h"


static bru_UI_Interface     uiInterface;
static bru_Preset           presetsArr[BRU_DEFAULT_PRESET_ARR_SIZE];
static bru_Machine_Setup    bruMachineSetup;
static bru_UI_Interface_Handle  hUserInterface;
static uint16_t bru_brewing_time_arr[BRU_DEFAULT_PRESET_ARR_SIZE] = {10, 150, 300, 100, 150, 250};
static float bru_brewing_termo_arr[BRU_DEFAULT_PRESET_ARR_SIZE] = {70.0f, 95.0f, 80.0f, 70.0f,  40.0f, 65.0f};
static float bru_brewing_water_amount_arr[BRU_DEFAULT_PRESET_ARR_SIZE] = {50.0f, 350.0f, 300.0f, 350.0f, 400.0f, 250.0f};

static void bru_UI_Presets_Init(bru_Preset_Arr arrPresets)
{
    for(int i = 0; i < BRU_DEFAULT_PRESET_ARR_SIZE; i++)
    {
        arrPresets[i].brewingTime = bru_brewing_time_arr[i];
        arrPresets[i].temperature = bru_brewing_termo_arr[i];
        arrPresets[i].waterAmount = bru_brewing_water_amount_arr[i];
    }
}

bru_UI_Interface_Handle get_ui_interface(void)
{
	return hUserInterface;
}

void bru_UI_Init(void)
{
 
    hUserInterface = &uiInterface;

    //need to add read/write from flash function
    bru_UI_Presets_Init(presetsArr);

    hUserInterface->presetsArr = presetsArr;
    hUserInterface->iPresetArrSize = BRU_DEFAULT_PRESET_ARR_SIZE;
    hUserInterface->iPresetCurrentIndex = 0;
    hUserInterface->bLongPressedButtonState = false;
    hUserInterface->iWindowRecall = 0;
    hUserInterface->iCountdownBrewingTime = 0;
    hUserInterface->bBrewingStartFlag = false;
    hUserInterface->bNavigationLoopFlag = false;
    hUserInterface->bBrewingProcessResumeFlag = false;
    hUserInterface->bSelfTestingReadyFlag = false;
    hUserInterface->bBrewingBluetoothPreset = true;
   

    //Seconds_set(1606824000);
    hUserInterface->iCurrentTime = INIT_UTC;               //01.12.2020 12:00;
    hUserInterface->iAlarmTime = 1606806000;                        //01.12.2020 07:00;

    hUserInterface->errorCode = (int) UI_ERROR_CODE_NONE;
    hUserInterface->bChamberWashingFlag = true;
    hUserInterface->bBrewingProcessFlag = false;

    hUserInterface->hMachineSetup = &bruMachineSetup;
    hUserInterface->hMachineSetup->bluetoothState = MS_BLUETOOTH_ON;
    hUserInterface->hMachineSetup->coldTeaPercentage = 0;
    hUserInterface->hMachineSetup->filterSetup = MS_FILTER_NOT_INSTALLED;
    hUserInterface->hMachineSetup->filterStatus = MS_FILTER_OK;
    hUserInterface->hMachineSetup->language = MS_ENGLISH;
    hUserInterface->hMachineSetup->redLed = 0;
    hUserInterface->hMachineSetup->greenLed = 0;
    hUserInterface->hMachineSetup->blueLed = 0;
    hUserInterface->hMachineSetup->rinseAmount = 0;
    hUserInterface->hMachineSetup->rinseConfiguration = MS_RINSE_TO_TRAY;
    hUserInterface->hMachineSetup->units = MS_METRIC; 
    hUserInterface->hMachineSetup->timeformat = TIMEFORMAT_24;
		
		
  
}

#ifndef __BSP_MENU_INFO_H
#define __BSP_MENU_INFO_H
#include "BSP_KeyScan.h"
#include "target_command.h"
#include "main.h"
#include "product_id.h"
#include "home_menu_2.h"
#include "power_on_menu_1.h"
#include "preset_choice_menu_3.h"
#include "preset_main_edit_menu_4.h"
#include "brewing_menu_5.h"
#include "washing_menu_6.h"
#include "preset_setup_menu_8.h"
#include "machine_setup_menu_9.h"
#include "steeping_time_edit_menu_10.h"
#include "water_temp_edit_menu_11.h"
#include "water_amount_edit_menu_12.h"
#include "preset_edit_menu_13.h"
#include "hot_water_edit_menu_14.h"
#include "alarm_time_edit_menu_15.h"
#include "time_edit_menu_16.h"
#include "pre_alarm_edit_menu_17.h"
#include "bluetooth_state_menu_18.h"
#include "language_menu_19.h"
#include "time_format_menu_20.h"
#include "unit_setting_menu_21.h"
#include "rinse_config_menu_22.h"
#include "water_filter_status_menu_23.h"
#include "maintenance_status_menu_24.h"
#include "rgb_status_menu_25.h"
#include "buzzer_status_menu_26.h"
#include "bru_ui_disp.h"
#include "record_data_core.h"
#include "pwm_buzzer_core.h"
#include "custome_ble_data_handler.h"

#include "bru_Menu_Check.h"
#ifdef __cplusplus
extern "C" {
#endif



extern const uint8_t command_32hz_timer_evt;

#define BACKLIGHT_TIMER_ID				20 
#define BACKLIGHT_ON_VALUE				(TIMER_CALL_HZ*180)    // 180��

#define  ENCODE_FILTER_TIMER_ID		(BACKLIGHT_TIMER_ID+1)
#define  ENCODE_FILTER_TIMER_VALUE			8



extern const uint8_t button1_push_evt;
extern const uint8_t button1_long_push_evt;
extern const uint8_t button1_release_evt; 
extern const uint8_t button2_push_evt;
extern const uint8_t button2_long_push_evt;
extern const uint8_t button2_release_evt; 

extern const uint8_t encoder_a_push_evt;
extern const uint8_t encoder_a_long_push_evt;
extern const uint8_t encoder_a_release_evt; 
extern const uint8_t encoder_b_push_evt;
extern const uint8_t encoder_b_long_push_evt;
extern const uint8_t encoder_b_release_evt; 
extern const uint8_t encoder_button_push_evt;
extern const uint8_t encoder_button_long_push_evt;
extern const uint8_t encoder_button_release_evt; 




extern CONST MENU_KEY tick_table;




unsigned char preMessageHandler(uint16_t *msg);
void init_key(void);
void backlight_init(void);
void set_backlight_value(uint16_t value);	


#ifdef __cplusplus
}
#endif

#endif


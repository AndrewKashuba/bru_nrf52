#include "BSP_menu_info.h"



#define NRF_LOG_MODULE_NAME language_menu_19
#define NRF_LOG_LEVEL       3
#define NRF_LOG_INFO_COLOR  4
#define NRF_LOG_DEBUG_COLOR 3
#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();





//######################################################################################################################################
static void language_19_updateDisplay(void *p);


static void language_19_right_handler(void *msg);
static void language_19_left_handler(void *msg);
static void language_19_save_handler(void *msg);
static void language_19_encode_handler(void *msg);

/*-----language Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_language_menu_19_FuncEn)
		ON_VOID_POINTER_MESSAGE(button2_release_evt,language_19_save_handler)
		ON_VOID_POINTER_MESSAGE(encoder_a_release_evt,language_19_right_handler)
		ON_VOID_POINTER_MESSAGE(encoder_b_release_evt,language_19_left_handler)
		ON_VOID_POINTER_MESSAGE(encoder_button_release_evt,language_19_encode_handler)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_language_menu_19_FuncEn,Menu_Function_language_19_cmd_func,preMessageHandler)   


CONST MENU_KEY op_language_menu_19_table= 
	{&Menu_Function_language_19_cmd_func,language_19_updateDisplay,NULL,NULL};   		

	


/**
* @brieaf  encode key
*/
static void language_19_encode_handler(void *msg)
{
	NRF_LOG_INFO("language_19_encode_handler");
}

/**
* @brieaf right key
*/
static void language_19_right_handler(void *msg)
{
	NRF_LOG_INFO("language_19_right_handler");
}

/**
* @brieaf left key
*/
static void language_19_left_handler(void *msg)
{
	NRF_LOG_INFO("language_19_left_handler");
}
/**
* @brieaf save key
*/
static void language_19_save_handler(void *msg)
{
	write_record_info_to_flash();
	get_machine_setup_info();
	NRF_LOG_INFO("language_19_save_handler");
}

/**
* @brieaf language page display 
*/
static void language_19_updateDisplay(void *p)
{
	NRF_LOG_INFO("=========language_19_updateDisplay============");
}



/**
* @brieaf jump to language page function
*/
void goto_language_menu_19_disp(void)
{
	jump_ui_func(&op_language_menu_19_table);
	
}














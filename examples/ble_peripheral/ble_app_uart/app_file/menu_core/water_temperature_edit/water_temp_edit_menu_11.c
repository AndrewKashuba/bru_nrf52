#include "BSP_menu_info.h"



#define NRF_LOG_MODULE_NAME water_temp_edit_11
#define NRF_LOG_LEVEL       3
#define NRF_LOG_INFO_COLOR  4
#define NRF_LOG_DEBUG_COLOR 3
#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();





//######################################################################################################################################
static void water_temp_11_updateDisplay(void *p);


static void water_temp_11_save_handler(void *msg);
static void water_temp_11_right_handler(void *msg);
static void water_temp_11_left_handler(void *msg);


/*-----water_temp Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_water_temp_11_FuncEn)
		ON_VOID_POINTER_MESSAGE(button2_release_evt,water_temp_11_save_handler)
		ON_VOID_POINTER_MESSAGE(encoder_a_release_evt,water_temp_11_right_handler)
		ON_VOID_POINTER_MESSAGE(encoder_b_release_evt,water_temp_11_left_handler)
		ON_VOID_POINTER_MESSAGE(encoder_button_release_evt,water_temp_11_save_handler)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_water_temp_11_FuncEn,Menu_Function_water_temp_11_cmd_func,preMessageHandler)   


CONST MENU_KEY op_water_temp_11_table= 
	{&Menu_Function_water_temp_11_cmd_func,water_temp_11_updateDisplay,NULL,NULL};   		

	


/**
* @brieaf right key
*/
static void water_temp_11_right_handler(void *msg)
{
	NRF_LOG_INFO("water_temp_11_right_handler");
	bru_BrewingTime_adj_water_temper_ENG(ENCODE_RIGHT);
}

/**
* @brieaf left key
*/
static void water_temp_11_left_handler(void *msg)
{
	NRF_LOG_INFO("water_temp_11_left_handler");
	bru_BrewingTime_adj_water_temper_ENG(ENCODE_LEFT);
}
/**
* @brieaf save key
*/
static void water_temp_11_save_handler(void *msg)
{
	write_record_info_to_flash();
	get_all_preset_info();
	DISP_FLAG_T *flag = get_disp_flag();
	flag->sel_draw_flag_1 = 1;
	goto_water_temper_disp();
	NRF_LOG_INFO("water_temp_11_save_handler");
}

/**
* @brieaf water_temp page display
*/
static void water_temp_11_updateDisplay(void *p)
{
	NRF_LOG_INFO("=========steeping_time_10_updateDisplay============");
	DISP_FLAG_T *flag = get_disp_flag();
	bru_LCD_WaterTemperature_Edit_ENG(&flag->sel_draw_flag_1,(const char*)"SAVE");
}



/**
* @brieaf jump to water_temp page funtion
*/
void goto_water_temp_edit_11_disp(void)
{
	jump_ui_func(&op_water_temp_11_table);
	
}














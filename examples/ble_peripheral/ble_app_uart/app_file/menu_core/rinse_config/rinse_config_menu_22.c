#include "BSP_menu_info.h"



#define NRF_LOG_MODULE_NAME rinse_config_22
#define NRF_LOG_LEVEL       3
#define NRF_LOG_INFO_COLOR  4
#define NRF_LOG_DEBUG_COLOR 3
#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();





//######################################################################################################################################
static void rinse_config_22_updateDisplay(void *p);


static void rinse_config_22_right_handler(void *msg);
static void rinse_config_22_left_handler(void *msg);
static void rinse_config_22_save_handler(void *msg);


/*-----rinse_config Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_rinse_config_22_FuncEn)
		ON_VOID_POINTER_MESSAGE(button2_release_evt,rinse_config_22_save_handler)
		ON_VOID_POINTER_MESSAGE(encoder_a_release_evt,rinse_config_22_right_handler)
		ON_VOID_POINTER_MESSAGE(encoder_b_release_evt,rinse_config_22_left_handler)
		ON_VOID_POINTER_MESSAGE(encoder_button_release_evt,rinse_config_22_save_handler)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_rinse_config_22_FuncEn,Menu_Function_rinse_config_22_cmd_func,preMessageHandler)   


CONST MENU_KEY op_rinse_config_22_table = 
	{&Menu_Function_rinse_config_22_cmd_func,rinse_config_22_updateDisplay,NULL,NULL};   		

	



/**
* @brieaf right key
*/
static void rinse_config_22_right_handler(void *msg)
{
	bru_Rinse_adj_ENG(ENCODE_RIGHT);
	NRF_LOG_INFO("rinse_config_22_right_handler");
}

/**
* @brieaf left key
*/
static void rinse_config_22_left_handler(void *msg)
{
	bru_Rinse_adj_ENG(ENCODE_LEFT);
	NRF_LOG_INFO("rinse_config_22_left_handler");
}
/**
* @brieaf save key
*/
static void rinse_config_22_save_handler(void *msg)
{
	write_record_info_to_flash();
	get_machine_setup_info();
	DISP_FLAG_T *p = get_disp_flag();
	p->sel_draw_flag_1 = 1;
	goto_rinse_config_9_disp();
	NRF_LOG_INFO("rinse_config_22_save_handler");
}

/**
* @brieaf rinse_config page display
*/
static void rinse_config_22_updateDisplay(void *p)
{
	NRF_LOG_INFO("=========rinse_config_22_updateDisplay============");
	DISP_FLAG_T *flag = get_disp_flag();
	bru_LCD_Rinse_config_Edit_ENG(&flag->sel_draw_flag_1);
}



/**
* @brieaf jump to rinse_config page function
*/
void goto_rinse_config_22_disp(void)
{
	jump_ui_func(&op_rinse_config_22_table);
	
}














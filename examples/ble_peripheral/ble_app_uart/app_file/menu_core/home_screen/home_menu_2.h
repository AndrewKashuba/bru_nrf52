#ifndef __HOME_MENU_2_H
#define __HOME_MENU_2_H
#include "target_command.h"

#ifdef __cplusplus
extern "C" {
#endif

extern CONST MENU_KEY op_home_table;
void goto_home_disp(void);
void goto_preset_disp(void);
void goto_machine_disp(void);  
void goto_steeping_disp(void);
#ifdef __cplusplus
}
#endif

#endif


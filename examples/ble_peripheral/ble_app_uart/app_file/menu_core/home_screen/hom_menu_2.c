#include "BSP_menu_info.h"
#include "bru_Context.h"
#include "dbprintf.h"

#define NRF_LOG_MODULE_NAME main_menu
#define NRF_LOG_LEVEL       3
#define NRF_LOG_INFO_COLOR  4
#define NRF_LOG_DEBUG_COLOR 3
#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();

//######################################################################################################################################
static void updateDisplay(void *p);

static void home_menu_button_handler(void *msg);
static void home_right_handler(void *msg);
static void home_left_handler(void *msg);
static void home_start_handler(void *msg);

/*-----home Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_MainFuncEn)
    ON_VOID_POINTER_MESSAGE(button2_release_evt,home_menu_button_handler)
		ON_VOID_POINTER_MESSAGE(encoder_a_release_evt,home_right_handler)
		ON_VOID_POINTER_MESSAGE(encoder_b_release_evt,home_left_handler)
		ON_VOID_POINTER_MESSAGE(button1_release_evt,home_start_handler)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */

INIT_MSG_ENTRIES(Menu_Function_MainFuncEn,Menu_Function_cmd_func,preMessageHandler)   

CONST MENU_KEY op_home_table= 
	{&Menu_Function_cmd_func,updateDisplay,NULL,NULL};   		

/**
* @brieaf  button key for home page
*/
static void home_menu_button_handler(void *msg)
{
	 DISP_FLAG_T *p = get_disp_flag();
	 p->sel_draw_flag_1 = 1;
	 bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
	 hUserInterface->bMenuPresetFlag = 0;
	 goto_steeping_disp();
	 NRF_LOG_INFO("=======home_menu_button_handler==========");	
}

/**
* @brieaf encode right key for home page
*/
static void home_right_handler(void *msg)
{
	DISP_FLAG_T *p = get_disp_flag();
	p->sel_draw_flag_1 = 1;
	bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
	hUserInterface->iPresetCurrentIndex = 1;
	goto_preset_1_3_setup_disp();
	NRF_LOG_INFO("=======home_right_handler==========");
}

/**
* @brieaf encode left key for home page
*/
static void home_left_handler(void *msg)
{
	DISP_FLAG_T *p = get_disp_flag();
	p->sel_draw_flag_1 = 1;
	bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
	hUserInterface->iPresetCurrentIndex = 0;
	hUserInterface->bMenuPresetFlag = 1;
//	goto_preset_main_edit_disp();
//	NRF_LOG_INFO("=======home_left_handler==========");
	//AK Gate
	extern void bru_Context_Task_Intro(bru_Tasks_List, bru_UI_Window_ID);
	bru_Context_Task_Intro(T_HomeScreen, UI_WINDOW_HOME_SCREEN);		
}


/**
* @brieaf  encode start key for home page
*/
static void home_start_handler(void *msg)  
{
//	bru_Menu_Check_Procedure(BRU_ERROR_CHECK_WHILE_PRE_BREWING);
//	DB_Printf("=======home_start_handler==========\r\n");
	
	//AK Gate
	extern void bru_Context_Task_Intro(bru_Tasks_List, bru_UI_Window_ID);
	bru_Context_Task_Intro(T_PreBrewing_Water, UI_WINDOW_HOME_SCREEN);	
}

/**
* @brieaf home page display
*/
static void updateDisplay(void *p)
{
	NRF_LOG_INFO("=========main updateDisplay============");
	bru_LCD_Update_Screen(UI_WINDOW_HOME_SCREEN, Init);
}

/**
* @brieaf jump to home page function
*/
void goto_home_disp(void)
{
	bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
	hUserInterface->iPresetCurrentIndex = 0;
	hUserInterface->bMenuBackFlag = 0;
	hUserInterface->bMenuPresetFlag = 0;
	
	jump_ui_func(&op_home_table);
	
}





//#######################################################################################################################
//=============================steeping display=============================
static void steeping_updateDisplay(void *p);

static void steeping_menu_ok_encode_handler(void *msg);
static void steeping_right_handler(void *msg);
static void steeping_left_handler(void *msg);


/*-----steeping Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_steeping_FuncEn)
    ON_VOID_MESSAGE(button1_release_evt,goto_home_disp)
		ON_VOID_POINTER_MESSAGE(button2_release_evt,steeping_menu_ok_encode_handler)
		ON_VOID_POINTER_MESSAGE(encoder_a_release_evt,steeping_right_handler)
		ON_VOID_POINTER_MESSAGE(encoder_b_release_evt,steeping_left_handler)
		ON_VOID_POINTER_MESSAGE(encoder_button_release_evt,steeping_menu_ok_encode_handler)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_steeping_FuncEn,Menu_steeping_cmd_func,preMessageHandler)   


CONST MENU_KEY op_steeping_table= 
	{&Menu_steeping_cmd_func,steeping_updateDisplay,NULL,NULL};   		


/**
* @brieaf jump to steeping display
*/
void goto_steeping_disp(void)
{
	jump_ui_func(&op_steeping_table);
	
}
	
/**
* @brieaf steeping display
*/
static void steeping_updateDisplay(void *p)
{
	 DISP_FLAG_T *flag = get_disp_flag();
	bru_LCD_Main_Menu_Steeping_Setup_ENG(&flag->sel_draw_flag_1);
	NRF_LOG_INFO("steeping_updateDisplay");
}


/**
* @brieaf steeping OK key
*/
static void steeping_menu_ok_encode_handler(void *msg)
{
	DISP_FLAG_T *p = get_disp_flag();
	p->sel_draw_flag_1 = 1;
	bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
	hUserInterface->iPresetCurrentIndex = 0;
	goto_preset_main_edit_disp();
}

/**
* @brieaf steeping right key
*/
static void steeping_right_handler(void *msg)
{
	
	goto_preset_disp();
}
/**
* @brieaf steeping left key
*/
static void steeping_left_handler(void *msg)
{
	
	goto_machine_disp();
}




//#######################################################################################################################
//=============================preset display=============================
static void preset_updateDisplay(void *p);
static void preset_menu_ok_encode_handler(void *msg);
static void preset_right_handler(void *msg);
static void preset_left_handler(void *msg);


/*-----preset Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_preset_FuncEn)
    ON_VOID_MESSAGE(button1_release_evt,goto_home_disp)
		ON_VOID_POINTER_MESSAGE(button2_release_evt,preset_menu_ok_encode_handler)
		ON_VOID_POINTER_MESSAGE(encoder_a_release_evt,preset_right_handler)
		ON_VOID_POINTER_MESSAGE(encoder_b_release_evt,preset_left_handler)
		ON_VOID_POINTER_MESSAGE(encoder_button_release_evt,preset_menu_ok_encode_handler)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_preset_FuncEn,Menu_preset_cmd_func,preMessageHandler)   


CONST MENU_KEY op_preset_table= 
	{&Menu_preset_cmd_func,preset_updateDisplay,NULL,NULL};   		

/**
* @brieaf jump to preset display
*/
void goto_preset_disp(void)
{
	jump_ui_func(&op_preset_table);
	
}
	
/**
* @brieaf preset display
*/
static void preset_updateDisplay(void *p)
{
	NRF_LOG_INFO("preset_updateDisplay");
	DISP_FLAG_T *flag = get_disp_flag();
	bru_LCD_Main_Menu_Preset_Setup_ENG(&flag->sel_draw_flag_1);
}


/**
* @brieaf  OK encode key for preset page
*/
static void preset_menu_ok_encode_handler(void *msg)
{
	DISP_FLAG_T *p = get_disp_flag();
	p->sel_draw_flag_1 = 1;
	bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
	hUserInterface->iPresetCurrentIndex = 1;
	goto_preset_setup_8_disp();
}

/**
* @brieaf  right key for preset page
*/
static void preset_right_handler(void *msg)
{
	
	goto_machine_disp();
}
/**
* @brieaf  left key for preset page
*/
static void preset_left_handler(void *msg)
{	
	goto_steeping_disp();
}

//#######################################################################################################################
//=============================machine display=============================
static void machine_updateDisplay(void *p);
static void machine_menu_ok_encode_handler(void *msg);
static void machine_right_handler(void *msg);
static void machine_left_handler(void *msg);

/*-----machine Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_machine_FuncEn)
    ON_VOID_MESSAGE(button1_release_evt,goto_home_disp)
		ON_VOID_POINTER_MESSAGE(button2_release_evt,machine_menu_ok_encode_handler)
		ON_VOID_POINTER_MESSAGE(encoder_a_release_evt,machine_right_handler)
		ON_VOID_POINTER_MESSAGE(encoder_b_release_evt,machine_left_handler)
		ON_VOID_POINTER_MESSAGE(encoder_button_release_evt,machine_menu_ok_encode_handler)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */

INIT_MSG_ENTRIES(Menu_Function_machine_FuncEn,Menu_machine_cmd_func,preMessageHandler)   

CONST MENU_KEY op_machine_table= 
	{&Menu_machine_cmd_func,machine_updateDisplay,NULL,NULL};   		

/**
* @brieaf jump to machine display
*/
void goto_machine_disp(void)
{
	jump_ui_func(&op_machine_table);
}
	
	
/**
* @brieaf machine display
*/
static void machine_updateDisplay(void *p)
{
	NRF_LOG_INFO("maching_updateDisplay");
	DISP_FLAG_T *flag = get_disp_flag();
	bru_LCD_Main_Menu_Machine_Setup_ENG(&flag->sel_draw_flag_1);
}


/**
* @brieaf machine OK key
*/
static void machine_menu_ok_encode_handler(void *msg)
{
	DISP_FLAG_T *p = get_disp_flag();
	p->sel_draw_flag_1 = 1;
	goto_machine_setup_9_disp();
}

/**
* @brieaf machine right key
*/
static void machine_right_handler(void *msg)
{
	
	goto_steeping_disp();
}
/**
* @brieaf machine left key
*/
void machine_left_handler(void *msg)
{
	
	goto_preset_disp();
}

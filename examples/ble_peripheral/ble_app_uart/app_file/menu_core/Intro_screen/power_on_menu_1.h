#ifndef __POWER_ON_MENU_1_H
#define __POWER_ON_MENU_1_H
#include "target_command.h"


#define POWER_ON_TIMER_ID   (98)
#define POWER_ON_TIMER_VALUE (TIMER_CALL_HZ*3)

#ifdef __cplusplus
extern "C" {
#endif



void power_on_ui_init(void);
void goto_home_from_check(void);
#ifdef __cplusplus
}
#endif

#endif


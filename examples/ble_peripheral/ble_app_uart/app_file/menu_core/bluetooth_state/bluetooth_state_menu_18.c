#include "BSP_menu_info.h"



#define NRF_LOG_MODULE_NAME bluetooth_state_18
#define NRF_LOG_LEVEL       3
#define NRF_LOG_INFO_COLOR  4
#define NRF_LOG_DEBUG_COLOR 3
#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();





//######################################################################################################################################
static void bluetooth_state_18_updateDisplay(void *p);


static void bluetooth_state_18_right_or_left_handler(void *msg);
static void bluetooth_state_18_save_handler(void *msg);


/*-----bluetooth_state Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_bluetooth_state_18_FuncEn)
		ON_VOID_POINTER_MESSAGE(button2_release_evt,bluetooth_state_18_save_handler)
		ON_VOID_POINTER_MESSAGE(encoder_a_release_evt,bluetooth_state_18_right_or_left_handler)
		ON_VOID_POINTER_MESSAGE(encoder_b_release_evt,bluetooth_state_18_right_or_left_handler)
		ON_VOID_POINTER_MESSAGE(encoder_button_release_evt,bluetooth_state_18_save_handler)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_bluetooth_state_18_FuncEn,Menu_Function_bluetooth_state_18_cmd_func,preMessageHandler)   


CONST MENU_KEY op_bluetooth_state_18_table= 
	{&Menu_Function_bluetooth_state_18_cmd_func,bluetooth_state_18_updateDisplay,NULL,NULL};   		

	



/**
* @brieaf right or left key
*/
static void bluetooth_state_18_right_or_left_handler(void *msg)
{
	
	bru_Bluetooth_State_adj_ENG();
	NRF_LOG_INFO("bluetooth_state_18_right_or_left_handler");
}

/**
* @brieaf save key
*/
static void bluetooth_state_18_save_handler(void *msg)
{
	write_record_info_to_flash();
	get_machine_setup_info();
	DISP_FLAG_T *p = get_disp_flag();
	p->sel_draw_flag_1 = 1;
	bru_ble_on_off_ENG();
	goto_machine_setup_9_disp();
	
	
	NRF_LOG_INFO("bluetooth_state_18_save_handler");
}

/**
* @brieaf bluetooth_state page display
*/
static void bluetooth_state_18_updateDisplay(void *p)
{
	NRF_LOG_INFO("=========bluetooth_state_18_updateDisplay============");
	DISP_FLAG_T *flag = get_disp_flag();
	bru_LCD_Bluetooth_State_Edit_ENG(&flag->sel_draw_flag_1);
}



/**
* @brieaf jump to bluetooth_state page function
*/
void goto_bluetooth_state_18_disp(void)
{
	jump_ui_func(&op_bluetooth_state_18_table);
	
}














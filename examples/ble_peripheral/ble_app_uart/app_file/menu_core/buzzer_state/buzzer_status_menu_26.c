#include "BSP_menu_info.h"



#define NRF_LOG_MODULE_NAME buzzer_status_26
#define NRF_LOG_LEVEL       3
#define NRF_LOG_INFO_COLOR  4
#define NRF_LOG_DEBUG_COLOR 3
#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();





//######################################################################################################################################
static void buzzer_26_updateDisplay(void *p);


static void buzzer_26_right_left_handler(void *msg);
static void buzzer_26_save_handler(void *msg);


/*-----buzzer Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_buzzer_26_FuncEn)
		ON_VOID_POINTER_MESSAGE(button2_release_evt,buzzer_26_save_handler)
		ON_VOID_POINTER_MESSAGE(encoder_a_release_evt,buzzer_26_right_left_handler)
		ON_VOID_POINTER_MESSAGE(encoder_b_release_evt,buzzer_26_right_left_handler)
		ON_VOID_POINTER_MESSAGE(encoder_button_release_evt,buzzer_26_save_handler)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_buzzer_26_FuncEn,Menu_Function_buzzer_26_cmd_func,preMessageHandler)   


CONST MENU_KEY op_buzzer_status_26_table = 
	{&Menu_Function_buzzer_26_cmd_func,buzzer_26_updateDisplay,NULL,NULL};   		

	


/**
* @brieaf right or left key
*/
static void buzzer_26_right_left_handler(void *msg)
{
	bru_Buzzer_State_adj_ENG();
	NRF_LOG_INFO("buzzer_26_right_left_handler");
}


/**
* @brieaf save key
*/
static void buzzer_26_save_handler(void *msg)
{
	write_record_info_to_flash();
	get_machine_setup_info();
	DISP_FLAG_T *p = get_disp_flag();
	p->sel_draw_flag_1 = 1;
	goto_buzzer_state_9_disp();
	NRF_LOG_INFO("buzzer_26_save_handler");
}

/**
* @brieaf buzzer page display
*/
static void buzzer_26_updateDisplay(void *p)
{
	NRF_LOG_INFO("=========buzzer_26_updateDisplay============");
	DISP_FLAG_T *flag = get_disp_flag();
	bru_LCD_Buzzer_state_Edit_ENG(&flag->sel_draw_flag_1);
}



/**
* @brieaf jump to buzzer page function
*/
void goto_buzzer_status_26_disp(void)
{
	jump_ui_func(&op_buzzer_status_26_table);
	
}














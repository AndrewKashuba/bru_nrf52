#include "BSP_menu_info.h"



#define NRF_LOG_MODULE_NAME water_filter_23
#define NRF_LOG_LEVEL       3
#define NRF_LOG_INFO_COLOR  4
#define NRF_LOG_DEBUG_COLOR 3
#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();





//######################################################################################################################################
static void water_filter_23_updateDisplay(void *p);


static void water_filter_23_right_handler(void *msg);
static void water_filter_23_left_handler(void *msg);
static void water_filter_23_save_handler(void *msg);

/*-----water_filter Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_water_filter_23_FuncEn)
		ON_VOID_POINTER_MESSAGE(button2_release_evt,water_filter_23_save_handler)
		ON_VOID_POINTER_MESSAGE(encoder_a_release_evt,water_filter_23_right_handler)
		ON_VOID_POINTER_MESSAGE(encoder_b_release_evt,water_filter_23_left_handler)
		ON_VOID_POINTER_MESSAGE(encoder_button_release_evt,water_filter_23_save_handler)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_water_filter_23_FuncEn,Menu_Function_water_filter_23_cmd_func,preMessageHandler)   


CONST MENU_KEY op_water_filter_23_table = 
	{&Menu_Function_water_filter_23_cmd_func,water_filter_23_updateDisplay,NULL,NULL};   	

	



/**
* @brieaf right key
*/
static void water_filter_23_right_handler(void *msg)
{
	NRF_LOG_INFO("water_filter_23_right_handler");
}

/**
* @brieaf left key
*/
static void water_filter_23_left_handler(void *msg)
{
	NRF_LOG_INFO("water_filter_23_left_handler");
}
/**
* @brieaf save key
*/
static void water_filter_23_save_handler(void *msg)
{
	get_machine_setup_info();
	write_record_info_to_flash();
	NRF_LOG_INFO("water_filter_23_save_handler");
}

/**
* @brieaf water_filter page display      
*/
static void water_filter_23_updateDisplay(void *p)
{
	NRF_LOG_INFO("=========water_filter_23_updateDisplay============");
}



/**
* @brieaf jump to water_filter page function
*/
void goto_water_filter_status_23_disp(void)
{
	jump_ui_func(&op_water_filter_23_table);
	
}














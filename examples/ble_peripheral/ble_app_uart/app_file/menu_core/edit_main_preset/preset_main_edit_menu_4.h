#ifndef __PRESET_MAIN_EDIT_MENU_4_H
#define __PRESET_MAIN_EDIT_MENU_4_H
#include "target_command.h"

#ifdef __cplusplus
extern "C" {
#endif


void goto_preset_main_edit_disp(void);
void goto_water_temper_disp(void);
void goto_water_amount_disp(void);
#ifdef __cplusplus
}
#endif

#endif


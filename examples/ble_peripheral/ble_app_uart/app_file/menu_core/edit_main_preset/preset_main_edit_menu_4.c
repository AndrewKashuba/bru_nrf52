#include "BSP_menu_info.h"



#define NRF_LOG_MODULE_NAME preset_main
#define NRF_LOG_LEVEL       3
#define NRF_LOG_INFO_COLOR  4
#define NRF_LOG_DEBUG_COLOR 3
#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();






//######################################################################################################################################
static void preset_main_updateDisplay(void *p);

static void preset_main_encode_left_handler(void *msg);
static void preset_main_encode_right_handler(void *msg);
static void preset_main_edit_handler(void *msg);
static void preset_main_back_handler(void *msg);

/*-----steeping time Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_preset_main_FuncEn)
    ON_VOID_POINTER_MESSAGE(encoder_b_release_evt,preset_main_encode_left_handler)
		ON_VOID_POINTER_MESSAGE(encoder_a_release_evt,preset_main_encode_right_handler)
		ON_VOID_POINTER_MESSAGE(button1_release_evt,preset_main_back_handler)
		ON_VOID_POINTER_MESSAGE(button2_release_evt,preset_main_edit_handler)
		ON_VOID_POINTER_MESSAGE(encoder_button_release_evt,preset_main_edit_handler)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_preset_main_FuncEn,Menu_Function_preset_main_cmd_func,preMessageHandler)   


CONST MENU_KEY op_preset_main_table= 
	{&Menu_Function_preset_main_cmd_func,preset_main_updateDisplay,NULL,NULL};   		

	



	
/**
* @brieaf steeping time encode left  key 
*/
static void preset_main_encode_left_handler(void *msg)
{
	goto_water_temper_disp();
	NRF_LOG_INFO("preset_main_encode_left_handler");
}

/**
* @brieaf steeping time setup encode right
*/
static void preset_main_encode_right_handler(void *msg)
{
	goto_water_amount_disp();
	NRF_LOG_INFO("preset_main_encode_right_handler");
}

/**
* @brieaf steeping time edit key
*/
static void preset_main_edit_handler(void *msg)
{
	DISP_FLAG_T *p = get_disp_flag();
	p->sel_draw_flag_1 = 1;
	goto_steeping_time_10_disp();
	NRF_LOG_INFO("preset_main_edit_handler");
}

/**
* @brieaf steeping time back key
*/
static void preset_main_back_handler(void *msg)
{
	bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
	DISP_FLAG_T *p = get_disp_flag();
	p->sel_draw_flag_1 = 1;
	if(hUserInterface->bMenuPresetFlag)
		goto_home_disp();
	else
	 goto_steeping_disp();
	NRF_LOG_INFO("preset_main_back_handler");
}


/**
* @brieaf steeping time page display
*/
static void preset_main_updateDisplay(void *p)
{
	NRF_LOG_INFO("=========preset_main_updateDisplay============");
	DISP_FLAG_T *flag = get_disp_flag();
	bru_LCD_BrewingTime_Steeping_ENG(&flag->sel_draw_flag_1);
}



/**
* @brieaf jump to steeping time page function
*/
void goto_preset_main_edit_disp(void)
{
	jump_ui_func(&op_preset_main_table);
	
}





//######################################################################################################################################
//============water temperature===================
static void water_temper_updateDisplay(void *p);

static void water_temper_left_handler(void *msg);
static void water_temper_right_handler(void *msg);
static void water_temper_edit_handler(void *msg);


/*-----water temperature Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_water_temper_FuncEn)
    ON_VOID_POINTER_MESSAGE(encoder_b_release_evt,water_temper_left_handler)
		ON_VOID_POINTER_MESSAGE(encoder_a_release_evt,water_temper_right_handler)
		ON_VOID_POINTER_MESSAGE(button1_release_evt,preset_main_back_handler)
		ON_VOID_POINTER_MESSAGE(button2_release_evt,water_temper_edit_handler)
		ON_VOID_POINTER_MESSAGE(encoder_button_release_evt,water_temper_edit_handler)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_water_temper_FuncEn,Menu_Function_water_temper_cmd_func,preMessageHandler)   


CONST MENU_KEY op_water_temper_table= 
	{&Menu_Function_water_temper_cmd_func,water_temper_updateDisplay,NULL,NULL};   	

	



	
/**
* @brieaf water temperature  left  key 
*/
static void water_temper_left_handler(void *msg)
{
	goto_water_amount_disp();
	NRF_LOG_INFO("water_temper_left_handler");
}

/**
* @brieaf water temperature right key
*/
static void water_temper_right_handler(void *msg)
{
	goto_preset_main_edit_disp();
	NRF_LOG_INFO("water_temper_right_handler");
}

/**
* @brieaf water temperature edit key
*/
static void water_temper_edit_handler(void *msg)
{
	DISP_FLAG_T *flag = get_disp_flag();
	flag->sel_draw_flag_1 = 1;
	goto_water_temp_edit_11_disp();
	NRF_LOG_INFO("water_temper_edit_handler");
}


/**
* @brieaf water temperature page display
*/
static void water_temper_updateDisplay(void *p)
{
	NRF_LOG_INFO("=========water_temper_updateDisplay============");
	DISP_FLAG_T *flag = get_disp_flag();
	bru_LCD_WaterTemperature_ENG(&flag->sel_draw_flag_1);
}



/**
* @brieaf jump to water temperature page function
*/
void goto_water_temper_disp(void)
{
	jump_ui_func(&op_water_temper_table);
	
}




//######################################################################################################################################
//============water amount===================
static void water_amount_updateDisplay(void *p);

static void water_amount_left_handler(void *msg);
static void water_amount_right_handler(void *msg);
static void water_amount_edit_handler(void *msg);


/*-----water amount Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_water_amount_FuncEn)
    ON_VOID_POINTER_MESSAGE(encoder_b_release_evt,water_amount_left_handler)
		ON_VOID_POINTER_MESSAGE(encoder_a_release_evt,water_amount_right_handler)
		ON_VOID_POINTER_MESSAGE(button1_release_evt,preset_main_back_handler)
		ON_VOID_POINTER_MESSAGE(button2_release_evt,water_amount_edit_handler)
		ON_VOID_POINTER_MESSAGE(encoder_button_release_evt,water_amount_edit_handler)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_water_amount_FuncEn,Menu_Function_water_amount_cmd_func,preMessageHandler)   


CONST MENU_KEY op_water_amount_table= 
	{&Menu_Function_water_amount_cmd_func,water_amount_updateDisplay,NULL,NULL};   		

	



	
/**
* @brieaf water amount left  key 
*/
static void water_amount_left_handler(void *msg)
{
	goto_preset_main_edit_disp();
	NRF_LOG_INFO("water_amount_left_handler");
}

/**
* @brieaf water amount right key
*/
static void water_amount_right_handler(void *msg)
{
	goto_water_temper_disp();
	NRF_LOG_INFO("water_amount_right_handler");
}

/**
* @brieaf water amount edit key
*/
static void water_amount_edit_handler(void *msg)
{
	DISP_FLAG_T *flag = get_disp_flag();
	flag->sel_draw_flag_1 = 1;
	goto_water_amout_edit_12_disp();
	NRF_LOG_INFO("water_amount_edit_handler");
}


/**
* @brieaf water amount page display
*/
static void water_amount_updateDisplay(void *p)
{
	NRF_LOG_INFO("=========water_temper_updateDisplay============");
	DISP_FLAG_T *flag = get_disp_flag();
	bru_LCD_WaterAmount_ENG(&flag->sel_draw_flag_1);
}



/**
* @brieaf jump to water amount page display
*/
void goto_water_amount_disp(void)
{
	jump_ui_func(&op_water_amount_table);
	
}










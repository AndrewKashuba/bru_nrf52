#include "BSP_menu_info.h"



#define NRF_LOG_MODULE_NAME alarm_time_edit_15
#define NRF_LOG_LEVEL       3
#define NRF_LOG_INFO_COLOR  4
#define NRF_LOG_DEBUG_COLOR 3
#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();






//######################################################################################################################################
//============== current time ===============================
static void current_time_15_updateDisplay(void *p);

static void current_time_15_back_handler(void *msg);
static void current_time_15_right_handler(void *msg);
static void current_time_15_left_handler(void *msg);
static void current_time_15_edit_handler(void *msg);


/*-----current time Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_current_time_15_FuncEn)
    ON_VOID_POINTER_MESSAGE(button1_release_evt,current_time_15_back_handler)
		ON_VOID_POINTER_MESSAGE(button2_release_evt,current_time_15_edit_handler)
		ON_VOID_POINTER_MESSAGE(encoder_a_release_evt,current_time_15_right_handler)
		ON_VOID_POINTER_MESSAGE(encoder_b_release_evt,current_time_15_left_handler)
		ON_VOID_POINTER_MESSAGE(encoder_button_release_evt,current_time_15_edit_handler)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_current_time_15_FuncEn,Menu_Function_current_time_edit_15_cmd_func,preMessageHandler)   


CONST MENU_KEY op_current_time_edit_15_table= 
	{&Menu_Function_current_time_edit_15_cmd_func,current_time_15_updateDisplay,NULL,NULL};   		

	

/**
* @brieaf  back key
*/
static void current_time_15_back_handler(void *msg)
{
	bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
	DISP_FLAG_T *p = get_disp_flag();
	p->sel_draw_flag_1 = 1;
	if(hUserInterface->bMenuBackFlag == 0)
	{
		goto_alarm_disp();
	}
	else
	{
		goto_alarm_8_disp();
	}
	NRF_LOG_INFO("current_time_15_back_handler");
}


/**
* @brieaf right key
*/
static void current_time_15_right_handler(void *msg)
{
	goto_alarm_time_edit_15_disp();
	NRF_LOG_INFO("current_time_15_right_handler");
}

/**
* @brieaf left key
*/
static void current_time_15_left_handler(void *msg)
{
	goto_alarm_preset_edit_15_disp();
	NRF_LOG_INFO("current_time_15_left_handler");
}
/**
* @brieaf edit key
*/
static void current_time_15_edit_handler(void *msg)
{
	DISP_FLAG_T *p = get_disp_flag();
	p->sel_draw_flag_1 = 1;
	goto_time_edit_16_disp();
	NRF_LOG_INFO("current_time_15_edit_handler");
}

/**
* @brieaf current time page display
*/
static void current_time_15_updateDisplay(void *p)
{
	NRF_LOG_INFO("=========current_time_15_updateDisplay============");
	DISP_FLAG_T *flag = get_disp_flag();
	bru_LCD_System_Time_ENG(&flag->sel_draw_flag_1,TIME_SEL);
}



/**
* @brieaf jump to current time page function
*/
void goto_current_time_edit_15_disp(void)
{
	jump_ui_func(&op_current_time_edit_15_table);
	
}



//######################################################################################################################################
//============== alarm time ===============================
static void alarm_time_15_updateDisplay(void *p);


static void alarm_time_15_right_handler(void *msg);
static void alarm_time_15_left_handler(void *msg);
static void alarm_time_15_edit_handler(void *msg);

/*-----alarm time Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_alarm_time_15_FuncEn)
    ON_VOID_POINTER_MESSAGE(button1_release_evt,current_time_15_back_handler)
		ON_VOID_POINTER_MESSAGE(button2_release_evt,alarm_time_15_edit_handler)
		ON_VOID_POINTER_MESSAGE(encoder_a_release_evt,alarm_time_15_right_handler)
		ON_VOID_POINTER_MESSAGE(encoder_b_release_evt,alarm_time_15_left_handler)
		ON_VOID_POINTER_MESSAGE(encoder_button_release_evt,alarm_time_15_edit_handler)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_alarm_time_15_FuncEn,Menu_Function_alarm_time_edit_15_cmd_func,preMessageHandler)   


CONST MENU_KEY op_alarm_time_edit_15_table= 
	{&Menu_Function_alarm_time_edit_15_cmd_func,alarm_time_15_updateDisplay,NULL,NULL};   		

	



/**
* @brieaf right key
*/
static void alarm_time_15_right_handler(void *msg)
{
	NRF_LOG_INFO("alarm_time_15_right_handler");
	goto_alarm_preset_edit_15_disp();
}

/**
* @brieaf left key
*/
static void alarm_time_15_left_handler(void *msg)
{
	goto_current_time_edit_15_disp();
	NRF_LOG_INFO("alarm_time_15_left_handler");
}
/**
* @brieaf edit key
*/
static void alarm_time_15_edit_handler(void *msg)
{
	DISP_FLAG_T *p = get_disp_flag();
	p->sel_draw_flag_1 = 1;
	goto_alarm_hour_edit_16_disp();
	NRF_LOG_INFO("alarm_time_15_edit_handler");
}

/**
* @brieaf alarm time page display
*/
static void alarm_time_15_updateDisplay(void *p)
{
	NRF_LOG_INFO("=========alarm_time_15_updateDisplay============");
	DISP_FLAG_T *flag = get_disp_flag();
	bru_LCD_System_Time_ENG(&flag->sel_draw_flag_1,ALARM_SEL);
}



/**
* @brieaf jump to alarm time page function
*/
void goto_alarm_time_edit_15_disp(void)
{
	jump_ui_func(&op_alarm_time_edit_15_table);
	
}



//######################################################################################################################################
//============== alarm preset ===============================
static void alarm_preset_15_updateDisplay(void *p);


static void alarm_preset_15_right_handler(void *msg);
static void alarm_preset_15_left_handler(void *msg);
static void alarm_preset_15_edit_handler(void *msg);


/*-----alarm preset Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_alarm_preset_15_FuncEn)
    ON_VOID_POINTER_MESSAGE(button1_release_evt,current_time_15_back_handler)
		ON_VOID_POINTER_MESSAGE(button2_release_evt,alarm_preset_15_edit_handler)
		ON_VOID_POINTER_MESSAGE(encoder_a_release_evt,alarm_preset_15_right_handler)
		ON_VOID_POINTER_MESSAGE(encoder_b_release_evt,alarm_preset_15_left_handler)
		ON_VOID_POINTER_MESSAGE(encoder_button_release_evt,alarm_preset_15_edit_handler)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_alarm_preset_15_FuncEn,Menu_Function_alarm_preset_edit_15_cmd_func,preMessageHandler)   


CONST MENU_KEY op_alarm_preset_edit_15_table= 
	{&Menu_Function_alarm_preset_edit_15_cmd_func,alarm_preset_15_updateDisplay,NULL,NULL};   	

	



/**
* @brieaf right key
*/
static void alarm_preset_15_right_handler(void *msg)
{
	goto_current_time_edit_15_disp();
	NRF_LOG_INFO("alarm_preset_15_right_handler");
}

/**
* @brieaf left key
*/
static void alarm_preset_15_left_handler(void *msg)
{

	goto_alarm_time_edit_15_disp();
	NRF_LOG_INFO("alarm_preset_15_left_handler");
}
/**
* @brieaf edit key
*/
static void alarm_preset_15_edit_handler(void *msg)
{
	 DISP_FLAG_T *p = get_disp_flag();
	 p->sel_draw_flag_1 = 1;
	goto_pre_alarm_edit_17_disp();
	NRF_LOG_INFO("alarm_preset_15_edit_handler");
}

/**
* @brieaf alarm preset page display
*/
static void alarm_preset_15_updateDisplay(void *p)
{
	NRF_LOG_INFO("=========alarm_preset_15_updateDisplay============");
	DISP_FLAG_T *flag = get_disp_flag();
	bru_LCD_Alarm_Preset_ENG(&flag->sel_draw_flag_1);
}



/**
* @brieaf jump to alarm preset page function
*/
void goto_alarm_preset_edit_15_disp(void)
{
	jump_ui_func(&op_alarm_preset_edit_15_table);
	
}







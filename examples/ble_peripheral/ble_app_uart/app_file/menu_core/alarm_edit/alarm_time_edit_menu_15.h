#ifndef __ALARM_TIME_EDIT_MENU_15_H
#define __ALARM_TIME_EDIT_MENU_15_H
#include "target_command.h"

#ifdef __cplusplus
extern "C" {
#endif

void goto_current_time_edit_15_disp(void);
void goto_alarm_time_edit_15_disp(void);
void goto_alarm_preset_edit_15_disp(void);
#ifdef __cplusplus
}
#endif

#endif


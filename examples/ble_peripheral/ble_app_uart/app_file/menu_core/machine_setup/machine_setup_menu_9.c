#include "BSP_menu_info.h"



#define NRF_LOG_MODULE_NAME maching_setup_9
#define NRF_LOG_LEVEL       3
#define NRF_LOG_INFO_COLOR  4
#define NRF_LOG_DEBUG_COLOR 3
#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();



static void goto_language_9_disp(void);



//######################################################################################################################################
//=========== bluetooth state =======================
static void machine_setup_9_updateDisplay(void *p);


static void machine_setup_9_back_handler(void *msg);
static void machine_setup_9_right_handler(void *msg);
static void machine_setup_9_left_handler(void *msg);
static void machine_setup_9_edit_handler(void *msg);
static void goto_water_filter_9_disp(void);
static void goto_maintenance_status_9_disp(void);


/*-----bluetooth state Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_machine_setup_9_FuncEn)
		ON_VOID_POINTER_MESSAGE(button1_release_evt,machine_setup_9_back_handler)
		ON_VOID_POINTER_MESSAGE(button2_release_evt,machine_setup_9_edit_handler)
		ON_VOID_POINTER_MESSAGE(encoder_a_release_evt,machine_setup_9_right_handler)
		ON_VOID_POINTER_MESSAGE(encoder_b_release_evt,machine_setup_9_left_handler)
		ON_VOID_POINTER_MESSAGE(encoder_button_release_evt,machine_setup_9_edit_handler)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_machine_setup_9_FuncEn,Menu_Function_machine_setup_9_cmd_func,preMessageHandler)   


CONST MENU_KEY op_machine_setup_9_table= 
	{&Menu_Function_machine_setup_9_cmd_func,machine_setup_9_updateDisplay,NULL,NULL};   		

	


/**
* @brieaf  back key
*/
static void machine_setup_9_back_handler(void *msg)
{
	 DISP_FLAG_T *p = get_disp_flag();
	 p->sel_draw_flag_1 = 1;
	goto_machine_disp();
	NRF_LOG_INFO("machine_setup_9_back_handler");
}

/**
* @brieaf right key
*/
static void machine_setup_9_right_handler(void *msg)
{
	goto_language_9_disp();
	NRF_LOG_INFO("machine_setup_9_right_handler");
}

/**
* @brieaf left key
*/
static void machine_setup_9_left_handler(void *msg)
{
	goto_buzzer_state_9_disp();
	NRF_LOG_INFO("machine_setup_9_left_handler");
}
/**
* @brieaf edit key
*/
static void machine_setup_9_edit_handler(void *msg)
{
	DISP_FLAG_T *flag = get_disp_flag();
	flag->sel_draw_flag_1 = 1;
	goto_bluetooth_state_18_disp();
	NRF_LOG_INFO("machine_setup_9_edit_handler");
}

/**
* @brieaf bluetooth state page display
*/
static void machine_setup_9_updateDisplay(void *p)
{
	NRF_LOG_INFO("=========maching_setup_9_updateDisplay============");
	DISP_FLAG_T *flag = get_disp_flag();
	bru_LCD_Bluetooth_State_ENG(&flag->sel_draw_flag_1);
	
}



/**
* @brieaf jump to bluetooth state page function
*/
void goto_machine_setup_9_disp(void)
{
	jump_ui_func(&op_machine_setup_9_table);
	
}



//######################################################################################################################################
//=========== language  =======================
static void language_9_updateDisplay(void *p);



static void language_9_right_handler(void *msg);
static void language_9_left_handler(void *msg);
static void language_9_edit_handler(void *msg);

/*-----language Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_language_9_FuncEn)
		ON_VOID_POINTER_MESSAGE(button1_release_evt,machine_setup_9_back_handler)
		ON_VOID_POINTER_MESSAGE(button2_release_evt,language_9_edit_handler)
		ON_VOID_POINTER_MESSAGE(encoder_a_release_evt,language_9_right_handler)
		ON_VOID_POINTER_MESSAGE(encoder_b_release_evt,language_9_left_handler)
    ON_VOID_POINTER_MESSAGE(encoder_b_release_evt,encoder_button_release_evt)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_language_9_FuncEn,Menu_Function_language_9_cmd_func,preMessageHandler)   


CONST MENU_KEY op_language_9_table= 
	{&Menu_Function_language_9_cmd_func,language_9_updateDisplay,NULL,NULL};   		

	



/**
* @brieaf right key
*/
static void language_9_right_handler(void *msg)
{
	goto_time_format_9_disp();
	NRF_LOG_INFO("language_9_right_handler");
}

/**
* @brieaf left key
*/
static void language_9_left_handler(void *msg)
{
	goto_machine_setup_9_disp();
	NRF_LOG_INFO("language_9_left_handler");
}
/**
* @brieaf edit key
*/
static void language_9_edit_handler(void *msg)
{
	
	NRF_LOG_INFO("language_9_edit_handler");
}

/**
* @brieaf language page display
*/
static void language_9_updateDisplay(void *p)
{
	NRF_LOG_INFO("=========language_9_updateDisplay============");
	DISP_FLAG_T *flag = get_disp_flag();
	bru_LCD_Language_ENG(&flag->sel_draw_flag_1);
}



/**
* @brieaf jump to language page function
*/
static void goto_language_9_disp(void)
{
	jump_ui_func(&op_language_9_table);
	
}




//######################################################################################################################################
//=========== time format =======================
static void time_format_9_updateDisplay(void *p);

static void time_format_9_right_handler(void *msg);
static void time_format_9_left_handler(void *msg);
static void time_format_9_edit_handler(void *msg);

/*-----time format Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_time_format_9_FuncEn)
		ON_VOID_POINTER_MESSAGE(button1_release_evt,machine_setup_9_back_handler)
		ON_VOID_POINTER_MESSAGE(button2_release_evt,time_format_9_edit_handler)
		ON_VOID_POINTER_MESSAGE(encoder_a_release_evt,time_format_9_right_handler)
		ON_VOID_POINTER_MESSAGE(encoder_b_release_evt,time_format_9_left_handler)
    ON_VOID_POINTER_MESSAGE(encoder_button_release_evt,time_format_9_edit_handler)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_time_format_9_FuncEn,Menu_Function_time_format_9_cmd_func,preMessageHandler)   


CONST MENU_KEY op_time_format_9_table= 
	{&Menu_Function_time_format_9_cmd_func,time_format_9_updateDisplay,NULL,NULL};   	

	



/**
* @brieaf right key
*/
static void time_format_9_right_handler(void *msg)
{
	goto_current_time_9_disp();
	NRF_LOG_INFO("time_format_9_right_handler");
}

/**
* @brieaf left key
*/
static void time_format_9_left_handler(void *msg)
{
	goto_language_9_disp();
	NRF_LOG_INFO("time_format_9_left_handler");
}
/**
* @brieaf edit key
*/
static void time_format_9_edit_handler(void *msg)
{
	DISP_FLAG_T *p = get_disp_flag();
	p->sel_draw_flag_1 = 1;
	goto_time_format_20_disp();
	NRF_LOG_INFO("time_format_9_edit_handler");
}

/**
* @brieaf time format page display
*/
static void time_format_9_updateDisplay(void *p)
{
	NRF_LOG_INFO("=========time_format_9_updateDisplay============");
	DISP_FLAG_T *flag = get_disp_flag();
	bru_LCD_Timeformat_ENG(&flag->sel_draw_flag_1);
}



/**
* @brieaf jump to time format page function
*/
void goto_time_format_9_disp(void)
{
	jump_ui_func(&op_time_format_9_table);
	
}



//######################################################################################################################################
//=========== current time =======================
static void current_time_9_updateDisplay(void *p);


static void current_time_9_right_handler(void *msg);
static void current_time_9_left_handler(void *msg);
static void current_time_9_edit_handler(void *msg);

/*-----current time Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_current_time_9_FuncEn)
		ON_VOID_POINTER_MESSAGE(button1_release_evt,machine_setup_9_back_handler)
		ON_VOID_POINTER_MESSAGE(button2_release_evt,current_time_9_edit_handler)
		ON_VOID_POINTER_MESSAGE(encoder_a_release_evt,current_time_9_right_handler)
		ON_VOID_POINTER_MESSAGE(encoder_b_release_evt,current_time_9_left_handler)
    ON_VOID_POINTER_MESSAGE(encoder_button_release_evt,current_time_9_edit_handler)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_current_time_9_FuncEn,Menu_Function_current_time_9_cmd_func,preMessageHandler)   


CONST MENU_KEY op_current_time_9_table= 
	{&Menu_Function_current_time_9_cmd_func,current_time_9_updateDisplay,NULL,NULL};   		

	


/**
* @brieaf right key
*/
static void current_time_9_right_handler(void *msg)
{
	goto_units_9_disp();
	NRF_LOG_INFO("current_time_9_right_handler");
}

/**
* @brieaf left key
*/
static void current_time_9_left_handler(void *msg)
{
	goto_time_format_9_disp();
	NRF_LOG_INFO("current_time_9_left_handler");
}
/**
* @brieaf edit key
*/
static void current_time_9_edit_handler(void *msg)
{
	DISP_FLAG_T *p = get_disp_flag();
	p->sel_draw_flag_1 = 1;
	goto_machine_time_edit_16_disp();
	NRF_LOG_INFO("current_time_9_edit_handler");

}


/**
* @brieaf current time page display
*/
static void current_time_9_updateDisplay(void *p)
{
	NRF_LOG_INFO("=========current_time_9_updateDisplay============");
	DISP_FLAG_T *flag = get_disp_flag();
	bru_LCD_System_Time_ENG(&flag->sel_draw_flag_1,TIME_SEL);
	
}



/**
* @brieaf jump to current time page function
*/
void goto_current_time_9_disp(void)
{
	
	jump_ui_func(&op_current_time_9_table);
	register_timer(SYS_ALARM_TIMER_ID,sys_alarm_timer_handler);
	set_timer_value(SYS_ALARM_TIMER_ID,SYS_ALARM_TIMER_VALUE,(void*)&op_current_time_9_table);
	
}



//######################################################################################################################################
//=========== units =======================
static void units_9_updateDisplay(void *p);


static void units_9_right_handler(void *msg);
static void units_9_left_handler(void *msg);
static void units_9_edit_handler(void *msg);

/*-----units Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_units_9_FuncEn)
		ON_VOID_POINTER_MESSAGE(button1_release_evt,machine_setup_9_back_handler)
		ON_VOID_POINTER_MESSAGE(button2_release_evt,units_9_edit_handler)
		ON_VOID_POINTER_MESSAGE(encoder_a_release_evt,units_9_right_handler)
		ON_VOID_POINTER_MESSAGE(encoder_b_release_evt,units_9_left_handler)
    ON_VOID_POINTER_MESSAGE(encoder_button_release_evt,units_9_edit_handler)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_units_9_FuncEn,Menu_Function_units_9_cmd_func,preMessageHandler)   


CONST MENU_KEY op_units_9_table= 
	{&Menu_Function_units_9_cmd_func,units_9_updateDisplay,NULL,NULL};   		

	


/**
* @brieaf right key
*/
static void units_9_right_handler(void *msg)
{
	goto_rinse_config_9_disp();
	NRF_LOG_INFO("units_9_right_handler");
}

/**
* @brieaf left key
*/
static void units_9_left_handler(void *msg)
{
	goto_current_time_9_disp();
	NRF_LOG_INFO("units_9_left_handler");
}
/**
* @brieaf edit key
*/
static void units_9_edit_handler(void *msg)
{
	DISP_FLAG_T *p = get_disp_flag();
	p->sel_draw_flag_1 = 1;
	goto_unit_setting_21_disp();
	NRF_LOG_INFO("units_9_edit_handler");
}

/**
* @brieaf units page display      
*/
static void units_9_updateDisplay(void *p)
{
	NRF_LOG_INFO("=========units_9_updateDisplay============");
	DISP_FLAG_T *flag = get_disp_flag();
	bru_LCD_Units_ENG(&flag->sel_draw_flag_1);
}



/**
* @brieaf jump to units page function
*/
void goto_units_9_disp(void)
{
	jump_ui_func(&op_units_9_table);
	
}



//######################################################################################################################################
//=========== rinse config =======================
static void rinse_config_9_updateDisplay(void *p);


static void rinse_config_9_right_handler(void *msg);
static void rinse_config_9_left_handler(void *msg);
static void rinse_config_9_edit_handler(void *msg);

/*-----MENU mode Key Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_rinse_config_9_FuncEn)
		ON_VOID_POINTER_MESSAGE(button1_release_evt,machine_setup_9_back_handler)
		ON_VOID_POINTER_MESSAGE(button2_release_evt,rinse_config_9_edit_handler)
		ON_VOID_POINTER_MESSAGE(encoder_a_release_evt,rinse_config_9_right_handler)
		ON_VOID_POINTER_MESSAGE(encoder_b_release_evt,rinse_config_9_left_handler)
    ON_VOID_POINTER_MESSAGE(encoder_button_release_evt,rinse_config_9_edit_handler)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_rinse_config_9_FuncEn,Menu_Function_rinse_config_9_cmd_func,preMessageHandler)   


CONST MENU_KEY op_rinse_config_9_table= 
	{&Menu_Function_rinse_config_9_cmd_func,rinse_config_9_updateDisplay,NULL,NULL};   		

	


/**
* @brieaf right key
*/
static void rinse_config_9_right_handler(void *msg)
{
	goto_water_filter_9_disp();
	NRF_LOG_INFO("rinse_config_9_right_handler");
}

/**
* @brieaf left key
*/
static void rinse_config_9_left_handler(void *msg)
{
	goto_units_9_disp();
	NRF_LOG_INFO("rinse_config_9_left_handler");
}
/**
* @brieaf edit key
*/
static void rinse_config_9_edit_handler(void *msg)
{
	DISP_FLAG_T *p = get_disp_flag();
	p->sel_draw_flag_1 = 1;
	goto_rinse_config_22_disp();
	NRF_LOG_INFO("rinse_config_9_edit_handler");
}

/**
* @brieaf rinse config page display       
*/
static void rinse_config_9_updateDisplay(void *p)
{
	NRF_LOG_INFO("=========rinse_config_9_updateDisplay============");
	DISP_FLAG_T *flag = get_disp_flag();
	bru_LCD_Rinse_config_ENG(&flag->sel_draw_flag_1);
}



/**
* @brieaf jump to rinse config page function
*/
void goto_rinse_config_9_disp(void)
{
	jump_ui_func(&op_rinse_config_9_table);
	
}


//######################################################################################################################################
//=========== water filter =======================
static void water_filter_9_updateDisplay(void *p);


static void water_filter_9_right_handler(void *msg);
static void water_filter_9_left_handler(void *msg);
static void water_filter_9_edit_handler(void *msg);

/*-----water filter Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_water_filter_9_FuncEn)
		ON_VOID_POINTER_MESSAGE(button1_release_evt,machine_setup_9_back_handler)
		ON_VOID_POINTER_MESSAGE(button2_release_evt,water_filter_9_edit_handler)
		ON_VOID_POINTER_MESSAGE(encoder_a_release_evt,water_filter_9_right_handler)
		ON_VOID_POINTER_MESSAGE(encoder_b_release_evt,water_filter_9_left_handler)
    ON_VOID_POINTER_MESSAGE(encoder_button_release_evt,water_filter_9_edit_handler)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_water_filter_9_FuncEn,Menu_Function_water_filter_9_cmd_func,preMessageHandler)   


CONST MENU_KEY op_water_filter_9_table= 
	{&Menu_Function_water_filter_9_cmd_func,water_filter_9_updateDisplay,NULL,NULL};   		

	



/**
* @brieaf right key
*/
static void water_filter_9_right_handler(void *msg)
{
	goto_maintenance_status_9_disp();
	NRF_LOG_INFO("water_filter_9_right_handler");
}

/**
* @brieaf left key
*/
static void water_filter_9_left_handler(void *msg)
{
	goto_rinse_config_9_disp();
	NRF_LOG_INFO("water_filter_9_left_handler");
}
/**
* @brieaf edit key
*/
static void water_filter_9_edit_handler(void *msg)
{
	NRF_LOG_INFO("water_filter_9_edit_handler");
}

/**
* @brieaf water filter page display
*/
static void water_filter_9_updateDisplay(void *p)
{
	NRF_LOG_INFO("=========water_filter_9_updateDisplay============");
	DISP_FLAG_T *flag = get_disp_flag();
	bru_LCD_Water_filter_status_123_liters_ENG(&flag->sel_draw_flag_1);
}



/**
* @brieaf jump to water filter page function
*/
static void goto_water_filter_9_disp(void)
{
	jump_ui_func(&op_water_filter_9_table);
	
}


//######################################################################################################################################
//=========== maintenance status =======================
static void maintenance_status_9_updateDisplay(void *p);


static void maintenance_status_9_right_handler(void *msg);
static void maintenance_status_9_left_handler(void *msg);
static void maintenance_status_9_edit_handler(void *msg);

/*-----maintenance status Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_maintenance_status_9_FuncEn)
		ON_VOID_POINTER_MESSAGE(button1_release_evt,machine_setup_9_back_handler)
		ON_VOID_POINTER_MESSAGE(button2_release_evt,maintenance_status_9_edit_handler)
		ON_VOID_POINTER_MESSAGE(encoder_a_release_evt,maintenance_status_9_right_handler)
		ON_VOID_POINTER_MESSAGE(encoder_b_release_evt,maintenance_status_9_left_handler)
    ON_VOID_POINTER_MESSAGE(encoder_button_release_evt,maintenance_status_9_edit_handler)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_maintenance_status_9_FuncEn,Menu_Function_maintenance_status_9_cmd_func,preMessageHandler)   


CONST MENU_KEY op_maintenance_status_9_table= 
	{&Menu_Function_maintenance_status_9_cmd_func,maintenance_status_9_updateDisplay,NULL,NULL};   		

	



/**
* @brieaf right key
*/
static void maintenance_status_9_right_handler(void *msg)
{
	goto_rgb_state_9_disp();
	NRF_LOG_INFO("maintenance_status_9_right_handler");
}

/**
* @brieaf left key
*/
static void maintenance_status_9_left_handler(void *msg)
{
	goto_water_filter_9_disp();
	NRF_LOG_INFO("maintenance_status_9_left_handler");
}
/**
* @brieaf edit key
*/
static void maintenance_status_9_edit_handler(void *msg)
{
	NRF_LOG_INFO("maintenance_status_9_edit_handler");
}

/**
* @brieaf maintenance status page display      
*/
static void maintenance_status_9_updateDisplay(void *p)
{
	NRF_LOG_INFO("=========maintenance_status_9_updateDisplay============");
	DISP_FLAG_T *flag = get_disp_flag();
	bru_LCD_Maintenance_status_ENG(&flag->sel_draw_flag_1);
}



/**
* @brieaf jump to maintenance status page function
*/
static void goto_maintenance_status_9_disp(void)
{
	jump_ui_func(&op_maintenance_status_9_table);
	
}




//######################################################################################################################################
//=========== rgb state =======================
static void rgb_state_9_updateDisplay(void *p);


static void rgb_state_9_right_handler(void *msg);
static void rgb_state_9_left_handler(void *msg);
static void rgb_state_9_edit_handler(void *msg);

/*-----rgb state Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_rgb_state_9_FuncEn)
		ON_VOID_POINTER_MESSAGE(button1_release_evt,machine_setup_9_back_handler)
		ON_VOID_POINTER_MESSAGE(button2_release_evt,rgb_state_9_edit_handler)
		ON_VOID_POINTER_MESSAGE(encoder_a_release_evt,rgb_state_9_right_handler)
		ON_VOID_POINTER_MESSAGE(encoder_b_release_evt,rgb_state_9_left_handler)
    ON_VOID_POINTER_MESSAGE(encoder_button_release_evt,rgb_state_9_edit_handler)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_rgb_state_9_FuncEn,Menu_Function_rgb_state_9_cmd_func,preMessageHandler)   


CONST MENU_KEY op_rgb_state_9_table= 
	{&Menu_Function_rgb_state_9_cmd_func,rgb_state_9_updateDisplay,NULL,NULL};   	

	



/**
* @brieaf right key
*/
static void rgb_state_9_right_handler(void *msg)
{
	goto_buzzer_state_9_disp();
	NRF_LOG_INFO("rgb_state_9_right_handler");
}

/**
* @brieaf left key
*/
static void rgb_state_9_left_handler(void *msg)
{
	goto_maintenance_status_9_disp();
	NRF_LOG_INFO("rgb_state_9_left_handler");
}
/**
* @brieaf edit key
*/
static void rgb_state_9_edit_handler(void *msg)
{
	DISP_FLAG_T *p = get_disp_flag();
	p->sel_draw_flag_1 = 1;
	goto_rgb_status_25_disp();
	NRF_LOG_INFO("rgb_state_9_edit_handler");
}

/**
* @brieaf rgb state page display       
*/
static void rgb_state_9_updateDisplay(void *p)
{
	NRF_LOG_INFO("=========rgb_state_9_updateDisplay============");
	DISP_FLAG_T *flag = get_disp_flag();
  bru_LCD_RGB_state_ENG(&flag->sel_draw_flag_1);
}



/**
* @brieaf jump to rgb state page function
*/
void goto_rgb_state_9_disp(void)
{
	jump_ui_func(&op_rgb_state_9_table);
	
}



//######################################################################################################################################
//=========== buzzer state =======================
static void buzzer_state_9_updateDisplay(void *p);


static void buzzer_state_9_right_handler(void *msg);
static void buzzer_state_9_left_handler(void *msg);
static void buzzer_state_9_edit_handler(void *msg);

/*-----buzzer state Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_buzzer_state_9_FuncEn)
		ON_VOID_POINTER_MESSAGE(button1_release_evt,machine_setup_9_back_handler)
		ON_VOID_POINTER_MESSAGE(button2_release_evt,buzzer_state_9_edit_handler)
		ON_VOID_POINTER_MESSAGE(encoder_a_release_evt,buzzer_state_9_right_handler)
		ON_VOID_POINTER_MESSAGE(encoder_b_release_evt,buzzer_state_9_left_handler)
    ON_VOID_POINTER_MESSAGE(encoder_button_release_evt,buzzer_state_9_edit_handler)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_buzzer_state_9_FuncEn,Menu_Function_buzzer_state_9_cmd_func,preMessageHandler)   


CONST MENU_KEY op_buzzer_state_9_table= 
	{&Menu_Function_buzzer_state_9_cmd_func,buzzer_state_9_updateDisplay,NULL,NULL};   		

	



/**
* @brieaf right key
*/
static void buzzer_state_9_right_handler(void *msg)
{
	goto_machine_setup_9_disp();
	NRF_LOG_INFO("buzzer_state_9_right_handler");
}

/**
* @brieaf left key
*/
static void buzzer_state_9_left_handler(void *msg)
{
	goto_rgb_state_9_disp();
	NRF_LOG_INFO("buzzer_state_9_left_handler");
}
/**
* @brieaf edit key
*/
static void buzzer_state_9_edit_handler(void *msg)
{
	DISP_FLAG_T *p = get_disp_flag();
	p->sel_draw_flag_1 = 1;
	goto_buzzer_status_26_disp();
	NRF_LOG_INFO("buzzer_state_9_edit_handler");
}

/**
* @brieaf buzzer state page display
*/
static void buzzer_state_9_updateDisplay(void *p)
{
	NRF_LOG_INFO("=========buzzer_state_9_updateDisplay============");
	DISP_FLAG_T *flag = get_disp_flag();
	bru_LCD_Buzzer_state_ENG(&flag->sel_draw_flag_1);
}



/**
* @brieaf jump to buzzer state page function
*/
void goto_buzzer_state_9_disp(void)
{
	jump_ui_func(&op_buzzer_state_9_table);
	
}

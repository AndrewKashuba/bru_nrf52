#ifndef __MACHINE_SETUP_MENU_9_H
#define __MACHINE_SETUP_MENU_9_H
#include "target_command.h"

#ifdef __cplusplus
extern "C" {
#endif

void goto_machine_setup_9_disp(void);
void goto_time_format_9_disp(void);
void goto_current_time_9_disp(void);
void goto_units_9_disp(void);
void goto_rinse_config_9_disp(void);
void goto_rgb_state_9_disp(void);
void goto_buzzer_state_9_disp(void);
#ifdef __cplusplus
}
#endif

#endif


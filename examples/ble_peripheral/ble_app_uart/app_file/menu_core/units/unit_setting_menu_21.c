#include "BSP_menu_info.h"



#define NRF_LOG_MODULE_NAME unit_setting_21
#define NRF_LOG_LEVEL       3
#define NRF_LOG_INFO_COLOR  4
#define NRF_LOG_DEBUG_COLOR 3
#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();





//######################################################################################################################################
static void unit_setting_21_updateDisplay(void *p);


static void unit_setting_21_right_left_handler(void *msg);
static void unit_setting_21_save_handler(void *msg);


/*-----unit Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_unit_setting_21_FuncEn)
		ON_VOID_POINTER_MESSAGE(button2_release_evt,unit_setting_21_save_handler)
		ON_VOID_POINTER_MESSAGE(encoder_a_release_evt,unit_setting_21_right_left_handler)
		ON_VOID_POINTER_MESSAGE(encoder_b_release_evt,unit_setting_21_right_left_handler)
		ON_VOID_POINTER_MESSAGE(encoder_button_release_evt,unit_setting_21_save_handler)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_unit_setting_21_FuncEn,Menu_Function_unit_setting_21_cmd_func,preMessageHandler)   


CONST MENU_KEY op_unit_setting_21_table= 
	{&Menu_Function_unit_setting_21_cmd_func,unit_setting_21_updateDisplay,NULL,NULL};   		

	


/**
* @brieaf right key
*/
static void unit_setting_21_right_left_handler(void *msg)
{
	bru_Units_adj_ENG();
	NRF_LOG_INFO("unit_setting_21_right_left_handler");
}

/**
* @brieaf save key
*/
static void unit_setting_21_save_handler(void *msg)
{
	write_record_info_to_flash();
	get_machine_setup_info();
	DISP_FLAG_T *p = get_disp_flag();
	p->sel_draw_flag_1 = 1;
  goto_units_9_disp();
	NRF_LOG_INFO("unit_setting_21_save_handler");
}

/**
* @brieaf unit page display
*/
static void unit_setting_21_updateDisplay(void *p)
{
	NRF_LOG_INFO("=========unit_setting_21_updateDisplay============");
	DISP_FLAG_T *flag = get_disp_flag();
	bru_LCD_Units_Edit_ENG(&flag->sel_draw_flag_1);
}



/**
* @brieaf jump to unit page function
*/
void goto_unit_setting_21_disp(void)
{
	jump_ui_func(&op_unit_setting_21_table);
	
}














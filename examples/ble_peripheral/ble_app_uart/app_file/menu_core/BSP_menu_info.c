#include "BSP_menu_info.h"
#include "pwm_rgbb_core.h"
#include "gpiote_drv_core.h"

#define NRF_LOG_MODULE_NAME menu_info
#define NRF_LOG_LEVEL       3
#define NRF_LOG_INFO_COLOR  4
#define NRF_LOG_DEBUG_COLOR 3
#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();





const uint8_t command_32hz_timer_evt = 0x01;


const uint8_t button1_push_evt =  BRU_BUTTON1_PUSH;                  // ok
const uint8_t button1_long_push_evt = BRU_BUTTON1_LONG_PUSH;
const uint8_t button1_release_evt = BRU_BUTTON1_RELEASE;
const uint8_t button2_push_evt =  BRU_BUTTON2_PUSH;                 // back
const uint8_t button2_long_push_evt = BRU_BUTTON2_LONG_PUSH;
const uint8_t button2_release_evt = BRU_BUTTON2_RELEASE;

const uint8_t encoder_a_push_evt =  BRU_ENCODER_OUT_A_PUSH;               // encode right
const uint8_t encoder_a_long_push_evt = BRU_ENCODER_OUT_A_LONG_PUSH;
const uint8_t encoder_a_release_evt = BRU_ENCODER_OUT_A_RELEASE;
const uint8_t encoder_b_push_evt =  BRU_ENCODER_OUT_B_PUSH;              // encode left 
const uint8_t encoder_b_long_push_evt = BRU_ENCODER_OUT_B_LONG_PUSH; 
const uint8_t encoder_b_release_evt = BRU_ENCODER_OUT_B_RELEASE; 
const uint8_t encoder_button_push_evt =  BRU_ENCODER_OUT_BUTTON_PUSH;        // encode button
const uint8_t encoder_button_long_push_evt = BRU_ENCODER_OUT_BUTTON_LONG_PUSH;
const uint8_t encoder_button_release_evt = BRU_ENCODER_OUT_BUTTON_RELEASE; 

/**
*  @brieaf  pre-filter message handler
*/
unsigned char preMessageHandler(uint16_t *msg)
{

	if(find_timer(BACKLIGHT_TIMER_ID)->timer_value == 0)
	{
		if(*msg & KEY_OFF_FLAG)
		{			
			set_backlight_value(BACKLIGHT_ON_VALUE);
		}
		
		
		return false;
	}
	set_backlight_value(BACKLIGHT_ON_VALUE);
	NRF_LOG_INFO("preMessageHandler");
	return true;    // return TRUE,no filter key message
}


/**
* @brieaf backlight timeout 
*/
static void backlight_off_handler(void *timer_value)
{

	if(get_current_disp() != &op_home_table)
	{		
		  TIMER_INFO_T *p  = timer_value;
		  p->timer_value = BACKLIGHT_ON_VALUE;
	}
	else
	{
		//ak !!!! temp solution just for 6 Dec 2021
		//ak!!!		set_backlight_duty(0);
	}
	NRF_LOG_INFO("backlight_off_handler");
}


/**
* @brieaf backlight time setting
*/
void set_backlight_value(uint16_t value)
{
	set_timer_value(BACKLIGHT_TIMER_ID,value,NULL);
	set_backlight_duty(100);
}

/**
* @brieaf initialate backlight timer
*/
void backlight_init(void)
{	
	register_timer(BACKLIGHT_TIMER_ID,backlight_off_handler);
//	set_backlight_value(BACKLIGHT_ON_VALUE);
	
}



/**
* @brieaf 32HZ callback handler
*/
static void tick_100hz_Handler(void* data)
{
	uint8_t *flag = data;
   OnTimer();   
	 
	*flag = 0; 
} 

//################################################################################################################################
/**
* @brieaf tick massage map
*/
BEGIN_MESSAGE_MAP(tick_Function_ModeFuncEn)
    ON_VOID_POINTER_MESSAGE(command_32hz_timer_evt,tick_100hz_Handler)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */


INIT_MSG_ENTRIES(tick_Function_ModeFuncEn,tick_Function_cmd_func,NULL)  

CONST MENU_KEY tick_table = {&tick_Function_cmd_func,NULL,NULL,NULL};

//#######################################################################################################################################




	
//#########################################################################################################
#define KEY_BUF_LEN     32
 static uint8_t key_value[KEY_BUF_LEN];
 static uint8_t key_index;

//static 
	void put_key_event(uint8_t evt)
{
	 key_value[key_index] = evt;
	 MENU_KEY const* p = get_current_disp();
//	 if(p->buzzer_flag != NULL)  // no key tone
//	 {
//		 *p->buzzer_flag = 1;  
//	 }
   PostMessage(&key_value[key_index],(void*)p,p->ptrFlag);
   if(++key_index >= KEY_BUF_LEN)
	 {
		 key_index = 0;
	 }
}

/**
* @brief key message handler
*/
static void bsp_event_handler(bsp_event_t event)
{

	 NRF_LOG_INFO("bsp_event_handler %X",event);
	
	 put_key_event(event);

}



///**
//* @brieaf encode filter timer
//*/ 
//void encode_filter_timer_handler( void *timer)
//{

////	 TIMER_INFO_T *pTime = timer;
////	 uint32_t *flag = pTime->pData;
////	CRITICAL_REGION_ENTER();
////	 *flag = 0;
////	CRITICAL_REGION_EXIT();	
//		gpiote_interrupt_switching(true,ENCODE_OUTA_PIN);
//}

///**
//* @brieaf Encode outA interrupt handler
//*/
//void encode_outA_int_handler(nrfx_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
//{
//	static uint32_t filter_flag;
//	gpiote_interrupt_switching(false,ENCODE_OUTA_PIN);
//	if((action == NRF_GPIOTE_POLARITY_HITOLO) && (nrf_gpio_pin_read(ENCODE_OUTA_PIN) == 0))
//	{
//		 nrf_delay_us(100);
//		 if(nrf_gpio_pin_read(ENCODE_OUTA_PIN) == 0)	//���˵��Ӳ������
//		 {
//			  uint8_t event;
//				if(nrf_gpio_pin_read(ENCODE_OUTB_PIN) == 0)
//				{ 
//					event = BRU_ENCODER_OUT_A_RELEASE;   
//				}
//				else
//				{
//					event = BRU_ENCODER_OUT_B_RELEASE;
//				}

//				put_key_event(event);
//			
//		}
//		
//	}
//	set_timer_value(ENCODE_FILTER_TIMER_ID,ENCODE_FILTER_TIMER_VALUE,(void*)&filter_flag);
////	gpiote_interrupt_switching(true,ENCODE_OUTA_PIN);
//}


/**
* @brieaf key initial
*/
void init_key(void)
{
		bsp_key_init(bsp_event_handler);

//		BSP_gpiote_init(ENCODE_OUTA_PIN,encode_outA_int_handler,NULL);
//		nrf_gpio_cfg_input(ENCODE_OUTB_PIN,NRF_GPIO_PIN_PULLUP);
//	
//	  register_timer(ENCODE_FILTER_TIMER_ID,encode_filter_timer_handler);
}




#include "BSP_menu_info.h"



#define NRF_LOG_MODULE_NAME hot_water_edit_14
#define NRF_LOG_LEVEL       3
#define NRF_LOG_INFO_COLOR  4
#define NRF_LOG_DEBUG_COLOR 3
#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();



static void goto_water_amount_14_disp(void);

//######################################################################################################################################
//============ water temper =====================
static void hot_water_edit_14_updateDisplay(void *p);


static void hot_water_edit_14_next_handler(void *msg);
static void hot_water_edit_14_right_handler(void *msg);
static void hot_water_edit_14_left_handler(void *msg);


/*-----water temper Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_hot_water_edit_14_FuncEn)
		ON_VOID_POINTER_MESSAGE(button2_release_evt,hot_water_edit_14_next_handler)
		ON_VOID_POINTER_MESSAGE(encoder_a_release_evt,hot_water_edit_14_right_handler)
		ON_VOID_POINTER_MESSAGE(encoder_b_release_evt,hot_water_edit_14_left_handler)
		ON_VOID_POINTER_MESSAGE(encoder_button_release_evt,hot_water_edit_14_next_handler)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_hot_water_edit_14_FuncEn,Menu_Function_hot_water_edit_14_cmd_func,preMessageHandler)   


CONST MENU_KEY op_hot_water_edit_14_table= 
	{&Menu_Function_hot_water_edit_14_cmd_func,hot_water_edit_14_updateDisplay,NULL,NULL};   		

	


/**
* @brieaf right key
*/
static void hot_water_edit_14_right_handler(void *msg)
{
	NRF_LOG_INFO("hot_water_edit_14_right_handler");
	bru_BrewingTime_adj_water_temper_ENG(ENCODE_RIGHT);
}

/**
* @brieaf left key
*/
static void hot_water_edit_14_left_handler(void *msg)
{
	bru_BrewingTime_adj_water_temper_ENG(ENCODE_LEFT);
	NRF_LOG_INFO("hot_water_edit_14_left_handler");
}
/**
* @brieaf next key
*/
static void hot_water_edit_14_next_handler(void *msg)
{
	NRF_LOG_INFO("hot_water_edit_14_next_handler");
	DISP_FLAG_T *flag = get_disp_flag();
	flag->sel_draw_flag_1 = 1;
	goto_water_amount_14_disp();
}

/**
* @brieaf water temper page display
*/
static void hot_water_edit_14_updateDisplay(void *p)
{
	NRF_LOG_INFO("=========hot_water_edit_14_updateDisplay============");
	DISP_FLAG_T *flag = get_disp_flag();
	bru_LCD_WaterTemperature_Edit_ENG(&flag->sel_draw_flag_1,(const char*)"NEXT");
}



/**
* @brieaf jump to water temper page function
*/
void goto_hot_water_edit_14_disp(void)
{
	jump_ui_func(&op_hot_water_edit_14_table);
	
}







//######################################################################################################################################
//============ water amount =====================
static void water_amount_14_updateDisplay(void *p);


static void water_amount_14_save_handler(void *msg);
static void water_amount_14_right_handler(void *msg);
static void water_amount_14_left_handler(void *msg);


/*-----water amount Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_water_amount_14_FuncEn)
		ON_VOID_POINTER_MESSAGE(button2_release_evt,water_amount_14_save_handler)
		ON_VOID_POINTER_MESSAGE(encoder_a_release_evt,water_amount_14_right_handler)
		ON_VOID_POINTER_MESSAGE(encoder_b_release_evt,water_amount_14_left_handler)
		ON_VOID_POINTER_MESSAGE(encoder_button_release_evt,water_amount_14_save_handler)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_water_amount_14_FuncEn,Menu_Function_water_amount_14_cmd_func,preMessageHandler)   


CONST MENU_KEY op_water_amount_14_table= 
	{&Menu_Function_water_amount_14_cmd_func,water_amount_14_updateDisplay,NULL,NULL};   		

	


/**
* @brieaf right key
*/
static void water_amount_14_right_handler(void *msg)
{
	NRF_LOG_INFO("water_amount_14_right_handler");
	bru_BrewingTime_adj_water_amount_ENG(ENCODE_RIGHT);
}

/**
* @brieaf left key
*/
static void water_amount_14_left_handler(void *msg)
{
	NRF_LOG_INFO("water_amount_14_left_handler");
	bru_BrewingTime_adj_water_amount_ENG(ENCODE_LEFT);
}
/**
* @brieaf save key
*/
static void water_amount_14_save_handler(void *msg)
{
	write_record_info_to_flash();
	get_all_preset_info();
	DISP_FLAG_T *flag = get_disp_flag();
	flag->sel_draw_flag_1 = 1;
	
	bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
	if(hUserInterface->bMenuBackFlag == 0)
	{
		goto_hot_water_setup_disp();
	}
	else
	{
		goto_hot_water_dispenser_8_disp();
	}
	NRF_LOG_INFO("water_amount_14_next_handler");
}

/**
* @brieaf water amount page display
*/
static void water_amount_14_updateDisplay(void *p)
{
	NRF_LOG_INFO("=========water_amount_14_updateDisplay============");
	DISP_FLAG_T *flag = get_disp_flag();
	bru_LCD_WaterAmount_Edit_ENG(&flag->sel_draw_flag_1,(const char*)"SAVE");
}



/**
* @brieaf jump to water amount page function
*/
static void goto_water_amount_14_disp(void)
{
	jump_ui_func(&op_water_amount_14_table);
	
}









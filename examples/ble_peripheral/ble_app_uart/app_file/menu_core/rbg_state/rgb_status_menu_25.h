#ifndef __RGB_STATUS_MENU_25_H
#define __RGB_STATUS_MENU_25_H
#include "target_command.h"

#ifdef __cplusplus
extern "C" {
#endif

void goto_rgb_status_25_disp(void);

#ifdef __cplusplus
}
#endif

#endif


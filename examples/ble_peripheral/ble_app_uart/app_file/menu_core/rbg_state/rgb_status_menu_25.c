#include "BSP_menu_info.h"



#define NRF_LOG_MODULE_NAME rgb_status_25
#define NRF_LOG_LEVEL       3
#define NRF_LOG_INFO_COLOR  4
#define NRF_LOG_DEBUG_COLOR 3
#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();





//######################################################################################################################################
static void rgb_25_updateDisplay(void *p);


static void rgb_25_right_left_handler(void *msg);
static void rgb_25_save_handler(void *msg);

/*-----rgb Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_rbg_25_FuncEn)
		ON_VOID_POINTER_MESSAGE(button2_release_evt,rgb_25_save_handler)
		ON_VOID_POINTER_MESSAGE(encoder_a_release_evt,rgb_25_right_left_handler)
		ON_VOID_POINTER_MESSAGE(encoder_b_release_evt,rgb_25_right_left_handler)
		ON_VOID_POINTER_MESSAGE(encoder_button_release_evt,rgb_25_save_handler)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_rbg_25_FuncEn,Menu_Function_rgb_25_cmd_func,preMessageHandler)   


CONST MENU_KEY op_rgb_status_25_table = 
	{&Menu_Function_rgb_25_cmd_func,rgb_25_updateDisplay,NULL,NULL};   		

	


/**
* @brieaf right or left key
*/
static void rgb_25_right_left_handler(void *msg)
{
	bru_RGB_State_adj_ENG();
	NRF_LOG_INFO("rgb_25_right_left_handler");
}

/**
* @brieaf save key
*/
static void rgb_25_save_handler(void *msg)
{
	write_record_info_to_flash();
	get_machine_setup_info();
	DISP_FLAG_T *p = get_disp_flag();
	p->sel_draw_flag_1 = 1;
	goto_rgb_state_9_disp();
	NRF_LOG_INFO("rgb_25_save_handler");
}

/**
* @brieaf rgb page display
*/
static void rgb_25_updateDisplay(void *p)
{
	NRF_LOG_INFO("=========rgb_25_updateDisplay============");
	DISP_FLAG_T *flag = get_disp_flag();
	bru_LCD_RGB_state_Edit_ENG(&flag->sel_draw_flag_1);
}



/**
* @brieaf ��ת��rgb status����
*/
void goto_rgb_status_25_disp(void)
{
	jump_ui_func(&op_rgb_status_25_table);
	
}














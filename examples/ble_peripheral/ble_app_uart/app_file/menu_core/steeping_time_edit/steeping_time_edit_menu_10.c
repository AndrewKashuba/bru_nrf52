#include "BSP_menu_info.h"



#define NRF_LOG_MODULE_NAME steeping_time_10
#define NRF_LOG_LEVEL       3
#define NRF_LOG_INFO_COLOR  4
#define NRF_LOG_DEBUG_COLOR 3
#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();





//######################################################################################################################################
static void steeping_time_10_updateDisplay(void *p);


static void steeping_time_10_save_handler(void *msg);
static void steeping_time_10_right_handler(void *msg);
static void steeping_time_10_left_handler(void *msg);

/*-----steeping_time Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_steeping_time_10_FuncEn)
		ON_VOID_POINTER_MESSAGE(button2_release_evt,steeping_time_10_save_handler)
		ON_VOID_POINTER_MESSAGE(encoder_a_release_evt,steeping_time_10_right_handler)
		ON_VOID_POINTER_MESSAGE(encoder_b_release_evt,steeping_time_10_left_handler)
		ON_VOID_POINTER_MESSAGE(encoder_button_release_evt,steeping_time_10_save_handler)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_steeping_time_10_FuncEn,Menu_Function_steping_time_10_cmd_func,preMessageHandler)   


CONST MENU_KEY op_steeping_time_10_table= 
	{&Menu_Function_steping_time_10_cmd_func,steeping_time_10_updateDisplay,NULL,NULL};   		

/**
* @brieaf right key
*/
static void steeping_time_10_right_handler(void *msg)
{
//	 NRF_LOG_INFO("steeping_time_10_right_handler");
//	 bru_BrewingTime_adj_steepint_time_ENG(ENCODE_RIGHT);
}

/**
* @brieaf left key
*/
static void steeping_time_10_left_handler(void *msg)
{
//	NRF_LOG_INFO("steeping_time_10_left_handler");
//	bru_BrewingTime_adj_steepint_time_ENG(ENCODE_LEFT);
}
/**
* @brieaf save key
*/
static void steeping_time_10_save_handler(void *msg)
{
//	write_record_info_to_flash();
//	get_all_preset_info();
//	DISP_FLAG_T *flag = get_disp_flag();
//	flag->sel_draw_flag_1 = 1;
//	goto_preset_main_edit_disp();
//	NRF_LOG_INFO("steeping_time_10_save_handler");
}

/**
* @brieaf steeping_time page display
*/
static void steeping_time_10_updateDisplay(void *p)
{
//	NRF_LOG_INFO("=========steeping_time_10_updateDisplay============");
//	DISP_FLAG_T *flag = get_disp_flag();
//	
//	bru_LCD_BrewingTime_Steeping_Edit_ENG(&flag->sel_draw_flag_1,(const char *)"SAVE");
}

/**
* @brieaf jump to steeping_time page function
*/
void goto_steeping_time_10_disp(void)
{
//	jump_ui_func(&op_steeping_time_10_table);
}

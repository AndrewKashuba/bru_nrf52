#include "BSP_menu_info.h"



#define NRF_LOG_MODULE_NAME time_edit_16
#define NRF_LOG_LEVEL       3
#define NRF_LOG_INFO_COLOR  4
#define NRF_LOG_DEBUG_COLOR 3
#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();




static void goto_time_minute_edit_16_disp(void);
static void goto_alarm_minute_edit_16_disp(void);
static void goto_machine_time_minute_edit_16_disp(void);

//######################################################################################################################################
//======== time hour edit ====================
static void time_16_updateDisplay(void *p);


static void time_16_right_handler(void *msg);
static void time_16_left_handler(void *msg);
static void time_16_save_handler(void *msg);


/*-----time hour Key Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_time_edit_16_FuncEn)
		ON_VOID_POINTER_MESSAGE(button2_release_evt,time_16_save_handler)
		ON_VOID_POINTER_MESSAGE(encoder_a_release_evt,time_16_right_handler)
		ON_VOID_POINTER_MESSAGE(encoder_b_release_evt,time_16_left_handler)
		ON_VOID_POINTER_MESSAGE(encoder_button_release_evt,time_16_save_handler)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_time_edit_16_FuncEn,Menu_Function_time_edit_16_cmd_func,preMessageHandler)   


CONST MENU_KEY op_time_edit_16_table= 
	{&Menu_Function_time_edit_16_cmd_func,time_16_updateDisplay,NULL,NULL};   		

	


/**
* @brieaf right key
*/
static void time_16_right_handler(void *msg)
{
	NRF_LOG_INFO("time_16_right_handler");
	bru_sys_time_alarm_adj_hour_ENG(TIME_SEL,ENCODE_RIGHT);
}

/**
* @brieaf left key
*/
static void time_16_left_handler(void *msg)
{
	bru_sys_time_alarm_adj_hour_ENG(TIME_SEL,ENCODE_LEFT);
	NRF_LOG_INFO("time_16_left_handler");
}
/**
* @brieaf save key
*/
static void time_16_save_handler(void *msg)
{
	goto_time_minute_edit_16_disp();
	NRF_LOG_INFO("time_16_save_handler");
}

/**
* @brieaf time hour page display
*/
static void time_16_updateDisplay(void *p)
{
	NRF_LOG_INFO("=========time_16_updateDisplay============");
	DISP_FLAG_T *flag = get_disp_flag();
	bru_LCD_System_Time_Edit_ENG(&flag->sel_draw_flag_1,TIME_SEL,ADJ_HOUR_POS);
}



/**
* @brieaf jump to time hour page function
*/
void goto_time_edit_16_disp(void)
{
	jump_ui_func(&op_time_edit_16_table);
	
}





//######################################################################################################################################
//======== time minute edit ====================
static void time_minute_16_updateDisplay(void *p);


static void time_minute_16_right_handler(void *msg);
static void time_minute_16_left_handler(void *msg);
static void time_minute_16_save_handler(void *msg);


/*-----time minute Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_time_minute_edit_16_FuncEn)
		ON_VOID_POINTER_MESSAGE(button2_release_evt,time_minute_16_save_handler)
		ON_VOID_POINTER_MESSAGE(encoder_a_release_evt,time_minute_16_right_handler)
		ON_VOID_POINTER_MESSAGE(encoder_b_release_evt,time_minute_16_left_handler)
		ON_VOID_POINTER_MESSAGE(encoder_button_release_evt,time_minute_16_save_handler)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_time_minute_edit_16_FuncEn,Menu_Function_time_minute_edit_16_cmd_func,preMessageHandler)   


CONST MENU_KEY op_time_minute_edit_16_table= 
	{&Menu_Function_time_minute_edit_16_cmd_func,time_minute_16_updateDisplay,NULL,NULL};   		

	



/**
* @brieaf right key
*/
static void time_minute_16_right_handler(void *msg)
{
	NRF_LOG_INFO("time_minute_16_right_handler");
	bru_sys_time_alarm_adj_minute_ENG(TIME_SEL,ENCODE_RIGHT);
}


/**
* @brieaf left key
*/
static void time_minute_16_left_handler(void *msg)
{
	NRF_LOG_INFO("time_minute_16_left_handler");
	bru_sys_time_alarm_adj_minute_ENG(TIME_SEL,ENCODE_LEFT);
}


/**
* @brieaf save key
*/
static void time_minute_16_save_handler(void *msg)
{
	 write_record_info_to_flash();
	 get_current_time_handler();
	 DISP_FLAG_T *p = get_disp_flag();
	 p->sel_draw_flag_1 = 1;
	 
	goto_current_time_edit_15_disp();
	
	NRF_LOG_INFO("time_minute_16_save_handler");
}

/**
* @brieaf time minute page display
*/
static void time_minute_16_updateDisplay(void *p)
{
	NRF_LOG_INFO("=========time_minute_16_updateDisplay============");
	DISP_FLAG_T *flag = get_disp_flag();
	bru_LCD_System_Time_Edit_ENG(&flag->sel_draw_flag_1,TIME_SEL,ADJ_MINUTE_POS);
}



/**
* @brieaf jump to time minute page function
*/
static void goto_time_minute_edit_16_disp(void)
{
	jump_ui_func(&op_time_minute_edit_16_table);
	
}





//######################################################################################################################################
//======== alarm hour edit ====================
static void alarm_hour_16_updateDisplay(void *p);


static void alarm_hour_16_right_handler(void *msg);
static void alarm_hour_16_left_handler(void *msg);
static void alarm_hour_16_save_handler(void *msg);


/*-----alarm hour Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_alarm_hour_edit_16_FuncEn)
		ON_VOID_POINTER_MESSAGE(button2_release_evt,alarm_hour_16_save_handler)
		ON_VOID_POINTER_MESSAGE(encoder_a_release_evt,alarm_hour_16_right_handler)
		ON_VOID_POINTER_MESSAGE(encoder_b_release_evt,alarm_hour_16_left_handler)
		ON_VOID_POINTER_MESSAGE(encoder_button_release_evt,alarm_hour_16_save_handler)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_alarm_hour_edit_16_FuncEn,Menu_Function_alarm_hour_edit_16_cmd_func,preMessageHandler)   


CONST MENU_KEY op_alarm_hour_edit_16_table= 
	{&Menu_Function_alarm_hour_edit_16_cmd_func,alarm_hour_16_updateDisplay,NULL,NULL};   	

	



/**
* @brieaf right key
*/
static void alarm_hour_16_right_handler(void *msg)
{
	bru_sys_time_alarm_adj_hour_ENG(ALARM_SEL,ENCODE_RIGHT);
	NRF_LOG_INFO("alarm_hour_16_right_handler");
}

/**
* @brieaf left key
*/
static void alarm_hour_16_left_handler(void *msg)
{
	bru_sys_time_alarm_adj_hour_ENG(ALARM_SEL,ENCODE_LEFT);
	NRF_LOG_INFO("alarm_hour_16_left_handler");
}
/**
* @brieaf save key
*/
static void alarm_hour_16_save_handler(void *msg)
{
	
	goto_alarm_minute_edit_16_disp();
	NRF_LOG_INFO("alarm_hour_16_save_handler");
}

/**
* @brieaf   alarm hour page display
*/
static void alarm_hour_16_updateDisplay(void *p)
{
	NRF_LOG_INFO("=========alarm_hour_16_updateDisplay============");
	DISP_FLAG_T *flag = get_disp_flag();
	bru_LCD_System_Time_Edit_ENG(&flag->sel_draw_flag_1,ALARM_SEL,ADJ_HOUR_POS);
}



/**
* @brieaf jump to alarm hour page function
*/
void goto_alarm_hour_edit_16_disp(void)
{
	jump_ui_func(&op_alarm_hour_edit_16_table);
	
}





//######################################################################################################################################
//======== alarm minute edit ====================
static void alarm_minute_16_updateDisplay(void *p);


static void alarm_minute_16_right_handler(void *msg);
static void alarm_minute_16_left_handler(void *msg);
static void alarm_minute_16_save_handler(void *msg);


/*-----alarm minute Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_alarm_minute_edit_16_FuncEn)
		ON_VOID_POINTER_MESSAGE(button2_release_evt,alarm_minute_16_save_handler)
		ON_VOID_POINTER_MESSAGE(encoder_a_release_evt,alarm_minute_16_right_handler)
		ON_VOID_POINTER_MESSAGE(encoder_b_release_evt,alarm_minute_16_left_handler)
		ON_VOID_POINTER_MESSAGE(encoder_button_release_evt,alarm_minute_16_save_handler)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_alarm_minute_edit_16_FuncEn,Menu_Function_alarm_minute_edit_16_cmd_func,preMessageHandler)   


CONST MENU_KEY op_alarm_minute_edit_16_table= 
	{&Menu_Function_alarm_minute_edit_16_cmd_func,alarm_minute_16_updateDisplay,NULL,NULL};   	

	


/**
* @brieaf right key
*/
static void alarm_minute_16_right_handler(void *msg)
{
	bru_sys_time_alarm_adj_minute_ENG(ALARM_SEL,ENCODE_RIGHT);
	NRF_LOG_INFO("alarm_minute_16_right_handler");
}

/**
* @brieaf left key
*/
static void alarm_minute_16_left_handler(void *msg)
{
	bru_sys_time_alarm_adj_minute_ENG(ALARM_SEL,ENCODE_LEFT);
	NRF_LOG_INFO("alarm_minute_16_left_handler");
}
/**
* @brieaf save key
*/
static void alarm_minute_16_save_handler(void *msg)
{
	write_record_info_to_flash();
	get_alarm_time_handler();
	 DISP_FLAG_T *p = get_disp_flag();
	 p->sel_draw_flag_1 = 1;
	goto_alarm_time_edit_15_disp();
	NRF_LOG_INFO("alarm_minute_16_save_handler");
}

/**
*   @brieaf alarm minute page display
*/
static void alarm_minute_16_updateDisplay(void *p)
{
	NRF_LOG_INFO("=========alarm_minute_16_updateDisplay============");
	DISP_FLAG_T *flag = get_disp_flag();
	bru_LCD_System_Time_Edit_ENG(&flag->sel_draw_flag_1,ALARM_SEL,ADJ_MINUTE_POS);
}

/**
* @brieaf jump to alarm minute page function
*/
static void goto_alarm_minute_edit_16_disp(void)
{
	jump_ui_func(&op_alarm_minute_edit_16_table);
}






//###########################################################################################################

//######################################################################################################################################
//======== machine time hour edit ====================
static void machine_time_16_updateDisplay(void *p);


static void machine_time_16_right_handler(void *msg);
static void machine_time_16_left_handler(void *msg);
static void machine_time_16_save_handler(void *msg);


/*-----machine_time hour Key Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_machine_time_edit_16_FuncEn)
		ON_VOID_POINTER_MESSAGE(button2_release_evt,machine_time_16_save_handler)
		ON_VOID_POINTER_MESSAGE(encoder_a_release_evt,machine_time_16_right_handler)
		ON_VOID_POINTER_MESSAGE(encoder_b_release_evt,machine_time_16_left_handler)
		ON_VOID_POINTER_MESSAGE(encoder_button_release_evt,machine_time_16_save_handler)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_machine_time_edit_16_FuncEn,Menu_Function_machine_time_edit_16_cmd_func,preMessageHandler)   


CONST MENU_KEY op_machine_time_edit_16_table= 
	{&Menu_Function_machine_time_edit_16_cmd_func,machine_time_16_updateDisplay,NULL,NULL};   		

	


/**
* @brieaf right key
*/
static void machine_time_16_right_handler(void *msg)
{
	NRF_LOG_INFO("machine_time_16_right_handler");
  bru_sys_time_alarm_adj_hour_ENG(TIME_SEL,ENCODE_RIGHT);
	
}

/**
* @brieaf left key
*/
static void machine_time_16_left_handler(void *msg)
{
	bru_sys_time_alarm_adj_hour_ENG(TIME_SEL,ENCODE_LEFT);
	NRF_LOG_INFO("machine_time_16_left_handler");
}
/**
* @brieaf save key
*/
static void machine_time_16_save_handler(void *msg)
{
	goto_machine_time_minute_edit_16_disp();
	NRF_LOG_INFO("machine_time_16_save_handler");
}

/**
* @brieaf machine_time hour page display
*/
static void machine_time_16_updateDisplay(void *p)
{
	NRF_LOG_INFO("=========machine_time_16_updateDisplay============");
	DISP_FLAG_T *flag = get_disp_flag();
	bru_LCD_System_Time_Edit_ENG(&flag->sel_draw_flag_1,TIME_SEL,ADJ_HOUR_POS);
}



/**
* @brieaf jump to machine_time hour page function
*/
void goto_machine_time_edit_16_disp(void)
{
	jump_ui_func(&op_machine_time_edit_16_table);
	
}





//######################################################################################################################################
//======== machine_time minute edit ====================
static void machine_time_minute_16_updateDisplay(void *p);


static void machine_time_minute_16_right_handler(void *msg);
static void machine_time_minute_16_left_handler(void *msg);
static void machine_time_minute_16_save_handler(void *msg);


/*-----machine_time minute Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_machine_time_minute_edit_16_FuncEn)
		ON_VOID_POINTER_MESSAGE(button2_release_evt,machine_time_minute_16_save_handler)
		ON_VOID_POINTER_MESSAGE(encoder_a_release_evt,machine_time_minute_16_right_handler)
		ON_VOID_POINTER_MESSAGE(encoder_b_release_evt,machine_time_minute_16_left_handler)
		ON_VOID_POINTER_MESSAGE(encoder_button_release_evt,machine_time_minute_16_save_handler)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_machine_time_minute_edit_16_FuncEn,Menu_Function_machine_time_minute_edit_16_cmd_func,preMessageHandler)   


CONST MENU_KEY op_machine_time_minute_edit_16_table= 
	{&Menu_Function_machine_time_minute_edit_16_cmd_func,machine_time_minute_16_updateDisplay,NULL,NULL};   		

	



/**
* @brieaf right key
*/
static void machine_time_minute_16_right_handler(void *msg)
{
	NRF_LOG_INFO("machine_time_minute_16_right_handler");
	bru_sys_time_alarm_adj_minute_ENG(TIME_SEL,ENCODE_RIGHT);
}


/**
* @brieaf left key
*/
static void machine_time_minute_16_left_handler(void *msg)
{
	NRF_LOG_INFO("machine_time_minute_16_left_handler");
	bru_sys_time_alarm_adj_minute_ENG(TIME_SEL,ENCODE_LEFT);
}


/**
* @brieaf save key
*/
static void machine_time_minute_16_save_handler(void *msg)
{
   write_record_info_to_flash();
	 get_current_time_handler();
	 DISP_FLAG_T *p = get_disp_flag();
	 p->sel_draw_flag_1 = 1;
	 goto_current_time_9_disp();
	NRF_LOG_INFO("machine_time_minute_16_save_handler");
}

/**
* @brieaf machine_time minute page display
*/
static void machine_time_minute_16_updateDisplay(void *p)
{
	NRF_LOG_INFO("=========machine_time_minute_16_updateDisplay============");
	DISP_FLAG_T *flag = get_disp_flag();
	bru_LCD_System_Time_Edit_ENG(&flag->sel_draw_flag_1,TIME_SEL,ADJ_MINUTE_POS);
}



/**
* @brieaf jump to machine_time minute page function
*/
static void goto_machine_time_minute_edit_16_disp(void)
{
	jump_ui_func(&op_machine_time_minute_edit_16_table);
	
}



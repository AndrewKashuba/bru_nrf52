#ifndef __TIME_EDIT_MENU_16_H
#define __TIME_EDIT_MENU_16_H
#include "target_command.h"

#ifdef __cplusplus
extern "C" {
#endif

void goto_time_edit_16_disp(void);
void goto_alarm_hour_edit_16_disp(void);
void goto_machine_time_edit_16_disp(void);
#ifdef __cplusplus
}
#endif

#endif


#ifndef __WASHING_MENU_6_H
#define __WASHING_MENU_6_H
#include "target_command.h"

#ifdef __cplusplus
extern "C" {
#endif

void goto_washing_disp(void);
void goto_washing_place_cup_6_disp(void);
void goto_washing_close_lid_6_disp(void);
void goto_washing_remove_cup_6_disp(void);

#ifdef __cplusplus
}
#endif

#endif


#include "BSP_menu_info.h"



#define NRF_LOG_MODULE_NAME washing
#define NRF_LOG_LEVEL       3
#define NRF_LOG_INFO_COLOR  4
#define NRF_LOG_DEBUG_COLOR 3
#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();





//######################################################################################################################################
static void washing_updateDisplay(void *p);


static void washing_back_handler(void *msg);

/*-----washing Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_washing_FuncEn)
		ON_VOID_POINTER_MESSAGE(button1_release_evt,washing_back_handler)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_washing_FuncEn,Menu_Function_washing_cmd_func,preMessageHandler)   


CONST MENU_KEY op_washing_table= 
	{&Menu_Function_washing_cmd_func,washing_updateDisplay,NULL,NULL};   		

	


/**
* @brieaf washing back key
*/
static void washing_back_handler(void *msg)
{
	DISP_FLAG_T *p = get_disp_flag();
	p->sel_draw_flag_1 = 1;
	goto_brewing_washing_5_disp();
	NRF_LOG_INFO("washing_back_handler");
}


/**
* @brieaf washing page display
*/
static void washing_updateDisplay(void *p)
{
//	NRF_LOG_INFO("=========washing_updateDisplay============");
//	bru_LCD_Chamber_Washing_ENG("STOP","");
}



/**
* @brieaf jump to washing page function
*/
void goto_washing_disp(void)
{
	jump_ui_func(&op_washing_table);
	
}







//######################################################################################################################################
static void washing_place_cup_6_updateDisplay(void *p);



/*-----washing_place_cup Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_washing_place_cup_6_FuncEn)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_washing_place_cup_6_FuncEn,Menu_Function_washing_place_cup_6_cmd_func,preMessageHandler)   


CONST MENU_KEY op_washing_place_cup_6_table= 
	{&Menu_Function_washing_place_cup_6_cmd_func,washing_place_cup_6_updateDisplay,NULL,NULL};   		

	

/**
* @brieaf washing_place_cup page display
*/
static void washing_place_cup_6_updateDisplay(void *p)
{
	NRF_LOG_INFO("=========washing_place_cup_6_updateDisplay============");
//	bru_LCD_Place_Cup_ENG("","");
}



/**
* @brieaf jump to washing_place_cup page function
*/
void goto_washing_place_cup_6_disp(void)
{
	jump_ui_func(&op_washing_place_cup_6_table);
	
}

//######################################################################################################################################
//static void washing_close_lid_6_updateDisplay(void *p);

///*-----washing_close_lid Message Map ---*/
//BEGIN_MESSAGE_MAP(Menu_Function_washing_close_lid_6_FuncEn)
//END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */

//INIT_MSG_ENTRIES(Menu_Function_washing_close_lid_6_FuncEn,Menu_Function_washing_close_lid_6_cmd_func,preMessageHandler)   

//CONST MENU_KEY op_washing_close_lid_6_table= 
//	{&Menu_Function_washing_close_lid_6_cmd_func,washing_close_lid_6_updateDisplay,NULL,NULL};   	

///**
//* @brieaf washing_close_lid page display
//*/
//static void washing_close_lid_6_updateDisplay(void *p)
//{
//	NRF_LOG_INFO("=========washing_close_lid_6_updateDisplay============");
//	bru_LCD_Close_Lid_ENG("","");
//}


///**
//* @brieaf jump to washing_close_lid page function
//*/
//void goto_washing_close_lid_6_disp(void)
//{
//	jump_ui_func(&op_washing_close_lid_6_table);
//}

//######################################################################################################################################
static void washing_remove_cup_6_updateDisplay(void *p);



/*-----washing_remove_cup Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_washing_remove_cup_6_FuncEn)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_washing_remove_cup_6_FuncEn,Menu_Function_washing_remove_cup_6_cmd_func,preMessageHandler)   


CONST MENU_KEY op_washing_remove_cup_6_table= 
	{&Menu_Function_washing_remove_cup_6_cmd_func,washing_remove_cup_6_updateDisplay,NULL,NULL};   		

	


/**
* @brieaf washing_remove_cup page display
*/
static void washing_remove_cup_6_updateDisplay(void *p)
{
	NRF_LOG_INFO("=========washing_remove_cup_6_updateDisplay============");
	
//	bru_LCD_Remove_Cup_ENG("","");
}



/**
* @brieaf jump to washing_remove_cup page function
*/
void goto_washing_remove_cup_6_disp(void)
{
	jump_ui_func(&op_washing_remove_cup_6_table);
	
}



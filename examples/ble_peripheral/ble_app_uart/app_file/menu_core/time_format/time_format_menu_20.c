#include "BSP_menu_info.h"



#define NRF_LOG_MODULE_NAME time_format_20
#define NRF_LOG_LEVEL       3
#define NRF_LOG_INFO_COLOR  4
#define NRF_LOG_DEBUG_COLOR 3
#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();





//######################################################################################################################################
static void time_format_20_updateDisplay(void *p);


static void time_format_20_right_left_handler(void *msg);
static void time_format_20_save_handler(void *msg);

/*-----MENU mode Key Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_time_format_20_FuncEn)
		ON_VOID_POINTER_MESSAGE(button2_release_evt,time_format_20_save_handler)
		ON_VOID_POINTER_MESSAGE(encoder_a_release_evt,time_format_20_right_left_handler)
		ON_VOID_POINTER_MESSAGE(encoder_b_release_evt,time_format_20_right_left_handler)
		ON_VOID_POINTER_MESSAGE(encoder_button_release_evt,time_format_20_save_handler)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_time_format_20_FuncEn,Menu_Function_time_format_20_cmd_func,preMessageHandler)   


CONST MENU_KEY op_time_format_20_table= 
	{&Menu_Function_time_format_20_cmd_func,time_format_20_updateDisplay,NULL,NULL};   	

	


/**
* @brieaf right or left key
*/
static void time_format_20_right_left_handler(void *msg)
{
	bru_Timeformat_adj_ENG();
	NRF_LOG_INFO("time_format_20_right_left_handler");
}

/**
* @brieaf save key
*/
static void time_format_20_save_handler(void *msg)
{
	write_record_info_to_flash();
	get_machine_setup_info();
	DISP_FLAG_T *p = get_disp_flag();
	p->sel_draw_flag_1 = 1;
	goto_time_format_9_disp();
	NRF_LOG_INFO("time_format_20_save_handler");
}

/**
* @brieaf time_format page display
*/
static void time_format_20_updateDisplay(void *p)
{
	NRF_LOG_INFO("=========time_format_20_updateDisplay============");
	DISP_FLAG_T *flag = get_disp_flag();
	bru_LCD_Timeformat_Edit_ENG(&flag->sel_draw_flag_1);
}



/**
* @brieaf jump to time_format page function
*/
void goto_time_format_20_disp(void)
{
	jump_ui_func(&op_time_format_20_table);
	
}














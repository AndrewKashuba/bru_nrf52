#include "BSP_menu_info.h"



#define NRF_LOG_MODULE_NAME pre_alarm_edit_17
#define NRF_LOG_LEVEL       3
#define NRF_LOG_INFO_COLOR  4
#define NRF_LOG_DEBUG_COLOR 3
#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();



static void goto_water_temper_edit_17_disp(void);
static void goto_water_amount_edit_17_disp(void);

//######################################################################################################################################
//============== steeping time =======================
static void pre_alarm_17_updateDisplay(void *p);


static void pre_alarm_17_right_handler(void *msg);
static void pre_alarm_17_left_handler(void *msg);
static void pre_alarm_17_next_handler(void *msg);

/*-----steeping time Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_pre_alarm_edit_17_FuncEn)
		ON_VOID_POINTER_MESSAGE(button2_release_evt,pre_alarm_17_next_handler)
		ON_VOID_POINTER_MESSAGE(encoder_a_release_evt,pre_alarm_17_right_handler)
		ON_VOID_POINTER_MESSAGE(encoder_b_release_evt,pre_alarm_17_left_handler)
		ON_VOID_POINTER_MESSAGE(encoder_button_release_evt,pre_alarm_17_next_handler)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_pre_alarm_edit_17_FuncEn,Menu_Function_pre_alarm_edit_17_cmd_func,preMessageHandler)   


CONST MENU_KEY op_pre_alarm_edit_17_table= 
	{&Menu_Function_pre_alarm_edit_17_cmd_func,pre_alarm_17_updateDisplay,NULL,NULL};   		

	


/**
* @brieaf right key
*/
static void pre_alarm_17_right_handler(void *msg)
{
	bru_BrewingTime_adj_steepint_time_ENG(ENCODE_RIGHT);
	NRF_LOG_INFO("pre_alarm_17_right_handler");
}

/**
* @brieaf left key
*/
static void pre_alarm_17_left_handler(void *msg)
{
	bru_BrewingTime_adj_steepint_time_ENG(ENCODE_LEFT);
	NRF_LOG_INFO("pre_alarm_17_left_handler");
}

/**
* @brieaf next key
*/
static void pre_alarm_17_next_handler(void *msg)
{
	 DISP_FLAG_T *p = get_disp_flag();
	 p->sel_draw_flag_1 = 1;
	 goto_water_temper_edit_17_disp(); 
	 NRF_LOG_INFO("pre_alarm_17_next_handler");
}

/**
@brieaf steeping time page display      
*/
static void pre_alarm_17_updateDisplay(void *p)
{
	NRF_LOG_INFO("=========pre_alarm_17_updateDisplay============");
	DISP_FLAG_T *flag = get_disp_flag();
	bru_LCD_BrewingTime_Steeping_Edit_ENG(&flag->sel_draw_flag_1,(const char*)"NEXT");
}



/**
* @brieaf jump to steeping time page function
*/
void goto_pre_alarm_edit_17_disp(void)
{
	jump_ui_func(&op_pre_alarm_edit_17_table);
	
}




//######################################################################################################################################
//============== water temper =======================
static void water_temper_17_updateDisplay(void *p);


static void water_temper_17_right_handler(void *msg);
static void water_temper_17_left_handler(void *msg);
static void water_temper_17_next_handler(void *msg);


/*-----water_temper Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_water_temper_edit_17_FuncEn)
		ON_VOID_POINTER_MESSAGE(button2_release_evt,water_temper_17_next_handler)
		ON_VOID_POINTER_MESSAGE(encoder_a_release_evt,water_temper_17_right_handler)
		ON_VOID_POINTER_MESSAGE(encoder_b_release_evt,water_temper_17_left_handler)
		ON_VOID_POINTER_MESSAGE(encoder_button_release_evt,water_temper_17_next_handler)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_water_temper_edit_17_FuncEn,Menu_Function_water_temper_edit_17_cmd_func,preMessageHandler)   


CONST MENU_KEY op_water_temper_edit_17_table= 
	{&Menu_Function_water_temper_edit_17_cmd_func,water_temper_17_updateDisplay,NULL,NULL};   	

	



/**
* @brieaf right key
*/
static void water_temper_17_right_handler(void *msg)
{
	bru_BrewingTime_adj_water_temper_ENG(ENCODE_RIGHT);
	NRF_LOG_INFO("water_temper_17_right_handler");
}

/**
* @brieaf left key
*/
static void water_temper_17_left_handler(void *msg)
{
	bru_BrewingTime_adj_water_temper_ENG(ENCODE_LEFT);
	NRF_LOG_INFO("water_temper_17_left_handler");
}
/**
* @brieaf next key
*/
static void water_temper_17_next_handler(void *msg)
{
	DISP_FLAG_T *p = get_disp_flag();
	p->sel_draw_flag_1 = 1;
	goto_water_amount_edit_17_disp();
	NRF_LOG_INFO("water_temper_17_next_handler");
}

/**
* @brieaf water_temper page display
*/
static void water_temper_17_updateDisplay(void *p)
{
	NRF_LOG_INFO("=========water_temper_17_updateDisplay============");
	DISP_FLAG_T *flag = get_disp_flag();
	bru_LCD_WaterTemperature_Edit_ENG(&flag->sel_draw_flag_1,(const char*)"NEXT");
}



/**
* @brieaf jump to water_temper page function
*/
static void goto_water_temper_edit_17_disp(void)
{
	jump_ui_func(&op_water_temper_edit_17_table);
	
}



//######################################################################################################################################
//============== water amount =======================
static void water_amount_17_updateDisplay(void *p);


static void water_amount_17_right_handler(void *msg);
static void water_amount_17_left_handler(void *msg);
static void water_amount_17_save_handler(void *msg);


/*-----water amount Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_water_amount_edit_17_FuncEn)
		ON_VOID_POINTER_MESSAGE(button2_release_evt,water_amount_17_save_handler)
		ON_VOID_POINTER_MESSAGE(encoder_a_release_evt,water_amount_17_right_handler)
		ON_VOID_POINTER_MESSAGE(encoder_b_release_evt,water_amount_17_left_handler)
		ON_VOID_POINTER_MESSAGE(encoder_button_release_evt,water_amount_17_save_handler)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_water_amount_edit_17_FuncEn,Menu_Function_water_amount_edit_17_cmd_func,preMessageHandler)   


CONST MENU_KEY op_water_amount_edit_17_table= 
	{&Menu_Function_water_amount_edit_17_cmd_func,water_amount_17_updateDisplay,NULL,NULL};   		

	


/**
* @brieaf right key
*/
static void water_amount_17_right_handler(void *msg)
{
	NRF_LOG_INFO("water_amount_17_right_handler");
	bru_BrewingTime_adj_water_amount_ENG(ENCODE_RIGHT);
}

/**
* @brieaf left key
*/
static void water_amount_17_left_handler(void *msg)
{
	NRF_LOG_INFO("water_amount_17_left_handler");
	bru_BrewingTime_adj_water_amount_ENG(ENCODE_LEFT);
}
/**
* @brieaf save key
*/
static void water_amount_17_save_handler(void *msg)
{
	write_record_info_to_flash();
	get_all_preset_info();
	DISP_FLAG_T *p = get_disp_flag();
	p->sel_draw_flag_1 = 1;
	goto_alarm_preset_edit_15_disp();
	NRF_LOG_INFO("water_amount_17_save_handler");
}

/**
* @brieaf water amount page display
*/
static void water_amount_17_updateDisplay(void *p)
{
	NRF_LOG_INFO("=========water_amount_17_updateDisplay============");
	DISP_FLAG_T *flag = get_disp_flag();
	bru_LCD_WaterAmount_Edit_ENG(&flag->sel_draw_flag_1,(const char*)"SAVE");
}



/**
* @brieaf jump to water amount page function
*/
static void goto_water_amount_edit_17_disp(void)
{
	jump_ui_func(&op_water_amount_edit_17_table);
	
}










#include "BSP_menu_info.h"



#define NRF_LOG_MODULE_NAME preset_setup_8
#define NRF_LOG_LEVEL       3
#define NRF_LOG_INFO_COLOR  4
#define NRF_LOG_DEBUG_COLOR 3
#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();



static void goto_default_presets_8_disp(void);

//######################################################################################################################################
static void preset_setup_8_updateDisplay(void *p);


static void preset_setup_8_back_handler(void *msg);
static void preset_setup_8_right_handler(void *msg);
static void preset_setup_8_left_handler(void *msg);
static void preset_setup_8_edit_handler(void *msg);

/*-----preset_setup Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_preset_setup_8_FuncEn)
		ON_VOID_POINTER_MESSAGE(button1_release_evt,preset_setup_8_back_handler)
		ON_VOID_POINTER_MESSAGE(button2_release_evt,preset_setup_8_edit_handler)
		ON_VOID_POINTER_MESSAGE(encoder_a_release_evt,preset_setup_8_right_handler)
		ON_VOID_POINTER_MESSAGE(encoder_b_release_evt,preset_setup_8_left_handler)
		ON_VOID_POINTER_MESSAGE(encoder_button_release_evt,preset_setup_8_edit_handler)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_preset_setup_8_FuncEn,Menu_Function_preset_setup_8_cmd_func,preMessageHandler)   


CONST MENU_KEY op_preset_setup_8_table= 
	{&Menu_Function_preset_setup_8_cmd_func,preset_setup_8_updateDisplay,NULL,NULL};   		

	


/**
* @brieaf preset setup 8 back key
*/
static void preset_setup_8_back_handler(void *msg)
{
	NRF_LOG_INFO("preset_setup_8_back_handler");
	DISP_FLAG_T *flag = get_disp_flag();
	flag->sel_draw_flag_1 = 1;
	
	goto_preset_disp();
}

/**
* @brieaf right key
*/
static void preset_setup_8_right_handler(void *msg)
{
	NRF_LOG_INFO("preset_setup_8_right_handler");
	DISP_FLAG_T *flag = get_disp_flag();
	flag->flush_disp_flag = 1; 
	bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
	if(++hUserInterface->iPresetCurrentIndex == 4)
	{
		goto_hot_water_dispenser_8_disp();
	}
	
}

/**
* @brieaf left key
*/
static void preset_setup_8_left_handler(void *msg)
{
	NRF_LOG_INFO("preset_setup_8_left_handler");
	DISP_FLAG_T *flag = get_disp_flag();
	flag->flush_disp_flag = 1;
	bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
	if(--hUserInterface->iPresetCurrentIndex == 0)
	{
		DISP_FLAG_T *flag = get_disp_flag();
	  flag->sel_draw_flag_1 = 1;
		hUserInterface->iPresetCurrentIndex = 1;
		goto_default_presets_8_disp();
	}
	
}
/**
* @brieaf edit key
*/
static void preset_setup_8_edit_handler(void *msg)
{
	DISP_FLAG_T *flag = get_disp_flag();
	flag->sel_draw_flag_1 = 1;
	goto_preset_edit_13_disp();
	NRF_LOG_INFO("preset_setup_8_edit_handler");
}

/**
* @brieaf preset_setup page display
*/
static void preset_setup_8_updateDisplay(void *p)
{
	NRF_LOG_INFO("=========preset_setup_8_updateDisplay============");
	DISP_FLAG_T *flag = get_disp_flag();
	bru_LCD_Preset_1_to_3_Menu_ENG(&flag->sel_draw_flag_1,(const char*)"BACK");

}



/**
* @brieaf jump to preset_setup page function
*/
void goto_preset_setup_8_disp(void)
{
	bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
	hUserInterface->bMenuBackFlag = 1;
	jump_ui_func(&op_preset_setup_8_table);
	
}




//######################################################################################################################################
//============ hot water dispenser  ============

static void hot_water_dispenser_8_updateDisplay(void *p);



static void hot_water_dispenser_8_right_handler(void *msg);
static void hot_water_dispenser_8_left_handler(void *msg);
static void hot_water_dispenser_8_edit_handler(void *msg);

/*-----hot water dispenser Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_hot_water_dispenser_8_FuncEn)
		ON_VOID_POINTER_MESSAGE(button1_release_evt,preset_setup_8_back_handler)
		ON_VOID_POINTER_MESSAGE(button2_release_evt,hot_water_dispenser_8_edit_handler)
		ON_VOID_POINTER_MESSAGE(encoder_a_release_evt,hot_water_dispenser_8_right_handler)
		ON_VOID_POINTER_MESSAGE(encoder_b_release_evt,hot_water_dispenser_8_left_handler)
		ON_VOID_POINTER_MESSAGE(encoder_button_release_evt,hot_water_dispenser_8_edit_handler)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_hot_water_dispenser_8_FuncEn,Menu_Function_hot_water_dispenser_8_cmd_func,preMessageHandler)   


CONST MENU_KEY op_hot_water_dispenser_8_table= 
	{&Menu_Function_hot_water_dispenser_8_cmd_func,hot_water_dispenser_8_updateDisplay,NULL,NULL};   		

	



/**
* @brieaf right key
*/
static void hot_water_dispenser_8_right_handler(void *msg)
{
	bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
	hUserInterface->iPresetCurrentIndex = 5;
	goto_alarm_8_disp();
	NRF_LOG_INFO("hot_water_dispenser_8_right_handler");
}

/**
* @brieaf left key
*/
static void hot_water_dispenser_8_left_handler(void *msg)
{
	bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
	hUserInterface->iPresetCurrentIndex = 3;
	goto_preset_setup_8_disp();
	NRF_LOG_INFO("hot_water_dispenser_8_left_handler");
}
/**
* @brieaf edit key
*/
static void hot_water_dispenser_8_edit_handler(void *msg)
{
	DISP_FLAG_T *flag = get_disp_flag();
	flag->sel_draw_flag_1 = 1;
	goto_hot_water_edit_14_disp();
	NRF_LOG_INFO("hot_water_dispenser_8_edit_handler");
}

/**
* @brieaf hot water dispenser page display
*/
static void hot_water_dispenser_8_updateDisplay(void *p)
{
	NRF_LOG_INFO("=========preset_3_setup_8_updateDisplay============");
	DISP_FLAG_T *flag = get_disp_flag();
	bru_LCD_Preset_HotWater_Menu_ENG(&flag->sel_draw_flag_1,(const char*)"BACK");
}



/**
* @brieaf jump to hot water dispenser page function
*/
void goto_hot_water_dispenser_8_disp(void)
{
	jump_ui_func(&op_hot_water_dispenser_8_table);
	
}



//######################################################################################################################################
//============ alarm  ============

static void alarm_8_updateDisplay(void *p);



static void alarm_8_right_handler(void *msg);
static void alarm_8_left_handler(void *msg);
static void alarm_8_edit_handler(void *msg);

/*-----alarm Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_alarm_8_FuncEn)
		ON_VOID_POINTER_MESSAGE(button1_release_evt,preset_setup_8_back_handler)
		ON_VOID_POINTER_MESSAGE(button2_release_evt,alarm_8_edit_handler)
		ON_VOID_POINTER_MESSAGE(encoder_a_release_evt,alarm_8_right_handler)
		ON_VOID_POINTER_MESSAGE(encoder_b_release_evt,alarm_8_left_handler)
		ON_VOID_POINTER_MESSAGE(encoder_button_release_evt,alarm_8_edit_handler)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_alarm_8_FuncEn,Menu_Function_alarm_8_cmd_func,preMessageHandler)   


CONST MENU_KEY op_alarm_8_table= 
	{&Menu_Function_alarm_8_cmd_func,alarm_8_updateDisplay,NULL,NULL};   	

	


/**
* @brieaf right key
*/
static void alarm_8_right_handler(void *msg)
{
	DISP_FLAG_T *flag = get_disp_flag();
	flag->sel_draw_flag_1 = 1;
	bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
	hUserInterface->iPresetCurrentIndex = 6;
	goto_default_presets_8_disp();
	NRF_LOG_INFO("alarm_8_right_handler");
}

/**
* @brieaf left key
*/
static void alarm_8_left_handler(void *msg)
{
	bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
	hUserInterface->iPresetCurrentIndex = 4;
	goto_hot_water_dispenser_8_disp();
	NRF_LOG_INFO("alarm_8_left_handler");
}
/**
* @brieaf edit key
*/
static void alarm_8_edit_handler(void *msg)
{
	DISP_FLAG_T *p = get_disp_flag();
	p->sel_draw_flag_1 = 1;
	goto_current_time_edit_15_disp();
	NRF_LOG_INFO("alarm_8_edit_handler");
}

/**
* @brieaf  alarm page display
*/
static void alarm_8_updateDisplay(void *p)
{
	NRF_LOG_INFO("=========alarm_8_updateDisplay============");
		DISP_FLAG_T *flag = get_disp_flag();
	bru_LCD_Preset_Alarm_Menu_ENG(&flag->sel_draw_flag_1,(const char*)"BACK");
}



/**
* @brieaf jump to alarm page function
*/
void goto_alarm_8_disp(void)
{
	jump_ui_func(&op_alarm_8_table);
	
}


//######################################################################################################################################
//============ default_presets  ============

static void default_presets_8_updateDisplay(void *p);


static void default_presets_8_back_handler(void *msg);
static void default_presets_8_right_handler(void *msg);
static void default_presets_8_left_handler(void *msg);
static void default_presets_8_edit_handler(void *msg);

/*-----default_presets Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_default_presets_8_FuncEn)
		ON_VOID_POINTER_MESSAGE(button1_release_evt,default_presets_8_back_handler)
		ON_VOID_POINTER_MESSAGE(button2_release_evt,default_presets_8_edit_handler)
		ON_VOID_POINTER_MESSAGE(encoder_a_release_evt,default_presets_8_right_handler)
		ON_VOID_POINTER_MESSAGE(encoder_b_release_evt,default_presets_8_left_handler)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_default_presets_8_FuncEn,Menu_Function_default_presets_8_cmd_func,preMessageHandler)   


CONST MENU_KEY op_default_presets_8_table= 
	{&Menu_Function_default_presets_8_cmd_func,default_presets_8_updateDisplay,NULL,NULL};   	

	


/**
* @brieaf default_presets 8 back key
*/
static void default_presets_8_back_handler(void *msg)
{
	NRF_LOG_INFO("default_presets_8_back_handler");
}

/**
* @brieaf right key
*/
static void default_presets_8_right_handler(void *msg)
{
	DISP_FLAG_T *flag = get_disp_flag();
	flag->sel_draw_flag_1 = 1;
	bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
	hUserInterface->iPresetCurrentIndex = 1;
	goto_preset_setup_8_disp();
	NRF_LOG_INFO("default_presets_8_right_handler");
}

/**
* @brieaf left key
*/
static void default_presets_8_left_handler(void *msg)
{
	DISP_FLAG_T *flag = get_disp_flag();
	flag->sel_draw_flag_1 = 1;
	bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
	hUserInterface->iPresetCurrentIndex = 6;
	goto_alarm_8_disp();
	NRF_LOG_INFO("default_presets_8_left_handler");
}
/**
* @brieaf ok key
*/
static void default_presets_8_edit_handler(void *msg)
{
	DISP_FLAG_T *flag = get_disp_flag();
	flag->sel_draw_flag_1 = 1;
	goto_home_disp();
	NRF_LOG_INFO("default_presets_8_ok_handler");
}

/**
* @brieaf default_presets page display
*/
static void default_presets_8_updateDisplay(void *p)
{
	NRF_LOG_INFO("=========default_presets_8_updateDisplay============");
	DISP_FLAG_T *flag = get_disp_flag();
  bru_LCD_Preset_default_presets_Menu_ENG(&flag->sel_draw_flag_1);
}



/**
* @brieaf jump to default_presets page function
*/
static void goto_default_presets_8_disp(void)
{
	jump_ui_func(&op_default_presets_8_table);
	
}






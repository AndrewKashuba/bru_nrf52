#ifndef __PRESET_SETUP_MENU_8_H
#define __PRESET_SETUP_MENU_8_H
#include "target_command.h"

#ifdef __cplusplus
extern "C" {
#endif

void goto_preset_setup_8_disp(void);
void goto_hot_water_dispenser_8_disp(void);
void goto_alarm_8_disp(void);
#ifdef __cplusplus
}
#endif

#endif


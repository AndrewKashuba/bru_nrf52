#include "BSP_menu_info.h"
#include "bru_Menu_Check.h"
#include "dbprintf.h"

#define NRF_LOG_MODULE_NAME pre_brewing
#define NRF_LOG_LEVEL       3
#define NRF_LOG_INFO_COLOR  4
#define NRF_LOG_DEBUG_COLOR 3
#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();



__WEAK void steeping_check_cup_placed(void)
{
	NRF_LOG_INFO("call __WEAK void steeping_check_cup_placed() func");
}

//######################################################################################################################################
//============pre add water =========================
static void pre_brewing_updateDisplay(void *p);




/*-----pre add water Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_pre_brewing_FuncEn)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_pre_brewing_FuncEn,Menu_Function_pre_brewing_cmd_func,preMessageHandler)   


CONST MENU_KEY op_pre_brewing_table= 
	{&Menu_Function_pre_brewing_cmd_func,pre_brewing_updateDisplay,NULL,NULL};   		

	


/**
* @brieaf pre add water page display
*/
static void pre_brewing_updateDisplay(void *p)
{
	NRF_LOG_INFO("=========pre_brewing_updateDisplay============");
//	bru_LCD_Add_Water_ENG((const char*)"",(const char*)""); // old velsion
	bru_LCD_Update_Screen(UI_WINDOW_ERROR_MSG_ADD_WATER, 0);
}

/**
* @brieaf jump to pre add water page function
*/
void goto_pre_add_water_disp(void)
{
	jump_ui_func(&op_pre_brewing_table);
	
}




//######################################################################################################################################
//============pre place chamber =========================
static void pre_place_chamber_updateDisplay(void *p);


/*-----pre place chamber Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_pre_place_chamber_FuncEn)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_pre_place_chamber_FuncEn,Menu_Function_pre_place_chamber_cmd_func,preMessageHandler)   


CONST MENU_KEY op_pre_place_chamber_table= 
	{&Menu_Function_pre_place_chamber_cmd_func,pre_place_chamber_updateDisplay,NULL,NULL};   	

	

/**
*   @brieaf pre place chamber page display
*/
static void pre_place_chamber_updateDisplay(void *p)
{
	NRF_LOG_INFO("=========pre_place_chamber_updateDisplay============");
	//ak bru_LCD_Place_Steeping_chamber_ENG("","");
}



/**
* @brieaf jump to pre place chamber page function
*/
void goto_pre_place_chamber_disp(void)
{
	jump_ui_func(&op_pre_place_chamber_table);
	
}



//######################################################################################################################################
//============pre place cup =========================
static void pre_place_cup_updateDisplay(void *p);


static void pre_place_cup_back_handler(void *msg);

/*-----pre place cup Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_pre_place_cup_FuncEn)
		ON_VOID_POINTER_MESSAGE(button1_release_evt,pre_place_cup_back_handler)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_pre_place_cup_FuncEn,Menu_Function_pre_place_cup_cmd_func,preMessageHandler)   


CONST MENU_KEY op_pre_place_cup_table= 
	{&Menu_Function_pre_place_cup_cmd_func,pre_place_cup_updateDisplay,NULL,NULL};   	

	


/**
* @brieaf pre place cup back key
*/
static void pre_place_cup_back_handler(void *msg)
{
	goto_home_disp();
	NRF_LOG_INFO("pre_place_cup_back_handler");
}


/**
* @brieaf pre place cup page display
*/
static void pre_place_cup_updateDisplay(void *p)
{
	NRF_LOG_INFO("=========pre_place_cup_updateDisplay============");
//	bru_LCD_Place_Cup_ENG("BACK","");
}



/**
* @brieaf jump to pre place cup page function
*/
void goto_pre_place_cup_disp(void)
{
	jump_ui_func(&op_pre_place_cup_table);
	
}







//######################################################################################################################################
//============close lid=========================
static void pre_close_lid_updateDisplay(void *p);



/*-----close lid Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_pre_close_lid_FuncEn)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_pre_close_lid_FuncEn,Menu_Function_pre_close_lid_cmd_func,preMessageHandler)   


CONST MENU_KEY op_pre_close_lid_table= 
	{&Menu_Function_pre_close_lid_cmd_func,pre_close_lid_updateDisplay,NULL,NULL};   		

	


/**
* @brieaf close lid page display
*/
static void pre_close_lid_updateDisplay(void *p)
{
	NRF_LOG_INFO("=========pre_close_lid_updateDisplay============");
	bru_LCD_Update_Screen(UI_WINDOW_ERROR_MSG_CLOSE_LID, 0);
}

/**
* @brieaf jump to close lid page display
*/
void goto_pre_close_lid_disp(void)
{
	jump_ui_func(&op_pre_close_lid_table);
}

//######################################################################################################################################
//============pre error code ========================
static void pre_error_code_updateDisplay(void *p);



/*-----pre error code Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_pre_error_code_FuncEn)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_pre_error_code_FuncEn,Menu_Function_pre_error_code_cmd_func,preMessageHandler)   


CONST MENU_KEY op_pre_error_code_table= 
	{&Menu_Function_pre_error_code_cmd_func,pre_error_code_updateDisplay,NULL,NULL};   		

	




/**
* @brieaf pre error code page display
*/
static void pre_error_code_updateDisplay(void *p)
{
//	bru_LCD_Error_XXX_ENG("","");
//	NRF_LOG_INFO("=========pre_error_code_updateDisplay============");
}



/**
* @brieaf jump to pre error code page function
*/
void goto_pre_error_code_disp(void)
{
	jump_ui_func(&op_pre_error_code_table);
	
}



//######################################################################################################################################
static void filling_up_updateDisplay(void *p);

static void filling_up_stop_handler(void *msg);

/*-----filling_up Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_filling_up_FuncEn)
		ON_VOID_POINTER_MESSAGE(button1_release_evt,filling_up_stop_handler)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_filling_up_FuncEn,Menu_Function_filling_up_cmd_func,preMessageHandler)   


CONST MENU_KEY op_filling_up_table= 
	{&Menu_Function_filling_up_cmd_func,filling_up_updateDisplay,NULL,NULL};   		

	



	
/**
* @brieaf filling_up stop key
*/
static void filling_up_stop_handler(void *msg)
{
	bru_Brewing_Process_Stop();
	steeping_check_cup_placed();
	NRF_LOG_INFO("filling_up_stop_handler");
}



/**
* @brieaf filling_up page display
*/
static void filling_up_updateDisplay(void *p)
{
	NRF_LOG_INFO("=========filling_up_updateDisplay============");
	//ak bru_LCD_FillingUp_ENG("STOP","");
}



/**
* @brieaf jump to filling_up page display
*/
void goto_filling_up_5_disp(void)
{
	jump_ui_func(&op_filling_up_table);
}






//######################################################################################################################################
//============= brewing add water =============================
static void brewing_place_cup_5_updateDisplay(void *p);



/*-----brewing add water Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_brewing_place_cup_5_FuncEn)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_brewing_place_cup_5_FuncEn,Menu_Function_brewing_place_cup_5_cmd_func,preMessageHandler)   


CONST MENU_KEY op_brewing_place_cup_5_table= 
	{&Menu_Function_brewing_place_cup_5_cmd_func,brewing_place_cup_5_updateDisplay,NULL,NULL};   	

	




/**
* @brieaf brewing add water page display
*/
static void brewing_place_cup_5_updateDisplay(void *p)
{
	NRF_LOG_INFO("=========brewing_place_cup_5_updateDisplay============");
	//bru_LCD_Place_Cup_ENG("","");
}



/**
* @brieaf jump to brewing add water page display
*/
void goto_brewing_place_cup_5_disp(void)
{
	jump_ui_func(&op_brewing_place_cup_5_table);
	
}




//######################################################################################################################################
//============= brewing steeping_cancel =============================
static void brewing_steeping_cancel_5_updateDisplay(void *p);



/*-----brewing steeping_cancel Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_brewing_steeping_cancel_5_FuncEn)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_brewing_steeping_cancel_5_FuncEn,Menu_Function_brewing_steeping_cancel_5_cmd_func,preMessageHandler)   

static uint8_t cancel_counter;

CONST MENU_KEY op_brewing_steeping_cancel_5_table= 
	{&Menu_Function_brewing_steeping_cancel_5_cmd_func,brewing_steeping_cancel_5_updateDisplay,NULL,NULL};   		

	


#define STEEPING_CANCEL_TIMER_ID 1254
#define STEEPING_CANCEL_TIMER_VALUE  (TIMER_CALL_HZ*1)

/**
* @brieaf brewing steeping_cancel page display
*/
static void brewing_steeping_cancel_5_updateDisplay(void *p)
{
//	NRF_LOG_INFO("=========brewing_steeping_cancel_5_updateDisplay============");
//	
//	DISP_FLAG_T *flag = get_disp_flag();
//  bru_LCD_Steeping_Cancel_ENG(&flag->sel_draw_flag_1,cancel_counter);
//	if(cancel_counter == 0)
//	{
//		bru_Cancel_Chamber_have_Water_Check_Over();		//add by wwang
//		delete_timer(STEEPING_CANCEL_TIMER_ID);
//		goto_home_disp();
//	}
}



static void steeping_cancel_timer_handler(void *p)
{
	UNUSED_PARAMETER(p);
	TIMER_INFO_T *timer = p;
	timer->timer_value = STEEPING_CANCEL_TIMER_VALUE;
	if(cancel_counter)
	{
		cancel_counter--;
	}
	DB_Printf("### STEP11: Steping %d\r\n", cancel_counter); 
	DISP_FLAG_T *flag = get_disp_flag();
	flag->flush_disp_flag = 1;
}

/**
* @brieaf jump to brewing steeping_cancel page function
*/
void goto_brewing_steeping_cancel_5_disp(void)
{
	DISP_FLAG_T *flag = get_disp_flag();
	flag->sel_draw_flag_1 = 1;
	cancel_counter = setCheckTimerValue();//3;			//add by wwang
	jump_ui_func(&op_brewing_steeping_cancel_5_table);
	register_timer(STEEPING_CANCEL_TIMER_ID,steeping_cancel_timer_handler);
	set_timer_value(STEEPING_CANCEL_TIMER_ID,STEEPING_CANCEL_TIMER_VALUE,NULL);
	
}

//add by wwang
uint16_t get_steeping_cancel_timer_id(void)
{
	return STEEPING_CANCEL_TIMER_ID;
}
//######################################################################################################################################
//============= brewing steeping =============================
static void brewing_steeping_5_updateDisplay(void *p);

static void brewing_steeping_5_stop_handler(void *msg);

/*-----brewing steeping Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_brewing_steeping_5_FuncEn)
		ON_VOID_POINTER_MESSAGE(button1_release_evt,brewing_steeping_5_stop_handler)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_brewing_steeping_5_FuncEn,Menu_Function_brewing_steeping_5_cmd_func,preMessageHandler)   


CONST MENU_KEY op_brewing_steeping_5_table= 
	{&Menu_Function_brewing_steeping_5_cmd_func,brewing_steeping_5_updateDisplay,NULL,NULL};   	

	


#define STEEPING_TIMER_ID    682
#define STEEPING_TIMER_VALUE  (TIMER_CALL_HZ*1)
	
/**
* @brieaf brewing add water stop key
*/
static void brewing_steeping_5_stop_handler(void *msg)
{
////	TIMER_INFO_T *timer = find_timer(STEEPING_TIMER_ID);
////	timer->timer_value = 0;
//	bru_Menu_Check_Procedure(BRU_ERROR_CHECK_WHILE_CANCELING_BREWING);
//	delete_timer(STEEPING_TIMER_ID);
//	steeping_check_cup_placed();
//	NRF_LOG_INFO("brewing_steeping_5_stop_handler");
}


static void steeping_time_coundown_handler(void *p)
{
//	TIMER_INFO_T *pTimer = p;
//	uint16_t *counter = pTimer->pData;
//	pTimer->timer_value = STEEPING_TIMER_VALUE;
//	DISP_FLAG_T *flag = get_disp_flag();
//	flag->flush_disp_flag = 1;
//	if(*counter)
//	{
//		(*counter)--;
//	}
//	NRF_LOG_INFO("steeping_time_coundown_handler %d",*counter);
}

/**
* @brieaf brewing add water page display
*/
static void brewing_steeping_5_updateDisplay(void *p)
{
//	NRF_LOG_INFO("=========brewing_steeping_5_updateDisplay============");
//	DISP_FLAG_T *flag = get_disp_flag();
//	bru_LCD_Steeping(&flag->sel_draw_flag_1);
//	bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
//	DB_Printf("### STEP10: Steping %d\r\n", hUserInterface->iCountdownBrewingTime); 
//	if(hUserInterface->iCountdownBrewingTime == 0)
//	{
//		delete_timer(STEEPING_TIMER_ID);
//		DB_Printf("### STEP12: Steping end\r\n"); 
//		bru_Menu_Check_Procedure(BRU_ERROR_CHECK_WHILE_PRE_SERVING);
//	}
}



/**
* @brieaf jump to brewing add water page function
*/
void goto_brewing_steeping_5_disp(void)
{
	DISP_FLAG_T *flag = get_disp_flag();
	flag->sel_draw_flag_1 = 1;
	jump_ui_func(&op_brewing_steeping_5_table);
	bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
	hUserInterface->iCountdownBrewingTime = hUserInterface->presetsArr[hUserInterface->iPresetCurrentIndex].brewingTime;
	register_timer(STEEPING_TIMER_ID,steeping_time_coundown_handler);
  set_timer_value(STEEPING_TIMER_ID,STEEPING_TIMER_VALUE,&hUserInterface->iCountdownBrewingTime);
	
}





//######################################################################################################################################
//============= brewing_serving =============================
static void brewing_serving_5_updateDisplay(void *p);

static void brewing_serving_5_stop_handler(void *msg);

/*-----brewing_serving Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_brewing_serving_5_FuncEn)
		ON_VOID_POINTER_MESSAGE(button1_release_evt,brewing_serving_5_stop_handler)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_brewing_serving_5_FuncEn,Menu_Function_brewing_serving_5_cmd_func,preMessageHandler)   


CONST MENU_KEY op_brewing_serving_5_table= 
	{&Menu_Function_brewing_serving_5_cmd_func,brewing_serving_5_updateDisplay,NULL,NULL};   		

	



	
/**
* @brieaf brewing_serving stop key
*/
static void brewing_serving_5_stop_handler(void *msg)
{
//	goto_home_disp();																									//delete by wwang
	bru_Serving_Chamber_have_Water_Btn_Stop();	//add by wwang
	NRF_LOG_INFO("brewing_serving_5_stop_handler");
}



/**
* @brieaf brewing_serving page display
*/
static void brewing_serving_5_updateDisplay(void *p)
{
	//ak bru_LCD_Serving_ENG("STOP","");
	NRF_LOG_INFO("=========brewing_serving_5_updateDisplay============");
	
	//add by wwang
	bru_Menu_Check_Procedure(BRU_ERROR_CHECK_WHILE_SERVING_HAVE_WATER);
}



/**
* @brieaf jump to brewing_serving page function
*/
void goto_brewing_serving_5_disp(void)
{
	jump_ui_func(&op_brewing_serving_5_table);
	
}




//######################################################################################################################################
//============= brewing washing =============================
static void brewing_washing_5_updateDisplay(void *p);

static void brewing_washing_5_stop_handler(void *msg);

/*-----brewing washing Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_brewing_washing_5_FuncEn)
		ON_VOID_POINTER_MESSAGE(button1_release_evt,brewing_washing_5_stop_handler)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_brewing_washing_5_FuncEn,Menu_Function_brewing_washing_5_cmd_func,preMessageHandler)   


CONST MENU_KEY op_brewing_washing_5_table= 
	{&Menu_Function_brewing_washing_5_cmd_func,brewing_washing_5_updateDisplay,NULL,NULL};   		

	



	
/**
* @brieaf brewing washing stop key
*/
static void brewing_washing_5_stop_handler(void *msg)
{
	bru_Cancel_Washing_Evt();
	NRF_LOG_INFO("brewing_washing_5_stop_handler");
}



/**
*  @brieaf brewing washing page display
*/
static void brewing_washing_5_updateDisplay(void *p)
{
//	NRF_LOG_INFO("=========brewing_washing_5_updateDisplay============");
//	bru_LCD_Chamber_Washing_ENG("STOP","");
}



/**
* @brieaf jump to brewing washing page function
*/
void goto_brewing_washing_5_disp(void)
{
	jump_ui_func(&op_brewing_washing_5_table);
	
}





//######################################################################################################################################
//============= brewing_enjoy =============================
static void brewing_enjoy_5_updateDisplay(void *p);



/*-----brewing_enjoy Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_brewing_enjoy_5_FuncEn)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_brewing_enjoy_5_FuncEn,Menu_Function_brewing_enjoy_5_cmd_func,preMessageHandler)   


CONST MENU_KEY op_brewing_enjoy_5_table= 
	{&Menu_Function_brewing_enjoy_5_cmd_func,brewing_enjoy_5_updateDisplay,NULL,NULL};   		

	




/**
* @brieaf brewing_enjoy page display
*/
static void brewing_enjoy_5_updateDisplay(void *p)
{
	NRF_LOG_INFO("=========brewing_enjoy_5_updateDisplay============");
	//ak bru_LCD_EngoyBRU_ENG();
	
}

#define ENJOY_TIMER_ID    428
#define ENJOY_TIME_VALUE  (TIMER_CALL_HZ*1)

static void enjoy_timer_handler(void *p)
{
	TIMER_INFO_T *timer = p;
	uint8_t *cnt = timer->pData;
	if(*cnt < 3)
	{
		(*cnt)++;
		timer->timer_value = ENJOY_TIME_VALUE;
		
		if(*cnt == 1)
		{
			bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
			if(hUserInterface->hMachineSetup->buzzerState == MS_BUZZER_ON)
			{
				set_buzzer_duty(0);
			}
		}
	}
	else
	{
		timer->timer_value = 0;
		goto_home_disp();
	}
}

/**
* @brieaf jump to brewing_enjoy page function
*/
void goto_brewing_enjoy_5_disp(void)
{
	static uint8_t enjoy_counter;
	jump_ui_func(&op_brewing_enjoy_5_table);
	register_timer(ENJOY_TIMER_ID,enjoy_timer_handler);
	enjoy_counter = 0;
	set_timer_value(ENJOY_TIMER_ID,ENJOY_TIME_VALUE,(void*)&enjoy_counter);
	bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
	if(hUserInterface->hMachineSetup->buzzerState == MS_BUZZER_ON)
	{
		set_buzzer_duty(50);
	}
}



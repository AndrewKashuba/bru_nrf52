#ifndef __PRE_BREWING_MENU_5_H
#define __PRE_BREWING_MENU_5_H
#include "target_command.h"

#ifdef __cplusplus
extern "C" {
#endif

void goto_pre_add_water_disp(void);
void goto_pre_place_chamber_disp(void);
void goto_pre_place_cup_disp(void);
void goto_pre_close_lid_disp(void);
void goto_pre_error_code_disp(void);

void goto_filling_up_5_disp(void);
void goto_brewing_place_cup_5_disp(void);
void goto_brewing_steeping_cancel_5_disp(void);
void goto_brewing_steeping_5_disp(void);
void goto_brewing_serving_5_disp(void);
void goto_brewing_washing_5_disp(void);
void goto_brewing_enjoy_5_disp(void);

uint16_t get_steeping_cancel_timer_id(void);

#ifdef __cplusplus
}
#endif

#endif


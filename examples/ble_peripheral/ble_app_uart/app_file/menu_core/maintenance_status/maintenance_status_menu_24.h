#ifndef __MAINTENANCE_STATUS_MENU_24_H
#define __MAINTENANCE_STATUS_MENU_24_H
#include "target_command.h"

#ifdef __cplusplus
extern "C" {
#endif

void goto_maintenance_status_24_disp(void);

#ifdef __cplusplus
}
#endif

#endif


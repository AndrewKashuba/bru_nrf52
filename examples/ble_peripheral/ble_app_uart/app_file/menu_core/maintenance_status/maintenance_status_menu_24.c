#include "BSP_menu_info.h"



#define NRF_LOG_MODULE_NAME maintenance_status_24
#define NRF_LOG_LEVEL       3
#define NRF_LOG_INFO_COLOR  4
#define NRF_LOG_DEBUG_COLOR 3
#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();





//######################################################################################################################################
static void maintenance_24_updateDisplay(void *p);


static void maintenance_24_right_handler(void *msg);
static void maintenance_24_left_handler(void *msg);
static void maintenance_24_save_handler(void *msg);


/*-----maintenance Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_maintenance_24_FuncEn)
		ON_VOID_POINTER_MESSAGE(button2_release_evt,maintenance_24_save_handler)
		ON_VOID_POINTER_MESSAGE(encoder_a_release_evt,maintenance_24_right_handler)
		ON_VOID_POINTER_MESSAGE(encoder_b_release_evt,maintenance_24_left_handler)
		ON_VOID_POINTER_MESSAGE(encoder_button_release_evt,maintenance_24_save_handler)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_maintenance_24_FuncEn,Menu_Function_maintenance_24_cmd_func,preMessageHandler)   


CONST MENU_KEY op_maintenance_status_24_table = 
	{&Menu_Function_maintenance_24_cmd_func,maintenance_24_updateDisplay,NULL,NULL};   	

	



/**
* @brieaf right key
*/
static void maintenance_24_right_handler(void *msg)
{
	NRF_LOG_INFO("maintenance_24_right_handler");
}

/**
* @brieaf left key
*/
static void maintenance_24_left_handler(void *msg)
{
	NRF_LOG_INFO("maintenance_24_left_handler");
}
/**
* @brieaf save key
*/
static void maintenance_24_save_handler(void *msg)
{
	write_record_info_to_flash();
	get_machine_setup_info();
	NRF_LOG_INFO("maintenance_24_save_handler");
}

/**
* @brieaf maintenance page display
*/
static void maintenance_24_updateDisplay(void *p)
{
	NRF_LOG_INFO("=========maintenance_24_updateDisplay============");
}



/**
* @brieaf jump to maintenance page function
*/
void goto_maintenance_status_24_disp(void)
{
	jump_ui_func(&op_maintenance_status_24_table);
	
}














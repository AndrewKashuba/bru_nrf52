#ifndef __PRESET_CHOICE_MENU_3_H
#define __PRESET_CHOICE_MENU_3_H
#include "target_command.h"

#ifdef __cplusplus
extern "C" {
#endif

#define SYS_ALARM_TIMER_ID 1147
#define SYS_ALARM_START_TIMER_ID 1148		//add by wwang
#define SYS_ALARM_TIMER_VALUE    (TIMER_CALL_HZ*1)

void goto_preset_1_3_setup_disp(void);
void goto_hot_water_setup_disp(void);
void goto_alarm_disp(void);
void goto_default_presets_disp(void);
void sys_alarm_timer_handler(void *p);

void bru_alarm_start_brewing(void);			//add by wwang
void bru_alarm_cancel_brewing(void);		//add by wwang
#ifdef __cplusplus
}
#endif

#endif


#include "BSP_menu_info.h"
#include "bru_Context.h"
#include "CtrlUnits.h"


#define NRF_LOG_MODULE_NAME preset_setup
#define NRF_LOG_LEVEL       3
#define NRF_LOG_INFO_COLOR  4
#define NRF_LOG_DEBUG_COLOR 3
#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();

//######################################################################################################################################
//======================== preset 1~3 setup =============================
static void preset_setup_1_3_updateDisplay(void *p);
static void preset_1_3_right_handler(void *msg);
static void preset_1_3_start_handler(void *msg);
static void preset_1_3_edit_handler(void *msg);
static void preset_1_3_left_handler(void *msg);

/*-----preset 1 Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_preset_1_3_FuncEn)
    ON_VOID_POINTER_MESSAGE(encoder_b_release_evt,preset_1_3_left_handler)
		ON_VOID_POINTER_MESSAGE(encoder_a_release_evt,preset_1_3_right_handler)
		ON_VOID_POINTER_MESSAGE(button1_release_evt,preset_1_3_start_handler)
		ON_VOID_POINTER_MESSAGE(button2_release_evt,preset_1_3_edit_handler)
		ON_VOID_POINTER_MESSAGE(encoder_button_long_push_evt,preset_1_3_start_handler)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_preset_1_3_FuncEn,Menu_Function_preset_1_3_cmd_func,preMessageHandler)   


CONST MENU_KEY op_preset_1_3_setup_table= 
	{&Menu_Function_preset_1_3_cmd_func,preset_setup_1_3_updateDisplay,NULL,NULL};   		


/**
* @brieaf preset setup encode left
*/
static void preset_1_3_left_handler(void *msg)
{
	DISP_FLAG_T *flag = get_disp_flag();
	flag->flush_disp_flag = 1;
	bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
	if(--hUserInterface->iPresetCurrentIndex == 0)
	{
		goto_home_disp();
	}
	
}	
	

/**
* @brieaf preset setup encode right
*/
static void preset_1_3_right_handler(void *msg)
{
	DISP_FLAG_T *flag = get_disp_flag();
	flag->flush_disp_flag = 1; 
	bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
	if(++hUserInterface->iPresetCurrentIndex == 4)
	{
		goto_hot_water_setup_disp();
	}

	NRF_LOG_INFO("preset_1_3_right_handler");
}

/**
* @brieaf preset setup start key
*/
static void preset_1_3_start_handler(void *msg)
{
////	goto_pre_add_water_disp();																//delete by WWang
//	bru_Menu_Check_Procedure(BRU_ERROR_CHECK_WHILE_PRE_BREWING); //add by WWang
//	NRF_LOG_INFO("preset_1_3_start_handler");

	extern void bru_Context_Task_Intro(bru_Tasks_List, bru_UI_Window_ID);
	bru_Context_Task_Intro(T_PreBrewing_Water, UI_WINDOW_PRESET_STATUS_MENU);	
}

/**
* @brieaf preset setup edit key
*/
static void preset_1_3_edit_handler(void *msg)
{
	DISP_FLAG_T *flag = get_disp_flag();
	flag->sel_draw_flag_1 = 1;
	goto_preset_edit_13_disp();
	NRF_LOG_INFO("preset_1_edit_handler");
}


/*
* @brieaf preset 1 display
*/
static void preset_setup_1_3_updateDisplay(void *p)
{
	NRF_LOG_INFO("=========preset_setup_1_updateDisplay============");
	DISP_FLAG_T *flag = get_disp_flag();
	bru_LCD_Preset_1_to_3_Menu_ENG(&flag->sel_draw_flag_1,(const char*)"START");
}



/**
* @brieaf jump to preset 1_3 page function
*/
void goto_preset_1_3_setup_disp(void)
{
	bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
	hUserInterface->bMenuBackFlag = 0;
	jump_ui_func(&op_preset_1_3_setup_table);
	
}





//######################################################################################################################################
//=========== hot water ==========================
static void hot_water_updateDisplay(void *p);
static void hot_water_right_handler(void *msg);
static void hot_water_start_handler(void *msg);
static void hot_water_edit_handler(void *msg);
static void hot_water_left_handler(void *msg);


/*-----hot water Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_hot_water_FuncEn)
    ON_VOID_POINTER_MESSAGE(encoder_b_release_evt,hot_water_left_handler)
		ON_VOID_POINTER_MESSAGE(encoder_a_release_evt,hot_water_right_handler)
		ON_VOID_POINTER_MESSAGE(button1_release_evt,hot_water_start_handler)
		ON_VOID_POINTER_MESSAGE(button2_release_evt,hot_water_edit_handler)
		ON_VOID_POINTER_MESSAGE(encoder_button_long_push_evt,hot_water_start_handler)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_hot_water_FuncEn,Menu_Function_hot_water_cmd_func,preMessageHandler)   


CONST MENU_KEY op_hot_water_setup_table= 
	{&Menu_Function_hot_water_cmd_func,hot_water_updateDisplay,NULL,NULL};   	



/**
* @brieaf hot water encode right
*/
static void hot_water_right_handler(void *msg)
{
	bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
	hUserInterface->iPresetCurrentIndex = 5;
	goto_alarm_disp();
	NRF_LOG_INFO("hot_water_right_handler");
}
/**
* @brieaf hot water encode left
*/
static void hot_water_left_handler(void *msg)
{
	bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
	hUserInterface->iPresetCurrentIndex = 3;
	goto_preset_1_3_setup_disp();
	NRF_LOG_INFO("hot_water_left_handler");
}

/**
* @brieaf hot water start key
*/
static void hot_water_start_handler(void *msg)
{
//	goto_pre_add_water_disp();																	//delete by WWang
//	bru_Menu_Check_Procedure(BRU_ERROR_CHECK_WHILE_PRE_BREWING); //add by WWang
//	NRF_LOG_INFO("hot_water_start_handler");

//	extern bru_Sensor_Status Sensors;
	extern void bru_Context_Task_Intro(bru_Tasks_List, bru_UI_Window_ID);
	bru_Context_Task_Intro(T_Dispenser_Water, UI_WINDOW_DISPENSER_STATUS_MENU);
}

/**
* @brieaf hot water edit key
*/
static void hot_water_edit_handler(void *msg)
{
	DISP_FLAG_T *flag = get_disp_flag();
	flag->sel_draw_flag_1 = 1;
	goto_hot_water_edit_14_disp();
	NRF_LOG_INFO("hot_water_edit_handler");
}


/**
* @brieaf hot water page display
*/
static void hot_water_updateDisplay(void *p)
{
	NRF_LOG_INFO("=========hot_water_updateDisplay============");
		DISP_FLAG_T *flag = get_disp_flag();
	  bru_LCD_Preset_HotWater_Menu_ENG(&flag->sel_draw_flag_1,(const char*)"START");
}



/**
* @brieaf jump to hot water page function
*/
void goto_hot_water_setup_disp(void)
{
	jump_ui_func(&op_hot_water_setup_table);
	
}


//######################################################################################################################################
//=========== alarm ==========================
static void alarm_updateDisplay(void *p);
static void alarm_right_handler(void *msg);
static void alarm_start_handler(void *msg);
static void alarm_edit_handler(void *msg);
static void alarm_left_handler(void *msg);

/*-----alarm Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_alarm_FuncEn)
    ON_VOID_POINTER_MESSAGE(encoder_b_release_evt,alarm_left_handler)
		ON_VOID_POINTER_MESSAGE(encoder_a_release_evt,alarm_right_handler)
		ON_VOID_POINTER_MESSAGE(button1_release_evt,alarm_start_handler)
		ON_VOID_POINTER_MESSAGE(button2_release_evt,alarm_edit_handler)
		ON_VOID_POINTER_MESSAGE(encoder_button_long_push_evt,alarm_start_handler)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_alarm_FuncEn,Menu_Function_alarm_cmd_func,preMessageHandler)   


CONST MENU_KEY op_alarm_table= 
	{&Menu_Function_alarm_cmd_func,alarm_updateDisplay,NULL,NULL};   	



/**
* @brieaf alarm encode right
*/
static void alarm_right_handler(void *msg)
{
	bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
	hUserInterface->iPresetCurrentIndex = 6;
	DISP_FLAG_T *flag = get_disp_flag();
	flag->sel_draw_flag_1 = 1;
	goto_default_presets_disp();
	NRF_LOG_INFO("alarm_right_handler");
}

/**
* @brieaf alarm encode left
*/
static void alarm_left_handler(void *msg)
{
	bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
	hUserInterface->iPresetCurrentIndex = 4;
  goto_hot_water_setup_disp();
}

/**
* @brieaf alarm_start_timer
*/
//add by WWang
static void alarm_start_timer(void *msg)
{
	bru_UI_Interface_Handle hUserInterface = get_ui_interface();
	unix_cal timeStructure = {0};
	unix_cal alarmStructure = {0};

	timer_to_cal(hUserInterface->iCurrentTime, &timeStructure);
  timer_to_cal(hUserInterface->iAlarmTime, &alarmStructure);
	
	if((timeStructure.hour != alarmStructure.hour) || (timeStructure.min != alarmStructure.min))
	{
		set_timer_value(SYS_ALARM_START_TIMER_ID, TIMER_CALL_HZ, NULL);
		return;
	}
	if(hUserInterface->bBrewingProcessFlag) return; //���ڼ���
	bru_Menu_Check_Procedure(BRU_ERROR_CHECK_WHILE_PRE_BREWING);	
	NRF_LOG_INFO("alarm_start_timer ***********");
}

/**
* @brieaf bru_alarm_start_brewing
*/
//add by WWang
void bru_alarm_start_brewing(void)
{
	register_timer(SYS_ALARM_START_TIMER_ID,alarm_start_timer);
	set_timer_value(SYS_ALARM_START_TIMER_ID, TIMER_CALL_HZ, NULL);
}

/**
* @brieaf bru_alarm_cancel_brewing
*/
//add by WWang
void bru_alarm_cancel_brewing(void)
{
	set_timer_value(SYS_ALARM_START_TIMER_ID, 0, NULL);
}	

/**
* @brieaf alarm start key
*/
static void alarm_start_handler(void *msg)
{
//	goto_pre_add_water_disp();																	//delete by WWang
	
//	uint32_t time;
	bru_UI_Interface_Handle hUserInterface = get_ui_interface();
	 
	//add by WWang
	bru_alarm_start_brewing();
	NRF_LOG_INFO("alarm_start_handler");
}

/**
* @brieaf alarm edit key
*/
static void alarm_edit_handler(void *msg)
{
	DISP_FLAG_T *p = get_disp_flag();
	p->sel_draw_flag_1 = 1;
	goto_current_time_edit_15_disp();
	NRF_LOG_INFO("alarm_edit_handler");
}




/**
* @brieaf flush current time every 60 seconds
*/
void sys_alarm_timer_handler(void *p)
{
	TIMER_INFO_T *timer = p;
	if(get_current_disp() != timer->pData)
		return;
	timer->timer_value = SYS_ALARM_TIMER_VALUE;
	bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
	if((hUserInterface->iCurrentTime % 60) == 0)
	{
			DISP_FLAG_T *flag = get_disp_flag();
		  flag->flush_disp_flag = 1;
		 	NRF_LOG_INFO("sys_alarm_timer_handler");
	}

}

/**
* @brieaf alarm page display
*/
static void alarm_updateDisplay(void *p)
{
	NRF_LOG_INFO("=========alarm_updateDisplay============");
	DISP_FLAG_T *flag = get_disp_flag();
	bru_LCD_Preset_Alarm_Menu_ENG(&flag->sel_draw_flag_1,(const char*)"START");
}



/**
* @brieaf jump to alarm page function
*/
void goto_alarm_disp(void)
{
	jump_ui_func(&op_alarm_table);
	register_timer(SYS_ALARM_TIMER_ID,sys_alarm_timer_handler);
	set_timer_value(SYS_ALARM_TIMER_ID,SYS_ALARM_TIMER_VALUE,(void*)&op_alarm_table);
}




//######################################################################################################################################
//=========== default presets ==========================
static void default_presets_updateDisplay(void *p);
static void default_presets_right_handler(void *msg);
static void default_presets_set_handler(void *msg);
static void default_presets_left_handler(void *msg);

/*-----default presets Message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_default_presets_FuncEn)
    ON_VOID_POINTER_MESSAGE(encoder_b_release_evt,default_presets_left_handler)
		ON_VOID_POINTER_MESSAGE(encoder_a_release_evt,default_presets_right_handler)
		ON_VOID_POINTER_MESSAGE(button2_release_evt,default_presets_set_handler)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_default_presets_FuncEn,Menu_Function_default_presets_cmd_func,preMessageHandler)   


CONST MENU_KEY op_default_presets_table= 
	{&Menu_Function_default_presets_cmd_func,default_presets_updateDisplay,NULL,NULL};   	



/**
* @brieaf default presets encode right
*/
static void default_presets_right_handler(void *msg)
{
//	goto_home_disp();
//	NRF_LOG_INFO("default_presets_right_handler");
	
	//AK Gate
	extern void bru_Context_Task_Intro(bru_Tasks_List, bru_UI_Window_ID);
	bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
	hUserInterface->iPresetCurrentIndex = 0;
	hUserInterface->bMenuBackFlag = 0;
	hUserInterface->bMenuPresetFlag = 0;	
	bru_Context_Task_Intro(T_HomeScreen, UI_WINDOW_HOME_SCREEN);	
}

/**
* @brieaf default presets encode left
*/
static void default_presets_left_handler(void *msg)
{
	bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
	hUserInterface->iPresetCurrentIndex = 5;
	DISP_FLAG_T *flag = get_disp_flag();
	flag->sel_draw_flag_1 = 1;
  goto_alarm_disp();
	NRF_LOG_INFO("default_presets_left_handler");
}


/**
* @brieaf default presets key
*/
static void default_presets_set_handler(void *msg)
{
	goto_home_disp();
	NRF_LOG_INFO("default_presets_set_handler");
}




/**
* @brieaf default presets page display
*/
static void default_presets_updateDisplay(void *p)
{
	NRF_LOG_INFO("=========default_presets_updateDisplay============");
	DISP_FLAG_T *flag = get_disp_flag();
  bru_LCD_Preset_default_presets_Menu_ENG(&flag->sel_draw_flag_1);
}



/**
* @brieaf jump to default presets page function
*/
void goto_default_presets_disp(void)
{
	jump_ui_func(&op_default_presets_table);
	
}


/**
  *************************************************************************************************
  * @file    : BSP_ADC.c
  * @author  : xxx Team
  * @version : V0.0.1
  * @date    : 2017.10.10
  * @brief   : This is ADC driver file.These functions should name begin as BSP_ADC_xx().
  *************************************************************************************************
  */

/* Includes Files -------------------------------------------------------------------------------*/
#include "BSP_ADC.h"
#include "main.h"
#include "target_command.h"
#include "nrf_delay.h"

#define NRF_LOG_MODULE_NAME bsp_adc
#define NRF_LOG_LEVEL       3
#define NRF_LOG_INFO_COLOR  4
#define NRF_LOG_DEBUG_COLOR 3
#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();

/* Define Macros --------------------------------------------------------------------------------*/
                                 
/* Define Variables -----------------------------------------------------------------------------*/
#define ADC_DATA_LENGTH   2																
																
static nrf_saadc_value_t     adc_buffer_pool[2][ADC_SAMPLES_BUFFER];
			
static int16_t adc_result_buffer[_MULT_TOTAL+1];


static volatile uint8_t first_discard_adc_cnt;	
static volatile uint8_t converting_flag;
																
static Bru_Multiplexer_Select_Table_Type multiplex_mode;															
		

static void saadc_callback(nrf_drv_saadc_evt_t const *p_event);


static const uint8_t s_adc_data_evt[] = {
    MULT_NTC_PROTECTION,
    MULT_NTC_TOP,
		MULT_NTC_BOTTOM ,
    MULT_PINCH_VALVE_CURRENT,
    MULT_IR_SENSOR,
    MULT_LID_POSITION,
    MULT_BREWING_CHAMBER,
    _MULT_TOTAL				   
};

//================================ factory hardware test mode =========================================
/**
* @brieaf bottom adc value in factory mode
*/
__WEAK void factory_get_ntc_bottom(void *data)
{
	int16_t *p = data;
	NRF_LOG_INFO("factory_get_ntc_bottom %d",*p);
}

/**
* @brieaf top adc value in factory mode
*/
__WEAK void factory_get_ntc_top(void *data)
{
	int16_t *p = data;
  NRF_LOG_INFO("factory_get_ntc_top %d",*p);
}

/**
* @brieaf prot adc value in factory mode
*/
__WEAK void factory_get_ntc_prot(void *data)
{
	int16_t *p = data;
  NRF_LOG_INFO("factory_get_ntc_prot %d",*p);
}

/**
* @brieaf pinch current adc value in factory
*/
__WEAK void factory_get_pinch_current(void *data)
{
	int16_t *p = data;
  NRF_LOG_INFO("factory_get_pinch_current %d",*p);
}

/**
* @brieaf ir sensor adc value in factory
*/
__WEAK void factory_get_ir_sensor(void *data)
{
	int16_t *p = data;
  NRF_LOG_INFO("factory_get_ir_sensor %d",*p);
}

/**
* @brieaf lid position adc value in factory
*/
__WEAK void factory_get_lid_position(void *data)
{
	int16_t *p = data;
  NRF_LOG_INFO("factory_get_lid_position %d",*p);
}


/**
* @brieaf brewing chamber adc value in factory
*/
__WEAK void factory_get_brewing_chamber(void *data)
{
	int16_t *p = data;
  NRF_LOG_INFO("factory_get_brewing_chamber %d",*p);
}


BEGIN_MESSAGE_MAP(factory_adc_Function_ModeFuncEn)
    ON_VOID_POINTER_MESSAGE(s_adc_data_evt[0],factory_get_ntc_prot)				
    ON_VOID_POINTER_MESSAGE(s_adc_data_evt[1],factory_get_ntc_top)		
		ON_VOID_POINTER_MESSAGE(s_adc_data_evt[2],factory_get_ntc_bottom)			
    ON_VOID_POINTER_MESSAGE(s_adc_data_evt[3],factory_get_pinch_current)
    ON_VOID_POINTER_MESSAGE(s_adc_data_evt[4],factory_get_ir_sensor)
    ON_VOID_POINTER_MESSAGE(s_adc_data_evt[5],factory_get_lid_position)
    ON_VOID_POINTER_MESSAGE(s_adc_data_evt[6],factory_get_brewing_chamber)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */


INIT_MSG_ENTRIES(factory_adc_Function_ModeFuncEn,factory_adc_Function_cmd_func,NULL)  

CONST MENU_KEY factory_adc_table = {&factory_adc_Function_cmd_func,NULL,NULL};
												
//=======================================================================

/**
* @brieaf bottom adc value
*/
__WEAK void get_ntc_bottom(void *data)
{
	int16_t *p = data;
	NRF_LOG_INFO("get_ntc_bottom %d",*p);
}

/**
* @brieaf top adc value
*/
__WEAK void get_ntc_top(void *data)
{
	int16_t *p = data;
  NRF_LOG_INFO("get_ntc_top %d",*p);
}

/**
* @brieaf prot adc value
*/
__WEAK void get_ntc_prot(void *data)
{
	int16_t *p = data;
  NRF_LOG_INFO("get_ntc_prot %d",*p);
}

/**
* @brieaf pinch current adc value
*/
__WEAK void get_pinch_current(void *data)
{
	int16_t *p = data;
  NRF_LOG_INFO("get_pinch_current %d",*p);
}

/**
* @brieaf ir sensor adc value
*/
__WEAK void get_ir_sensor(void *data)
{
	int16_t *p = data;
  NRF_LOG_INFO("get_ir_sensor %d",*p);
}

/**
* @brieaf lid position adc value
*/
__WEAK void get_lid_position(void *data)
{
	int16_t *p = data;
  NRF_LOG_INFO("get_lid_postion %d",*p);
}

/**
* @brieaf brewing chamber adc value
*/
__WEAK void get_brewing_chamber(void *data)
{
	int16_t *p = data;
  NRF_LOG_INFO("get_brewing_chamber %d",*p);
}


BEGIN_MESSAGE_MAP(adc_Function_ModeFuncEn)
    ON_VOID_POINTER_MESSAGE(s_adc_data_evt[0],get_ntc_prot)
    ON_VOID_POINTER_MESSAGE(s_adc_data_evt[1],get_ntc_top)		
		ON_VOID_POINTER_MESSAGE(s_adc_data_evt[2],get_ntc_bottom)
    ON_VOID_POINTER_MESSAGE(s_adc_data_evt[3],get_pinch_current)
    ON_VOID_POINTER_MESSAGE(s_adc_data_evt[4],get_ir_sensor)
    ON_VOID_POINTER_MESSAGE(s_adc_data_evt[5],get_lid_position)
    ON_VOID_POINTER_MESSAGE(s_adc_data_evt[6],get_brewing_chamber)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */


INIT_MSG_ENTRIES(adc_Function_ModeFuncEn,adc_Function_cmd_func,NULL)  

CONST MENU_KEY adc_table = {&adc_Function_cmd_func,NULL,NULL};
												
CONST MENU_KEY *pADC_table = &adc_table;


/**
* @brieaf enter factory test adc mode 
*/
void enter_factory_test_adc_mode(void)
{
		pADC_table = &factory_adc_table;
}

/**
* @brieaf exit factory test adc mode 
*/
void exit_factory_test_adc_mode(void)
{
		pADC_table = &adc_table;
}

/**
* @brieaf control multiplex adc input 
*/
//static void multiplex_ctrl_pin(uint8_t s0,uint8_t s1,uint8_t s2)	//delete by wwang
static void multiplex_ctrl_pin(uint8_t s2,uint8_t s1,uint8_t s0) 		//add by wwang
{
	if(s0)
		nrf_gpio_pin_set(MULTIPLEX_S0_PIN);
	else
		nrf_gpio_pin_clear(MULTIPLEX_S0_PIN);
	
	if(s1)
		nrf_gpio_pin_set(MULTIPLEX_S1_PIN);
	else
		nrf_gpio_pin_clear(MULTIPLEX_S1_PIN);
	
	if(s2)
		nrf_gpio_pin_set(MULTIPLEX_S2_PIN);
	else
		nrf_gpio_pin_clear(MULTIPLEX_S2_PIN);
}
																
/**
* @brieaf start adc convertion
*/
void start_multiplex_messurement(Bru_Multiplexer_Select_Table_Type multiplex_pin)
{
	multiplex_mode = multiplex_pin;
	switch(multiplex_mode)
	{
		case MULT_NTC_PROTECTION:
			multiplex_ctrl_pin(0,0,0);
			break;
		case  MULT_NTC_TOP:
			multiplex_ctrl_pin(0,1,0);
			break;
		case MULT_NTC_BOTTOM:
			multiplex_ctrl_pin(0,0,1);
			break;
		case MULT_PINCH_VALVE_CURRENT:
			multiplex_ctrl_pin(0,1,1);
			break;           
    case MULT_IR_SENSOR:
			multiplex_ctrl_pin(1,0,0);
			break;
    case MULT_LID_POSITION:
			multiplex_ctrl_pin(1,1,0);
			break;
    case MULT_BREWING_CHAMBER:
			multiplex_ctrl_pin(1,0,1);
			break;
		default:
			multiplex_ctrl_pin(1,1,1);
			break;
	}
	converting_flag = false;
	for(uint8_t i=0; i<ADC_SAMPLES_BUFFER; i++)
	{
		nrfx_saadc_sample();
		nrf_delay_us(15);
	}
	uint32_t i = 5000;
	while(!converting_flag & i)
	{
		i--;
	}
	converting_flag = false;
}	
																
/* Define Functions -----------------------------------------------------------------------------*/

/**------------------------------------------------------------------------------------------------
  * @brief  : This is ADC sample function
  * @param  : None
  * @retval : None
  *----------------------------------------------------------------------------------------------*/
static void saadc_callback(nrf_drv_saadc_evt_t const *p_event)
{
         
    if(p_event->type == NRF_DRV_SAADC_EVT_DONE)
    {
      ret_code_t err_code;

			err_code = nrf_drv_saadc_buffer_convert(p_event->data.done.p_buffer, ADC_SAMPLES_BUFFER);
			APP_ERROR_CHECK(err_code);
												
			if(first_discard_adc_cnt)
							return;
			
			int32_t sum = 0;
			int32_t max = -4096;
			int32_t min = 4096;
			for(uint8_t i=0; i<ADC_SAMPLES_BUFFER; i++)
			{
							sum += p_event->data.done.p_buffer[i];
							if(max < p_event->data.done.p_buffer[i])
											max = p_event->data.done.p_buffer[i];
							if(min > p_event->data.done.p_buffer[i])
											min = p_event->data.done.p_buffer[i];
			}
			sum = sum - max - min;
			adc_result_buffer[multiplex_mode] = sum / (ADC_SAMPLES_BUFFER-2);
			
			PostMessage((void*)&s_adc_data_evt[multiplex_mode],(void*)pADC_table,&adc_result_buffer[multiplex_mode]);

			converting_flag = true;			
    }
}
																
/**------------------------------------------------------------------------------------------------
  * @brief  : This is ADC init function
  * @param  : None
  * @retval : None
  *----------------------------------------------------------------------------------------------*/

 void BSP_ADC_Init(void)
{	 
    ret_code_t err_code;
    err_code = nrf_drv_saadc_init(NULL, saadc_callback);
    APP_ERROR_CHECK(err_code);
       
    nrf_saadc_channel_config_t lowbatt_channel = NRF_DRV_SAADC_DEFAULT_CHANNEL_CONFIG_SE(MUTILPLEX_INPUT);
    lowbatt_channel.gain = NRF_SAADC_GAIN1_4;
	  lowbatt_channel.reference = NRF_SAADC_REFERENCE_VDD4;
		err_code = nrf_drv_saadc_channel_init(0, &lowbatt_channel);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_drv_saadc_buffer_convert(adc_buffer_pool[0], ADC_SAMPLES_BUFFER);
    APP_ERROR_CHECK(err_code);

     err_code = nrf_drv_saadc_buffer_convert(adc_buffer_pool[1], ADC_SAMPLES_BUFFER);
     APP_ERROR_CHECK(err_code);

     nrf_saadc_event_clear(NRF_SAADC_EVENT_DONE);
	
	  uint8_t list[MULTIPLEX_PIN_CTRL_LEN] = MULTIPLEX_PIN_CTRL_LIST;
	  for(uint8_t i = 0; i < MULTIPLEX_PIN_CTRL_LEN; ++i)
    {
			  nrf_gpio_cfg_output(list[i]);
        nrf_gpio_pin_set(list[i]);
    } 
		
		exit_factory_test_adc_mode();
	 
		first_discard_adc_cnt = 1;
	  for(uint8_t i=0; i<150; i++)
	  {			 
			nrfx_saadc_sample();
			nrf_delay_ms(5);
		}
	  first_discard_adc_cnt = 0;
}

/* End of Flie ----------------------------------------------------------------------------------*/

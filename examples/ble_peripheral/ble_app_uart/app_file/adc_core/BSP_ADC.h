/**
  *************************************************************************************************
  * @file    : BSP_ADC.h
  * @author  : xxx Team
  * @version : V0.0.1
  * @date    : 2017.10.10
  * @brief   : This is ADC driver file.These functions should name begin as BSP_ADC_xx().
  *************************************************************************************************
  */

/* Define to prevent recursive inclusion --------------------------------------------------------*/
#ifndef _BSP_ADC_H_
#define _BSP_ADC_H_

#ifdef __cplusplus
extern "C" {
#endif
#include "sdk_config.h"
#if SAADC_ENABLED
/* Includes Files -------------------------------------------------------------------------------*/

#include "nrf_drv_saadc.h"
#include "nrf_drv_timer.h"
#include "nrf_drv_ppi.h"
#include "nrf_drv_gpiote.h"


// <<< Use Configuration Wizard in Context Menu >>>\n

// <h> ADC channel config
// <o> ADC channel 
// <0=> NRF_SAADC_INPUT_DISABLED
// <1=> P02_AIN0
// <2=> P03_AIN1
// <3=> P04_AIN2
// <4=> P05_AIN3
// <5=> P28_AIN4
// <6=> P29_AIN5
// <7=> P30_AIN6
// <8=> P31_AIN7
// <9=> VDD_INPUT

#ifndef MUTILPLEX_INPUT 
#define MUTILPLEX_INPUT 4
#endif

// <o> MULTIPLEX_S0 CTRL PIN
// <0-31>
#ifndef MULTIPLEX_S0_PIN
#define MULTIPLEX_S0_PIN 8
#endif

// <o> MULTIPLEX_S1 CTRL PIN
// <0-31>
#ifndef MULTIPLEX_S1_PIN
#define MULTIPLEX_S1_PIN 9
#endif

// <o> MULTIPLEX_S2 CTRL PIN
// <0-31>
#ifndef MULTIPLEX_S2_PIN
#define MULTIPLEX_S2_PIN 10
#endif


// </h>

// <<< end of configuration section >>>

/* Exported Macros ------------------------------------------------------------------------------*/
#define ADC_SAMPLES_BUFFER   10
#define MULTIPLEX_PIN_CTRL_LIST  {MULTIPLEX_S0_PIN,MULTIPLEX_S1_PIN,MULTIPLEX_S2_PIN}
#define MULTIPLEX_PIN_CTRL_LEN   3


typedef enum
{
		MULT_NTC_PROTECTION               = 0,		//modify by wwang
    MULT_NTC_TOP                      = 1,
    MULT_NTC_BOTTOM               		= 2,
    MULT_PINCH_VALVE_CURRENT          = 3,
    MULT_IR_SENSOR                    = 4,
    MULT_LID_POSITION                 = 5,
    MULT_BREWING_CHAMBER              = 6,
    _MULT_TOTAL				                = 7
}Bru_Multiplexer_Select_Table_Type;


/* Exported Functions ---------------------------------------------------------------------------*/
void BSP_ADC_Init(void);
//void BSP_ADC_DeInit(void);
void start_multiplex_messurement(Bru_Multiplexer_Select_Table_Type multiplex_pin);
void enter_factory_test_adc_mode(void);
void exit_factory_test_adc_mode(void);


#endif

#ifdef __cplusplus
}
#endif

#endif
/* End of Flie ----------------------------------------------------------------------------------*/

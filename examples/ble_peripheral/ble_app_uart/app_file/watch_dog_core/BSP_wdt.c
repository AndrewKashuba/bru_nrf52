#include "BSP_wdt.h"
#include "nrf_drv_wdt.h"
#include "nrfx_wdt.h"

nrf_drv_wdt_channel_id m_channel_id;
static void wdt_event_handler(void)
{
}

void BSP_WDT_Init(void)
{
	#if WDT_ENABLED
	//Configure WDT.
	nrf_drv_wdt_config_t config = NRF_DRV_WDT_DEAFULT_CONFIG;
	uint32_t err_code = nrf_drv_wdt_init(&config, wdt_event_handler);
	APP_ERROR_CHECK(err_code);
	err_code = nrf_drv_wdt_channel_alloc(&m_channel_id);
	APP_ERROR_CHECK(err_code);
	nrf_drv_wdt_enable();
	#endif
}
void BSP_WDT_Feed(void)
{
	#if WDT_ENABLED
	nrf_drv_wdt_channel_feed(m_channel_id);
	#endif
}

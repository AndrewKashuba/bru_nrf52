#ifndef __BSP_WDT_H_
#define __BSP_WDT_H_

#ifdef __cplusplus
extern "C" {
#endif


void BSP_WDT_Init(void);
void BSP_WDT_Feed(void);

#ifdef __cplusplus
}
#endif

#endif

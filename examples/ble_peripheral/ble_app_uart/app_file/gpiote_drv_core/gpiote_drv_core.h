#ifndef __GPIOTE_DRV_CORE_H
#define __GPIOTE_DRV_CORE_H
#include "nrf_drv_gpiote.h"

#ifdef __cplusplus
extern "C" {
#endif


#define GPIOTE_DRV_CORE_ENABLED  1

#if GPIOTE_DRV_CORE_ENABLED && GPIOTE_ENABLED
void BSP_gpiote_init(nrfx_gpiote_pin_t pin,nrfx_gpiote_evt_handler_t pin_evt_handler,nrf_drv_gpiote_in_config_t *config);
void disable_gpiote_evt_handler(nrfx_gpiote_pin_t pin);
void gpiote_interrupt_switching(bool flag,nrfx_gpiote_pin_t pin);

#endif

#ifdef __cplusplus
           }
#endif


#endif

#include "gpiote_drv_core.h"


#define NRF_LOG_MODULE_NAME gpiote_drv_core
#define NRF_LOG_LEVEL       3
#define NRF_LOG_INFO_COLOR  4
#define NRF_LOG_DEBUG_COLOR 3
#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();


#if GPIOTE_DRV_CORE_ENABLED && GPIOTE_ENABLED




/**
* @brief 开启sense事件中断
*/
void BSP_gpiote_init(nrfx_gpiote_pin_t pin,nrfx_gpiote_evt_handler_t pin_evt_handler,nrf_drv_gpiote_in_config_t *config)
{
  

    NRF_LOG_INFO("charge_int_flag");
    if (!nrf_drv_gpiote_is_init())
    {      
         nrf_drv_gpiote_init();     
    }
    
    nrf_drv_gpiote_in_config_t in_config={          \
        .is_watcher = false,                        \
        .hi_accuracy = true,                     \
        .pull = NRF_GPIO_PIN_PULLUP,                \
        .sense = NRF_GPIOTE_POLARITY_HITOLO,        \
    };
		if(config == NULL)
		{
			config = &in_config;
		}
  
		nrf_drv_gpiote_in_init(pin, config, pin_evt_handler);

		nrf_drv_gpiote_in_event_enable(pin, true);
	  
	
}

/**
* @brief 关闭GPIOTE事件中断
*/
void disable_gpiote_evt_handler(nrfx_gpiote_pin_t pin)
{
	
    NRF_LOG_INFO("disable_charge_handler");
	
    nrf_drv_gpiote_in_event_disable(pin);
	  nrf_drv_gpiote_in_uninit(pin);
	  nrf_gpio_cfg_input(pin, NRF_GPIO_PIN_NOPULL);
	
	

}

/**
* @brieaf enable or disable gpio interrupt 
*/
void gpiote_interrupt_switching(bool flag,nrfx_gpiote_pin_t pin)
{
	if(flag)
		nrf_drv_gpiote_in_event_enable(pin, true);
	else
		nrf_drv_gpiote_in_event_disable(pin);
}


#endif

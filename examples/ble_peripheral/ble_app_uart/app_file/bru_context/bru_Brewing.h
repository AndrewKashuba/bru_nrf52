/*
 * 	bru_Brewing.h
 *	Brewing process
 *
 *  Created on: Dec 14, 2021
 *      Author: Andrew Kashuba
 */
 
#ifndef __BRU_BREWING_H__
#define __BRU_BREWING_H__
/***********************************************************************
*           Basic Header Files  
*************************************************************************/
//#include "stdint.h"
//#include "stdbool.h"


/***********************************************************************
*           Global Macro Define  
*************************************************************************/

/***********************************************************************
*           Global Type Define  
*************************************************************************/

/***********************************************************************
*           Global Variable Declarations   
*************************************************************************/

/***********************************************************************
*           Global Function Declarations   
*************************************************************************/

/**
 * @brief Water Sensor initialization.
 */
//extern void bru_Water_Sensor_Init(void);

/**
 * @brief Get current water level status of water tank.
 *
 * @return	1		Indicates water
 *					0		Means there is no water.
 */
//extern WaterSensorStatus bru_Water_Status(void);

/***********************************************************************
*          end
*************************************************************************/
#endif /* #ifndef __BRU_BREWING_H__ */

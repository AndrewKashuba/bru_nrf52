/*
 * 	bru_Brewing.c
 *	Brewing process
 *
 *  Created on: Dec 14, 2021
 *      Author: Andrew Kashuba
 */
 
/***********************************************************************
*           Include Files  
*************************************************************************/
//#include <stdint.h>
//#include <stdbool.h>
//#include "dbprintf.h"
//#include "event_queue_c.h"
//#include "FreeRTOS.h"
//#include "task.h"

//#include "BSP_menu_info.h"
#include "bru_Context.h"
//#include "bru_Tests.h"
//#include "bru_PinchValve.h"
//#include "bru_Pump.h"
//#include "bru_Water_Heating.h"
//#include "CtrlUnits.h"

/***********************************************************************
*           Macro Define  
*************************************************************************/

/***********************************************************************
*           Type Define  
*************************************************************************/

/***********************************************************************
*           Static Function Declarations  
*************************************************************************/

/***********************************************************************
*          	Extern Variable Declarations 
*************************************************************************/
//extern bru_Sensor_Status Sensors;
//extern bru_Sensor_EventMap EventMap;
//extern bru_Context BruTask; 
//extern event_id_t event;

/***********************************************************************
*          	Extern Function Declarations	
*************************************************************************/
//extern bool GetEvent(void);
//extern bool evt_queue_Post(gevent_t*);
//extern void CallSubTask(bru_Tasks_List);
//extern void UpdateTaskScreen(bru_UI_Window_ID, bool);
//extern void bru_Context_Next_Task(bru_Tasks_List);
//extern void bru_Context_Task_Exit(void);
//extern void msg_unused_event(event_id_t);

/***********************************************************************
*           Global Variable Define  
*************************************************************************/
char* pTaskLogo;
	
/***********************************************************************
*           Static Variable Define 
*************************************************************************/


/************************************************************************/

//=== Context Core =============================================================

void GetTaskLogo(bru_Tasks_List task)
{
	switch(task)
	{
		case T_Null:
			pTaskLogo = "T_Null";
			break;
		case ST_ADD_WATER:
			pTaskLogo = "ST_ADD_WATER";
			break;
		case ST_PLACE_STEEPING_CHAMBER:
			pTaskLogo = "ST_PLACE_STEEPING_CHAMBER";
			break;
		case ST_PLEASE_CUP:
			pTaskLogo = "ST_PLEASE_CUP";
			break;
		case ST_REMOVE_CUP:
			pTaskLogo = "ST_REMOVE_CUP";
			break;
		case ST_CLOSE_LID:
			pTaskLogo = "ST_CLOSE_LID";
			break;
		case ST_ERROR_xx:
			pTaskLogo = "ST_ERROR_xx";
			break;
		case T_IntroScreen:
			pTaskLogo = "T_IntroScreen";
			break;
		case T_HomeScreen:
			pTaskLogo = "T_HomeScreen";
			break;
		case T_Preset_1_Menu:
			pTaskLogo = "T_Preset_1_Menu";
			break;
		case T_Preset_2_Menu:
			pTaskLogo = "T_Preset_2_Menu";
			break;		
		case T_Preset_3_Menu:
			pTaskLogo = "T_Preset_3_Menu";
			break;		
		case T_Dispenser_Menu:
			pTaskLogo = "T_Dispenser_Menu";
			break;		
		case T_Alarm_Menu:
			pTaskLogo = "T_Alarm_Menu";
			break;		
		case T_Preset_Time_Edit:
			pTaskLogo = "T_Preset_Time_Edit";
			break;		
		case T_Preset_Temp_Edit:
			pTaskLogo = "T_Preset_Temp_Edit";
			break;		
		case T_Preset_Water_Edit:
			pTaskLogo = "T_Preset_Water_Edit";
			break;
		case T_Brewing_Setup_Time:
			pTaskLogo = "T_Brewing_Setup_Time";
			break;
		case T_Brewing_Setup_Time_Edit:
			pTaskLogo = "T_Brewing_Setup_Time_Edit";
			break;
		case T_Brewing_Setup_Temperature:
			pTaskLogo = "T_Brewing_Setup_Temperature";
			break;
		case T_Brewing_Setup_Temperature_Edit:
			pTaskLogo = " T_Brewing_Setup_Temperature_Edit";
			break;
		case T_Brewing_Setup_Water:
			pTaskLogo = "T_Brewing_Setup_Water";
			break;
		case T_Brewing_Setup_Water_Edit:
			pTaskLogo = "T_Brewing_Setup_Water_Edit";
			break;
		case T_PreBrewing_Water:
			pTaskLogo = "T_PreBrewing_Water";
			break;
		case T_PreBrewing_SChamber:
			pTaskLogo = "T_PreBrewing_SChamber";
			break;
		case T_PreBrewing_Cup:
			pTaskLogo = "T_PreBrewing_Cup";
			break;
		case T_PreBrewing_Lid:
			pTaskLogo = "T_PreBrewing_Lid";
			break;
		case T_PreBrewing_PV:
			pTaskLogo = "T_PreBrewing_PV";
			break;
		case T_Brewing_FillingUp:
			pTaskLogo = "T_Brewing_FillingUp";
			break;
		case T_Brewing_Steeping:
			pTaskLogo = "T_Brewing_Steeping";
			break;
		case T_Brewing_Cancel:
			pTaskLogo = "T_Brewing_Cancel";
			break;
		case T_Brewing_Serving:
			pTaskLogo = "T_Brewing_Serving";
			break;
		case T_PostBrewing_Washing:
			pTaskLogo = "T_PostBrewing_Washing";
			break;
		case T_PostBrewing_Washing_Serving:
			pTaskLogo = "T_PostBrewing_Washing_Serving";
			break;
		case T_PostBrewing_Enjoy:
			pTaskLogo = "T_PostBrewing_Enjoy";
			break;
		case T_Dispenser_Water:
			pTaskLogo = "T_Dispenser_Water";
			break;
		case T_Dispenser_SChamber:
			pTaskLogo = "T_Dispenser_SChamber";
			break;
		case T_Dispenser_Cup:
			pTaskLogo = "T_Dispenser_Cup";
			break;
		case T_Dispenser_Lid:
			pTaskLogo = "T_Dispenser_Lid";
			break;
		case T_Dispenser_PV:
			pTaskLogo = "T_Dispenser_PV";
			break;
		case T_Dispenser_Dispensing:
			pTaskLogo = "T_Dispenser_Dispensing";
			break;
		case T_Dispenser_Serving:
			pTaskLogo = "T_Dispenser_Serving";
			break;
		case T_TestScreen_1:
			pTaskLogo = "T_TestScreen_1";
			break;
		case T_TestScreen_2:
			pTaskLogo = "T_TestScreen_2";
			break;
		case T_TestScreen_3:
			pTaskLogo = "T_TestScreen_3";
			break;
		case T_TestScreen_4:
			pTaskLogo = "T_TestScreen_4";
			break;
		case T_TestScreen_5:
			pTaskLogo = "T_TestScreen_5";
			break;
		case T_TestScreen_6:
			pTaskLogo = "T_TestScreen_6";
			break;
		case T_TestScreen_7:
			pTaskLogo = "T_TestScreen_7";
			break;
		default: pTaskLogo = "ERROR! Unknown task logo!";
	}
}

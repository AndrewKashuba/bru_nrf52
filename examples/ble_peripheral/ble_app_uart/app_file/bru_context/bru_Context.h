/*
 * bru_Context.h
 *
 *  Created on: Nov 20, 2021
 *      Author: Andrew Kashuba
 */

#ifndef BRU_CONTEXT_H_
#define BRU_CONTEXT_H_

#include <stdint.h>
#include <stdbool.h>
#include "bru_ui_disp.h"

#define BRU_PRESET_WATER_AMOUNT_MAX_ML          (450.0f)
#define BRU_PRESET_WATER_AMOUNT_INF_ML          (500.0f)
#define BRU_PRESET_WATER_AMOUNT_MIN_ML          (50.0f)

#define BRU_PRESET_TEMPERATURE_MAX         (100.0f)
#define BRU_PRESET_TEMPERATURE_MIN         (40.0f)

#define RINSE_TIME	1200

#define Init (1)
#define Update (0)

typedef void (*bru_Context_Task_Init_Fxn) (uint32_t ArgInit);
typedef bool (*bru_Context_Task_Core_Fxn) (uint32_t ArgCore);
typedef void (*bru_Context_Task_Final_Fxn) (uint32_t ArgFin);

typedef enum
{
    Suspend = 0,
    Initialized,
		Core,
		Finalizing
} _ContextSM;

typedef enum
{
  T_Null = 0,
	ST_ADD_WATER,
	ST_PLACE_STEEPING_CHAMBER,
	ST_PLEASE_CUP,
	ST_REMOVE_CUP,
	ST_CLOSE_LID,
	ST_ERROR_xx,
	T_IntroScreen,
	T_HomeScreen,
	T_Preset_1_Menu,
	T_Preset_2_Menu,
	T_Preset_3_Menu,
	T_Dispenser_Menu,
	T_Alarm_Menu,	
	T_Preset_Time_Edit,
	T_Preset_Temp_Edit,
	T_Preset_Water_Edit,
	T_Brewing_Setup_Time,
	T_Brewing_Setup_Time_Edit,
	T_Brewing_Setup_Temperature,
	T_Brewing_Setup_Temperature_Edit,	
	T_Brewing_Setup_Water,
	T_Brewing_Setup_Water_Edit,
	T_PreBrewing_Water,
	T_PreBrewing_SChamber,
	T_PreBrewing_Cup,
	T_PreBrewing_Lid,
	T_PreBrewing_PV,
	T_Brewing_FillingUp,
	T_Brewing_Steeping,
	T_Brewing_Cancel,
	T_Brewing_Serving,
	T_PostBrewing_Washing,
	T_PostBrewing_Washing_Serving,
	T_PostBrewing_Enjoy,	
	T_Dispenser_Water,
	T_Dispenser_SChamber,
	T_Dispenser_Cup,
	T_Dispenser_Lid,
	T_Dispenser_PV,
	T_Dispenser_Dispensing,
	T_Dispenser_Serving,	
	T_TestScreen_1,
	T_TestScreen_2,
	T_TestScreen_3,
	T_TestScreen_4,
	T_TestScreen_5,
	T_TestScreen_6,
	T_TestScreen_7,
  TASKS_LIST_END
} bru_Tasks_List;

typedef enum
{
	MAIN_BTN_NO = 0,
	MAIN_BTN_LEFT,
	MAIN_BTN_RIGHT,
	MAIN_BTN_BOTH,
} bru_main_btn;

typedef struct bru_Context_
{
  bru_Tasks_List TaskName;
  uint32_t ArgInit, ArgCore, ArgFin;
  bru_Context_Task_Init_Fxn  fxnInitTask;         // task initialization function
  bru_Context_Task_Core_Fxn  fxnCoreTask;         // task core function
  bru_Context_Task_Final_Fxn fxnFinalTask;        // task finalization function
  bru_UI_Window_ID TaskScreen;										// task screen
	bru_BTN_Label LeftLabel;
	bru_BTN_Label RightLabel;
	bru_main_btn MainButton;
} bru_Context;

typedef enum
{
    msg_Bru_started,
    msg_Bru_sleeping,
    msg_Bru_waked,
    msg_Begin_brewing,
    msg_End_brewing,
    msg_Delay,
    msg_IRsensor_Value
} bru_Msg_List;


extern void TaskLink(bru_Tasks_List, 
							bru_Context_Task_Init_Fxn,
							bru_Context_Task_Core_Fxn,
							bru_Context_Task_Final_Fxn);

#endif /* BRU_CONTEXT_H_ */

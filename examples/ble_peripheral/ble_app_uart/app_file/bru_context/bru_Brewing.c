/*
 * 	bru_Brewing.c
 *	Brewing process
 *
 *  Created on: Dec 14, 2021
 *      Author: Andrew Kashuba
 */
 
/***********************************************************************
*           Include Files  
*************************************************************************/
#include <stdint.h>
#include <stdbool.h>
#include "dbprintf.h"
#include "event_queue_c.h"
#include "FreeRTOS.h"
#include "task.h"

#include "bru_Context.h"
#include "bru_Brewing.h"
#include "bru_PinchValve.h"
#include "bru_Pump.h"
#include "bru_Water_Heating.h"
#include "CtrlUnits.h"

/***********************************************************************
*           Macro Define  
*************************************************************************/

/***********************************************************************
*           Type Define  
*************************************************************************/

/***********************************************************************
*           Static Function Declarations  
*************************************************************************/

/***********************************************************************
*          	Extern Variable Declarations 
*************************************************************************/
extern bru_Sensor_Status Sensors;
extern bru_Sensor_EventMap EventMap;
extern bru_Context BruTask; 
extern event_id_t event;
extern bool fFirstCup;

/***********************************************************************
*          	Extern Function Declarations	
*************************************************************************/
extern bool GetEvent(void);
extern bool evt_queue_Post(gevent_t*);
extern void CallSubTask(bru_Tasks_List);
extern void UpdateTaskScreen(bru_UI_Window_ID, bool);
extern void bru_Context_Next_Task(bru_Tasks_List);
extern void msg_unused_event(event_id_t);
extern bru_Tasks_List jmp_to_preset_menu(void);
extern void SetupButtons(bru_main_btn, bru_BTN_Label, bru_BTN_Label);
extern void EncoderClick(void);

/***********************************************************************
*           Global Variable Define  
*************************************************************************/

/***********************************************************************
*           Static Variable Define 
*************************************************************************/

/***********************************************************************
*                                   end
*************************************************************************/

int32_t GetServingTime(void)
{
	bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
	
	// Serving Time
	if((BruTask.TaskName == T_Brewing_Serving) && 
		 (hUserInterface->hMachineSetup->rinseConfiguration != MS_RINSE_OFF))
		switch((int)hUserInterface->presetsArr[hUserInterface->iPresetCurrentIndex].waterAmount) {
			case 50: return 6000;
			case 100: return 7000;
			case 150: return 8000;
			case 200: return 9000;
			case 250: return 10000;
			case 300: return 11000;
			case 350: return 12000;
			case 400: return 13000;
			case 450: return 14000;
			default: 
				DB_Printf("WARNING! Unknown waterAmount in GetServingTime()!\n");
				return 14000;
		}
	else if((BruTask.TaskName == T_Brewing_Serving)	&& 
					(hUserInterface->hMachineSetup->rinseConfiguration == MS_RINSE_OFF))
		switch((int)hUserInterface->presetsArr[hUserInterface->iPresetCurrentIndex].waterAmount)	{
			case 50: return 9000;
			case 100: return 10000;
			case 150: return 11000;
			case 200: return 12000;
			case 250: return 13000;
			case 300: return 14000;
			case 350: return 15000;
			case 400: return 16000;
			case 450: return 17000;
			default: 
				DB_Printf("WARNING! Unknown waterAmount in GetServingTime()!\n");
				return 17000;
		}
	//	Rinse Serving Time
	else if((BruTask.TaskName == T_PostBrewing_Washing_Serving) && 
		 (hUserInterface->hMachineSetup->rinseConfiguration != MS_RINSE_OFF))
		switch((int)hUserInterface->presetsArr[hUserInterface->iPresetCurrentIndex].waterAmount)	{
			case 50: return 9000;
			case 100: return 10000;
			case 150: return 11000;
			case 200: return 12000;
			case 250: return 13000;
			case 300: return 14000;
			case 350: return 15000;
			case 400: return 16000;
			case 450: return 17000;
			default: 
				DB_Printf("WARNING! Unknown waterAmount in GetServingTime()!\n");
				return 17000;
		}	
	else {
		DB_Printf("WARNING! GetServingTime() not for this task!\n");
		return 17000;
	}	
}

//--- T_PreBrewing_Water -------------------------------------------------------
static void PreBrewing_Water_Init(uint32_t Arg)
{
}

static bool PreBrewing_Water_Core(uint32_t Arg)
{
	if(Sensors.Water) return true;
	CallSubTask(ST_ADD_WATER);
	return false;
}

static void PreBrewing_Water_Fin(uint32_t Arg)
{
	bru_Context_Next_Task(T_PreBrewing_SChamber);
}

//--- T_PreBrewing_SChamber ----------------------------------------------------
static void PreBrewing_SChamber_Init(uint32_t Arg)
{
}

static bool PreBrewing_SChamber_Core(uint32_t Arg)
{
	if(Sensors.BrewingChamber) return true;
	CallSubTask(ST_PLACE_STEEPING_CHAMBER);
	return false;
}

static void PreBrewing_SChamber_Fin(uint32_t Arg)
{
	bru_Context_Next_Task(T_PreBrewing_Cup);	
}

//--- T_PreBrewing_Cup ---------------------------------------------------------
static void PreBrewing_Cup_Init(uint32_t Arg)
{
}

static bool PreBrewing_Cup_Core(uint32_t Arg)
{	
	if(Sensors.Cup) return true;
	CallSubTask(ST_PLEASE_CUP);
	return false;
}

static void PreBrewing_Cup_Fin(uint32_t Arg)
{
	bru_Context_Next_Task(T_PreBrewing_Lid);	
}

//--- T_PreBrewing_Lid ---------------------------------------------------------
static void PreBrewing_Lid_Init(uint32_t Arg)
{
}

static bool PreBrewing_Lid_Core(uint32_t Arg)
{	
	if(Sensors.Lid) return true;
	CallSubTask(ST_CLOSE_LID);
	return false;
}

static void PreBrewing_Lid_Fin(uint32_t Arg)
{
	bru_Context_Next_Task(T_PreBrewing_PV);	
}

//--- T_PreBrewing_PV ----------------------------------------------------------
static void PreBrewing_PV_Init(uint32_t Arg)
{
	EventMap.pv_close_enb = true;
	EventMap.pv_error_enb = true;
	EventMap.cancel_enb = true;
	Pinch_Valve(ToClose);	
}

static bool PreBrewing_PV_Core(uint32_t Arg)
{
	if(GetEvent())
		switch(event)	
		{
			case EVENT_PV_CLOSE:
				return true;
			
			case EVENT_PV_OPEN:
				bru_Context_Next_Task(jmp_to_preset_menu());
				return false;
			
			case EVENT_PV_ERROR:
				CallSubTask(ST_ERROR_xx);
				return false;

			case EVENT_CANCEL:
				EventMap.pv_open_enb = true;
				Pinch_Valve(ToOpen);
				break;
			
			default: msg_unused_event(event);
		}

	vTaskDelay(250);	
	
	return false;
}

static void PreBrewing_PV_Fin(uint32_t Arg)
{
	bru_Context_Next_Task(T_Brewing_FillingUp);
}

//--- T_Brewing_FillingUp ------------------------------------------------------
static void Brewing_FillingUp_Init(uint32_t Arg)
{
	set_rgb_duty(0,0,100); //ak	
	SetupButtons(MAIN_BTN_LEFT, bSTOP, b__);
	UpdateTaskScreen(UI_WINDOW_BREWING_STATUS_FILLING_UP, Init);
	EventMap.left_button_enb = true;
	EventMap.encoder_button_enb = true;
	EventMap.water_enb = true;
	EventMap.lid_open_enb = true;
	EventMap.lid_close_enb = true;
	TimeCapture_Init();
	
	bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
	
	DB_Printf("Preset: index: %01d, temp: %02d, water: %03d\n", hUserInterface->iPresetCurrentIndex,
		(int)hUserInterface->presetsArr[hUserInterface->iPresetCurrentIndex].temperature,
		(int)hUserInterface->presetsArr[hUserInterface->iPresetCurrentIndex].waterAmount);
	
	bru_Water_Prepare_Start(hUserInterface->presetsArr[hUserInterface->iPresetCurrentIndex].waterAmount,\
													hUserInterface->presetsArr[hUserInterface->iPresetCurrentIndex].temperature);	
}

static bool Brewing_FillingUp_Core(uint32_t Arg)
{
	if (bru_Pid_Step()) return true;
		
	if(GetEvent())
		switch(event)	
		{
			case EVENT_LEFT_BUTTON_CLICK:													// btn Stop
				bru_Water_Prepare_Stop();
				fFirstCup = false;
				bru_Context_Next_Task(T_Brewing_Cancel);   					// Steeping Cancel
				break;
			
			case EVENT_WATER_NO:																	// Check Water evt
				bru_Water_Prepare_Pause();
				CallSubTask(ST_ADD_WATER);
				break;
			
			case 	EVENT_WATER_OK:
				bru_Water_Prepare_Resume();
				break;
				
			case EVENT_LID_OPENED:																// Check Lid evt
				bru_Water_Prepare_Pause();
				CallSubTask(ST_CLOSE_LID);
				return false;				
			
			case EVENT_LID_CLOSED:
				bru_Water_Prepare_Resume();
				return false;			

			case EVENT_ENCODER_BUTTON_CLICK:
				EncoderClick();
				break;
			
			default: msg_unused_event(event);
		}

	vTaskDelay(250);	
	return false;
}

static void Brewing_FillingUp_Fin(uint32_t Arg)
{
	bru_Water_Prepare_Stop();
	fFirstCup = false;
	bru_Context_Next_Task(T_Brewing_Steeping);
}

//--- T_Brewing_Steeping -------------------------------------------------------
static void Brewing_Steeping_Init(uint32_t Arg)
{
	bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
	hUserInterface->iCountdownBrewingTime = 
		hUserInterface->presetsArr[hUserInterface->iPresetCurrentIndex].brewingTime;
	SetupButtons(MAIN_BTN_LEFT, bSTOP, b__);
	UpdateTaskScreen(UI_WINDOW_BREWING_STATUS_STEEPING, Init);
	EventMap.left_button_enb = true;
	EventMap.encoder_button_enb = true;
	StartTickGenerator(1000);
}

static bool Brewing_Steeping_Core(uint32_t Arg)
{
	bru_UI_Interface_Handle hUserInterface = get_ui_interface();

	if(GetEvent())
		switch(event)	
		{			
			case EVENT_TICKS:
				hUserInterface->iCountdownBrewingTime--;
				if(hUserInterface->iCountdownBrewingTime == 0) 
					bru_Context_Next_Task(T_Brewing_Serving);					// Steeping Done
				else UpdateTaskScreen(UI_WINDOW_BREWING_STATUS_STEEPING, Update);
				break;
				
			case EVENT_LEFT_BUTTON_CLICK:													// btn Stop
				bru_Context_Next_Task(T_Brewing_Cancel);   					// Steeping Cancel
				break;
			
			case EVENT_ENCODER_BUTTON_CLICK:
				EncoderClick();
				break;
			
			default: msg_unused_event(event);
		}
	return false;
}

static void Brewing_Steeping_Fin(uint32_t Arg)
{

}

//--- T_Brewing_Cancel ---------------------------------------------------------
static void Brewing_Cancel_Init(uint32_t Arg)
{
	UpdateTaskScreen(UI_WINDOW_BREWING_STEEPING_CANCEL, Init);
	bru_Pump_Stop(); 																					// Pump stop
}

static bool Brewing_Cancel_Core(uint32_t Arg)
{
	if(Sensors.Cup) return true;
	CallSubTask(ST_PLEASE_CUP);
	return false;
}

static void Brewing_Cancel_Fin(uint32_t Arg)
{
	set_rgb_duty(0,0,0); //ak	
	Pinch_Valve(ToOpen);
	vTaskDelay(5000);
	bru_Context_Next_Task(jmp_to_preset_menu());
}

//--- T_Brewing_Serving --------------------------------------------------------
static void Brewing_Serving_Init(uint32_t Arg)
{
	//SetupButtons(MAIN_BTN_LEFT, bSTOP, b__);
	BruTask.TaskScreen = UI_WINDOW_BREWING_STATUS_SERVING;
	EventMap.right_button_enb = true;								// for time capture
	AlarmTimerStart(GetServingTime());
//!!!! temp solution just for 6 Dec 2021
//	SensorEventMap.cup_on_place_enb = true;
//	SensorEventMap.cup_removed_enb = true;
	
	if(Sensors.Cup) {
		UpdateTaskScreen(UI_WINDOW_BREWING_STATUS_SERVING, Init);
		set_rgb_duty(100,0,0); //ak	
		Pinch_Valve(ToOpen);
	}
//!!!! temp solution just for 6 Dec 2021
//	else {
//		ContextSM = Initialized;	
//		CallSubTask(ST_PLEASE_CUP);
//	}
}

static bool Brewing_Serving_Core(uint32_t Arg)
{
	if(GetEvent())
		switch(event)	
		{
			case EVENT_RIGHT_BUTTON_CLICK:
				TimeCapture_Begin();											// time capture begin
				break;		
			
			case EVENT_RIGHT_BUTTON_RESUME:
				TimeCapture_End();												// time capture end
				break;	
			
			case EVENT_ALARM_TIMER:
				return true; 															// Serving Done
/* !!!! temp solution just for 6 Dec 2021
// disabled before IR sensor update		
			case EVENT_CUP_REMOVED:
				CallSubTask(ST_PLEASE_CUP);
				bru_Pump_Stop();							
				AlarmTimerPause(); SensorEventMap.one_sec_tick_enb = false;
				return false;

			case EVENT_CUP_ON_PLACE:
				bru_Pump_Start();					
				AlarmTimerResume(); SensorEventMap.one_sec_tick_enb = true;
				return false;
*/				
			default: msg_unused_event(event);
		}
	return false;
}

static void Brewing_Serving_Fin(uint32_t Arg)
{
	set_rgb_duty(0,0,0); //ak	
	bru_Pump_Stop();							
	bru_Context_Next_Task(T_PostBrewing_Washing);
}

//--- T_PostBrewing_Washing ----------------------------------------------------
static void PostBrewing_Washing_Init(uint32_t Arg)
{
	set_rgb_duty(0,0,100); //ak	
	BruTask.TaskScreen = UI_WINDOW_BREWING_STATUS_CHAMBER_WASHING;
	bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
	SetupButtons(MAIN_BTN_LEFT, bSTOP, b__);
	EventMap.encoder_button_enb = true;
	EventMap.left_button_enb = true;
	EventMap.right_button_enb = true;								// for time capture
	EventMap.lid_open_enb = true;
	EventMap.lid_close_enb = true;

	switch(hUserInterface->hMachineSetup->rinseConfiguration)
	{
		case MS_RINSE_TO_CUP:
			if(Sensors.Cup)	{
				AlarmTimerStart(RINSE_TIME);
				bru_Pump_Start();
				UpdateTaskScreen(UI_WINDOW_BREWING_STATUS_CHAMBER_WASHING, Init);
			} 
			else 
				CallSubTask(ST_PLEASE_CUP);
			EventMap.cup_on_place_enb = true;
			EventMap.cup_removed_enb = true;
			break;

		case MS_RINSE_TO_TRAY:
			if(!Sensors.Cup) {
				AlarmTimerStart(RINSE_TIME);
				bru_Pump_Start();
				UpdateTaskScreen(UI_WINDOW_BREWING_STATUS_CHAMBER_WASHING, Init);
			}
			else 
				CallSubTask(ST_REMOVE_CUP);
			EventMap.cup_on_place_enb = true;
			EventMap.cup_removed_enb = true;
			break;

		default: 
			bru_Context_Next_Task(T_PostBrewing_Enjoy);
			break;
	}	
}

static bool PostBrewing_Washing_Core(uint32_t Arg)
{
	bru_UI_Interface_Handle hUserInterface = get_ui_interface();
	
	if(GetEvent())
		switch(event)	
		{
			case EVENT_RIGHT_BUTTON_CLICK:
				TimeCapture_Begin();											// time capture begin
				break;		
			
			case EVENT_RIGHT_BUTTON_RESUME:
				TimeCapture_End();												// time capture end
				break;	
			
			case EVENT_ALARM_TIMER:
				bru_Pump_Stop();					
				return true;
			
			case EVENT_LEFT_BUTTON_CLICK:
				bru_Context_Next_Task(T_Brewing_Cancel);
				break;
			
			case EVENT_LID_OPENED:
				bru_Pump_Stop();
 				AlarmTimerPause();
				CallSubTask(ST_CLOSE_LID);
				return false;

			case EVENT_LID_CLOSED:
				AlarmTimerResume();
				bru_Pump_Start();
				return false;
			
			case EVENT_CUP_REMOVED:
				if(hUserInterface->hMachineSetup->rinseConfiguration == MS_RINSE_TO_CUP) 
				{
					CallSubTask(ST_PLEASE_CUP);
					bru_Pump_Stop();							
  				AlarmTimerPause();
				}
				else if (hUserInterface->hMachineSetup->rinseConfiguration == MS_RINSE_TO_TRAY)
				{
					bru_Pump_Start();
					AlarmTimerResume();
				}
				return false;

			case EVENT_CUP_ON_PLACE:
				if(hUserInterface->hMachineSetup->rinseConfiguration == MS_RINSE_TO_CUP) 
				{
					bru_Pump_Start();					
					AlarmTimerResume();
				}
				else if (hUserInterface->hMachineSetup->rinseConfiguration == MS_RINSE_TO_TRAY)
				{
					CallSubTask(ST_REMOVE_CUP);
					bru_Pump_Stop();		
  				AlarmTimerPause();
				}
				return false;
			
			case EVENT_ENCODER_BUTTON_CLICK:
				EncoderClick();
				break;
			
			default: msg_unused_event(event);
		}
		
	return false;
}

static void PostBrewing_Washing_Fin(uint32_t Arg)
{
	bru_UI_Interface_Handle hUserInterface = get_ui_interface();

	switch(hUserInterface->hMachineSetup->rinseConfiguration)
	{
		case MS_RINSE_TO_CUP:
			bru_Context_Next_Task(T_PostBrewing_Washing_Serving);
			break;

		case MS_RINSE_OFF:
		case MS_RINSE_TO_TRAY:
			bru_Context_Next_Task(T_PostBrewing_Enjoy);
			break;

		default: break;
	}
}

//--- T_PostBrewing_Washing_Serving --------------------------------------------
static void PostBrewing_Washing_Serving_Init(uint32_t Arg)
{
	UpdateTaskScreen(UI_WINDOW_BREWING_STATUS_SERVING, Init);
	EventMap.right_button_enb = true;								// for time capture
	AlarmTimerStart(GetServingTime());	
}

static bool PostBrewing_Washing_Serving_Core(uint32_t Arg)
{
	if(GetEvent())
		switch(event)	
		{
			case EVENT_RIGHT_BUTTON_CLICK:
				TimeCapture_Begin();											// time capture begin
				break;		
			
			case EVENT_RIGHT_BUTTON_RESUME:
				TimeCapture_End();												// time capture end
				break;	
			
			case EVENT_ALARM_TIMER:
				return true; 															// Serving Done
	
			default: msg_unused_event(event);
		}
	return false;
}

static void PostBrewing_Washing_Serving_Fin(uint32_t Arg)
{
	bru_Context_Next_Task(T_PostBrewing_Enjoy);
}

//--- T_PostBrewing_Enjoy ------------------------------------------------------
static void PostBrewing_Enjoy_Init(uint32_t Arg)
{
	UpdateTaskScreen(UI_WINDOW_BREWING_STATUS_ENJOY, Init);
}

static bool PostBrewing_Enjoy_Core(uint32_t Arg)
{
	extern void set_buzzer_duty(uint8_t);
	
	set_buzzer_duty(50);
	vTaskDelay(600);	
	set_buzzer_duty(0);
	vTaskDelay(600);	
	set_buzzer_duty(50);
	vTaskDelay(600);	
	set_buzzer_duty(0);
	vTaskDelay(600);	
	set_buzzer_duty(50);
	vTaskDelay(600);	
	set_buzzer_duty(0);
	if(TimeCapture_Result()) vTaskDelay(2000); 			// for debuge / TimeCapture show
	return true;
}

static void PostBrewing_Enjoy_Fin(uint32_t Arg)
{
	set_rgb_duty(0,0,0); //ak	
	bru_Context_Next_Task(jmp_to_preset_menu());
}

//=== Connecting Units ========================================================

void TaskLink_Brewing_Init(void)
{
	TaskLink(T_PreBrewing_Water, &PreBrewing_Water_Init, &PreBrewing_Water_Core, &PreBrewing_Water_Fin);
	TaskLink(T_PreBrewing_SChamber, &PreBrewing_SChamber_Init, &PreBrewing_SChamber_Core, &PreBrewing_SChamber_Fin);
	TaskLink(T_PreBrewing_Cup, &PreBrewing_Cup_Init, &PreBrewing_Cup_Core, &PreBrewing_Cup_Fin);
	TaskLink(T_PreBrewing_Lid, &PreBrewing_Lid_Init, &PreBrewing_Lid_Core, &PreBrewing_Lid_Fin);
	TaskLink(T_PreBrewing_PV, &PreBrewing_PV_Init, &PreBrewing_PV_Core, &PreBrewing_PV_Fin);
	TaskLink(T_Brewing_FillingUp, &Brewing_FillingUp_Init, &Brewing_FillingUp_Core, &Brewing_FillingUp_Fin);
	TaskLink(T_Brewing_Steeping, &Brewing_Steeping_Init, &Brewing_Steeping_Core, &Brewing_Steeping_Fin);
	TaskLink(T_Brewing_Cancel, &Brewing_Cancel_Init, &Brewing_Cancel_Core, &Brewing_Cancel_Fin);
	TaskLink(T_Brewing_Serving, &Brewing_Serving_Init, &Brewing_Serving_Core, &Brewing_Serving_Fin);
	TaskLink(T_PostBrewing_Washing, &PostBrewing_Washing_Init, &PostBrewing_Washing_Core, &PostBrewing_Washing_Fin);
	TaskLink(T_PostBrewing_Washing_Serving, &PostBrewing_Washing_Serving_Init, &PostBrewing_Washing_Serving_Core, &PostBrewing_Washing_Serving_Fin);
	TaskLink(T_PostBrewing_Enjoy, &PostBrewing_Enjoy_Init, &PostBrewing_Enjoy_Core, &PostBrewing_Enjoy_Fin);	
}


/*
 * 	bru_Tests.c
 *	Tests of Bru masghine
 *
 *  Created on: Dec 14, 2021
 *      Author: Andrew Kashuba
 */
 
/***********************************************************************
*           Include Files  
*************************************************************************/
#include <stdint.h>
#include <stdbool.h>
#include "dbprintf.h"
#include "event_queue_c.h"
#include "FreeRTOS.h"
#include "task.h"

#include "BSP_menu_info.h"
#include "bru_Context.h"
#include "bru_Tests.h"
#include "bru_PinchValve.h"
#include "bru_Pump.h"
#include "bru_Water_Heating.h"
#include "CtrlUnits.h"

/***********************************************************************
*           Macro Define  
*************************************************************************/

/***********************************************************************
*           Type Define  
*************************************************************************/

/***********************************************************************
*           Static Function Declarations  
*************************************************************************/

/***********************************************************************
*          	Extern Variable Declarations 
*************************************************************************/
extern bru_Sensor_Status Sensors;
extern bru_Sensor_EventMap EventMap;
extern bru_Context BruTask; 
extern event_id_t event;

/***********************************************************************
*          	Extern Function Declarations	
*************************************************************************/
extern bool GetEvent(void);
extern bool evt_queue_Post(gevent_t*);
extern void CallSubTask(bru_Tasks_List);
extern void UpdateTaskScreen(bru_UI_Window_ID, bool);
extern void bru_Context_Next_Task(bru_Tasks_List);
extern void bru_Context_Task_Exit(void);
extern void msg_unused_event(event_id_t);

/***********************************************************************
*           Global Variable Define  
*************************************************************************/

/***********************************************************************
*           Static Variable Define 
*************************************************************************/


/************************************************************************/


//=== Test Tasks ===============================================================

//--- T_TestScreen_1 -----------------------------------------------------------
static void TestScreen_1_Init(uint32_t Arg)
{
	extern bool fDisplayStartSolution;
	
	if(!fDisplayStartSolution) vTaskDelay(500); 							// temp solution!!!
	fDisplayStartSolution = true;															// temp solution!!!
	UpdateTaskScreen(UI_WINDOW_TEST_1, Init);
	set_backlight_value(BACKLIGHT_ON_VALUE);
	EventMap.encoder_turn_enb = true;	
}

static bool TestScreen_1_Core(uint32_t Arg)
{	
	if(GetEvent())
		switch(event)	
		{
			case EVENT_ENCODER_TURN_LEFT:
				bru_Context_Next_Task(T_TestScreen_7);					
				break;
			
			case EVENT_ENCODER_TURN_RIGHT:
				bru_Context_Next_Task(T_TestScreen_2);
				break;
			
			default: msg_unused_event(event);	
		}		
	else
		vTaskDelay(50);
	
	UpdateTaskScreen(UI_WINDOW_TEST_1, Update);
	return false;
}

//--- T_TestScreen_2 -----------------------------------------------------------
static void TestScreen_2_Init(uint32_t Arg)
{
	UpdateTaskScreen(UI_WINDOW_TEST_2, Init);
	EventMap.encoder_turn_enb = true;
	EventMap.left_button_enb = true;
	EventMap.right_button_enb = true;
}

static bool TestScreen_2_Core(uint32_t Arg)
{	
	if(GetEvent())
		switch(event)	
		{
			case EVENT_ENCODER_TURN_LEFT:
				bru_Context_Next_Task(T_TestScreen_1);					
				break;
			
			case EVENT_ENCODER_TURN_RIGHT:
				bru_Context_Next_Task(T_TestScreen_3);
				break;
			
			case EVENT_LEFT_BUTTON_CLICK:								// Buzzer	On
				buzzer_on(50);	
				UpdateTaskScreen(UI_WINDOW_TEST_2, Update);																																		
				break;
			
			case EVENT_RIGHT_BUTTON_CLICK:							// LED On
				set_rgb_duty(100,100,100);
				UpdateTaskScreen(UI_WINDOW_TEST_2, Update);																
				break;			

			case EVENT_LEFT_BUTTON_RESUME:							// Buzzer	Off
				UpdateTaskScreen(UI_WINDOW_TEST_2, Update);																
				buzzer_off();	
				break;
			
			case EVENT_RIGHT_BUTTON_RESUME:							// LED Off
				UpdateTaskScreen(UI_WINDOW_TEST_2, Update);																
				set_rgb_duty(0,0,0);
				break;			
			
			default: msg_unused_event(event);	
		}
	else
		vTaskDelay(50);
	
	return false;
}

//--- T_TestScreen_3 -----------------------------------------------------------
static void TestScreen_3_Init(uint32_t Arg)
{
	UpdateTaskScreen(UI_WINDOW_TEST_3, Init);
	EventMap.encoder_turn_enb = true;
	EventMap.left_button_enb = true; 					// left - Open
	EventMap.right_button_enb = true;					// right - Close
	EventMapSetup(PV_all_events);	
}

static bool TestScreen_3_Core(uint32_t Arg)
{	
	if(GetEvent())
		switch(event)	
		{
			case EVENT_ENCODER_TURN_LEFT:
				bru_Context_Next_Task(T_TestScreen_2);					
				break;
			
			case EVENT_ENCODER_TURN_RIGHT:
				bru_Context_Next_Task(T_TestScreen_4);
				break;
			
			case EVENT_LEFT_BUTTON_CLICK:
				Pinch_Valve(ToOpen);												// Open PV				
				break;
			
			case EVENT_RIGHT_BUTTON_CLICK:
				Pinch_Valve(ToClose);												// Close PV				
				break;			

			case EVENT_PV_BEGIN:
			case EVENT_PV_CLOSE:
			case EVENT_PV_OPEN:
			case EVENT_PV_ERROR:
				UpdateTaskScreen(UI_WINDOW_TEST_3, Update);
				return false;	
			
			default: msg_unused_event(event);	
		}
	else
		vTaskDelay(50);
	
	return false;
}

//--- T_TestScreen_4 -----------------------------------------------------------
static void TestScreen_4_Init(uint32_t Arg)
{
	UpdateTaskScreen(UI_WINDOW_TEST_4, Init);
	EventMap.encoder_turn_enb = true;
	StartTickGenerator(500);
}

static bool TestScreen_4_Core(uint32_t Arg)
{	
	if(GetEvent())
		switch(event)	
		{
			case EVENT_ENCODER_TURN_LEFT:
				bru_Context_Next_Task(T_TestScreen_3);					
				break;
			
			case EVENT_ENCODER_TURN_RIGHT:
				bru_Context_Next_Task(T_TestScreen_5);
				break;

			case EVENT_TICKS:
				UpdateTaskScreen(UI_WINDOW_TEST_4, Update);
				break;
			
			default: msg_unused_event(event);	
		}
	else
		vTaskDelay(50);
	
	return false;
}

//--- T_TestScreen_5 -----------------------------------------------------------
static void TestScreen_5_Init(uint32_t Arg)
{
	UpdateTaskScreen(UI_WINDOW_TEST_5, Init);
	EventMap.encoder_turn_enb = true;
	StartTickGenerator(500);	
}

static bool TestScreen_5_Core(uint32_t Arg)
{	
	if(GetEvent())
		switch(event)	
		{
			case EVENT_ENCODER_TURN_LEFT:
				bru_Context_Next_Task(T_TestScreen_4);					
				break;
			
			case EVENT_ENCODER_TURN_RIGHT:
				bru_Context_Next_Task(T_TestScreen_6);
				break;

			case EVENT_TICKS:
				UpdateTaskScreen(UI_WINDOW_TEST_5, Update);
				break;
			
			default: msg_unused_event(event);	
		}
	else
		vTaskDelay(50);
	
	return false;
}

//--- T_TestScreen_6 -----------------------------------------------------------
static void TestScreen_6_Init(uint32_t Arg)
{
	UpdateTaskScreen(UI_WINDOW_TEST_6, Init);
	EventMap.encoder_turn_enb = true;
	StartTickGenerator(500);
}

static bool TestScreen_6_Core(uint32_t Arg)
{	
	if(GetEvent())
		switch(event)	
		{
			case EVENT_ENCODER_TURN_LEFT:
				bru_Context_Next_Task(T_TestScreen_5);					
				break;
			
			case EVENT_ENCODER_TURN_RIGHT:
				bru_Context_Next_Task(T_TestScreen_7);
				break;
			
			case EVENT_TICKS:
				UpdateTaskScreen(UI_WINDOW_TEST_6, Update);
				break;

			default: msg_unused_event(event);	
		}
	else
		vTaskDelay(50);
	
	return false;
}

//--- T_TestScreen_7 -----------------------------------------------------------
static void TestScreen_7_Init(uint32_t Arg)
{
	UpdateTaskScreen(UI_WINDOW_TEST_7, Init);
	EventMap.encoder_turn_enb = true;
	EventMap.left_button_enb = true; 					// left - Start
}

static bool TestScreen_7_Core(uint32_t Arg)
{	
	if (bru_Pid_Step()) {
		bru_Water_Prepare_Stop();
		set_rgb_duty(0,0,0);
		EventMap.left_button_enb = true;
		UpdateTaskScreen(UI_WINDOW_TEST_7, Update);
	}
	
	if(GetEvent())
		switch(event)	
		{
			case EVENT_ENCODER_TURN_LEFT:
				bru_Context_Next_Task(T_TestScreen_6);					
				break;
			
			case EVENT_ENCODER_TURN_RIGHT:
				bru_Context_Next_Task(T_TestScreen_1);
				break;
			
			case EVENT_LEFT_BUTTON_CLICK:								// Filling up (100 ml, 45 C)
				DB_Printf("T_Brewing_FillingUp\n");
				set_rgb_duty(100,0,0);
				EventMap.left_button_enb = false;
				EventMap.lid_open_enb = true;
				EventMap.lid_close_enb = true;
				DB_Printf("Preset: temp: %02d C, water: %03d ml\n", 45, 100);
				bru_Water_Prepare_Start((float)100.0,(float)45.0);
				UpdateTaskScreen(UI_WINDOW_TEST_7, Update);
				break;			
			
			case EVENT_WATER_NO:												// Check Water evt
				// ???
				break;
			
			case EVENT_LID_OPENED:											// Check Lid evt
				bru_Water_Prepare_Pause();
				CallSubTask(ST_CLOSE_LID);
				return false;				
			
			case EVENT_LID_CLOSED:
				bru_Water_Prepare_Resume();
				return false;			

			default: msg_unused_event(event);			
		}
	else
		vTaskDelay(250);
	
	return false;
}

//=== Connecting Units ========================================================

static void Null_Proc(uint32_t Arg) {vTaskDelay(10);}

void TaskLink_Tests_Init(void)
{
	TaskLink(T_TestScreen_1, &TestScreen_1_Init, &TestScreen_1_Core, &Null_Proc);
	TaskLink(T_TestScreen_2, &TestScreen_2_Init, &TestScreen_2_Core, &Null_Proc);
	TaskLink(T_TestScreen_3, &TestScreen_3_Init, &TestScreen_3_Core, &Null_Proc);
	TaskLink(T_TestScreen_4, &TestScreen_4_Init, &TestScreen_4_Core, &Null_Proc);
	TaskLink(T_TestScreen_5, &TestScreen_5_Init, &TestScreen_5_Core, &Null_Proc);
	TaskLink(T_TestScreen_6, &TestScreen_6_Init, &TestScreen_6_Core, &Null_Proc);
	TaskLink(T_TestScreen_7, &TestScreen_7_Init, &TestScreen_7_Core, &Null_Proc);	
}


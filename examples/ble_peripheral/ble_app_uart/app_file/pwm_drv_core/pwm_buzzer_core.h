#ifndef __PWM_BUZZER_CORE_H
#define __PWM_BUZZER_CORE_H

#include "nrf_drv_pwm.h"
#include "nrf_gpio.h"
#include "nrf_drv_gpiote.h"
#include "app_util_platform.h"
#include "nrf_drv_pwm.h"
#include "nrf_assert.h"


#ifdef __cplusplus
extern "C" {
#endif


#define BUZZER_PIN    17
#define BUZZER_PWM_LIST {BUZZER_PIN}
#define BUZZER_PWM_LEN   1


#define BUZZER_PWM_TIMER   1    // PWM 1
#define BUZZER_TIMER_PERIOD   200   // 5KHZ

/* Exported Functions ---------------------------------------------------------------------------*/
void BUZZER_PWM_Init(void);
void BUZZER_PWM_DeInit(void);
void set_buzzer_duty(uint8_t buzzer_percent);
void buzzer_on(uint8_t buzzer_percent);
void buzzer_off(void);

#ifdef __cplusplus
}
#endif
#endif


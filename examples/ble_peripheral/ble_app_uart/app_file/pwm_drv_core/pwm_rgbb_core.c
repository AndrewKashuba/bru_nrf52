#include "pwm_rgbb_core.h"


/**
  *************************************************************************************************
  * @file    : BSP_PWM.c
  * @author  : xxx Team
  * @version : V0.0.1
  * @date    : 2017.10.10
  * @brief   : This is PWM driver file.These functions should name begin as BSP_PWM_xx().
  *************************************************************************************************
  */

/* Includes Files -------------------------------------------------------------------------------*/

#if PWM_ENABLED

#define NRF_LOG_MODULE_NAME pwm_rgbb_core
#define NRF_LOG_LEVEL       3
#define NRF_LOG_INFO_COLOR  4
#define NRF_LOG_DEBUG_COLOR 3
#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();



/* Define Macros --------------------------------------------------------------------------------*/




/* Define Variables -----------------------------------------------------------------------------*/

// =============== RGB + BACKLIGHT PWM ========================
 
static nrf_drv_pwm_t rgbb_pwm_instance = NRF_DRV_PWM_INSTANCE(RGBB_PWM_TIMER);


static nrf_pwm_values_individual_t rgbb_pwm_chn_values;


static nrf_pwm_sequence_t const rgbb_pwm_chn_cfg =
{
    .values.p_individual = &rgbb_pwm_chn_values,
    .length              = NRF_PWM_VALUES_LENGTH(rgbb_pwm_chn_values),
    .repeats             = 0,
    .end_delay           = 0
};

static volatile uint8_t rgbb_init_flag;

/* Define Functions -----------------------------------------------------------------------------*/

/**------------------------------------------------------------------------------------------------
  * @brief  : This is PWM led init function
  * @param  : None
  * @retval : None
  *----------------------------------------------------------------------------------------------*/
void RGBB_PWM_Init(void)
{
	if(rgbb_init_flag == 0)
	{
		rgbb_init_flag = 1;
    nrf_drv_pwm_config_t const pwm_cfg =
    {
        .output_pins =
        {
             RED_LED_PIN | NRF_DRV_PWM_PIN_INVERTED, // channel 0
             GREEN_LED_PIN | NRF_DRV_PWM_PIN_INVERTED, // channel 1
             BLUE_LED_PIN | NRF_DRV_PWM_PIN_INVERTED, // channel 2
             BACKLIGHT_PIN | NRF_DRV_PWM_PIN_INVERTED  // channel 3
        },
        .irq_priority = APP_IRQ_PRIORITY_LOWEST,
        .base_clock   = NRF_PWM_CLK_1MHz,
        .count_mode   = NRF_PWM_MODE_UP,
        .top_value    = RGBB_TIMER_PERIOD,
        .load_mode    = NRF_PWM_LOAD_INDIVIDUAL,
        .step_mode    = NRF_PWM_STEP_AUTO
    };

    rgbb_pwm_chn_values.channel_0 = RGBB_TIMER_PERIOD;
		rgbb_pwm_chn_values.channel_1 = RGBB_TIMER_PERIOD;
		rgbb_pwm_chn_values.channel_2 = RGBB_TIMER_PERIOD;
		rgbb_pwm_chn_values.channel_3 = RGBB_TIMER_PERIOD;

    APP_ERROR_CHECK(nrf_drv_pwm_init(&rgbb_pwm_instance, &pwm_cfg, NULL));
		
    nrf_drv_pwm_simple_playback(&rgbb_pwm_instance, &rgbb_pwm_chn_cfg, 1, NRF_DRV_PWM_FLAG_LOOP);   
		set_rgb_duty(0,0,0);
	}		
}

/**
* @brief set RGB led pwm duty
*/

#include "bru_data.h"

void set_rgb_duty(uint8_t r_percent,uint8_t g_percent,uint8_t b_percent)
{	
	uint32_t temp1 = r_percent;
	uint32_t temp2 = g_percent;
	uint32_t temp3 = b_percent;
	
	temp1 = temp1 * RGBB_TIMER_PERIOD / 100;//255;
	temp2 = temp2 * RGBB_TIMER_PERIOD / 100;//255;
	temp3 = temp3 * RGBB_TIMER_PERIOD / 100;//255;
	
	bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
	if(hUserInterface->hMachineSetup->redLed || 
		 hUserInterface->hMachineSetup->greenLed || 
		 hUserInterface->hMachineSetup->blueLed) {	
			rgbb_pwm_chn_values.channel_0 = RGBB_TIMER_PERIOD-temp1;
			rgbb_pwm_chn_values.channel_1 = RGBB_TIMER_PERIOD-temp2;
			rgbb_pwm_chn_values.channel_2 = RGBB_TIMER_PERIOD-temp3;		 }
}

/**
* @brieaf set backlight pwm duty
*/
void set_backlight_duty(uint8_t bl_percent)
{
	uint32_t temp1 =  bl_percent;
	
	temp1 = temp1 * RGBB_TIMER_PERIOD / 100;
	
	rgbb_pwm_chn_values.channel_3 = RGBB_TIMER_PERIOD-temp1;
}

/**------------------------------------------------------------------------------------------------
  * @brief  : This is PWM  deinit function
  * @param  : None
  * @retval : None
  *----------------------------------------------------------------------------------------------*/
void RGBB_PWM_DeInit(void)
{
	if(rgbb_init_flag)
	{
		rgbb_init_flag = 0;
    uint8_t list[RGBB_PWM_LEN] = RGBB_PWM_LIST;
    nrf_drv_pwm_stop(&rgbb_pwm_instance,true);  
    nrf_drv_pwm_uninit(&rgbb_pwm_instance);
    
    for(uint8_t i = 0; i < RGBB_PWM_LEN; ++i)
    {
			  nrf_gpio_cfg_output(list[i]);
        nrf_gpio_pin_clear(list[i]);
    }   
	}
}

#endif

/* End of Flie ----------------------------------------------------------------------------------*/



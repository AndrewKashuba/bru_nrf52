#include "pwm_pump_core.h"


/**
  *************************************************************************************************
  * @file    : BSP_PWM.c
  * @author  : xxx Team
  * @version : V0.0.1
  * @date    : 2017.10.10
  * @brief   : This is PWM driver file.These functions should name begin as BSP_PWM_xx().
  *************************************************************************************************
  */

/* Includes Files -------------------------------------------------------------------------------*/

#if PWM_ENABLED

#define NRF_LOG_MODULE_NAME pwm_pump_core
#define NRF_LOG_LEVEL       3
#define NRF_LOG_INFO_COLOR  4
#define NRF_LOG_DEBUG_COLOR 3
#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();



/* Define Macros --------------------------------------------------------------------------------*/




/* Define Variables -----------------------------------------------------------------------------*/


// ===============PUMP PWM ========================
 
static nrf_drv_pwm_t pump_pwm_instance = NRF_DRV_PWM_INSTANCE(PUMP_PWM_TIMER);


static nrf_pwm_values_individual_t pump_pwm_chn_values;

static nrf_pwm_sequence_t const pump_pwm_chn_cfg =
{
    .values.p_individual = &pump_pwm_chn_values,
    .length              = NRF_PWM_VALUES_LENGTH(pump_pwm_chn_values),
    .repeats             = 0,
    .end_delay           = 0
};

static volatile uint8_t pump_init_flag;

/* Define Functions -----------------------------------------------------------------------------*/



/**------------------------------------------------------------------------------------------------
  * @brief  : This is PWM pumb init function
  * @param  : None
  * @retval : None
  *----------------------------------------------------------------------------------------------*/
void PUMP_PWM_Init(void)
{
	if(pump_init_flag == 0)
	{
		pump_init_flag = 1;
    nrf_drv_pwm_config_t const pwm_cfg =
    {
        .output_pins =
        {
             PUMP_PIN | NRF_DRV_PWM_PIN_INVERTED, // channel 0
             NRF_DRV_PWM_PIN_NOT_USED, // channel 1
             NRF_DRV_PWM_PIN_NOT_USED, // channel 2
             NRF_DRV_PWM_PIN_NOT_USED  // channel 3
        },
        .irq_priority = APP_IRQ_PRIORITY_LOWEST,
        .base_clock   = NRF_PWM_CLK_1MHz,
        .count_mode   = NRF_PWM_MODE_UP,
        .top_value    = PUMP_TIMER_PERIOD,
        .load_mode    = NRF_PWM_LOAD_INDIVIDUAL,
        .step_mode    = NRF_PWM_STEP_AUTO
    };

    pump_pwm_chn_values.channel_0 = PUMP_TIMER_PERIOD;
		
    APP_ERROR_CHECK(nrf_drv_pwm_init(&pump_pwm_instance, &pwm_cfg, NULL));

		set_pump_raw_duty(PUMP_TIMER_PERIOD);
	}
		
}

/**
* @brief set pumb pwm raw duty
*/
void set_pump_raw_duty(uint32_t pump_duty)
{

        pump_pwm_chn_values.channel_0 = pump_duty;
        
				if(pump_duty == PUMP_TIMER_PERIOD)
				{
						if(!nrf_drv_pwm_is_stopped(&pump_pwm_instance))
						{
										nrf_drv_pwm_stop(&pump_pwm_instance,true);
						}
						nrf_gpio_pin_clear(PUMP_PIN);
				}
				else if(nrf_drv_pwm_is_stopped(&pump_pwm_instance))
				{        
						nrf_drv_pwm_simple_playback(&pump_pwm_instance, &pump_pwm_chn_cfg, 1, NRF_DRV_PWM_FLAG_LOOP);         
				}

}

/**------------------------------------------------------------------------------------------------
  * @brief  : This is PWM pumb deinit function
  * @param  : None
  * @retval : None
  *----------------------------------------------------------------------------------------------*/
void PUMP_PWM_DeInit(void)
{
	if(pump_init_flag)
	{
	  pump_init_flag = 0;
    uint8_t list[PUMP_PWM_LEN] = PUMP_PWM_LIST;
    nrf_drv_pwm_stop(&pump_pwm_instance,true);  
    nrf_drv_pwm_uninit(&pump_pwm_instance);
   
    for(uint8_t i = 0; i < PUMP_PWM_LEN; ++i)
    {
			  nrf_gpio_cfg_output(list[i]);
        nrf_gpio_pin_clear(list[i]);
    }   
	}
}

#endif

/* End of Flie ----------------------------------------------------------------------------------*/



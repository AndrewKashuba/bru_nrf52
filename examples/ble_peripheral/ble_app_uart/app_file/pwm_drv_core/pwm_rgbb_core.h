#ifndef __PWM_RGBB_CORE_H
#define __PWM_RGBB_CORE_H

#include "nrf_drv_pwm.h"
#include "nrf_gpio.h"
#include "nrf_drv_gpiote.h"
#include "app_util_platform.h"
#include "nrf_drv_pwm.h"
#include "nrf_assert.h"


#ifdef __cplusplus
extern "C" {
#endif


#define RED_LED_PIN    11
#define GREEN_LED_PIN  12
#define BLUE_LED_PIN   13
#define BACKLIGHT_PIN  29
#define RGBB_PWM_LEN		4

#define RGBB_PWM_LIST        {RED_LED_PIN,GREEN_LED_PIN,BLUE_LED_PIN,BACKLIGHT_PIN}

#define RGBB_PWM_TIMER           2       //PWM2
#define RGBB_TIMER_PERIOD    1000ul    //100HZ

/* Exported Functions ---------------------------------------------------------------------------*/
void RGBB_PWM_Init(void);
void RGBB_PWM_DeInit(void);
void set_rgb_duty(uint8_t r_percent,uint8_t g_percent,uint8_t b_percent);
void set_backlight_duty(uint8_t bl_percent);



#ifdef __cplusplus
}
#endif
#endif


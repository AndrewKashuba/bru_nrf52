#include "pwm_buzzer_core.h"
#include "nrf_delay.h"

/**
  *************************************************************************************************
  * @file    : BSP_PWM.c
  * @author  : xxx Team
  * @version : V0.0.1
  * @date    : 2017.10.10
  * @brief   : This is PWM driver file.These functions should name begin as BSP_PWM_xx().
  *************************************************************************************************
  */

/* Includes Files -------------------------------------------------------------------------------*/

#if PWM_ENABLED

#define NRF_LOG_MODULE_NAME pwm_buzzer_core
#define NRF_LOG_LEVEL       3
#define NRF_LOG_INFO_COLOR  4
#define NRF_LOG_DEBUG_COLOR 3
#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();

/* Define Macros --------------------------------------------------------------------------------*/


/* Define Variables -----------------------------------------------------------------------------*/

// ===============BUZZER PWM ========================
 
static nrf_drv_pwm_t buzzer_pwm_instance = NRF_DRV_PWM_INSTANCE(BUZZER_PWM_TIMER);


static nrf_pwm_values_individual_t buzzer_pwm_chn_values;


static nrf_pwm_sequence_t const buzzer_pwm_chn_cfg =
{
    .values.p_individual = &buzzer_pwm_chn_values,
    .length              = NRF_PWM_VALUES_LENGTH(buzzer_pwm_chn_values),
    .repeats             = 0,
    .end_delay           = 0
};

static volatile uint8_t buzzer_init_flag;

/* Define Functions -----------------------------------------------------------------------------*/


/**------------------------------------------------------------------------------------------------
  * @brief  : This is PWM buzzer init function
  * @param  : None
  * @retval : None
  *----------------------------------------------------------------------------------------------*/
void BUZZER_PWM_Init(void)
{
	if(buzzer_init_flag == 0)
	{
		buzzer_init_flag = 1;
    nrf_drv_pwm_config_t const pwm_cfg =
    {
        .output_pins =
        {
             BUZZER_PIN | NRF_DRV_PWM_PIN_INVERTED, // channel 0
             NRF_DRV_PWM_PIN_NOT_USED, // channel 1
             NRF_DRV_PWM_PIN_NOT_USED, // channel 2
             NRF_DRV_PWM_PIN_NOT_USED  // channel 3
        },
        .irq_priority = APP_IRQ_PRIORITY_LOWEST,
        .base_clock   = NRF_PWM_CLK_1MHz,
        .count_mode   = NRF_PWM_MODE_UP,
        .top_value    = BUZZER_TIMER_PERIOD,
        .load_mode    = NRF_PWM_LOAD_INDIVIDUAL,
        .step_mode    = NRF_PWM_STEP_AUTO
    };

    buzzer_pwm_chn_values.channel_0 = BUZZER_TIMER_PERIOD;
		
    APP_ERROR_CHECK(nrf_drv_pwm_init(&buzzer_pwm_instance, &pwm_cfg, NULL));
	  set_buzzer_duty(0); 
	}		
}

#include "bru_data.h"

/**
* @brief set buzzer pwm duty
*/
void set_buzzer_duty(uint8_t buzzer_percent)
{	
	uint32_t temp1 = buzzer_percent * BUZZER_TIMER_PERIOD / 100;

	bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
	if(hUserInterface->hMachineSetup->buzzerState == MS_BUZZER_ON) 
	{
		buzzer_pwm_chn_values.channel_0 = BUZZER_TIMER_PERIOD - temp1;
		if(buzzer_percent == 0) {
			if(!nrf_drv_pwm_is_stopped(&buzzer_pwm_instance))
				nrf_drv_pwm_stop(&buzzer_pwm_instance,true);
			nrf_gpio_pin_clear(BUZZER_PIN);
		}
		else if(nrf_drv_pwm_is_stopped(&buzzer_pwm_instance))
				nrf_drv_pwm_simple_playback(&buzzer_pwm_instance, &buzzer_pwm_chn_cfg, 1, NRF_DRV_PWM_FLAG_LOOP); 	
	}	
}

void buzzer_on(uint8_t buzzer_percent)		// just for test
{
	uint32_t temp1 = buzzer_percent * BUZZER_TIMER_PERIOD / 100;

	buzzer_pwm_chn_values.channel_0 = BUZZER_TIMER_PERIOD - temp1;
	nrf_drv_pwm_simple_playback(&buzzer_pwm_instance, &buzzer_pwm_chn_cfg, 1, NRF_DRV_PWM_FLAG_LOOP); 
}

void buzzer_off(void)											// just for test
{
	buzzer_pwm_chn_values.channel_0 = BUZZER_TIMER_PERIOD;
	nrf_drv_pwm_stop(&buzzer_pwm_instance,true);
}

/**------------------------------------------------------------------------------------------------
  * @brief  : This is PWM buzzer deinit function
  * @param  : None
  * @retval : None
  *----------------------------------------------------------------------------------------------*/
void BUZZER_PWM_DeInit(void)
{
	if(buzzer_init_flag)
	{
		buzzer_init_flag = 0;
    uint8_t list[BUZZER_PWM_LEN] = BUZZER_PWM_LIST;
    nrf_drv_pwm_stop(&buzzer_pwm_instance,true);  
    nrf_drv_pwm_uninit(&buzzer_pwm_instance);
    
    for(uint8_t i = 0; i < BUZZER_PWM_LEN; ++i)
    {
			  nrf_gpio_cfg_output(list[i]);
        nrf_gpio_pin_clear(list[i]);
    }   
	}
}

#endif

/* End of Flie ----------------------------------------------------------------------------------*/



#ifndef __PWM_PUMP_CORE_H
#define __PWM_PUMP_CORE_H

#include "nrf_drv_pwm.h"
#include "nrf_gpio.h"
#include "nrf_drv_gpiote.h"
#include "app_util_platform.h"
#include "nrf_drv_pwm.h"
#include "nrf_assert.h"


#ifdef __cplusplus
extern "C" {
#endif


#define PUMP_PIN    26
#define PUMP_PWM_LIST {PUMP_PIN}
#define PUMP_PWM_LEN   1


#define PUMP_PWM_TIMER   0    // PWM 1
#define PUMP_TIMER_PERIOD   1000   // 100HZ

/* Exported Functions ---------------------------------------------------------------------------*/
void PUMP_PWM_Init(void);
void PUMP_PWM_DeInit(void);
void set_pump_raw_duty(uint32_t pump_duty);


#ifdef __cplusplus
}
#endif
#endif


#include "dbprintf.h"
#include "test_hardware_core.h"
#include "target_command.h"
#include "BSP_menu_info.h"
#include "pwm_buzzer_core.h"
#include "nrf_delay.h"
#include "bru_WaterSensor.h"
#include "bru_PinchValve.h"
#include "BSP_ADC.h"
#include "bru_Multiplexer_Check.h"

#define NRF_LOG_MODULE_NAME test_hardware_core
#define NRF_LOG_LEVEL       3
#define NRF_LOG_INFO_COLOR  4
#define NRF_LOG_DEBUG_COLOR 3
#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();

#define BRU_PINCH_VALVE_IN_1_PIN				19
#define BRU_PINCH_VALVE_IN_2_PIN				18	
#define BRU_PINCH_VALVE_OPEN_POS_PIN		6
#define BRU_PINCH_VALVE_CLOSE_POS_PIN		7	

#define BRU_RELAY_ZERO_CROSS_PIN		27


void key_release_handler(void)
{
	set_buzzer_duty(50);
	nrf_delay_ms(62);
	set_buzzer_duty(0);
	static uint8_t i;
	switch(i)
	{
		case 0:
			set_rgb_duty(100,0,0);
		
			break;
		case 1:
			set_rgb_duty(0,100,0);
			break;
		case 2:
			set_rgb_duty(0,0,100);
			break;
		case 3:
			set_rgb_duty(100,100,0);
			break;
		case 4:
			set_rgb_duty(100,0,100);
			break;
		case 5:
			set_rgb_duty(0,100,100);
			break;
		case 6:
			set_rgb_duty(100,100,100);
			break;
		case 7:
			set_rgb_duty(0,0,0);
			break;
	}
	if(++i>7)
		i = 0;
	
	nrf_gpio_cfg_output(25);
	nrf_gpio_pin_toggle(25);
	
}

static void key_long_release_handler(void)
{
	NVIC_SystemReset();
}

//######################################################################################################################################
static void test_hardware_updateDisplay(void *p);


/*-----hello bru page message Map ---*/
BEGIN_MESSAGE_MAP(Menu_Function_TestFuncEn)
    ON_VOID_MESSAGE(button2_release_evt,key_release_handler)
	  ON_VOID_MESSAGE(button1_release_evt,key_release_handler)
		ON_VOID_MESSAGE(encoder_a_release_evt,key_release_handler)
		ON_VOID_MESSAGE(encoder_b_release_evt,key_release_handler)
		ON_VOID_MESSAGE(encoder_button_release_evt,key_release_handler)
    ON_VOID_MESSAGE(encoder_button_long_push_evt,key_long_release_handler)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */



INIT_MSG_ENTRIES(Menu_Function_TestFuncEn,Menu_test_hardware_cmd_func,NULL)   


CONST MENU_KEY op_test_table= 
	{&Menu_test_hardware_cmd_func,test_hardware_updateDisplay,NULL,NULL};   		





float top_temperature;
float bottom_temperature;
extern uint8_t flashId;
	
/**
* @brieaf bottom adc value in factory mode
*/
void factory_get_ntc_bottom(void *data)
{
	 bottom_temperature =  calc_temperature_value(data);
	
}

/**
* @brieaf top adc value in factory mode
*/
void factory_get_ntc_top(void *data)
{
	top_temperature =  calc_temperature_value(data);
}

/**
* @brieaf prot adc value in factory mode
*/
void factory_get_ntc_prot(void *data)
{
	int16_t *p = data;
  NRF_LOG_INFO("factory_get_ntc_prot %d",*p);
}

/**
* @brieaf pinch current adc value in factory
*/
void factory_get_pinch_current(void *data)
{
	int16_t *p = data;
  NRF_LOG_INFO("factory_get_pinch_current %d",*p);
}

/**
* @brieaf ir sensor adc value in factory
*/
void factory_get_ir_sensor(void *data)
{
	int16_t *p = data;
	static uint8_t flag =0;
	if(*p > 1000)
	{
		if(flag == 0)
		{
			flag = 1;
			key_release_handler();
		}
	}
	else
	{
		flag = 0;
	}
}

/**
* @brieaf lid position adc value in factory
*/
void factory_get_lid_position(void *data)
{
	int16_t *p = data;
//  NRF_LOG_INFO("get_lid_postion %d",*p);
	
	static uint8_t flag =0;
	if(*p < 1000)
	{
		if(flag == 0)
		{
			flag = 1;
			key_release_handler();
		}
	}
	else
	{
		flag = 0;
	}
}


/**
* @brieaf brewing chamber adc value in factory
*/
void factory_get_brewing_chamber(void *data)
{
		int16_t *p = data;
//  NRF_LOG_INFO("get_brewing_chamber %d",*p);
		static uint8_t flag =0;
	if(*p < 1000)
	{
		if(flag == 0)
		{
			flag = 1;
			key_release_handler();
		}
	}
	else
	{
		flag = 0;
	}
}
	
	
	

/**
* @brieaf hello bru display page function
*/
static void test_hardware_updateDisplay(void *p)
{
	NRF_LOG_INFO("=========HELLO BRU %d============",flashId);
	char buf[64];
	DB_Printf(buf,"FLASH: %s\r\n",flashId ? "OK":"ERR");
  bru_LCD_WriteString_18Arial_R(45,40,buf,Font_24x27,0xffff,0x0000);
	DB_Printf(buf,"TOP: %.1f\r\n",top_temperature);
	bru_LCD_WriteString_18Arial_R(45,90,buf,Font_24x27,0xffff,0x0000);
	DB_Printf(buf,"BOT: %.1f\r\n",bottom_temperature);
	bru_LCD_WriteString_18Arial_R(45,140,buf,Font_24x27,0xffff,0x0000);
}

void bru_Pinch_Valve_Set_Direction(Bru_Pinch_Valve_Direction_Type direction);
bool bru_Pinch_Valve_Is_Open(void);
bool bru_Pinch_Valve_Is_Close(void);

#define TEST_CHECK_TIMER_ID     9547
/**
* @brieaf timer for 3 seconds 
*/
void test_check_timer(void *timer)
{
  TIMER_INFO_T * p = timer;
	p->timer_value = 1;
	
	static uint8_t water_flag;
	
//	if(bru_Water_Check()) //ak
//	{
		if(water_flag == 0)
		{
			water_flag = 1;
			key_release_handler();
		}
//	}
//  else
//	{
//		water_flag = 0;
//	}
	
	
	static uint8_t pinch_open_flag;
	if(bru_Pinch_Valve_Is_Open()==0)
	{
		if(pinch_open_flag == 0)
		{
			pinch_open_flag = 1;
			key_release_handler();
			bru_Pinch_Valve_Set_Direction(PINCH_VALVE_TO_SLEEP);
	    bru_Pinch_Valve_Set_Direction(PINCH_VALVE_TO_OPEN);
		}
	}
	else
	{
		pinch_open_flag = 0;
	}
	
	static uint8_t pinch_close_flag;
	if(bru_Pinch_Valve_Is_Close()==0)
	{
		if(pinch_close_flag == 0)
		{
			pinch_close_flag = 1;
			key_release_handler();
			bru_Pinch_Valve_Set_Direction(PINCH_VALVE_TO_SLEEP);
    	bru_Pinch_Valve_Set_Direction(PINCH_VALVE_TO_CLOSE);
		}
	}
	else
	{
		pinch_close_flag = 0;
	}
	static uint8_t j;
	if(++j == 10)
	{
		j = 0;
//		NRF_LOG_INFO("=====================================");
		for(uint8_t i = MULT_NTC_TOP; i<_MULT_TOTAL; i++) 
		{		
//ak!!!			start_multiplex_messurement(i);	
		}
	}
	static uint8_t disp_cnt;
	if(++disp_cnt >= 100)
	{
		disp_cnt = 0;
		DISP_FLAG_T *p = get_disp_flag();
	  p->flush_disp_flag = 1;
	}
	static uint32_t zero_hi,zero_lo;
	if(nrf_gpio_pin_read(BRU_RELAY_ZERO_CROSS_PIN))
		zero_hi++;
	else
		zero_lo++;
	static uint8_t zero_flag = 1;;
	if((zero_hi > 20) && (zero_lo > 20))
	{
		if(zero_flag == 0)
		{
				key_release_handler();
		}
		zero_flag = 1;
	}
}

/**
* @brieaf power on ui initial
*/
void test_ui_init(void)
{
	target_command_init(&op_test_table,NULL,NULL);   
	register_timer(TEST_CHECK_TIMER_ID,test_check_timer);
  set_timer_value(TEST_CHECK_TIMER_ID,1,NULL);
//ak!!!	 bru_Water_Sensor_Init();
	
	
	nrf_gpio_cfg_output(BRU_PINCH_VALVE_IN_1_PIN);
	nrf_gpio_cfg_output(BRU_PINCH_VALVE_IN_2_PIN);
	
	nrf_gpio_cfg_input(BRU_PINCH_VALVE_OPEN_POS_PIN,NRF_GPIO_PIN_NOPULL);
	nrf_gpio_cfg_input(BRU_PINCH_VALVE_CLOSE_POS_PIN,NRF_GPIO_PIN_NOPULL);
	
	nrf_gpio_cfg_input(BRU_RELAY_ZERO_CROSS_PIN,NRF_GPIO_PIN_NOPULL);
	enter_factory_test_adc_mode();
}





//=========================================================











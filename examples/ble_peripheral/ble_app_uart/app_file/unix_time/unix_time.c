#include "unix_time.h"
#include "bru_data.h"
#include "bru_ui_disp.h"


void timer_to_cal(uint32_t timer, unix_cal * unix_time)
{
	uint32_t a;
	char b;
	char c;
	char d;
	unsigned long time;

	time = timer%SEC_A_DAY;
	a = ((timer+43200)/(86400>>1)) + (2440587<<1) + 1;
	a>>=1;
	unix_time->wday = a%7;
	a+=32044;
	b=(4*a+3)/146097;
	a=a-(146097*b)/4;
	c=(4*a+3)/1461;
	a=a-(1461*c)/4;
	d=(5*a+2)/153;
	unix_time->mday=a-(153*d+2)/5+1;
	unix_time->mon=d+3-12*(d/10);
	unix_time->year=100*b+c-4800+(d/10);
	unix_time->hour=time/3600;
	unix_time->min=(time%3600)/60;
	unix_time->sec=(time%3600)%60;
}

uint32_t cal_to_timer(unix_cal * unix_time)
{
	char a;
	int y;
	char m;
	uint32_t Uday;
	uint32_t time;

	a=((14-unix_time->mon)/12);
	y=unix_time->year+4800-a;
	m=unix_time->mon+(12*a)-3;
	Uday=(unix_time->mday+((153*m+2)/5)+365*y+(y/4)-(y/100)+(y/400)-32045)-2440588;
	time=Uday*86400;
	time+=unix_time->sec+unix_time->min*60+unix_time->hour*3600;
	return time;
}

uint32_t Seconds_get(void)
{
	uint32_t utc;
	CRITICAL_REGION_ENTER();
	bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
	utc = hUserInterface->iCurrentTime;
	CRITICAL_REGION_EXIT();
	
	return utc;
}

void utc_set(uint32_t utc)
{
	CRITICAL_REGION_ENTER();
	bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
	hUserInterface->iCurrentTime = utc;
	CRITICAL_REGION_EXIT();
}

void utc_running_seconds(void)
{
	bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
	hUserInterface->iCurrentTime++;
}

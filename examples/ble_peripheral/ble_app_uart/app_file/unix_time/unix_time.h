
#ifndef BRU_UNIX_TIME_H_
#define BRU_UNIX_TIME_H_

#include <stdint.h>

#define INIT_UTC                     (1609502400)//(1609430400)   
#define SEC_A_DAY 86400



typedef struct
{
    int year;
    char mon;
    char mday;
    char hour;
    char min;
    char sec;
    char wday;
}unix_cal;

void timer_to_cal (uint32_t timer, unix_cal * unix_time);
uint32_t cal_to_timer (unix_cal * unix_time);
uint32_t Seconds_get(void);
void utc_set(uint32_t utc);
void utc_running_seconds(void);

#endif

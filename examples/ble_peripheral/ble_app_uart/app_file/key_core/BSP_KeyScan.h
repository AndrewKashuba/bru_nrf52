#ifndef __BSP_KEYSCAN_H__
#define __BSP_KEYSCAN_H__




#ifdef __cplusplus
extern "C" {
#endif

/* Includes Files -------------------------------------------------------------------------------*/
#include "nrf_drv_gpiote.h"

/* Exported Macros ------------------------------------------------------------------------------*/
    
#define REPEAT_LONG_KEY_ENBLED   0

#define BUTTONS_NUMBER 3


#define BUTTON_1       1
#define BUTTON_2       0
#define BUTTON_3       14
#define ENCODE_OUTA_PIN		16
#define ENCODE_OUTB_PIN   15

#define BUTTON_PULL    NRF_GPIO_PIN_PULLUP

#define BUTTONS_ACTIVE_STATE 0

#define BUTTONS_LIST { BUTTON_1, BUTTON_2, BUTTON_3}

#define BSP_BUTTON_0   BUTTON_1
#define BSP_BUTTON_1   BUTTON_2
#define BSP_BUTTON_2   BUTTON_3


#define BSP_BUTTON_ACTION_PUSH      (APP_BUTTON_PUSH)    /**< Represents pushing a button. See @ref bsp_button_action_t. */
#define BSP_BUTTON_ACTION_RELEASE   (APP_BUTTON_RELEASE) /**< Represents releasing a button. See @ref bsp_button_action_t. */
#define BSP_BUTTON_ACTION_LONG_PUSH (2)                  /**< Represents pushing and holding a button for @ref BSP_LONG_PUSH_TIMEOUT_MS milliseconds. See also @ref bsp_button_action_t. */



#define BUTTON_ERASE_BONDING BSP_BUTTON_0_MASK
#define BUTTON_ERASE_ALL     BSP_BUTTON_1_MASK
#define BUTTON_ADVERTISE     BSP_BUTTON_0_MASK
#define BUTTON_CLEAR_EVT     BSP_BUTTON_1_MASK
#define BUTTON_CAPSLOCK      BSP_BUTTON_2_MASK
#define BSP_BUTTONS_ALL      0xFFFFFFFF
#define BSP_BUTTONS_NONE     0




#define KEY_OFF_FLAG    0X10
#define KEY_LONG_FLAG   0X20

#define BSP_LONG_PUSH_TIMEOUT_MS (1500) /**< The time to hold for a long push (in milliseconds). */
/* Exported Types -------------------------------------------------------------------------------*/

#define BRU_BUTTON1_SHORT    					 0X06
#define BRU_BUTTON2_SHORT   			     0X07
#define BRU_ENCODER_OUT_BUTTON_SHORT   0X08
#define BRU_ENCODER_OUT_A_SHORT        0X09
#define BRU_ENCODER_OUT_B_SHORT        0X0A


#define KEY_OFF_FLAG    0X10
#define KEY_LONG_FLAG   0X20


typedef enum
{
    BSP_EVENT_NOTHING = 0,                  /**< Assign this event to an action to prevent the action from generating an event (disable the action). */
    BSP_EVENT_DEFAULT,                      /**< Assign this event to an action to assign the default event to the action. */            
    BSP_EVENT_KEY_0,                        /**< Default event of the push action of BSP_BUTTON_0 (only if this button is present). */
    BSP_EVENT_KEY_1,                        /**< Default event of the push action of BSP_BUTTON_1 (only if this button is present). */
    BSP_EVENT_KEY_2,                        /**< Default event of the push action of BSP_BUTTON_2 (only if this button is present). */
    BSP_EVENT_KEY_3,                        /**< Default event of the push action of BSP_BUTTON_3 (only if this button is present). */
    BRU_BUTTON1_PUSH = BRU_BUTTON1_SHORT,
    BRU_BUTTON1_RELEASE = BRU_BUTTON1_SHORT|KEY_OFF_FLAG,
    BRU_BUTTON1_LONG_PUSH = BRU_BUTTON1_SHORT|KEY_LONG_FLAG,
    BRU_BUTTON2_PUSH = BRU_BUTTON2_SHORT,
    BRU_BUTTON2_RELEASE = BRU_BUTTON2_SHORT|KEY_OFF_FLAG,
    BRU_BUTTON2_LONG_PUSH = BRU_BUTTON2_SHORT|KEY_LONG_FLAG,
	  BRU_ENCODER_OUT_BUTTON_PUSH = BRU_ENCODER_OUT_BUTTON_SHORT,
    BRU_ENCODER_OUT_BUTTON_RELEASE = BRU_ENCODER_OUT_BUTTON_SHORT|KEY_OFF_FLAG,
    BRU_ENCODER_OUT_BUTTON_LONG_PUSH = BRU_ENCODER_OUT_BUTTON_SHORT|KEY_LONG_FLAG,	
    BRU_ENCODER_OUT_A_PUSH = BRU_ENCODER_OUT_A_SHORT,
    BRU_ENCODER_OUT_A_RELEASE = BRU_ENCODER_OUT_A_SHORT|KEY_OFF_FLAG,
    BRU_ENCODER_OUT_A_LONG_PUSH = BRU_ENCODER_OUT_A_SHORT|KEY_LONG_FLAG,
    BRU_ENCODER_OUT_B_PUSH = BRU_ENCODER_OUT_B_SHORT,
    BRU_ENCODER_OUT_B_RELEASE = BRU_ENCODER_OUT_B_SHORT|KEY_OFF_FLAG,
    BRU_ENCODER_OUT_B_LONG_PUSH = BRU_ENCODER_OUT_B_SHORT|KEY_LONG_FLAG,
} bsp_event_t;



typedef struct
{
    bsp_event_t push_event;      /**< The event to fire on regular button press. */
    bsp_event_t long_push_event; /**< The event to fire on long button press. */
    bsp_event_t release_event;   /**< The event to fire on button release. */
} bsp_button_event_cfg_t;


    

typedef uint8_t bsp_button_action_t; /**< The different actions that can be performed on a button. */

typedef void (* bsp_event_callback_t)(bsp_event_t);



/* Exported Constants ---------------------------------------------------------------------------*/

/* Exported Variables ---------------------------------------------------------------------------*/


/* Exported Functions ---------------------------------------------------------------------------*/
void bsp_key_init(bsp_event_callback_t callback);
void encode_outA_int_handler(nrfx_gpiote_pin_t pin, nrf_gpiote_polarity_t action);

#ifdef __cplusplus
}
#endif

#endif

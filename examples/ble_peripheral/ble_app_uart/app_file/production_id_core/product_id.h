#ifndef __PRODUCT_ID_H__
#define __PRODUCT_ID_H__
#include "sdk_config.h"
#include "stdint.h"
#include "stdbool.h"
#include "BSP_InnerFlash.h"
#include "BSP_OutFlash.h"


#define ID_BYTE_LENGTH  5


/**��ƷID��**/
typedef struct 
{
  uint8_t len;
  uint8_t data[ID_BYTE_LENGTH];
}product_id_t;




void readId(void);
void saveProductID(uint8_t *id);


uint32_t get_dev_id(void);


#endif

/**
  *************************************************************************************************
  * @file    : BSP_InnerFlash.h
  * @author  : xxx Team
  * @version : V0.0.1
  * @date    : 2017.10.10
  * @brief   : This is innerflash driver file.These functions should name begin as BSP_InnerFlash_xx().
  *************************************************************************************************
  */

/* Define to prevent recursive inclusion --------------------------------------------------------*/
#ifndef _BSP_INNERFLASH_H_
#define _BSP_INNERFLASH_H_

#ifdef __cplusplus
extern "C" {
#endif

/* Includes Files -------------------------------------------------------------------------------*/
#include "stdint.h"
#include "product_id.h"
#include "stddef.h"
#include "nrf_delay.h"
#include "nrf_fstorage.h"
#include "nrf_fstorage_sd.h"
#include "nrf_nvmc.h"

#if INNER_FLASH_ENABLED
/* Exported Macros ------------------------------------------------------------------------------*/
#define INNERFLASH_STR_ADDR     0x00077000 //
#define INNERFLASH_END_ADDR     0x00078000 //
#define INNERFLASH_PAG_SIZE     2048
#define RUNNOTE_BUFF_FLAG   0xAA5555aa  
#define RUNNOTE_BUFF_SIZE   (256)
#define RUNNOTE_INFO_SIZE   (8)
#define RUNNOTE_SAVE_ADDR   (0)
/* Define Types ---------------------------------------------------------------------------------*/

#ifndef U32_BLANK
#define U32_BLANK				(0xffffffff)
#endif

#define WRITE_ID_ADDR   (INNERFLASH_STR_ADDR-INNERFLASH_STR_ADDR)






/* Exported Types -------------------------------------------------------------------------------*/


/* Exported Functions ---------------------------------------------------------------------------*/
void BSP_InnerFlash_Init(void);
void BSP_InnerFlash_DeInit(void);
void BSP_InnerFlash_EraseBuff(uint32_t addr);


void write_data_to_flash(uint32_t addr,uint8_t *dat,uint8_t len);
void read_data_from_flash(uint32_t addr,uint8_t *dat,uint8_t len);


#endif


#ifdef __cplusplus
}
#endif

#endif
/* End of Flie ----------------------------------------------------------------------------------*/

/**
  *************************************************************************************************
  * @file    : BSP_InnerFlash.c
  * @author  : xxx Team
  * @version : V0.0.1
  * @date    : 2017.10.10
  * @brief   : This is innerflash driver file.These functions should name begin as BSP_InnerFlash_xx().
  *************************************************************************************************
  */

/* Includes Files -------------------------------------------------------------------------------*/

#include "BSP_InnerFlash.h"


#if INNER_FLASH_ENABLED

#define NRF_LOG_MODULE_NAME Inner_flash
#if ANT_HRM_LOG_ENABLED
#define NRF_LOG_LEVEL       3
#define NRF_LOG_INFO_COLOR  3
#else // ANT_HRM_LOG_ENABLED
#define NRF_LOG_LEVEL       0
#endif // ANT_HRM_LOG_ENABLED
#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();


/* Define Macros --------------------------------------------------------------------------------*/




/* Define Types ---------------------------------------------------------------------------------*/

/* Define Constants -----------------------------------------------------------------------------*/
void fs_evt_handler(nrf_fstorage_evt_t *p_evt);

NRF_FSTORAGE_DEF(nrf_fstorage_t flash_cfg) =
{
    .evt_handler    = fs_evt_handler,
    .start_addr     = INNERFLASH_STR_ADDR,
    .end_addr       = INNERFLASH_END_ADDR,
};

/* Define Variables -----------------------------------------------------------------------------*/



/**------------------------------------------------------------------------------------------------
  * @brief  : This is innerflash erase function
  * @param  : None
  * @retval : None
  *----------------------------------------------------------------------------------------------*/
void BSP_InnerFlash_EraseBuff(uint32_t addr)
{
  
    addr = (INNERFLASH_STR_ADDR + addr) & 0xfffff000;                
   
    nrf_fstorage_erase(&flash_cfg, addr, 1, NULL);
    while(nrf_fstorage_is_busy(&flash_cfg));
    nrf_delay_ms(40);
}






/**------------------------------------------------------------------------------------------------
  * @brief  : This is innerflash write function
  * @param  : None
  * @retval : None
  *----------------------------------------------------------------------------------------------*/
void BSP_InnerFlash_WriteBuff(uint32_t addr, const uint8_t *buff, uint32_t size)
{
    

    addr = (INNERFLASH_STR_ADDR + addr + 3) / 4 * 4;        
    //buff = (buff + 3) / 4 * 4;                            
    size = (size + 3) / 4 * 4;                              



     nrf_fstorage_write(&flash_cfg, addr, (uint32_t *)buff, size, NULL);
     while(nrf_fstorage_is_busy(&flash_cfg));


   
}


/**------------------------------------------------------------------------------------------------
  * @brief  : This is innerflash read function
  * @param  : None
  * @retval : None
  *----------------------------------------------------------------------------------------------*/
void BSP_InnerFlash_ReadBuff(uint32_t addr, uint8_t *buff, uint32_t size)
{
    
    
    addr = (INNERFLASH_STR_ADDR + addr + 3) / 4 * 4;        


    nrf_fstorage_read(&flash_cfg, addr, buff, size);
		//NRF_LOG_INFO("flash read rt: %d",rt);
    while(nrf_fstorage_is_busy(&flash_cfg));

   
}


void write_data_to_flash(uint32_t addr,uint8_t *dat,uint8_t len)
{
	uint8_t temp[len]; 
	memcpy(&temp,dat,len);
	BSP_InnerFlash_EraseBuff(addr);
	BSP_InnerFlash_WriteBuff(addr,temp,len);
	
}

void read_data_from_flash(uint32_t addr,uint8_t *dat,uint8_t len)
{
	uint8_t temp[len]; 
	BSP_InnerFlash_ReadBuff(addr,temp,len);
	memcpy(dat,temp,len);
}



/**------------------------------------------------------------------------------------------------
  * @brief  : This is innerflash init function
  * @param  : None
  * @retval : None
  *----------------------------------------------------------------------------------------------*/
void BSP_InnerFlash_Init(void)
{
	uint32_t rt=0;
#if (FDS_BACKEND == 0)
    //code here
#else
    rt = nrf_fstorage_init(&flash_cfg, &nrf_fstorage_sd, NULL);
	
	NRF_LOG_INFO("fs init rt : %d",rt);
    while(nrf_fstorage_is_busy(&flash_cfg));
#endif
}

/**------------------------------------------------------------------------------------------------
  * @brief  : This is innerflash deinit function
  * @param  : None
  * @retval : None
  *----------------------------------------------------------------------------------------------*/
void BSP_InnerFlash_DeInit(void)
{
#if (FDS_BACKEND == 0)
    //code here
#else
    nrf_fstorage_uninit(&flash_cfg, NULL);
    while(nrf_fstorage_is_busy(&flash_cfg));
#endif
}


/* Define Functions -----------------------------------------------------------------------------*/
void fs_evt_handler(nrf_fstorage_evt_t *p_evt)
{
    //code here
     if (p_evt->result != NRF_SUCCESS)
    {
        NRF_LOG_INFO("-->ERROR while executing an fstorage operation.\n");
        return;
    }
    switch (p_evt->id)
    {
        case NRF_FSTORAGE_EVT_WRITE_RESULT:
        {
			
            NRF_LOG_INFO("wrote %d bytes at address 0x%x.\n", p_evt->len, p_evt->addr);
                        
        } break;

        case NRF_FSTORAGE_EVT_ERASE_RESULT:
        {
           NRF_LOG_INFO("erased address 0x%x.\n",p_evt->addr);
                         
        } break;

        default:
            break;
    }
}



void readInnerFlashData(uint8_t *dat,uint16_t len)
{
		read_data_from_flash(WRITE_ID_ADDR,dat,len);
    NRF_LOG_INFO("readInnerFlashData call in BSP_InnerFlash.c");
}

/**
* @brieaf 
*/		
void writeInnerFlashData(uint8_t *dat,uint16_t len)
{
	 write_data_to_flash(WRITE_ID_ADDR,dat,len);
	 NRF_LOG_INFO("writeInnerFlashData call in BSP_InnerFlash.c");
}

#endif


#include "product_id.h"



#define NRF_LOG_MODULE_NAME product_id
#if ANT_HRM_LOG_ENABLED
#define NRF_LOG_LEVEL       3
#define NRF_LOG_INFO_COLOR  3
#else // ANT_HRM_LOG_ENABLED
#define NRF_LOG_LEVEL       0
#endif // ANT_HRM_LOG_ENABLED
#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();


static product_id_t product_id;

//--------------read production ID from flash-----------------------------------------
void readId(void)
{


#if INNER_FLASH_ENABLED	 
	 read_data_from_flash(WRITE_ID_ADDR,(uint8_t*)&product_id,sizeof(product_id_t));
#else
	readFlashData(DEVICE_ID_ADDR,(uint8_t*)&product_id,sizeof(product_id_t),FAST_READ);
#endif
	 NRF_LOG_INFO("===========product_id.len: %d\n",product_id.len);
//	 NRF_LOG_HEXDUMP_INFO((uint8_t*)&product_id,sizeof(product_id_t));
	 if((product_id.len == 0xff) || (product_id.len == 0))
	 {
		  product_id.len = 5;
		  product_id.data[0] =0;
		  product_id.data[1] =0;
		  product_id.data[2] =0;
		  product_id.data[3] =0;
		  product_id.data[4] =1;
	 }
	 
}

/**
* @brieaf get device ID
*/
uint32_t get_dev_id(void)
{
	uint32_t tmp_dev_id = ((uint32_t)product_id.data[1]<<24)|
                   ((uint32_t )product_id.data[2]<<16) |
									 ((uint32_t )product_id.data[3]<<8) |
                   (uint32_t )product_id.data[4];  
	
	return tmp_dev_id;
}

//-------------save prodution ID to flash------------------------------------------
void saveProductID(uint8_t *id)
{
	product_id.len = id[0];
	for(uint32_t i=0;i<ID_BYTE_LENGTH;i++)
	{
	  product_id.data[i] = id[i+1];
	  NRF_LOG_INFO("ID[%d]=%x",i,id[i+1]);
	}
#if INNER_FLASH_ENABLED	 
	write_data_to_flash(WRITE_ID_ADDR,(uint8_t*)&product_id,sizeof(product_id_t));
#else
	writeIDToFlash(&product_id);
#endif
}













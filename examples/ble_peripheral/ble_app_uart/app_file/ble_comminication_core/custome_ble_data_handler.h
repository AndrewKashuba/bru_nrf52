#ifndef __CUSTOME_BLE_DATA_HANDLER_H__
#define __CUSTOME_BLE_DATA_HANDLER_H__
#include "stdint.h"
#include "nrf_soc.h"
#include "ble_nus.h"
#include "target_command.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct 
{
	uint8_t data[NRF_SDH_BLE_GATT_MAX_MTU_SIZE];
	uint8_t len;
}BLE_DATA_INFO_T;


typedef enum
{
	
	CONFIG_UTC = 0X08,
	ENTER_DFU = 0x27,
	WRITE_DEVICE_ID_CMD = 0x29,
	RESET_FACTORY = 0X30,
  MACHINE_SETUP = 0XD0,
	SET_ALARM = 0XC0,
	SET_PRESET = 0XB0,
	STSP_BREWING = 0XB1,
	GET_PRESET_INFO = 0XB2,
	GET_ALARM_INFO = 0XB3,
	GET_CURRENT_TIME = 0XB4,
	GET_ERROR_STATUS = 0XB5,
	GET_MACHINE_INFO = 0XB6,
	GET_BRU_STATUS = 0XB7,
	GET_BRU_WORK_STATUS = 0XB8,
	PRODUCE_RESET=0XF3,

	DATA_HEAD = 0xff
	
}DATA_COMMAND_PACKAGE;

extern volatile uint8_t isBleBusy;

uint8_t get_checksum(uint8_t const*dat, uint8_t len);
void nus_data_handler(ble_nus_evt_t * p_evt);
void notify_gps_data(uint64_t lng,uint64_t lan,uint8_t N,uint8_t E,uint8_t A);
uint32_t send_data_command(uint8_t cmd,uint8_t *data,uint8_t len);

void get_bru_device_status(void);
void get_machine_setup_info(void);
void get_error_code(void);
void get_current_time_handler(void);
void get_alarm_time_handler(void);
void get_all_preset_info(void);
void set_bru_stsp_brewing_handler(void *p);
void set_preset_handler(void *p);
void set_alarm_handler(void *p);
void machine_setup_handler(void *p);
void get_bru_work_status_handler(void);
#ifdef __cplusplus
}
#endif

#endif


#include "custome_ble_data_handler.h"
#include "main.h"
#include "product_id.h"
#include "bru_data.h"
#include "pwm_rgbb_core.h"
#include "record_data_core.h"
#include "bru_ui_disp.h"

#define NRF_LOG_MODULE_NAME ble_communication_core
#define NRF_LOG_LEVEL       3
#define NRF_LOG_INFO_COLOR  4
#define NRF_LOG_DEBUG_COLOR 3
#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();

#define DFU_TIMER_ID				551
#define DFU_TIMER_VALUE     (TIMER_CALL_HZ/2)

#define BLE_BUF_LENGTH  4


static BLE_DATA_INFO_T s_ble_buf[BLE_BUF_LENGTH];
static uint8_t s_ble_index;
static void replyToApp(uint8_t cmd);
volatile uint8_t isBleBusy;



static const uint8_t sync_utc_evt = CONFIG_UTC;
static const uint8_t enter_dfu_evt = ENTER_DFU;
static const uint8_t write_id_evt = WRITE_DEVICE_ID_CMD;
static const uint8_t reset_factory_evt = RESET_FACTORY;
static const uint8_t machine_setup_evt = MACHINE_SETUP;
static const uint8_t set_alarm_evt = SET_ALARM;
static const uint8_t set_preset_evt = SET_PRESET;
static const uint8_t stsp_brewing_evt = STSP_BREWING;
static const uint8_t get_preset_info_evt = GET_PRESET_INFO;
static const uint8_t get_alarm_info_evt = GET_ALARM_INFO;
static const uint8_t get_current_time_evt = GET_CURRENT_TIME;
static const uint8_t get_error_status_evt = GET_ERROR_STATUS;
static const uint8_t get_machine_info_evt = GET_MACHINE_INFO;
static const uint8_t get_bru_status_evt = GET_BRU_STATUS;
static const uint8_t product_reset_evt = PRODUCE_RESET;
static const uint8_t get_bru_work_status_evt = GET_BRU_WORK_STATUS;


static uint8_t  sendBufferPlus[NRF_SDH_BLE_GATT_MAX_MTU_SIZE]; 


static void dfu_handler(void);
static void write_id_handler(void *p);
static void sync_utc_handler(void *p);
static void reset_device_handler(void);
static void restore_factory_handler(void);
void get_bru_device_status(void);
void get_machine_setup_info(void);
void get_error_code(void);
void get_current_time_handler(void);
void get_alarm_time_handler(void);
void get_all_preset_info(void);
void set_bru_stsp_brewing_handler(void *p);
void set_preset_handler(void *p);
void set_alarm_handler(void *p);
void machine_setup_handler(void *p);
void get_bru_work_status_handler(void);


BEGIN_MESSAGE_MAP(ble_Function_ModeFuncEn)
    ON_VOID_MESSAGE(enter_dfu_evt,dfu_handler)
		ON_VOID_POINTER_MESSAGE(write_id_evt,write_id_handler)
		ON_VOID_POINTER_MESSAGE(sync_utc_evt,sync_utc_handler)
		ON_VOID_MESSAGE(product_reset_evt,reset_device_handler)
		ON_VOID_MESSAGE(reset_factory_evt,restore_factory_handler)
		ON_VOID_MESSAGE(get_bru_status_evt,get_bru_device_status)
		ON_VOID_MESSAGE(get_machine_info_evt,get_machine_setup_info)
		ON_VOID_MESSAGE(get_error_status_evt,get_error_code)
		ON_VOID_MESSAGE(get_current_time_evt,get_current_time_handler)
		ON_VOID_MESSAGE(get_alarm_info_evt,get_alarm_time_handler)
		ON_VOID_MESSAGE(get_preset_info_evt,get_all_preset_info)
		ON_VOID_POINTER_MESSAGE(stsp_brewing_evt,set_bru_stsp_brewing_handler)
		ON_VOID_POINTER_MESSAGE(set_preset_evt,set_preset_handler)
		ON_VOID_POINTER_MESSAGE(set_alarm_evt,set_alarm_handler)
		ON_VOID_POINTER_MESSAGE(machine_setup_evt,machine_setup_handler)
		ON_VOID_MESSAGE(get_bru_work_status_evt,get_bru_work_status_handler)
END_MESSAGE_MAP(NULL)   /* if END_MESSAGE_MAP param is NULL,then msg isn't match,no default handler */


INIT_MSG_ENTRIES(ble_Function_ModeFuncEn,ble_Function_cmd_func,NULL)  

CONST MENU_KEY ble_table = {&ble_Function_cmd_func,NULL,NULL};


static void set_update_disp_flag(void)
{
	DISP_FLAG_T *p = get_disp_flag();
	p->flush_disp_flag = 1;
}

/**--------------------------------------------------------------------------
*  @breaf enter dfu mode by ble command         
*----------------------------------------------------------------------------*/

static void enterDFU(void *p)
{
	  sd_power_gpregret_clr(0,0xffffffff); 
	  sd_power_gpregret_set(0,0xb1);
	  NVIC_SystemReset();
}

static void dfu_handler(void)
{
	 replyToApp(ENTER_DFU);
	 register_timer(DFU_TIMER_ID,enterDFU);
	 set_timer_value(DFU_TIMER_ID,DFU_TIMER_VALUE,NULL);
						
}

/**
* @brieaf write id
*/
static void write_id_handler(void *p)
{
	 BLE_DATA_INFO_T *pData = p;
	 uint8_t *dat = pData->data;
	
		saveProductID(&dat[3]);
		replyToApp(WRITE_DEVICE_ID_CMD); 		
	
}

/**
* @brieaf sync utc
*/
static void sync_utc_handler(void *p)
{
	  BLE_DATA_INFO_T *pData = p;
	  uint8_t *dat = pData->data;
		uint32_t utc = paserData(&dat[3],4);	
		utc_set(utc);
		NRF_LOG_INFO("CONFIG_UTC %d",utc);
		replyToApp(CONFIG_UTC);  
	  set_update_disp_flag();
}


/**
* @brieaf reset device
*/
static void reset_device_handler(void)
{
	  NVIC_SystemReset();  			//device reset
}

/**
* @brieaf restore factory
*/
static void restore_factory_handler(void)
{
	
}

/**
* @brieaf get bru device status
*/
void get_bru_device_status(void)
{
	bru_UI_Interface_Handle p = get_ui_interface();
	uint8_t status = p->bru_device_status;
	send_data_command(get_bru_status_evt,&status,1);
}


/**
* @brieaf get machine setup info
*/
void get_machine_setup_info(void)
{
	bru_Machine_Setup_Handle p = get_ui_interface()->hMachineSetup;
	uint8_t buf[9];
	buf[0] = p->bluetoothState;
	buf[1] = p->units;
	buf[2] = p->rinseConfiguration;
	buf[3] = p->timeformat;
	buf[4] = p->buzzerState;
	buf[5] = p->redLed;
	buf[6] = p->greenLed;
	buf[7] = p->blueLed;
	buf[8] = p->language;
	send_data_command(get_machine_info_evt,buf,sizeof(buf)/sizeof(buf[0]));
}


/**
* @brieaf get error code
*/
void get_error_code(void)
{
	uint8_t error_code = get_ui_interface()->errorCode;
	send_data_command(get_error_status_evt,&error_code,1);
}

/**
* @brieaf parse u32 to buf and send data by ble
*/
static void parse_u32_to_buf(uint8_t cmd,uint32_t utc)
{
  uint8_t buf[4];
  buf[0] = (uint8_t)(utc >> 24);
  buf[1] = (uint8_t)(utc >> 16);
  buf[2] = (uint8_t)(utc >> 8);
  buf[3] = (uint8_t)utc;        
  send_data_command(cmd,buf,4);
}

/**
* @brieaf get current time handler
*/
void get_current_time_handler(void)
{
	parse_u32_to_buf(get_current_time_evt,get_ui_interface()->iCurrentTime);
}


/**
* @brieaf get alarm information
*/
void get_alarm_time_handler(void)
{
        unix_cal unix_time;
        timer_to_cal(get_ui_interface()->iAlarmTime,&unix_time);
        uint8_t buf[2];
  buf[0] = unix_time.hour;
  buf[1] = unix_time.min;
        send_data_command(get_alarm_info_evt,buf,2);
}

/**
* @brieaf get all preset setup information
*/
void get_all_preset_info(void)
{
	uint8_t buf[BRU_DEFAULT_PRESET_ARR_SIZE*5];
	bru_Preset_Arr pData = get_ui_interface()->presetsArr;
	uint8_t *pBuf = buf;
	for(uint8_t i=0; i<BRU_DEFAULT_PRESET_ARR_SIZE; i++)
	{
		*pBuf++ = (uint8_t)pData[i].temperature;
		*pBuf++ = (uint8_t)(pData[i].brewingTime >> 8);
		*pBuf++ = (uint8_t)pData[i].brewingTime;
		*pBuf++ = (uint8_t)((uint32_t)pData[i].waterAmount >> 8);
		*pBuf++ = (uint8_t)pData[i].waterAmount;
	}
	send_data_command(get_preset_info_evt,buf,sizeof(buf)/sizeof(buf[0]));
}

/**
* @brieaf start or stop brewing operation
*/
__WEAK void start_or_stop_operation(uint8_t index,uint8_t stsp)
{
	
}

/**
* @brieaf get bru work status
*/
void set_bru_stsp_brewing_handler(void *data) 
{
	BLE_DATA_INFO_T *pData = data;
	uint8_t *dat = pData->data;
	uint8_t buf[2];
	bru_UI_Interface_Handle p = get_ui_interface();

	
	if(dat[4] && p->bBrewingProcessFlag)
	{
		buf[1] = 2;
		buf[0] = p->iPresetCurrentIndex;
	}
	else
	{ 
		
//		p->bBrewingStartFlag = dat[4];
//		p->iPresetCurrentIndex = dat[3];
		buf[1] = dat[4];
		buf[0] = dat[3];
//		set_update_disp_flag();
		start_or_stop_operation(dat[3],dat[4]);
	}
	
	send_data_command(stsp_brewing_evt,buf,sizeof(buf)/sizeof(buf[0]));
	
}

/**
* @brieaf set preset setup handler
*/
void set_preset_handler(void *p)
{
	BLE_DATA_INFO_T *pData = p;
	uint8_t *dat = pData->data;
	bru_UI_Interface_Handle pUI = get_ui_interface();
	uint8_t result = 0;
	if(pUI->bBrewingStartFlag)
	{
		result = 2;
	}
	else if(dat[3] >= BRU_DEFAULT_PRESET_ARR_SIZE)
	{
		result = 1;
	}
	else
	{
		
		bru_Preset_Arr presetData = &pUI->presetsArr[dat[3]];
		presetData->temperature = dat[4];
		presetData->brewingTime = paserData(&dat[5],2);
		presetData->waterAmount = paserData(&dat[7],2);
		write_record_info_to_flash();
	  set_update_disp_flag();
	}
	send_data_command(set_preset_evt,&result,1);
}


/**
* @brieaf set alarm info
*/
void set_alarm_handler(void *p)
{
	BLE_DATA_INFO_T *pData = p;
	uint8_t *dat = pData->data;
	bru_UI_Interface_Handle pUI = get_ui_interface();
	uint8_t result = 0;
	if(pUI->bBrewingStartFlag)
	{
		result = 2;
	}
	else if((dat[3] > 23) || (dat[4] > 59))
	{
		result = 1;
	}
	else
	{
		uint32_t t1 = dat[3];
		uint32_t t2 = dat[4];
    t1 *= 3600;
    t2 *=60;	
    t1 += t2;		
		pUI->iAlarmTime = t1;
		write_record_info_to_flash();
	  set_update_disp_flag();
	}
	send_data_command(set_alarm_evt,&result,1);
}

/**
* @brieaf machine setup handler
*/
void machine_setup_handler(void *p)
{
	BLE_DATA_INFO_T *pData = p;
	uint8_t *dat = pData->data;
	bru_UI_Interface_Handle pUI = get_ui_interface();
	bru_Machine_Setup_Handle pMachine = pUI->hMachineSetup;
	uint8_t result = 0;
	if(pUI->bBrewingStartFlag)
	{
		result = 2;
	}
	else if(dat[11] != 0)
	{
		result = 1;
	}
	else
	{
		
		pMachine->bluetoothState = (bru_MS_Bletooth_Sate)dat[3];
		pMachine->units = (bru_MS_Units)dat[4];
		pMachine->rinseConfiguration = (bru_MS_Rinse_Config)dat[5];
		pMachine->timeformat = (bru_TimeFormat)dat[6];
		pMachine->buzzerState = dat[7];
		pMachine->redLed =   dat[8];
		pMachine->greenLed = dat[9];
		pMachine->blueLed =  dat[10];
		pMachine->language = (bru_MS_Language)dat[11];
		bru_ble_on_off_ENG();
		set_rgb_duty(pMachine->redLed,pMachine->greenLed,pMachine->blueLed);
		write_record_info_to_flash();
		set_update_disp_flag();
	}
	send_data_command(machine_setup_evt,&result,1);
}


/**
* @brieaf get bru work status
*/
void get_bru_work_status_handler(void)
{
	
	uint8_t buf[2];
	bru_UI_Interface_Handle p = get_ui_interface();

	buf[0] = p->iPresetCurrentIndex;
	buf[1] = p->bBrewingStartFlag;
	
	if(p->bBrewingStartFlag)
	{
		buf[1] = 2;
	
	}
	send_data_command(get_bru_work_status_evt,buf,sizeof(buf)/sizeof(buf[0]));
}

/**
* @brieaf send data by ble
*/
uint32_t send_data_command(uint8_t cmd,uint8_t *data,uint8_t len)
{
	 
	 sendBufferPlus[0] = 0xff;
	 sendBufferPlus[1] = 4;
	 sendBufferPlus[2] = cmd;
	 if(data != NULL)
	 {
		 for(uint8_t i=0; i<len; i++)
		 {
			  sendBufferPlus[3+i] = data[i];
		 }
		 sendBufferPlus[1] += len;
	 }
	 len = sendBufferPlus[1]-1;
	 sendBufferPlus[len] = get_checksum(sendBufferPlus,len);
	return ble_updateData(sendBufferPlus,sendBufferPlus[1]);  
	
}

/**
* @brieaf reply command to app
*/
static void replyToApp(uint8_t cmd)
{
	    sendBufferPlus[0] = 0xff;
			sendBufferPlus[1] = 4;
		  sendBufferPlus[2] = cmd; 
	    sendBufferPlus[3] = get_checksum(sendBufferPlus,3);
		  ble_updateData(sendBufferPlus,4);  
}





/**@brief Function for handling the data from the Nordic UART Service.
 *
 * @details This function will process the data received from the Nordic UART BLE Service and send
 *          it to the UART module.
 *
 * @param[in] p_evt       Nordic UART Service event.
 */
/**@snippet [Handling the data received over BLE] */
void nus_data_handler(ble_nus_evt_t * p_evt)
{
 //   NRF_LOG_INFO("nus_data_handler");
	
	  switch(p_evt->type)
		{
			case BLE_NUS_EVT_RX_DATA:		
      {		
       
					 uint8_t const *dat = p_evt->params.rx_data.p_data;
					 uint8_t len = p_evt->params.rx_data.length;
				// check header
					if(dat[0] != 0xff)
						return;
				 // check valid length
					 if((len < 4)||(len > NRF_SDH_BLE_GATT_MAX_MTU_SIZE))
						 return;
					 // check valid length
					 if(len != dat[1])
						 return;
				//check checksum 
					if(dat[len-1] != get_checksum(dat,len-1))
						return;				
					memcpy(s_ble_buf[s_ble_index].data,p_evt->params.rx_data.p_data,p_evt->params.rx_data.length);
					s_ble_buf[s_ble_index].len = p_evt->params.rx_data.length;	
					PostMessage((void*)&s_ble_buf[s_ble_index].data[2],(void*)&ble_table,&s_ble_buf[s_ble_index]);
					if(++s_ble_index >= BLE_BUF_LENGTH)
					{
						s_ble_index = 0;
					}
			}
				break;
		  case BLE_NUS_EVT_TX_RDY:
          isBleBusy=0;
				break;
			default:
				break;
			
		}

}



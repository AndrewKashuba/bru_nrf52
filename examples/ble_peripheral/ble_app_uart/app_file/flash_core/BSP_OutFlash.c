#include "BSP_OutFlash.h"
#include "product_id.h"

#if !INNER_FLASH_ENABLED

#define NRF_LOG_MODULE_NAME BSP_OutFlash
#define NRF_LOG_LEVEL       3
#define NRF_LOG_INFO_COLOR  4
#define NRF_LOG_DEBUG_COLOR 3
#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();




/**------------------------------------------------------------------------------------------------
* @brief  : reset flash
  *----------------------------------------------------------------------------------------------*/
static void resetFlash(void)
{
	sendFlashCommand(ENABLE_RESET);
	sendFlashCommand(RESET_FLASH);
	nrf_delay_ms(20);
}

/**
* @brieaf flash initial
*/
bool flash_init(void)
{
		nrf_gpio_cfg_output(FLASH_SCLK);
    nrf_gpio_pin_set(FLASH_SCLK);
		nrf_gpio_cfg_output(FLASH_SO);
    nrf_gpio_pin_set(FLASH_SO);
    nrf_gpio_cfg_output(OUT_FLASH_CS_PIN);
    nrf_gpio_pin_set(OUT_FLASH_CS_PIN);
	  
    spi_master_cfg_t spi_cfg = SPI_MASTER_CFG_DEFAULT;

    spi_cfg.SPI_Freq         = NRF_DRV_SPI_FREQ_8M;
    spi_cfg.SPI_Pin_SCK      = FLASH_SCLK;
    spi_cfg.SPI_Pin_MISO     = FLASH_SO;
    spi_cfg.SPI_Pin_MOSI     = FLASH_SI;
    

    spi1_init(&spi_cfg);
		nrf_gpio_cfg_input(FLASH_SI, NRF_GPIO_PIN_PULLUP);
		
    resetFlash();
	
	  return releaseDeepPowerDownID();
}







/**------------------------------------------------------------------------------------------------
* @brief  : write command to flash
  *----------------------------------------------------------------------------------------------*/
void sendFlashCommand(E_OUT_FLASH cmd)
{
	nrf_gpio_pin_clear(OUT_FLASH_CS_PIN);
	
	uint8_t buf;
	buf = cmd;
	spi_writeData(&buf,1);
	nrf_gpio_pin_set(OUT_FLASH_CS_PIN);

}






/**------------------------------------------------------------------------------------------------
  * @brief  : The Read Status Register (RDSR) command is for reading the Status Register. 
  * The Status Register may be read at any time, even while a Program, Erase or Write 
  * Status Register cycle is in progress
  * @param  : NONE
  * @retval : flash status
  *---------------------------------------------------------------------------------------------*/
static uint8_t readStatusRegister(void)
{
	nrf_gpio_pin_clear(OUT_FLASH_CS_PIN);
	
	uint8_t buf;
	buf = READ_STATUS_REG_05;

	spi_writeData(&buf,1);
	spi_readData(&buf,1);
	
	nrf_gpio_pin_set(OUT_FLASH_CS_PIN);
	
	return buf & FLASH_BUSY_WIP;
}

//---------------flash busy check------------------------
static uint8_t checkFlashBusy(uint32_t timeout)
{
	uint8_t status;
	uint32_t cnt = timeout; 
	do
	{
		status = readStatusRegister();
		BSP_WDT_Feed();	
    nrf_delay_us(600);		
	}while(status && --cnt);
	
	return status;
}

//----------------fill address info.----------------------------------
static void writeAddrCmdData(uint32_t addr,E_OUT_FLASH cmd)
{
	uint8_t buf[4];
	buf[0] = cmd;
	// 24bit address is 0x000000
	buf[1] = (addr >> 16) & 0xff;   
	buf[2] = (addr >> 8) & 0xff;	 
	buf[3] = addr & 0xff;         
	
	spi_writeData(buf,4);
}


/**------------------------------------------------------------------------------------------------
  * @brief  : read data from flash
  *----------------------------------------------------------------------------------------------*/
void readFlashData(uint32_t addr,uint8_t *out,uint16_t len,E_OUT_FLASH cmd)
{
	nrf_gpio_pin_clear(OUT_FLASH_CS_PIN);
	 nrf_delay_us(3);
	writeAddrCmdData(addr,cmd);
	if(cmd == FAST_READ)
	{
		uint8_t dummy = 0;
		spi_writeData(&dummy,1);
	}
	spi_readData(out,len);
	nrf_gpio_pin_set(OUT_FLASH_CS_PIN);
	nrf_delay_us(3);
}

/**------------------------------------------------------------------------------------------------
  * @brief  : wakeup flash and read flash id
  *----------------------------------------------------------------------------------------------*/
bool releaseDeepPowerDownID(void)
{
	uint8_t buf;
	readFlashData(0,&buf,1,RELEASE_DEEP_POWER_DOWN);   // 0 is dummy data
	NRF_LOG_INFO("Flash id=%x",buf);
 // id is 0x14 or 0x13
	return (buf == FLASH_DEVICE_ID) || (buf == FLASH_8Mbit_ID) || (buf == FLASH_ID);
}

/**------------------------------------------------------------------------------------------------
* @brief  : page program
  *----------------------------------------------------------------------------------------------*/
static void pageProgram(uint32_t addr,uint8_t *out,uint16_t len)
{
	checkFlashBusy(30); 

	sendFlashCommand(WRITE_ENABLE); 
	checkFlashBusy(30); 

	nrf_gpio_pin_clear(OUT_FLASH_CS_PIN);
	writeAddrCmdData(addr,PAGE_PROGRAM);

	spi_writeData(out,len);
	nrf_gpio_pin_set(OUT_FLASH_CS_PIN);	

	sendFlashCommand(WRITE_DISABLE);  
	
	checkFlashBusy(30); 
}




/**------------------------------------------------------------------------------------------------
* @brief  : erase one sector
  *----------------------------------------------------------------------------------------------*/
void eraseSectorFlash(uint32_t addr)
{
	checkFlashBusy(30); 	

	sendFlashCommand(WRITE_ENABLE); 
	checkFlashBusy(30); 
	nrf_gpio_pin_clear(OUT_FLASH_CS_PIN);
	writeAddrCmdData(addr,SECTOR_ERASER);
	nrf_gpio_pin_set(OUT_FLASH_CS_PIN);
	
	sendFlashCommand(WRITE_DISABLE);  
	checkFlashBusy(666); 
}



/**------------------------------------------------------------------------------------------------
* @brief  : write data to flash
  *----------------------------------------------------------------------------------------------*/
void writeDataForCount(uint32_t count,uint8_t *pDat,uint16_t len)
{
	uint16_t mod = count & 0xff;  
	uint16_t total;
  total	= mod + len;
	uint16_t div = 256-mod;
	
	if(total > 256)
	{
		pageProgram(count,pDat,div);
		count += div;
		len -= div;
		pageProgram(count,&pDat[div],len);
	}
	else
	{
		pageProgram(count,pDat,len);
	}
	
}







/**------------------------------------------------------------------------------------------------
* @brief  : write device id
  *----------------------------------------------------------------------------------------------*/
void writeIDToFlash(void *id)
{
 
	eraseSectorFlash(ERASER_ALL_FLAG_ADDR);
	uint32_t buf = ERASER_ALL_FLAG;
	writeDataForCount(ERASER_ALL_FLAG_ADDR,(uint8_t*)&buf,sizeof(uint32_t));  
	writeDataForCount(DEVICE_ID_ADDR,(uint8_t*)id,sizeof(product_id_t)); 
}



/**------------------------------------------------------------------------------------------------
* @brief  :  eraser all flash 
  *----------------------------------------------------------------------------------------------*/
void restetFactory(void)
{
	product_id_t id;
	readFlashData(DEVICE_ID_ADDR,(uint8_t*)&id,sizeof(product_id_t),FAST_READ);

  writeIDToFlash(&id);
	
	for(uint32_t i=DATA_BEGIN_ADDR; i<(ERASER_ALL_FLAG_ADDR-0x1000); i+= 0x1000)
	{
		eraseSectorFlash(i);
	}
  
}




/**
* @brief API ERROR CODE record
*/
void write_error_code(uint32_t addr,void* dat,uint8_t len)
{
	eraseSectorFlash(addr);
	writeDataForCount(addr,dat,len);
}

/**
* @brief read error code 
*/
void read_error_code(uint32_t addr,void* dat,uint8_t len)
{
	readFlashData(addr,dat,len,FAST_READ);
}

#endif

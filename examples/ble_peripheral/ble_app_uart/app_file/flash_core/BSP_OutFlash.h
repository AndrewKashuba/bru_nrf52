/**
  *************************************************************************************************
  * @file    : BSP_Output.h
  * @author  : xxx Team
  * @version : V0.0.1
  * @date    : 2017.10.10
  * @brief   : This is output driver file.These functions should name begin as BSP_Output_xx().
  *************************************************************************************************
  */

/* Define to prevent recursive inclusion --------------------------------------------------------*/
#ifndef _BSP_OUTFLASH_H_
#define _BSP_OUTFLASH_H_
#include "BSP_SPI1.h"
#include "BSP_wdt.h"


#ifdef __cplusplus
extern "C" {
#endif



#if !INNER_FLASH_ENABLED

typedef struct
{
	 uint8_t age;                    /*!< in years */
	 uint8_t sex;               		 /*!< Male/Femal*/
   uint8_t userInfo;               /*!< Male/Femal, Smoker: Y/N. Medicated: Y/N; a definition of the bit masks can be found in RP7_INT_BLE-Protocol */
   uint8_t weight;                 /*!< in kg */
   uint8_t height;       
	 uint32_t user_id;
	 uint32_t user_id2;
}USER_ID_INFO_T;



#define OUT_FLASH_CS_PIN    22
#define FLASH_SI		24
#define FLASH_SO    20
#define FLASH_SCLK  23
 


#define FLASH_BUSY_WIP					   0X01

#define FLASH_ERROR			0X01
#define FALSH_OK				0X00
#define FLASH_DEVICE_ID 0X17
#define FLASH_8Mbit_ID  0X13
#define FLASH_ID        0x15



#define FLASH_OFFSET_SIZE    0x000000
#define DATA_BEGIN_ADDR			 (0+FLASH_OFFSET_SIZE)


#define ERASER_ALL_FLAG_ADDR (0XFFF000UL+FLASH_OFFSET_SIZE)  //第256页的首地址
#define ERASER_ALL_FLAG   	 0X5533DD53  //是否擦除flash数据
 
#define DEVICE_ID_ADDR    (ERASER_ALL_FLAG_ADDR+4+sizeof(USER_ID_INFO_T))

#define UINT32T_SPACE_BLANK			(uint32_t)0xffffffff




typedef enum
{
	READ_STATUS_REG_05 = 0X05,   //只使用低8位的状态值，高8位0x35命令未用
	WRITE_DISABLE = 0X04,
	WRITE_ENABLE	= 0X06,
	READ_DEVICE_ID = 0x90,
  READ_DATA = 0X03,
	DEEP_POWER_DOWN = 0XB9,
	RELEASE_DEEP_POWER_DOWN = 0XAB,
	ENABLE_RESET = 0X66,
	RESET_FLASH = 0X99,
	PAGE_PROGRAM = 0X02,
	CHIP_ERASE = 0Xc7,
	FAST_READ = 0X0B,
	SECTOR_ERASER = 0X20,
	
}E_OUT_FLASH;


//###########################################################################################

uint8_t readStatusRegister(void);
void readFlashData(uint32_t addr,uint8_t *out,uint16_t len,E_OUT_FLASH cmd);
void sendFlashCommand(E_OUT_FLASH cmd);
bool releaseDeepPowerDownID(void);


void eraseSectorFlash(uint32_t addr);
void writeDataForCount(uint32_t count,uint8_t *pDat,uint16_t len);

void writeIDToFlash(void *id);

void restetFactory(void);
void read_error_code(uint32_t addr,void* dat,uint8_t len);
void write_error_code(uint32_t addr,void* dat,uint8_t len);
bool flash_init(void);

#endif

#ifdef __cplusplus
}
#endif

#endif
/* End of Flie ----------------------------------------------------------------------------------*/

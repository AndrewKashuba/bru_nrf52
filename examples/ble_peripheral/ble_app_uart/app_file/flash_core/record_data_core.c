#include "record_data_core.h"


#define NRF_LOG_MODULE_NAME record_data_core
#define NRF_LOG_LEVEL       3
#define NRF_LOG_INFO_COLOR  4
#define NRF_LOG_DEBUG_COLOR 3
#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();


static SAVE_TO_FLASH_INFO_T s_flash_info;


#define GET_SECTOR_ADDR(sector,page)  (((uint32_t)sector << 12) + (uint32_t)(page<<8))

#define RECORD_DATA_BEGIN_ADDR      GET_SECTOR_ADDR(0,0)
#define RECORD_DATA_END_ADDR        (GET_SECTOR_ADDR(1,0)-1)

#define EVERY_SECTOR_MAX_RECORD_COUNT       (RECORD_DATA_END_ADDR/sizeof(SAVE_TO_FLASH_INFO_T))
#define EVERY_PAGE_COUNT   								  (255/sizeof(SAVE_TO_FLASH_INFO_T))
	
static uint32_t writeable_addr = RECORD_DATA_BEGIN_ADDR;


/**
* @brieaf move data to ui data
*/
void move_data_to_ui_data(SAVE_TO_FLASH_INFO_T * pData)
{
	bru_UI_Interface_Handle p = get_ui_interface();
	p->hMachineSetup->bluetoothState = (bru_MS_Bletooth_Sate)pData->machine_info.ble_state;
	p->hMachineSetup->buzzerState = pData->machine_info.buzzer_state;
	p->hMachineSetup->language = (bru_MS_Language)pData->machine_info.language;
	p->hMachineSetup->timeformat = (bru_TimeFormat)pData->machine_info.timeformat;
	p->hMachineSetup->units = (bru_MS_Units)pData->machine_info.units;
	p->hMachineSetup->rinseConfiguration = (bru_MS_Rinse_Config)pData->machine_info.rinse_config;
	p->hMachineSetup->redLed = pData->machine_info.rgb_color_r;
	p->hMachineSetup->greenLed = pData->machine_info.rgb_color_g;
	p->hMachineSetup->blueLed = pData->machine_info.rgb_color_b;
	p->iCurrentTime = pData->current_time;
	p->iAlarmTime = pData->alarm_time;
	for(uint8_t i=0; i<BRU_DEFAULT_PRESET_ARR_SIZE; i++)
	{
		p->presetsArr[i].temperature = pData->preset_info[i].temperature;
		p->presetsArr[i].brewingTime = pData->preset_info[i].brewingTime;
		p->presetsArr[i].waterAmount = pData->preset_info[i].waterAmount;
	}
	NRF_LOG_INFO("move_data_to_ui_data");
}

/**
* @brieaf move ui data to flash data
*/
void move_ui_data_to_flash_data(SAVE_TO_FLASH_INFO_T * pData)
{
	bru_UI_Interface_Handle p = get_ui_interface();
	pData->machine_info.ble_state = p->hMachineSetup->bluetoothState;
	pData->machine_info.buzzer_state = p->hMachineSetup->buzzerState; 
	pData->machine_info.language = p->hMachineSetup->language;
	pData->machine_info.timeformat = p->hMachineSetup->timeformat;
	pData->machine_info.units = p->hMachineSetup->units;
	pData->machine_info.rinse_config = p->hMachineSetup->rinseConfiguration;
	pData->machine_info.rgb_color_r = p->hMachineSetup->redLed;
	pData->machine_info.rgb_color_g = p->hMachineSetup->greenLed;
	pData->machine_info.rgb_color_b = p->hMachineSetup->blueLed;
	pData->current_time = p->iCurrentTime;
	pData->alarm_time = p->iAlarmTime;
	for(uint8_t i=0; i<BRU_DEFAULT_PRESET_ARR_SIZE; i++)
	{
		pData->preset_info[i].temperature = p->presetsArr[i].temperature;
		pData->preset_info[i].brewingTime = p->presetsArr[i].brewingTime;
		pData->preset_info[i].waterAmount = p->presetsArr[i].waterAmount;
	}
	
	NRF_LOG_INFO("move_ui_data_to_flash_data");
	NRF_LOG_HEXDUMP_INFO((uint8_t*)&s_flash_info.current_time,sizeof(SAVE_TO_FLASH_INFO_T));
}



/**------------------------------------------------------------------------------------------------
* @brief  : flash check bank content
  *----------------------------------------------------------------------------------------------*/
static uint8_t checkDataBlank(void * dat,uint8_t len)
{
	uint8_t flag = 1;
	uint8_t *ptr = (uint8_t*)dat;
	for(uint8_t i=0;i<len;i++)
	{
		if(*ptr++ != 0xff)
		{
			flag = 0;
			break;
		}
	}
	
	return flag;
} 

/**
* @brieaf find writeable addr 
*/
static void find_writeable_addr(void)
{
	uint32_t addr = RECORD_DATA_BEGIN_ADDR;
	uint32_t pos = sizeof(SAVE_TO_FLASH_INFO_T);
	uint32_t j;
	uint8_t buf[pos+1];
	for(uint32_t i = addr; i<RECORD_DATA_END_ADDR; i+= pos)
	{
		j = i+sizeof(SAVE_TO_FLASH_INFO_T);
		if(j <= RECORD_DATA_END_ADDR)
			pos = sizeof(SAVE_TO_FLASH_INFO_T);
	  else 
		{
			addr = i;
			break;
		}
	
		readFlashData(i,buf,pos,FAST_READ);
//			NRF_LOG_HEXDUMP_INFO(buf,pos);
		if(checkDataBlank(buf,pos))
		{
			addr = i;
			break;
		}
	}
	writeable_addr = addr;
	NRF_LOG_INFO("---------------> find writeable_addr %x",writeable_addr);

}

/**
* @brieaf reset record data
*/
void reset_record_flash(void)
{
	eraseSectorFlash(RECORD_DATA_BEGIN_ADDR);
	writeable_addr = RECORD_DATA_BEGIN_ADDR;
	NRF_LOG_INFO("reset_record_flash");
}

void write_record_info_to_flash(void)
{
 
	if((writeable_addr + sizeof(SAVE_TO_FLASH_INFO_T)) >= RECORD_DATA_END_ADDR)
 	{
		reset_record_flash();
	}		
	
	move_ui_data_to_flash_data(&s_flash_info);
	
	writeDataForCount(writeable_addr,(uint8_t*)&s_flash_info.current_time,sizeof(SAVE_TO_FLASH_INFO_T));
	writeable_addr += sizeof(SAVE_TO_FLASH_INFO_T);
	NRF_LOG_INFO("=========== writeable_addr %x ==========",writeable_addr);
	
}


static bool read_record_info_from_data(void)
{
	uint32_t addr = writeable_addr;
	if(writeable_addr >= RECORD_DATA_BEGIN_ADDR +sizeof(SAVE_TO_FLASH_INFO_T))
		addr -= sizeof(SAVE_TO_FLASH_INFO_T);
	readFlashData(addr,(uint8_t*)&s_flash_info.current_time,sizeof(SAVE_TO_FLASH_INFO_T),FAST_READ);
	
	NRF_LOG_HEXDUMP_INFO((uint8_t*)&s_flash_info.current_time,sizeof(SAVE_TO_FLASH_INFO_T));
	return checkDataBlank((uint8_t*)&s_flash_info.current_time,sizeof(SAVE_TO_FLASH_INFO_T));
	
}



/**
* @brieaf initialate flash data
*/
void flash_data_init(bool flag)
{
  
	find_writeable_addr();
	if(read_record_info_from_data() || !flag)
	{
		move_ui_data_to_flash_data(&s_flash_info);
	}
	else
	{
		move_data_to_ui_data(&s_flash_info);
	}
}





















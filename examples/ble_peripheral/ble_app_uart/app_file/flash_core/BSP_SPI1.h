#ifndef __BSP_SPI_H__
#define __BSP_SPI_H__


#ifdef __cplusplus
extern "C" {
#endif
#include "sdk_config.h"
//#if SPI_ENABLED   
#include "stdint.h"


//void BSP_SPI_DeInit(void);
//void BSP_SPI_init(spi_master_cfg_t *spi_cfg);
//void BSP_SPI_readAndWriteData(uint8_t *tx,uint8_t txLen,uint8_t *rx,uint8_t rxLen);

//#else

#include "nrf_drv_gpiote.h"
#include "nrf_drv_spi.h"
#include "nrf_delay.h"



typedef uint16_t ret_t;

#define END_SUCCESS                                  (0x0000)
#define END_NOT_COMPLETE                             (0x0001)


#define MY_FW_ERROR_BASE_NUM                         (0x2000)       ///< Global error base
#define MY_ERROR_TM_BASE_NUM                         (0x2020)       ///< TM error base
#define PIXSACD_ERROR_BASE_NUM                       (0x2030)
#define MY_QUEUE_ERROR_BASE_NUM                      (0x2040)
#define I2C_EEPROM_RET_BASE_NUM                      (0x2050)
#define DATA_MAGT_RET_BASE_NUM                       (0x2060)
#define I2C_LIS3DH_RET_BASE_NUM                      (0x2070)
#define I2C_IQS333_RET_BASE_NUM                      (0X2080)
#define I2C_UVIS25_RET_BASE_NUM                      (0x2090)
#define I2C_AS7000_RET_BASE_NUM                      (0X20b0)
#define MY_PROTOCOL_APP_LAYER_BASE_NUM               (0x20C0)
#define MY_PROTOCOL_TRANS_LAYER_BASE_NUM             (0x20D0)

#define I2C_LIS3DH_SUCCESS                           (END_SUCCESS)
#define I2C_LIS3DH_ERROR                             (I2C_LIS3DH_RET_BASE_NUM + 0)
#define I2C_LIS3DH_GET_WHO_AM_I_ERR                  (I2C_LIS3DH_RET_BASE_NUM + 1)


/*******************************************************/
#define SPI_PIN_DISCONNECTED 0xFFFFFFFF /**< A value used to the PIN deinitialization. */

#define SPI_MASTER_CFG_DEFAULT                                              \
{                                                                           \
    NRF_DRV_SPI_FREQ_8M, /**< Serial clock frequency 1 Mbps. */      \
    SPI_PIN_DISCONNECTED,       /**< SCK pin DISCONNECTED. */               \
    SPI_PIN_DISCONNECTED,       /**< MISO pin DISCONNECTED. */              \
    SPI_PIN_DISCONNECTED,       /**< MOSI pin DISCONNECTED. */              \
    SPI_CONFIG_ORDER_MsbFirst,  /**< Bits order MSB. */                     \
    SPI_CONFIG_CPOL_ActiveHigh, /**< Serial clock polarity ACTIVEHIGH. */   \
    SPI_CONFIG_CPHA_Leading,    /**< Serial clock phase LEADING. */         \
};

typedef struct
{
    nrf_drv_spi_frequency_t SPI_Freq;          /**< SPI master frequency */
    
    uint32_t SPI_Pin_SCK;       /**< SCK pin number. */
    uint32_t SPI_Pin_MISO;      /**< MISO pin number. */
    uint32_t SPI_Pin_MOSI;      /**< MOSI pin number .*/

    /*考虑到spi总线制 CS的配置在spi_drive.c(ep:lis3dh_driver.c)文件里配置*/    
    //uint32_t SPI_Pin_CS;        /**< Slave select pin number. */
    uint8_t  SPI_CONFIG_ORDER;   /**< Bytes order LSB or MSB shifted out first. */
    uint8_t  SPI_CONFIG_CPOL;    /**< Serial clock polarity ACTIVEHIGH or ACTIVELOW. */
    uint8_t  SPI_CONFIG_CPHA;    /**< Serial clock phase LEADING or TRAILING. */
}spi_master_cfg_t;


/*************************************************
整理后的合并在一起

******************************************************/
void spi1_init(spi_master_cfg_t *spi_cfg);
void spi1_uninit(spi_master_cfg_t *spi_cfg);
void spi_writeData(uint8_t *wrData,uint8_t wrLen);
void spi_readData(uint8_t *rdData,uint8_t rdLen);
#endif
#ifdef __cplusplus
           }
#endif
           
//#endif

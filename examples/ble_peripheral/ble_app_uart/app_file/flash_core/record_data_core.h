#ifndef __RECORD_DATA_CORE_H
#define __RECORD_DATA_CORE_H

#include "BSP_OutFlash.h"
#include "bru_data.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct
{
	uint32_t rgb_color_r : 8;
	uint32_t rgb_color_g : 8;
	uint32_t rgb_color_b : 8;
	uint32_t language : 2;
	uint32_t rinse_config : 2;
	uint32_t buzzer_state : 1;
	uint32_t ble_state : 1;
	uint32_t timeformat : 1;
	uint32_t units : 1;
}SAVE_MACHINE_INFO;

typedef struct 
{
	uint32_t  temperature : 7;
	uint32_t  brewingTime : 13;
	uint32_t  waterAmount : 12;
}SAVE_PRESET_INFO;

typedef struct
{
	uint32_t current_time;
	uint32_t alarm_time;
	SAVE_PRESET_INFO  preset_info[BRU_DEFAULT_PRESET_ARR_SIZE];
	SAVE_MACHINE_INFO machine_info;
}SAVE_TO_FLASH_INFO_T;


void flash_data_init(bool flag);
void write_record_info_to_flash(void);
void reset_record_flash(void);

#ifdef __cplusplus
}
#endif
	
#endif

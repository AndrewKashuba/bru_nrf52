
#include "BSP_SPI1.h" 
#include "nrf_drv_spi.h"
#include "app_util_platform.h"
#include "nrf_gpio.h"
#include "nrf_delay.h"
#include "boards.h"
#include "app_error.h"
#include <string.h>
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
#include "nrf_pwr_mgmt.h"
#define SPI_INSTANCE  1 /**< SPI instance index. */
static const nrf_drv_spi_t spi = NRF_DRV_SPI_INSTANCE(SPI_INSTANCE);  /**< SPI instance. */
static volatile bool spi_xfer_done;  /**< Flag used to indicate that SPI instance completed the transfer. */
static bool spi_init_flag=0;

/**
 * @brief SPI user event handler.
 * @param event
 */
void spi1_event_handler(nrf_drv_spi_evt_t const * p_event,
                       void *                    p_context)
{
    spi_xfer_done = true;
//    NRF_LOG_INFO("spi1_Transfer completed.");
    if (p_event->data.done.rx_length == 0)
       return;
		
//     NRF_LOG_INFO(" Received len:%d",p_event->data.done.rx_length);
     
    
}


//###################################################################################################
/**
* @brieaf SPI1 initial for flash
*/
void BSP_SPI_init(spi_master_cfg_t *spi_cfg)
{	
	nrf_drv_spi_config_t spi_config = NRF_DRV_SPI_DEFAULT_CONFIG;
	spi_config.frequency= spi_cfg->SPI_Freq;
	spi_config.ss_pin   = NRF_DRV_SPI_PIN_NOT_USED;//SPI_SS_PIN;
	spi_config.miso_pin = spi_cfg->SPI_Pin_MISO;//SPI_MISO_PIN;
	spi_config.mosi_pin = spi_cfg->SPI_Pin_MOSI;//SPI_MOSI_PIN;
	spi_config.sck_pin  = spi_cfg->SPI_Pin_SCK;//SPI_SCK_PIN;
	APP_ERROR_CHECK(nrf_drv_spi_init(&spi, &spi_config, NULL, NULL));
}

/**
* @brieaf spi2 de-initial
*/
void BSP_SPI_DeInit(void)
{

   nrf_drv_spi_uninit(&spi);
		                   
}

/**
* @brieaf read and write data interface
*/
void BSP_SPI_readAndWriteData(uint8_t *tx,uint8_t txLen,uint8_t *rx,uint8_t rxLen)
{
	      spi_xfer_done = false;
				uint8_t tx_temp_buf[256];//兼容 const 类型，否则出现地址报错
				memcpy(tx_temp_buf,tx,txLen);	
        APP_ERROR_CHECK(nrf_drv_spi_transfer(&spi,tx_temp_buf,txLen,rx,rxLen));
//        while (!spi_xfer_done)
//        {
//           nrf_pwr_mgmt_run();
//        }
}

/*************************************************************************   
**	function name:	spi1 config		                                         
**************************************************************************/
void spi1_init(spi_master_cfg_t *spi_cfg)
{
	if(spi_init_flag)
		return;
	spi_init_flag=1;		

	BSP_SPI_init(spi_cfg);
		
}

/**
* @brieaf spi1 de-initial
*/
void spi1_uninit(spi_master_cfg_t *spi_cfg)
{
		if(spi_init_flag==0)
			return;
		spi_init_flag=0;

		BSP_SPI_DeInit();
		
}

/**
* @brieaf spi1 write data interface
*/
void spi_writeData(uint8_t*wrData,uint8_t wrLen)
{
	if(spi_init_flag==0)
		return;		
	#if SPI_ENABLED 
	BSP_SPI_readAndWriteData(wrData,wrLen,NULL,0);
	#else
	 uint8_t temp;
	 ASSERT(wrData != NULL);  
	 while(wrLen--)
	 {
        NRF_SPI1->TXD = *wrData;
        while(!NRF_SPI1->EVENTS_READY);  
        NRF_SPI1->EVENTS_READY = 0;
        temp   = NRF_SPI1->RXD;
		    wrData++;
   }
	 UNUSED_VARIABLE(temp);
	#endif
}

/**
* @brieaf spi1 read data interface
*/
void spi_readData(uint8_t *rdData,uint8_t rdLen)
{
	if(spi_init_flag==0)
		return;		
	#if SPI_ENABLED 
	BSP_SPI_readAndWriteData(NULL,0,rdData,rdLen);
	#else
	  ASSERT(rdData != NULL);  
	   while(rdLen--)
    {
        NRF_SPI1->TXD = 0xFF;
        while(!NRF_SPI1->EVENTS_READY);
        NRF_SPI1->EVENTS_READY = 0;
        
        *rdData   = NRF_SPI1->RXD; 
			  rdData++;
    }
	#endif
}





#ifndef __TARGET_COMMAND_H
#define __TARGET_COMMAND_H
#include "app_util_platform.h"

#ifdef __cplusplus
extern "C" {
#endif


#define CONST const



#ifdef __TYPEDEF
    #ifndef  __TYPEDEF_ONCE
    #define __TYPEDEF_ONCE
    typedef unsigned char uint8_t;
    typedef char int8_t;
    typedef unsigned short  uint16_t;
    typedef short int16_t;
    typedef unsigned long uint32_t;
    typedef long int32_t;
        #ifndef NULL
            #define NULL (void*)0
        #endif
    #endif
#endif // __TYPEDEF end



#ifndef FALSE
    #define FALSE   (0)
#endif
            
#ifndef TRUE
    #define TRUE    (1)
#endif



typedef void (*AFX_PMSG)(uint16_t * event);
typedef void (*AFX_VOID)(void);
typedef void (*AFX_PFN)( void *self);
typedef void (*AFX_PVMSG)( void*self,uint16_t event);
typedef void (*AFX_MSG)(uint16_t event);
typedef uint8_t (*PreFilterMsg)(uint16_t *msg);



/************************************************************************/

enum AfxSig
{
  AfxSig_vv=0,      // void (void)
  AfxSig_vpv,     // void (void*)
  AfxSig_vpvuc,	// void (void*,uint16_t)
  
};
 




typedef union
{
	AFX_VOID pfn;      //without param
	AFX_PFN  pvfn;  	//with void* param
	AFX_PVMSG pvmfn;  //with void*,unsigned short params

}AFX_UNION_PFN;






typedef struct tagMSGMAP_ENTRY
{
	void const * message;
	uint16_t message_len;
	uint16_t nSign;
	AFX_VOID pfn;
}AFX_MSGMAP_ENTRY;
 
typedef struct tagCmdTarget
{
	const AFX_MSGMAP_ENTRY *lpEn;
	const PreFilterMsg preMsgHandler;
}cmdTarget;

/*################################################*/ 
#define OK 1   
#define ERROR 0   
  
//queue size 
#define QUEUESIZE 64   
  
   
typedef  int ElemType;   

typedef  int State;  

typedef struct tagSqMsg
{
	 void *lpMsgEn;
	 void *extraData;
	 void * msg;
}QueueMsg;

//circle queue data structure  
typedef struct _CircleQueue  
{  
    QueueMsg data_elem[QUEUESIZE];   
    uint8_t front;   
    uint8_t rear; 
    int count;  
}CircleQueue;  
  

/*##################################################*/
//    ============    ON_VOID_MESSAGE  without para function
//    ============    ON_VOID_POINTER_MESSAGE   with void* param function
//    ============    ON_VOID_POINTER_UINT16_T_MESSAGE  with void* and uint16_t params function
//    ============    ON_STRING_MESSAGE  with void* param function by string contain match command
//    ============    ON_FULL_STRING_MESSAGE  with void* param function by string full match command

#define ON_VOID_MESSAGE(message,pfn) {(uint8_t*)&message,sizeof(message),AfxSig_vv,pfn},
#define ON_VOID_POINTER_MESSAGE(message,pfn)  {(uint8_t*)&message,sizeof(message),AfxSig_vpv,(AFX_VOID)(AFX_PFN)&pfn},
#define ON_VOID_POINTER_UINT16_T_MESSAGE(message,pfn)  {(uint8_t*)&message,sizeof(message),AfxSig_vpvuc,(AFX_VOID)(AFX_PVMSG)&pfn},
#define ON_STRING_MESSAGE(message,pfn) {(uint8_t*)message,(sizeof(message)-1),AfxSig_vpv,(AFX_VOID)(AFX_PFN)&pfn},
#define ON_FULL_STRING_MESSAGE(message,pfn) {(uint8_t*)message,sizeof(message),AfxSig_vpv,(AFX_VOID)(AFX_PFN)&pfn},

#define DECLARE_MESSAGE_MAP()\
const cmdTarget* lpEn; 
extern const uint8_t default_evt;


#define BEGIN_MESSAGE_MAP(theClass)\
static CONST AFX_MSGMAP_ENTRY theClass[]={\
	
#define END_MESSAGE_MAP(lpDef) \
			{NULL,0,AfxSig_vpv,(AFX_VOID)lpDef } \
};\



#define INIT_MSG_ENTRIES(theClass,cmdTargetName,preMsgHandler)\
 static CONST cmdTarget cmdTargetName = {theClass,preMsgHandler};

#define GET_MESSAGE_MAP(theClass, cmdTargetName) \
		theClass.lpEn = &cmdTargetName;


#define DECLARE_cmdTarget(theClass) \
extern CONST cmdTarget theClass;

extern QueueMsg *lpGetMsg;
//void OnTime(uint8_t *count, AFX_VOID pfn);
void OnCmdMsg(void*  event, void *lpEn, void * extraData);
QueueMsg *GetMessage(void) ; 
State PostMessage(void* msg,  void *self, void *extra);	


 //##############################################################################################################
 //=========================utils=====================================================
 

/**
* @brieaf calc structure member offset
*/
#define CALC_OFFSET(TYPE , MEMBER)((uint32_t)(&(((TYPE *)0)->MEMBER)))


/**
* @brieaf align 4 bytes
*/
#define ALIGN_WORDS(addr)     (((addr+3)>>2)<<2) 
 
 
uint8_t compare_string_data(uint8_t const*src,uint8_t const*obj,uint16_t len);
uint16_t calc_8bit_Fletcher_checksum(uint8_t *dat,uint8_t len);
uint8_t find_command_index(uint8_t *src,const uint8_t **cmd,uint8_t len);
uint8_t compare_string(uint8_t const *s,uint8_t const *o);
uint8_t get_checksum(uint8_t const*dat, uint8_t len);
uint32_t paserData(uint8_t *buf,uint8_t parse_len);



//################################################################################################################
/****************************************************************
		function menu struct 
*****************************************************************/
typedef struct tagMenuKey
{
	DECLARE_MESSAGE_MAP()	//must be put it in first position
	AFX_PFN dispFunc;
	void *ptrFlag;
	uint8_t *buzzer_flag;
}MENU_KEY;

typedef struct
{
	uint8_t flush_disp_flag;
	uint8_t clean_screen_flag;
	uint8_t sel_draw_flag_1;
	uint8_t sel_draw_flag_2;
}DISP_FLAG_T;

void disp_set_clean_func(AFX_VOID func);
void set_disp_flag(bool flush_flag,bool cls_screen_flag);
DISP_FLAG_T *get_disp_flag(void);
void jump_ui_func(const  MENU_KEY *ui);
MENU_KEY const* get_current_disp(void);
void target_command_init(const  MENU_KEY *ui,AFX_VOID cls,AFX_PFN key_fun);
//###############################################################################################################

extern const uint32_t TIMER_CALL_HZ;

typedef struct TIMER_OP_T
{
	struct TIMER_OP_T *next;
	void *pData;
	uint16_t timer_id;
	uint16_t timer_value;	
	AFX_PFN  onTimer;
}TIMER_INFO_T;

void OnTimer(void);
void set_timer_value(uint16_t id,uint16_t value,void *data);
void register_timer(uint16_t id,AFX_PFN fun);
void delete_timer(uint16_t id);
TIMER_INFO_T *find_timer(uint16_t id);

//----------another timer-------------------
void register_never_timer(uint16_t id,AFX_PFN fun);
void set_never_timer_value(uint16_t id,uint16_t value,void *data);
void delete_never_timer(uint16_t id);
void never_OnTimer(void);
TIMER_INFO_T *find_never_timer(uint16_t id);

#ifdef __cplusplus
}
#endif

#endif


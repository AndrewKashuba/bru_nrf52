#include "target_command.h"
#include "stdlib.h"
#include "nrf_log.h"


/*#################################################################*/

static DISP_FLAG_T s_disp_update_flag;   // display flag
static const  MENU_KEY *current_menu = NULL;
static void dispFunc(const MENU_KEY* self);

const uint32_t TIMER_CALL_HZ     =   100;                   // 100HZ

CircleQueue sqMsg={0};

QueueMsg *lpGetMsg;
AFX_PFN  keyToneFunc = NULL;
 
	
/************************************************* 
Function: 	  EnQueue 
Description:	put in queue
Input:		  CircleQueue *queue 
						ElemType e 
Output: 
Return:		  success - OK，fail-ERROR 
Others: 
*************************************************/  
State PostMessage(void* msg, void *self, void *extra)  
{  
	State status = OK;
	CRITICAL_REGION_ENTER();
		QueueMsg *pMsg = &sqMsg.data_elem[sqMsg.rear];
	//check queue is full ?   
	if(sqMsg.count != QUEUESIZE)  
	{    
			// put in queue  
			pMsg->lpMsgEn= self; 
			pMsg->msg = msg;
			pMsg->extraData = extra;
				
			sqMsg.rear = (sqMsg.rear + 1) % QUEUESIZE;	
			
			sqMsg.count++;  
	}
	else 
	{
		status = ERROR;
	}
	CRITICAL_REGION_EXIT();
	return status;  

}  
	
/************************************************* 
Function: 	  DeQueue 
Description:	 take out queue
Input:		  CircleQueue *queue 
Output: 
Return:		   
Others: 
*************************************************/  
QueueMsg *GetMessage(void)	
{  
	QueueMsg *e=NULL;
	CRITICAL_REGION_ENTER()
	
	if(sqMsg.count != 0)  
	{  
	
		e = &sqMsg.data_elem[sqMsg.front];  
		 
		sqMsg.front = (sqMsg.front + 1) % QUEUESIZE;  
	
	 sqMsg.count--;  
	}
	CRITICAL_REGION_EXIT();
	return e;  

}  
	
 



/*************************************************************************    
**	function name:	get_checksum
**	description:	calculate the checksum value for the input data 				                            
**	input para:		dat--start address of the data which you want to calculate 
**						 checksum
**					len--the length you want to calculate	
**	return:			checksum value				                                         
**************************************************************************/
uint8_t get_checksum(uint8_t const*dat, uint8_t len)
{
	uint8_t chksum = 0;
 
	for (uint8_t i = 0; i < len; i++)
		chksum += dat[i];

		chksum = 0 - chksum;
	  chksum ^= 0x3a;
	return chksum;
}



/**
* @brieaf  parse buf to uint8_t or uint16_t or uint32_t
*/
uint32_t paserData(uint8_t *buf,uint8_t parse_len)
{
	ASSERT(parse_len < 5);
	uint32_t temp = 0;
	for(uint8_t i=0; i<parse_len; i++)
	{
		temp <<= 8;
		temp |= buf[i];
		
	}
	
	return temp;
}



/**
* @brief compare string
*/
uint8_t compare_string(uint8_t const *s,uint8_t const *o)
{
	uint8_t index = 0;
	
	while(*o != '\0')
	{
		if(*s++ != *o++)
		{
			index = 1;
			break;
		}
	}
	return index;
}



/**
* @brief find string to matching cmd string
*/
uint8_t find_command_index(uint8_t *src,const uint8_t **cmd,uint8_t len)
{
	uint8_t index = 255;
	
	for(uint8_t i=0; i<len; i++)
	{
		 
		if(compare_string(src,cmd[i])==0)
		{
			index = i;
			break;
		}
	
	}
	return index;
}




/**
* @brieaf calc fletcher checksum
*/
uint16_t calc_8bit_Fletcher_checksum(uint8_t *dat,uint8_t len)
{
	uint8_t checksum1 = 0;
	uint8_t checksum2 = 0;
	for(uint8_t i=0; i<len; i++)
	{
		checksum1 += dat[i];
		checksum2 += checksum1;
	}
	return (checksum1 <<8) | checksum2;
}	



/**
* @brieaf compare string by length
*/
uint8_t compare_string_data(uint8_t const*src,uint8_t const*obj,uint16_t len)
{
	uint8_t index = 0;
	
	for(uint16_t i=0; i<len; i++)
	{
		
		if(*src++ != *obj++)
		{
			index = 1;
			break;
		}
	}
	return index;
}


/***********************************************************************************
	Routine Name:	OnCmdMsg
	Form:	 void OnCmdMsg(uint8_t  event, void *lpEn)
	Parameters:	uint8_t  event----	pass key msg value into it
				void *lpEn----point to own its msg object
	Return Value:	void
	Description: according to event value to search msg in lpEn object msg map,then call associated function
	                  with it
*************************************************************************************/
void OnCmdMsg(void * event,void *lpEn, void * extraData)
{ 	
	// you are check up lpEn value whether is null point before call this function operating
	if(lpEn != NULL)
	{
		// the lpEn pointer be converted cmdTarget object pointer
		const cmdTarget *pCmd = (const cmdTarget*)(*(void **)lpEn); 
	   
		// get msg map entries address
		const AFX_MSGMAP_ENTRY *ptr = pCmd->lpEn;
		
		// declare and define union function pointer-set object
		AFX_UNION_PFN AllFunc ;
		uint8_t * msg = event;
		// if no msg map entries address,then no any operate and direct return it
		if(pCmd == NULL )
			return;
			
		// below be named msg prefilter,if the return value of the virtual function of own object is  FALSE ,will skip below msg 
		if((pCmd->preMsgHandler != NULL) && (pCmd->preMsgHandler(event)==FALSE)) 
		{																			
			return;
		}
	  
		// search msg map  in class object
		
		while((msg != NULL) && compare_string_data(msg,ptr->message,ptr->message_len))
		{
			ptr++;
		}
		// if msg handler function is null,then skip below msg
		AllFunc.pfn = ptr->pfn;
		if(AllFunc.pfn == NULL)
			return;

		// acording to nSign value, to call belong to its function protype
		switch(ptr->nSign)
		{
			case AfxSig_vpv:
				AllFunc.pvfn(extraData);
			break;
			case AfxSig_vpvuc:
				AllFunc.pvmfn(extraData,*msg);
			break;
	
			default:
				AllFunc.pfn();
			break;
		}
		/**************************************************************************/
		// 按键处理函数,如果要重写按键函数,必须调用SetKeyToneHandler
		
			if(keyToneFunc != NULL)  
			{
				MENU_KEY *pMenu = lpEn;
				if(pMenu->buzzer_flag != NULL)
				{
					keyToneFunc(pMenu->buzzer_flag);
				}
			}	
    }
	 
		dispFunc(current_menu);	//display function handler   
}


/*#######################################################################*/

/*************************************************************************
	 display hander 
*****************************************************************************/
static AFX_VOID s_clean_disp_func = NULL;

static void dispFunc(const MENU_KEY* self)
{
	if(s_disp_update_flag.clean_screen_flag || s_disp_update_flag.flush_disp_flag)
	{
		const MENU_KEY *cp = self;
	
		if((cp!= 0) && (cp->dispFunc != 0))
		{
			  if(s_disp_update_flag.clean_screen_flag && (s_clean_disp_func != NULL))
				{
					s_clean_disp_func();
					
				}
				s_disp_update_flag.clean_screen_flag = 0;
        cp->dispFunc(cp->ptrFlag);
			  s_disp_update_flag.flush_disp_flag = 0;
		}
		 
	}
}

/**
* @brieaf get display  flag
*/
DISP_FLAG_T *get_disp_flag(void)
{
	return &s_disp_update_flag;
}	

/**
* @brieaf set display flush flag and clean screen flag
*/
void set_disp_flag(bool flush_flag,bool cls_screen_flag)
{
	s_disp_update_flag.clean_screen_flag = cls_screen_flag;
	s_disp_update_flag.clean_screen_flag = flush_flag;
}


/**
* @brieaf jump to spec ui
*/
void jump_ui_func(const  MENU_KEY *ui)
{
	if(current_menu != ui)
	{
	 current_menu = ui;
	 set_disp_flag(true,true); 
	}
}

/**
* @brieaf get current display page
*/
MENU_KEY const* get_current_disp(void)
{
	return current_menu;
}

/**
* @brieaf set clean screen function
*/
void disp_set_clean_func(AFX_VOID func)
{
	s_clean_disp_func = func;
}


/**
* @brieaf target command init
*/
void target_command_init(const  MENU_KEY *ui,AFX_VOID cls,AFX_PFN key_fun)
{
	jump_ui_func(ui);
	s_clean_disp_func = cls;
	keyToneFunc = key_fun;
}
//##################################################################################################
static TIMER_INFO_T *pTimerInfo = NULL;    // timer module
static TIMER_INFO_T *pNeverTimerInfo = NULL;  


/**
* @brieaf find timer by id
*/
static TIMER_INFO_T *com_find_timer(uint16_t id,TIMER_INFO_T **pHeader)
{
	 TIMER_INFO_T *p = *pHeader;
	
		while(p != NULL)
		{
			if(p->timer_id == id)
			{
				
				break;
			}
			p = p->next;
		}
		return p;
}



/**
* @brieaf set timer value
*/
void com_set_timer_value(uint16_t id,uint16_t value,TIMER_INFO_T **pHeader,void *data)
{
	  CRITICAL_REGION_ENTER();
	  TIMER_INFO_T *p = com_find_timer(id,pHeader);
	  if(p != NULL)
		{
			p->timer_value = value;
			p->pData = data;
		}
	  CRITICAL_REGION_EXIT();
}

static void com_register_timer(uint16_t id,AFX_PFN fun,TIMER_INFO_T **pheader)
{
	if(com_find_timer(id,pheader) != NULL)  // timer already exsit
	{
		NRF_LOG_INFO("register_timer ID %d is already present",id);
		return;
	}
	
  TIMER_INFO_T *p = malloc(sizeof(TIMER_INFO_T));

	if(p == NULL)
	{
		NRF_LOG_INFO("############################### register_timer %x is failed ####################################",p);
		
		return;
	}
 	NRF_LOG_INFO("register_timer %x",p);
	
	p->next = NULL;
	p->onTimer = fun;
	p->timer_id = id;
	p->timer_value = 0;

  if(*pheader == NULL)
	{
		*pheader = p;
	}
	else
	{
		TIMER_INFO_T *pNext = *pheader;
		while(pNext->next != NULL)
		{
			pNext = pNext->next;
		}
		
		pNext->next = p;
	}
	
}




/**
* @brieaf delete timer by id
* @note can't delete timer in owner callback function,else device will die 
*/
void com_delete_timer(uint16_t id,TIMER_INFO_T **pheader)
{
	 TIMER_INFO_T *p = *pheader;
	 TIMER_INFO_T *pre = p;
	
		while(p != NULL)
		{
			if(p->timer_id == id)
			{			
				pre->next = p->next;
				if(p == *pheader)  // delete owner
					*pheader = pre->next;
				NRF_LOG_INFO("delete_timer %x,%d",p,p->timer_id);
				
				free(p);
				
				break;
			}
			pre = p;
			p = p->next;
		}
		
}


/**
* @brief timer process handler
*/
void com_OnTimer(TIMER_INFO_T **pheader)
{
	TIMER_INFO_T *p = *pheader;
	
		while(p != NULL)
		{
//			NRF_LOG_INFO("OnTimer %x",p);
			if(p->timer_value)
			{
				if(--p->timer_value == 0)
				{
					p->onTimer(p);
				}
			}
			p = p->next;
		}
	
}

//===========================================================================================
/**
* @brieaf find timer by id
*/
TIMER_INFO_T *find_timer(uint16_t id)
{
	return com_find_timer(id,&pTimerInfo);
}


/**
* @brief register timer
*/
void register_timer(uint16_t id,AFX_PFN fun)
{
	com_register_timer(id,fun,&pTimerInfo);
}	


/**
* @brieaf set timer value
*/
void set_timer_value(uint16_t id,uint16_t value,void *data)
{
	com_set_timer_value(id,value,&pTimerInfo,data);
}

/**
* @brieaf delete timer by id
* @note can't delete timer in owner callback function,else device will die 
*/
void delete_timer(uint16_t id)
{
	com_delete_timer(id,&pTimerInfo);
}


/**
* @brief timer process handler
*/
void OnTimer(void)
{
	com_OnTimer(&pTimerInfo);

}

//=========================================================================================
//----------- below time is call in 1hz----------------------
/**
* @brieaf find timer by id
*/
TIMER_INFO_T *find_never_timer(uint16_t id)
{
	return com_find_timer(id,&pNeverTimerInfo);
}

/**
* @brief register timer
*/
void register_never_timer(uint16_t id,AFX_PFN fun)
{
	com_register_timer(id,fun,&pNeverTimerInfo);
}	



/**
* @brieaf set timer vuale
*/
void set_never_timer_value(uint16_t id,uint16_t value,void *data)
{
	com_set_timer_value(id,value,&pNeverTimerInfo,data);
}

/**
* @brieaf delete timer by id
* @note can't delete timer in owner callback function,else device will die 
*/
void delete_never_timer(uint16_t id)
{
	com_delete_timer(id,&pNeverTimerInfo);
}


/**
* @brief timer process handler
*/
void never_OnTimer(void)
{
	com_OnTimer(&pNeverTimerInfo);

}




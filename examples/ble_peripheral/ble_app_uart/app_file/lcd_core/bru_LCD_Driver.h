/*
 * bru_LCD_Driver.h
 *
 *  Created on: Jun 24, 2020
 *      Author: AltiFluid
 */

#ifndef BRU_LCD_DRIVER_H_
#define BRU_LCD_DRIVER_H_
#include "pwm_rgbb_core.h"
#include "BSP_SPI2.h"
#include "bru_LCD_Fonts.h"
#include "nrf_delay.h"
#include "bru_data.h"

// 0x07FF - Yellow, 0xFFE0 - Magenta, 0xF81F - Azure, 
#define BRU_LCD_WHITE_COLOUR                0xFFFF
#define BRU_LCD_BLACK_COLOUR                0x0000
#define BRU_LCD_RED_COLOUR                  0xF800
#define BRU_LCD_GREEN_COLOUR                0x001F
#define BRU_LCD_BLUE_COLOUR                 0x07E0
#define BRU_LCD_MAIN_BTN_COLOUR             0xFC1F 
#define BRU_LCD_XXXX_COLOUR                 0x0A0A

#define BRU_LCD_USE_HORIZONTAL 1  //Set the display direction 0,1,2,3   four directions
#define ST7735_WIDTH  240
#define ST7735_HEIGHT 240







void bru_LCD_GC9A01_Initial(void);
void clean_screen_display(void);

void bru_LCD_Draw_buttons_ENG(const char* l_button,const char* r_button);

void bru_LCD_SetPos(unsigned int Xstart, unsigned int Ystart, unsigned int Xend, unsigned int Yend);
void bru_LCD_ClearScreen(unsigned int bColor, uint8_t start_X, uint8_t start_Y, uint8_t end_X, uint8_t end_Y);
void bru_LCD_Draw_buttons(const char* l_button,const char* r_button);
void bru_LCD_Draw_Circle(unsigned int x0, unsigned int y0,unsigned char r, unsigned int color);
void bru_LCD_DrawRectangle(unsigned int x1, unsigned int y1, unsigned int x2, unsigned int y2, unsigned int color);
void bru_LCD_DrawLine(unsigned int x1, unsigned int y1, unsigned int x2, unsigned int y2, unsigned int color);
void bru_LCD_DrawPoint(unsigned int x, unsigned int y, unsigned int color);
void bru_LCD_LinesForButtons(uint16_t color);
void bru_LCD_DispRGBGray(void);
void bru_LCD_fill_center(unsigned int bcolor);
void bru_LCD_DrawImage_lib(uint16_t x, uint16_t y, uint16_t w, uint16_t h, const uint16_t* data);

void bru_LCD_WriteChar_22Arial(uint16_t x, uint16_t y, char ch, bru_LCD_FontDef font, uint16_t color, uint16_t bgcolor, uint8_t font_h);
void bru_LCD_WriteString_22Arial(uint16_t x, uint16_t y, const char* str, bru_LCD_FontDef font, uint16_t color, uint16_t bgcolor);
uint8_t bru_LCD_FontCNT(char symb,  bru_LCD_FontDef width_f);
void bru_LCD_WriteChar_24Arial(uint16_t x, uint16_t y, char ch, bru_LCD_FontDef font, uint16_t color, uint16_t bgcolor, uint8_t font_h);
void bru_LCD_WriteString_24Arial(uint16_t x, uint16_t y, const char* str, bru_LCD_FontDef font, uint16_t color, uint16_t bgcolor);
void bru_LCD_WriteChar_28Arial(uint16_t x, uint16_t y, char ch, bru_LCD_FontDef font, uint16_t color, uint16_t bgcolor, uint8_t font_h);
void bru_LCD_WriteString_28Arial(uint16_t x, uint16_t y, const char* str, bru_LCD_FontDef font, uint16_t color, uint16_t bgcolor);
void bru_LCD_WriteChar_16Arial_R(uint16_t x, uint16_t y, char ch, bru_LCD_FontDef font, uint16_t color, uint16_t bgcolor, uint8_t font_h);
void bru_LCD_WriteString_16Arial_R(uint16_t x, uint16_t y, const char* str, bru_LCD_FontDef font, uint16_t color, uint16_t bgcolor);
void bru_LCD_WriteChar_16Arial(uint16_t x, uint16_t y, char ch, bru_LCD_FontDef font, uint16_t color, uint16_t bgcolor, uint8_t font_h);
void bru_LCD_WriteString_16Arial(uint16_t x, uint16_t y, const char* str, bru_LCD_FontDef font, uint16_t color, uint16_t bgcolor);
void bru_LCD_WriteChar_14Arial(uint16_t x, uint16_t y, char ch, bru_LCD_FontDef font, uint16_t color, uint16_t bgcolor, uint8_t font_h);
void bru_LCD_WriteString_14Arial(uint16_t x, uint16_t y, const char* str, bru_LCD_FontDef font, uint16_t color, uint16_t bgcolor);
void bru_LCD_WriteChar_16MBArial(uint16_t x, uint16_t y, char ch, bru_LCD_FontDef font, uint16_t color, uint16_t bgcolor, uint8_t font_h);
void bru_LCD_WriteString_16MBArial(uint16_t x, uint16_t y, const char* str, bru_LCD_FontDef font, uint16_t color, uint16_t bgcolor);
void bru_LCD_WriteChar_18Arial_R(uint16_t x, uint16_t y, char ch, bru_LCD_FontDef font, uint16_t color, uint16_t bgcolor, uint8_t font_h);
void bru_LCD_WriteString_18Arial_R(uint16_t x, uint16_t y, const char* str, bru_LCD_FontDef font, uint16_t color, uint16_t bgcolor);
void lcd_draw_picture(uint8_t x1,uint8_t y1,uint8_t w,uint8_t h,uint8_t *p_buf);
void clean_above_menu_display(void);

#endif /* BRU_LCD_DRIVER_H_ */

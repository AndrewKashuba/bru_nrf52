/*
 * bru_LCD_Driver.c
 *
 *  Created on: Jun 24, 2020
 *      Author: AltiFluid
 */
#include "bru_LCD_Driver.h"
#include "nrf_drv_gpiote.h"
#include "stdlib.h"
#include "string.h"
#include "target_command.h"

#define BRU_LCD_SPI_ARR_SIZE                (960)


#define FONT_ARR_SIZE    (1024)
static uint16_t data_font[FONT_ARR_SIZE] = {0};
//=============================================================
//write command
static void bru_LCD_Write_Cmd(unsigned char CMD)
{
    nrf_gpio_pin_clear(LCD_DC_PIN);  
	  BSP_spi2_send(&CMD,1);
}
//===================================================================
//write parameter
static void  bru_LCD_Write_Cmd_Data (unsigned char CMDP)
{
    nrf_gpio_pin_set(LCD_DC_PIN);  
	  BSP_spi2_send(&CMDP,1);
}
//===================================================================
//write data byte
static void bru_LCD_Write_Data(unsigned char DH,unsigned char DL)
{
	  uint8_t tempArr[2];
    tempArr[0] = DH;
    tempArr[1] = DL;
	
		nrf_gpio_pin_set(LCD_DC_PIN);  
	  BSP_spi2_send(tempArr,2);
}

//===================================================================
//write data byte
static void bru_LCD_Write_Data_lib(uint8_t* buff, uint16_t len)
{
    nrf_gpio_pin_set(LCD_DC_PIN);  
    BSP_spi2_send(buff,len);
}
//write data byte
static void bru_LCD_Write_Data_lib_16(uint16_t* buff, uint16_t len)
{
    nrf_gpio_pin_set(LCD_DC_PIN);  
    BSP_spi2_send((uint8_t*)buff,len*2);
}

//==============================================================
//write  data word
static void bru_LCD_Write_Data_U16(unsigned int y)
{
    unsigned char m,n;
    m=y>>8;
    n=y;
    bru_LCD_Write_Data(m,n);
}

////void bru_LCD_show_picture(void)
////{
////   unsigned char i,j;

////   bru_LCD_SetPos(60,40,179,199);
////    for(j=0;j<160;j++)
////    {
////        for(i=0;i<120;i++)
////        {
//////            Write_Data_U16(pic[n++]);
////        }
////    }


////    return;
////}
void bru_LCD_fill_center(unsigned int bcolor)
{
    unsigned char i,j;
    unsigned char w,h;

    w = rand() % 200;
    h = rand() % 200;
    bru_LCD_SetPos(w,h,w+40,h+40);

    for(j=0;j<40;j++)
    {
        for(i=0;i<40;i++)
        {
            bru_LCD_Write_Data_U16(bcolor);
        }
    }
}


void bru_LCD_DrawPoint(unsigned int x,unsigned int y,unsigned int color)
{
    bru_LCD_SetPos(x,y,x,y);

    uint8_t data[] = { color >> 8, color & 0xFF };
    bru_LCD_Write_Data_lib(data,2);
    //bru_LCD_Write_Data_U16(color);
}

void bru_LCD_DrawLine(unsigned int x1,unsigned int y1,unsigned int x2,unsigned int y2,unsigned int color)
{
    unsigned int t;
    int xerr=0,yerr=0,delta_x,delta_y,distance;
    int incx,incy,uRow,uCol;
    delta_x=x2-x1;
    delta_y=y2-y1;
    uRow=x1;
    uCol=y1;
    if(delta_x>0)incx=1;
    else if (delta_x==0)incx=0;
    else {incx=-1;delta_x=-delta_x;}
    if(delta_y>0)incy=1;
    else if (delta_y==0)incy=0;
    else {incy=-1;delta_y=-delta_x;}
    if(delta_x>delta_y)distance=delta_x;
    else distance=delta_y;
    for(t=0;t<distance+1;t++)
    {
        bru_LCD_DrawPoint(uRow,uCol,color);
        xerr+=delta_x;
        yerr+=delta_y;
        if(xerr>distance)
        {
            xerr-=distance;
            uRow+=incx;
        }
        if(yerr>distance)
        {
            yerr-=distance;
            uCol+=incy;
        }
    }
}
void bru_LCD_DrawRectangle(unsigned int x1, unsigned int y1, unsigned int x2, unsigned int y2,unsigned int color)
{
    bru_LCD_DrawLine(x1,y1,x2,y1,color);
    bru_LCD_DrawLine(x1,y1,x1,y2,color);
    bru_LCD_DrawLine(x1,y2,x2,y2,color);
    bru_LCD_DrawLine(x2,y1,x2,y2,color);
}
void bru_LCD_Draw_Circle(unsigned int x0,unsigned int y0,unsigned char r,unsigned int color)
{
    int a,b;
    a=0;b=r;
    while(a<=b)
    {
        bru_LCD_DrawPoint(x0-b,y0-a,color);             //3
        bru_LCD_DrawPoint(x0+b,y0-a,color);             //0
        bru_LCD_DrawPoint(x0-a,y0+b,color);             //1
        bru_LCD_DrawPoint(x0-a,y0-b,color);             //2
        bru_LCD_DrawPoint(x0+b,y0+a,color);             //4
        bru_LCD_DrawPoint(x0+a,y0-b,color);             //5
        bru_LCD_DrawPoint(x0+a,y0+b,color);             //6
        bru_LCD_DrawPoint(x0-b,y0+a,color);             //7
        a++;
        if((a*a+b*b)>(r*r))
        {
            b--;
        }
    }
}
//============================================================
//show gray

void bru_LCD_DispRGBGray(void)
{
    unsigned int  A,B,C;
    unsigned int  i,j,k,DH;
    bru_LCD_SetPos(0,0,239,239);//240x240
    for(k=0;k<60;k++)
    {
        A=0;
        B=0;
        C=0;
        for(i=0;i<16;i++)
        {
            for(j=0;j<15;j++)
            {
                DH=(A<<11)+(B<<5)+C;
                bru_LCD_Write_Data_U16(DH);
            }
            A=A+2;
            B=B+4;
            C=C+2;
        }

    }


    for(k=0;k<60;k++)
    {
        A=0;
        B=0;
        C=0;
        for(i=0;i<16;i++)
        {
            for(j=0;j<15;j++)
            {
                DH=(A<<11)+B+C;
                bru_LCD_Write_Data_U16(DH);
            }
            A=A+2;

        }

    }

    for(k=0;k<60;k++)
    {
        A=0;
        B=0;
        C=0;
        for(i=0;i<16;i++)
        {
            for(j=0;j<15;j++)
            {
                DH=A+(B<<5)+C;
                bru_LCD_Write_Data_U16(DH);
            }
            B=B+4;

        }

    }

    for(k=0;k<60;k++)
    {
        A=0;
        B=0;
        C=0;
        for(i=0;i<16;i++)
        {
            for(j=0;j<15;j++)
            {
                DH=A+B+C;
                bru_LCD_Write_Data_U16(DH);
            }
            C=C+2;

        }
    }
}



/**
* @brieaf hardware reset
*/
void lcd_reset(void)
{
	nrf_gpio_cfg_output(RESETB_PIN);
	nrf_gpio_pin_set(RESETB_PIN);
	nrf_delay_ms(30);
	nrf_gpio_cfg_output(RESETB_PIN);
	nrf_gpio_pin_clear(RESETB_PIN);
	nrf_delay_ms(100);
	nrf_gpio_pin_set(RESETB_PIN);
	nrf_delay_ms(30);
}



void bru_LCD_GC9A01_Initial(void)
{
    BSP_spi2_init();
		lcd_reset();
	
			
			
		/////////////GC9A01+1.09 HSD IPS//////////////
		bru_LCD_Write_Cmd(0xFE);			 
		bru_LCD_Write_Cmd(0xEF); 

		bru_LCD_Write_Cmd(0xEB);	
		bru_LCD_Write_Cmd_Data(0x14); 

		bru_LCD_Write_Cmd(0x84);			
		bru_LCD_Write_Cmd_Data(0x40);
			
		bru_LCD_Write_Cmd(0x86);			
		bru_LCD_Write_Cmd_Data(0xFF);
			 
		bru_LCD_Write_Cmd(0xC0);			
		bru_LCD_Write_Cmd_Data(0x1A); 

		bru_LCD_Write_Cmd(0x88);			
		bru_LCD_Write_Cmd_Data(0x0A);

		bru_LCD_Write_Cmd(0x89);			
		bru_LCD_Write_Cmd_Data(0x21); 

		bru_LCD_Write_Cmd(0x8A);			
		bru_LCD_Write_Cmd_Data(0x00); 

		bru_LCD_Write_Cmd(0x8B);			
		bru_LCD_Write_Cmd_Data(0x80); 

		bru_LCD_Write_Cmd(0x8C);			
		bru_LCD_Write_Cmd_Data(0x01); 

		bru_LCD_Write_Cmd(0x8D);			
		bru_LCD_Write_Cmd_Data(0x01);
			
		bru_LCD_Write_Cmd(0x8F);			
		bru_LCD_Write_Cmd_Data(0xFF); 

		bru_LCD_Write_Cmd(0xB6);			
		bru_LCD_Write_Cmd_Data(0x20); 

		bru_LCD_Write_Cmd(0x36);			
		if(BRU_LCD_USE_HORIZONTAL==0)bru_LCD_Write_Cmd_Data(0x18);
    else if(BRU_LCD_USE_HORIZONTAL==1)bru_LCD_Write_Cmd_Data(0x28);
    else if(BRU_LCD_USE_HORIZONTAL==2)bru_LCD_Write_Cmd_Data(0x48);
    else bru_LCD_Write_Cmd_Data(0x88);
	  

		bru_LCD_Write_Cmd(0x3A);		 //Pixel Format set
		bru_LCD_Write_Cmd_Data(0x05);     //18bits


		bru_LCD_Write_Cmd(0x90);			
		bru_LCD_Write_Cmd_Data(0x08);
		bru_LCD_Write_Cmd_Data(0x08);
		bru_LCD_Write_Cmd_Data(0x08);
		bru_LCD_Write_Cmd_Data(0x08); 

		bru_LCD_Write_Cmd(0xBD);			
		bru_LCD_Write_Cmd_Data(0x06);
			
		bru_LCD_Write_Cmd(0xBC);			
		bru_LCD_Write_Cmd_Data(0x00);	

		bru_LCD_Write_Cmd(0xFF);			
		bru_LCD_Write_Cmd_Data(0x60);
		bru_LCD_Write_Cmd_Data(0x01);
		bru_LCD_Write_Cmd_Data(0x04);

		bru_LCD_Write_Cmd(0xC9);			
		bru_LCD_Write_Cmd_Data(0x22);//

		bru_LCD_Write_Cmd(0xBE);			
		bru_LCD_Write_Cmd_Data(0x11); 

		bru_LCD_Write_Cmd(0xE1);			
		bru_LCD_Write_Cmd_Data(0x10);
		bru_LCD_Write_Cmd_Data(0x0E);

		bru_LCD_Write_Cmd(0xDF);			
		bru_LCD_Write_Cmd_Data(0x21);
		bru_LCD_Write_Cmd_Data(0x0c);
		bru_LCD_Write_Cmd_Data(0x02);

		bru_LCD_Write_Cmd(0xF0);   
		bru_LCD_Write_Cmd_Data(0x45);//VR62[5:0]
		bru_LCD_Write_Cmd_Data(0x09);//VR61[5:0]
		bru_LCD_Write_Cmd_Data(0x08);//VR59[4:0]
		bru_LCD_Write_Cmd_Data(0x08);//VR57[4:0]
		bru_LCD_Write_Cmd_Data(0x26);//VR63[7:4]/VR50[3:0]
		bru_LCD_Write_Cmd_Data(0x2A);//VR43[6:0]

	 bru_LCD_Write_Cmd(0xF1);    
	 bru_LCD_Write_Cmd_Data(0x43);//VR20[6:0]
	 bru_LCD_Write_Cmd_Data(0x70);//VR36[7:5]/VR6[4:0]
	 bru_LCD_Write_Cmd_Data(0x72);//VR27[7:5]/VR4[4:0]
	 bru_LCD_Write_Cmd_Data(0x36);//VR2[5:0]
	 bru_LCD_Write_Cmd_Data(0x37);//VR1[5:0] 
	 bru_LCD_Write_Cmd_Data(0x6F);//VR13[7:4]/VR0[3:0]

	 bru_LCD_Write_Cmd(0xF2);   
	 bru_LCD_Write_Cmd_Data(0x45);//VR62[5:0]
	 bru_LCD_Write_Cmd_Data(0x09);//VR61[5:0]
	 bru_LCD_Write_Cmd_Data(0x08);//VR59[4:0]
	 bru_LCD_Write_Cmd_Data(0x08);//VR57[4:0]
	 bru_LCD_Write_Cmd_Data(0x26);//VR63[7:4]/VR50[3:0]
	 bru_LCD_Write_Cmd_Data(0x2A);//VR43[6:0]

	 bru_LCD_Write_Cmd(0xF3);   
	 bru_LCD_Write_Cmd_Data(0x43);//VR20[6:0]
	 bru_LCD_Write_Cmd_Data(0x70);//VR36[7:5]/VR6[4:0]
	 bru_LCD_Write_Cmd_Data(0x72);//VR27[7:5]/VR4[4:0]
	 bru_LCD_Write_Cmd_Data(0x36);//VR2[5:0]
	 bru_LCD_Write_Cmd_Data(0x37);//VR1[5:0]
	 bru_LCD_Write_Cmd_Data(0x6F);//VR13[7:4]/VR0[3:0]

	bru_LCD_Write_Cmd(0xED);	
	bru_LCD_Write_Cmd_Data(0x1B); 
	bru_LCD_Write_Cmd_Data(0x8B); 

	bru_LCD_Write_Cmd(0xAE);			
	bru_LCD_Write_Cmd_Data(0x77);
		
	bru_LCD_Write_Cmd(0xCD);			
	bru_LCD_Write_Cmd_Data(0x63);
		
	bru_LCD_Write_Cmd(0xAC);			
	bru_LCD_Write_Cmd_Data(0x27);//44		


	bru_LCD_Write_Cmd(0x70);		//VGH VGL_CLK	
	bru_LCD_Write_Cmd_Data(0x07);
	bru_LCD_Write_Cmd_Data(0x07);
	bru_LCD_Write_Cmd_Data(0x04);
	bru_LCD_Write_Cmd_Data(0x06); //bit[4:0] VGH 01
	bru_LCD_Write_Cmd_Data(0x0F); //bit[4:0] VGL
	bru_LCD_Write_Cmd_Data(0x09);
	bru_LCD_Write_Cmd_Data(0x07);
	bru_LCD_Write_Cmd_Data(0x08);
	bru_LCD_Write_Cmd_Data(0x03);

	bru_LCD_Write_Cmd(0xE8);			
	bru_LCD_Write_Cmd_Data(0x24); // 04:column 14:1-dot 24:2-dot inversion

	bru_LCD_Write_Cmd(0x62);			
	bru_LCD_Write_Cmd_Data(0x18);
	bru_LCD_Write_Cmd_Data(0x0D);
	bru_LCD_Write_Cmd_Data(0x71);
	bru_LCD_Write_Cmd_Data(0xED);
	bru_LCD_Write_Cmd_Data(0x70); 
	bru_LCD_Write_Cmd_Data(0x70);
	bru_LCD_Write_Cmd_Data(0x18);
	bru_LCD_Write_Cmd_Data(0x0F);
	bru_LCD_Write_Cmd_Data(0x71);
	bru_LCD_Write_Cmd_Data(0xEF);
	bru_LCD_Write_Cmd_Data(0x70); 
	bru_LCD_Write_Cmd_Data(0x70);

	bru_LCD_Write_Cmd(0x63);			
	bru_LCD_Write_Cmd_Data(0x18);
	bru_LCD_Write_Cmd_Data(0x11);
	bru_LCD_Write_Cmd_Data(0x71);
	bru_LCD_Write_Cmd_Data(0xF1);
	bru_LCD_Write_Cmd_Data(0x70); 
	bru_LCD_Write_Cmd_Data(0x70);
	bru_LCD_Write_Cmd_Data(0x18);
	bru_LCD_Write_Cmd_Data(0x13);
	bru_LCD_Write_Cmd_Data(0x71);
	bru_LCD_Write_Cmd_Data(0xF3);
	bru_LCD_Write_Cmd_Data(0x70); 
	bru_LCD_Write_Cmd_Data(0x70);

	bru_LCD_Write_Cmd(0x64);			
	bru_LCD_Write_Cmd_Data(0x28);
	bru_LCD_Write_Cmd_Data(0x29);
	bru_LCD_Write_Cmd_Data(0xF1);
	bru_LCD_Write_Cmd_Data(0x01);
	bru_LCD_Write_Cmd_Data(0xF1);
	bru_LCD_Write_Cmd_Data(0x00);
	bru_LCD_Write_Cmd_Data(0x07);

	bru_LCD_Write_Cmd(0x66);			
	bru_LCD_Write_Cmd_Data(0x3C);
	bru_LCD_Write_Cmd_Data(0x00);
	bru_LCD_Write_Cmd_Data(0xCD);
	bru_LCD_Write_Cmd_Data(0x67);
	bru_LCD_Write_Cmd_Data(0x45);
	bru_LCD_Write_Cmd_Data(0x45);
	bru_LCD_Write_Cmd_Data(0x10);
	bru_LCD_Write_Cmd_Data(0x00);
	bru_LCD_Write_Cmd_Data(0x00);
	bru_LCD_Write_Cmd_Data(0x00);

	bru_LCD_Write_Cmd(0x67);			
	bru_LCD_Write_Cmd_Data(0x00);
	bru_LCD_Write_Cmd_Data(0x3C);
	bru_LCD_Write_Cmd_Data(0x00);
	bru_LCD_Write_Cmd_Data(0x00);
	bru_LCD_Write_Cmd_Data(0x00);
	bru_LCD_Write_Cmd_Data(0x01);
	bru_LCD_Write_Cmd_Data(0x54);
	bru_LCD_Write_Cmd_Data(0x10);
	bru_LCD_Write_Cmd_Data(0x32);
	bru_LCD_Write_Cmd_Data(0x98);

	bru_LCD_Write_Cmd(0x74);		//frame rate	
	bru_LCD_Write_Cmd_Data(0x10);	
	bru_LCD_Write_Cmd_Data(0x85);	//60Hz
	bru_LCD_Write_Cmd_Data(0x80);
	bru_LCD_Write_Cmd_Data(0x00); //bit[4:0] VGH
	bru_LCD_Write_Cmd_Data(0x00); //bit[4:0] VGL
	bru_LCD_Write_Cmd_Data(0x4E);
	bru_LCD_Write_Cmd_Data(0x00);					
		
	bru_LCD_Write_Cmd(0x98);			
	bru_LCD_Write_Cmd_Data(0x3e);
	bru_LCD_Write_Cmd_Data(0x07);

	bru_LCD_Write_Cmd(0x35);	
	bru_LCD_Write_Cmd(0x21);
	nrf_delay_ms(120);

	bru_LCD_Write_Cmd(0x11);
	nrf_delay_ms(320);

  bru_LCD_ClearScreen(BRU_LCD_BLACK_COLOUR, 0, 0, ST7735_WIDTH, ST7735_HEIGHT);
	bru_LCD_Write_Cmd(0x29);
	nrf_delay_ms(120);

	bru_LCD_Write_Cmd(0x2C);
//	set_backlight_duty(100);

}
void bru_LCD_SetPos(unsigned int Xstart, unsigned int Ystart, unsigned int Xend, unsigned int Yend)
{
   bru_LCD_Write_Cmd(0x2a);
   bru_LCD_Write_Cmd_Data(Xstart>>8);
   bru_LCD_Write_Cmd_Data(Xstart);
   bru_LCD_Write_Cmd_Data(Xend>>8);
   bru_LCD_Write_Cmd_Data(Xend);

   bru_LCD_Write_Cmd(0x2b);
   bru_LCD_Write_Cmd_Data(Ystart>>8);
   bru_LCD_Write_Cmd_Data(Ystart);
   bru_LCD_Write_Cmd_Data(Yend>>8);
   bru_LCD_Write_Cmd_Data(Yend);

   bru_LCD_Write_Cmd(0x2c);//LCD_WriteCMD(GRAMWR);

	
	
//   bru_LCD_Write_Cmd(0x3c);
}
//===============================================================
//clear screen
void bru_LCD_ClearScreen(unsigned int bColor, uint8_t start_X, uint8_t start_Y, uint8_t end_X, uint8_t end_Y)
{

  bru_LCD_SetPos(start_X, start_Y, end_X - 1, end_Y - 1);
 
	
	bru_LCD_Write_Cmd(0x2C);
	uint16_t w = end_X-start_X;
	uint16_t h = end_Y-start_Y;
	uint32_t len=w*h<<1;
	uint16_t loop=len/254;
	uint8_t remain=len%254;
	uint8_t data_buf[254];
	
	bColor = (bColor >> 8) | (bColor << 8);
	memset(data_buf,bColor,254);

	
	nrf_gpio_pin_set(LCD_DC_PIN);
	for(uint16_t i=0;i<loop;i++)
		BSP_spi2_send(data_buf,254);
	if(remain)
		BSP_spi2_send(data_buf,remain);
   
}
/**
* @brieaf clean full screen
*/
void clean_screen_display(void)
{
	 bru_LCD_ClearScreen(BRU_LCD_BLACK_COLOUR, 0, 0, ST7735_WIDTH, ST7735_HEIGHT);
}

void clean_above_menu_display(void)
{
	 bru_LCD_ClearScreen(0x0000, 0, 0, 239, 184);
}

void DrawImage_lib(uint16_t x, uint16_t y, uint16_t w, uint16_t h, const uint16_t* data) {
    if((x >= ST7735_WIDTH) || (y >= ST7735_HEIGHT)) return;
    if((x + w - 1) >= ST7735_WIDTH) return;
    if((y + h - 1) >= ST7735_HEIGHT) return;

    bru_LCD_SetPos(x, y, x+w-1, y+h-1);
    bru_LCD_Write_Data_lib((uint8_t*)data,sizeof(uint16_t)*w*h);
    //ST7735_WriteData((uint8_t*)data, sizeof(uint16_t)*w*h);
}


uint8_t bru_LCD_FontCNT(char symb, bru_LCD_FontDef width_f)
{
    uint8_t wdt_flt = 0;
    wdt_flt = width_f.data_widht[symb-32];
    return(wdt_flt);
}

void bru_LCD_WriteChar_22Arial(uint16_t x, uint16_t y, char ch, bru_LCD_FontDef font, uint16_t color, uint16_t bgcolor, uint8_t font_w)
{
    uint32_t i = 0;
    uint32_t b = 0;
    uint32_t j = 0;
    uint32_t c = 0;
    uint32_t buf_cnt = 0;

    bru_LCD_SetPos(x, y, x+font.width-1, y+font.height-1);



    for(i = 0; i < 2*(font.height); i+=2)//*4
    {
       b = font.data[(ch - 32) * 2*font.height + i];
       c = font.data[(ch - 32) * 2*font.height + i+1];
       for(j = 0; j < 16; j++)  // width_tmp
       {
           if((b << j) & 0x8000)
               data_font[buf_cnt] = color;
           else
               data_font[buf_cnt] = bgcolor;
           buf_cnt++;
       }
       if(buf_cnt > FONT_ARR_SIZE - 16)
       {
           bru_LCD_Write_Data_lib_16(data_font, buf_cnt);
           buf_cnt = 0;
       }
       for(j = 0; j < 13; j++)
       {
           if((c << j) & 0x8000)
               data_font[buf_cnt] = color;
           else
               data_font[buf_cnt] = bgcolor;
           buf_cnt++;
       }
       if(buf_cnt > FONT_ARR_SIZE - 16)
       {
           bru_LCD_Write_Data_lib_16(data_font, buf_cnt);
           buf_cnt = 0;
       }
    }
    if(buf_cnt > 0)
    {
        bru_LCD_Write_Data_lib_16(data_font, buf_cnt);
        buf_cnt = 0;
    }


}


void bru_LCD_WriteString_22Arial(uint16_t x, uint16_t y, const char* str, bru_LCD_FontDef font, uint16_t color, uint16_t bgcolor)
{
    uint8_t f_widht;
    while(*str) {
     f_widht = 0;
     f_widht = bru_LCD_FontCNT(*str, font);
     if(x + font.width >= ST7735_WIDTH) {
         x = 0;
         y += font.height;
         if(y + font.height >= ST7735_HEIGHT) {
             break;
         }
         if(*str == ' ') {
             // skip spaces in the beginning of the new line
             str++;
             continue;
         }
     }
     bru_LCD_WriteChar_22Arial(x, y, *str, font, color, bgcolor, f_widht);
     x += f_widht;
     str++;
   }
}

void bru_LCD_WriteChar_24Arial(uint16_t x, uint16_t y, char ch, bru_LCD_FontDef font, uint16_t color, uint16_t bgcolor, uint8_t font_w)
{
    uint32_t i = 0;
    uint32_t b = 0;
    uint32_t j = 0;
    uint32_t c = 0;
    bru_LCD_SetPos(x, y, x+font.width-1, y+font.height-1);
    for(i = 0; i < 2*(font.height); i+=2)//*4
    {
        b = font.data[(ch - 32) * 2*font.height + i];
        c = font.data[(ch - 32) * 2*font.height + i+1];
        for(j = 0; j < 16; j++)  // width_tmp
        {
            if((b << j) & 0x8000)
            {
                uint8_t data[] = { color >> 8, color & 0xFF };
                bru_LCD_Write_Data_lib(data, sizeof(data));
            }
            else
            {
                uint8_t data[] = { bgcolor >> 8, bgcolor & 0xFF };
                bru_LCD_Write_Data_lib(data, sizeof(data));
            }
        }
        for(j = 0; j < 13; j++)
        {
            if((c << j) & 0x8000)
            {
                uint8_t data[] = { color >> 8, color & 0xFF };
                bru_LCD_Write_Data_lib(data, sizeof(data));
            }
            else
            {
                uint8_t data[] = { bgcolor >> 8, bgcolor & 0xFF };
                bru_LCD_Write_Data_lib(data, sizeof(data));
            }
        }
    }
}


void bru_LCD_WriteString_24Arial(uint16_t x, uint16_t y, const char* str, bru_LCD_FontDef font, uint16_t color, uint16_t bgcolor)
{
    uint8_t f_widht;
    while(*str) {
      f_widht = 0;
      f_widht = bru_LCD_FontCNT(*str, font);
      if(x + font.width >= ST7735_WIDTH) {
          x = 0;
          y += font.height;
          if(y + font.height >= ST7735_HEIGHT) {
              break;
          }
          if(*str == ' ') {
              // skip spaces in the beginning of the new line
              str++;
              continue;
          }
      }
      bru_LCD_WriteChar_24Arial(x, y, *str, font, color, bgcolor, f_widht);
      x += f_widht;
      str++;
    }
}
void bru_LCD_WriteChar_28Arial(uint16_t x, uint16_t y, char ch, bru_LCD_FontDef font, uint16_t color, uint16_t bgcolor, uint8_t font_w)
{
    uint32_t i = 0;
    uint32_t b = 0;
    uint32_t j = 0;
    uint32_t c = 0;
    uint32_t d = 0;
    uint32_t buf_cnt = 0;
    uint8_t qnt = 0;

//    bru_LCD_SetPos(x, y, x+font.width-1, y+font.height-1);
    bru_LCD_SetPos(x, y, x+font_w-1, y+font.height-1);

  
    for(i = 0; i < 3*(font.height); i+=3)//*4
    {
        b = font.data[(ch - 32) * 3*font.height + i];
        c = font.data[(ch - 32) * 3*font.height + i+1];
        d = font.data[(ch - 32) * 3*font.height + i+2];

        if(font_w >= 16)
            qnt = 16;
        else
            qnt = font_w & 0x0f;

        for(j = 0; j < qnt; j++)  // width_tmp
        {
            if((b << j) & 0x8000)
                data_font[buf_cnt] = color;
            else
                data_font[buf_cnt] = bgcolor;
            buf_cnt++;
        }
        if(buf_cnt > (FONT_ARR_SIZE - 16))
        {
            bru_LCD_Write_Data_lib_16(data_font, buf_cnt);
            buf_cnt = 0;
        }

        if(font_w >= 32)
            qnt = 16;
        else if((font_w > 16) && (font_w < 32))
            qnt = font_w & 0x0f;
        else
            qnt = 0;

        for(j = 0; j < qnt; j++)
        {
            if((c << j) & 0x8000)
                data_font[buf_cnt] = color;
            else
                data_font[buf_cnt] = bgcolor;
            buf_cnt++;
        }
        if(buf_cnt > (FONT_ARR_SIZE - 16))
        {
            bru_LCD_Write_Data_lib_16(data_font, buf_cnt);
            buf_cnt = 0;
        }

        if(font_w >= 48)
            qnt = 16;
        else if((font_w > 32) && (font_w < 48))
            qnt = font_w & 0x0f;
        else
            qnt = 0;

        for(j = 0; j < qnt; j++)
        {
            if((d << j) & 0x8000)
                data_font[buf_cnt] = color;
            else
                data_font[buf_cnt] = bgcolor;
            buf_cnt++;
        }
        if(buf_cnt > (FONT_ARR_SIZE - 16))
        {
            bru_LCD_Write_Data_lib_16(data_font, buf_cnt);
            buf_cnt = 0;
        }
    }
    if(buf_cnt > 0)
    {
        bru_LCD_Write_Data_lib_16(data_font, buf_cnt);
        buf_cnt = 0;
    }

  
}
//void bru_LCD_WriteChar_28Arial(uint16_t x, uint16_t y, char ch, bru_LCD_FontDef font, uint16_t color, uint16_t bgcolor, uint8_t font_w)
//{
//    uint32_t i = 0;
//    uint32_t b = 0;
//    uint32_t j = 0;
//    uint32_t c = 0;
//    uint32_t d = 0;
//    uint32_t buf_cnt = 0;
//
//    bru_LCD_SetPos(x, y, x+font.width-1, y+font.height-1);
//
//    for(i = 0; i < 3*(font.height); i+=3)//*4
//    {
//        b = font.data[(ch - 32) * 3*font.height + i];
//        c = font.data[(ch - 32) * 3*font.height + i+1];
//        d = font.data[(ch - 32) * 3*font.height + i+2];
//        for(j = 0; j < 16; j++)  // width_tmp
//        {
//            if((b << j) & 0x8000)
//            {
//             uint8_t data[] = { color >> 8, color & 0xFF };
//             bru_LCD_Write_Data_lib(data, sizeof(data));
//            }
//            else
//            {
//             uint8_t data[] = { bgcolor >> 8, bgcolor & 0xFF };
//             bru_LCD_Write_Data_lib(data, sizeof(data));
//            }
//        }
//        for(j = 0; j < 16; j++)
//        {
//            if((c << j) & 0x8000)
//            {
//             uint8_t data[] = { color >> 8, color & 0xFF };
//             bru_LCD_Write_Data_lib(data, sizeof(data));
//            }
//            else
//            {
//             uint8_t data[] = { bgcolor >> 8, bgcolor & 0xFF };
//             bru_LCD_Write_Data_lib(data, sizeof(data));
//            }
//        }
//
//        for(j = 0; j < 5; j++)
//        {
//            if((d << j) & 0x8000)
//            {
//                uint8_t data[] = { color >> 8, color & 0xFF };
//                bru_LCD_Write_Data_lib(data, sizeof(data));
//            }
//            else
//            {
//                uint8_t data[] = { bgcolor >> 8, bgcolor & 0xFF };
//                bru_LCD_Write_Data_lib(data, sizeof(data));
//            }
//        }
//    }
//}
void bru_LCD_WriteString_28Arial(uint16_t x, uint16_t y, const char* str, bru_LCD_FontDef font, uint16_t color, uint16_t bgcolor)
{
   uint8_t f_widht;
   while(*str) {
       f_widht = 0;
       f_widht = bru_LCD_FontCNT(*str, font);
       if(x + font.width >= ST7735_WIDTH) {
           x = 0;
           y += font.height;
           if(y + font.height >= ST7735_HEIGHT) {
               break;
           }
           if(*str == ' ') {
               // skip spaces in the beginning of the new line
               str++;
               continue;
           }
       }
       bru_LCD_WriteChar_28Arial(x, y, *str, font, color, bgcolor, f_widht);
       x += f_widht;
       str++;
   }
}

void bru_LCD_WriteChar_16Arial_R(uint16_t x, uint16_t y, char ch, bru_LCD_FontDef font, uint16_t color, uint16_t bgcolor, uint8_t font_w)
{
    uint32_t i = 0;
    uint32_t b = 0;
    uint32_t j = 0;
    uint32_t c = 0;
    bru_LCD_SetPos(x, y, x+font.width-1, y+font.height-1);
    for(i = 0; i < 2*(font.height); i+=2)//*4
    {
        b = font.data[(ch - 32) * 2*font.height + i];
        c = font.data[(ch - 32) * 2*font.height + i+1];
        for(j = 0; j < 16; j++)  // width_tmp
        {
            if((b << j) & 0x8000)
            {
              uint8_t data[] = { color >> 8, color & 0xFF };
              bru_LCD_Write_Data_lib(data, sizeof(data));
            }
            else
            {
              uint8_t data[] = { bgcolor >> 8, bgcolor & 0xFF };
              bru_LCD_Write_Data_lib(data, sizeof(data));
            }
        }
        for(j = 0; j < 5; j++)
        {
            if((c << j) & 0x8000)
            {
              uint8_t data[] = { color >> 8, color & 0xFF };
              bru_LCD_Write_Data_lib(data, sizeof(data));
            }
            else
            {
              uint8_t data[] = { bgcolor >> 8, bgcolor & 0xFF };
              bru_LCD_Write_Data_lib(data, sizeof(data));
            }
        }
    }
}


void bru_LCD_WriteString_16Arial_R(uint16_t x, uint16_t y, const char* str, bru_LCD_FontDef font, uint16_t color, uint16_t bgcolor)
{
    uint8_t f_widht;
    while(*str) {
        f_widht = 0;
        f_widht = bru_LCD_FontCNT(*str, font);
        if(x + font.width >= ST7735_WIDTH) {
            x = 0;
            y += font.height;
            if(y + font.height >= ST7735_HEIGHT) {
                break;
            }
            if(*str == ' ') {
                // skip spaces in the beginning of the new line
                str++;
                continue;
            }
        }
        bru_LCD_WriteChar_16Arial_R(x, y, *str, font, color, bgcolor, f_widht);
        x += f_widht;
        str++;
    }
}
void bru_LCD_WriteChar_16Arial(uint16_t x, uint16_t y, char ch, bru_LCD_FontDef font, uint16_t color, uint16_t bgcolor, uint8_t font_w)
 {
    uint32_t i = 0;
    uint32_t b = 0;
    uint32_t j = 0;
    uint32_t c = 0;
    bru_LCD_SetPos(x, y, x+font.width-1, y+font.height-1);
    for(i = 0; i < 2*(font.height); i+=2)//*4
    {
        b = font.data[(ch - 32) * 2*font.height + i];
        c = font.data[(ch - 32) * 2*font.height + i+1];
        for(j = 0; j < 16; j++)  // width_tmp
        {
            if((b << j) & 0x8000)
            {
                uint8_t data[] = { color >> 8, color & 0xFF };
                bru_LCD_Write_Data_lib(data, sizeof(data));
            }
            else
            {
                uint8_t data[] = { bgcolor >> 8, bgcolor & 0xFF };
                bru_LCD_Write_Data_lib(data, sizeof(data));
            }
        }
        for(j = 0; j < 5; j++)
        {
            if((c << j) & 0x8000)
            {
                uint8_t data[] = { color >> 8, color & 0xFF };
                bru_LCD_Write_Data_lib(data, sizeof(data));
            }
            else
            {
                uint8_t data[] = { bgcolor >> 8, bgcolor & 0xFF };
                bru_LCD_Write_Data_lib(data, sizeof(data));
            }
        }
     }
 }


void bru_LCD_WriteString_16Arial(uint16_t x, uint16_t y, const char* str, bru_LCD_FontDef font, uint16_t color, uint16_t bgcolor)
{
  uint8_t f_widht;
  while(*str) {
      f_widht = 0;
      f_widht = bru_LCD_FontCNT(*str, font);
      if(x + font.width >= ST7735_WIDTH) {
          x = 0;
          y += font.height;
          if(y + font.height >= ST7735_HEIGHT) {
              break;
          }
          if(*str == ' ') {
              // skip spaces in the beginning of the new line
              str++;
              continue;
          }
      }
      bru_LCD_WriteChar_16Arial(x, y, *str, font, color, bgcolor, f_widht);
      x += f_widht;
      str++;
  }
}
void bru_LCD_WriteChar_14Arial(uint16_t x, uint16_t y, char ch, bru_LCD_FontDef font, uint16_t color, uint16_t bgcolor, uint8_t font_w)
{
    uint32_t i = 0;
    uint32_t b = 0;
    uint32_t j = 0;
    uint32_t c = 0;
    uint32_t buf_cnt = 0;

    bru_LCD_SetPos(x, y, x+font.width-1, y+font.height-1);

 

    for(i = 0; i < 2*(font.height); i+=2)//*4
    {
      b = font.data[(ch - 32) * 2*font.height + i];
      c = font.data[(ch - 32) * 2*font.height + i+1];
      for(j = 0; j < 16; j++)  // width_tmp
      {
          if((b << j) & 0x8000)
              data_font[buf_cnt] = color;
          else
              data_font[buf_cnt] = bgcolor;
          buf_cnt++;
      }
      if(buf_cnt > FONT_ARR_SIZE - 16)
      {
          bru_LCD_Write_Data_lib_16(data_font, buf_cnt);
          buf_cnt = 0;
      }
      for(j = 0; j < 5; j++)
      {
          if((c << j) & 0x8000)
              data_font[buf_cnt] = color;
          else
              data_font[buf_cnt] = bgcolor;
      }
      if(buf_cnt > FONT_ARR_SIZE - 16)
      {
          bru_LCD_Write_Data_lib_16(data_font, buf_cnt);
          buf_cnt = 0;
      }
    }
    if(buf_cnt > 0)
    {
        bru_LCD_Write_Data_lib_16(data_font, buf_cnt);
        buf_cnt = 0;
    }

   
}


void bru_LCD_WriteString_14Arial(uint16_t x, uint16_t y, const char* str, bru_LCD_FontDef font, uint16_t color, uint16_t bgcolor)
{
    uint8_t f_widht;
    while(*str) {
        f_widht = 0;
        f_widht = bru_LCD_FontCNT(*str, font);
        if(x + font.width >= ST7735_WIDTH)
        {
            x = 0;
            y += font.height;
            if(y + font.height >= ST7735_HEIGHT)
            {
                break;
            }

            if(*str == ' ')
            {
                // skip spaces in the beginning of the new line
                str++;
                continue;
            }
        }
        bru_LCD_WriteChar_14Arial(x, y, *str, font, color, bgcolor, f_widht);
        x += f_widht;
        str++;
    }
}
//void bru_LCD_WriteChar_16MBArial(uint16_t x, uint16_t y, char ch, bru_LCD_FontDef font, uint16_t color, uint16_t bgcolor, uint8_t font_w)
//{
//    uint32_t i = 0;
//    uint32_t b = 0;
//    uint32_t j = 0;
//    uint32_t c = 0;
//    uint32_t buf_cnt = 0;
//
//    bru_LCD_SetPos(x, y, x+font.width-1, y+font.height-1);
//    for(i = 0; i < 2*(font.height); i+=2)//*4
//    {
//         b = font.data[(ch - 32) * 2*font.height + i];
//         c = font.data[(ch - 32) * 2*font.height + i+1];
//         for(j = 0; j < 16; j++)  // width_tmp
//         {
//             if((b << j) & 0x8000)
//             {
//                 data_font[buf_cnt] = (color >> 8);
//                 buf_cnt++;
//                 data_font[buf_cnt] = (color & 0xFF);
//                 buf_cnt++;
//             }
//             else
//             {
//                 data_font[buf_cnt] = (bgcolor >> 8);
//                 buf_cnt++;
//                 data_font[buf_cnt] = (bgcolor & 0xFF);
//                 buf_cnt++;
//             }
//         }
//         for(j = 0; j < 5; j++)
//         {
//             if((c << j) & 0x8000)
//             {
//                 data_font[buf_cnt] = (color >> 8);
//                 buf_cnt++;
//                 data_font[buf_cnt] = (color & 0xFF);
//                 buf_cnt++;
//             }
//             else
//             {
//                 data_font[buf_cnt] = (bgcolor >> 8);
//                 buf_cnt++;
//                 data_font[buf_cnt] = (bgcolor & 0xFF);
//                 buf_cnt++;
//             }
//         }
//    }
//}
void bru_LCD_WriteChar_16MBArial(uint16_t x, uint16_t y, char ch, bru_LCD_FontDef font, uint16_t color, uint16_t bgcolor, uint8_t font_w)
{
    uint32_t i = 0;
    uint32_t b = 0;
    uint32_t j = 0;
    uint32_t c = 0;
    uint32_t buf_cnt = 0;

    bru_LCD_SetPos(x, y, x+font.width-1, y+font.height-1);

  
    for(i = 0; i < 2*(font.height); i+=2)//*4
    {
         b = font.data[(ch - 32) * 2*font.height + i];
         c = font.data[(ch - 32) * 2*font.height + i+1];
         for(j = 0; j < 16; j++)  // width_tmp
         {
             if((b << j) & 0x8000)
                 data_font[buf_cnt] = color;
             else
                 data_font[buf_cnt] = bgcolor;
             buf_cnt++;
         }
         if(buf_cnt > FONT_ARR_SIZE - 16)
         {
             bru_LCD_Write_Data_lib_16(data_font, buf_cnt);
             buf_cnt = 0;
         }
         for(j = 0; j < 5; j++)
         {
             if((c << j) & 0x8000)
                 data_font[buf_cnt] = color;
             else
                 data_font[buf_cnt] = bgcolor;
             buf_cnt++;
         }
         if(buf_cnt > FONT_ARR_SIZE - 16)
         {
             bru_LCD_Write_Data_lib_16(data_font, buf_cnt);
             buf_cnt = 0;
         }
    }
    if(buf_cnt > 0)
    {
        bru_LCD_Write_Data_lib_16(data_font, buf_cnt);
        buf_cnt = 0;
    }

  
}

void bru_LCD_WriteString_16MBArial(uint16_t x, uint16_t y, const char* str, bru_LCD_FontDef font, uint16_t color, uint16_t bgcolor)
{
  uint8_t f_widht;
  while(*str)
  {
      f_widht = 0;
      f_widht = bru_LCD_FontCNT(*str, font);
      if(x + font.width >= ST7735_WIDTH)
      {
          x = 0;
          y += font.height;
          if(y + font.height >= ST7735_HEIGHT)
          {
              break;
          }
          if(*str == ' ')
          {
              // skip spaces in the beginning of the new line
              str++;
              continue;
          }
      }
      bru_LCD_WriteChar_16MBArial(x, y, *str, font, color, bgcolor, f_widht);
      x += f_widht;
      str++;
  }
}
//void bru_LCD_WriteChar_18Arial_R(uint16_t x, uint16_t y, char ch, bru_LCD_FontDef font, uint16_t color, uint16_t bgcolor, uint8_t font_w) // for Arial 24
//{
//  uint32_t i, b, j, c;
//  volatile uint8_t k;
//  bru_LCD_SetPos(x, y, x+font.width-1, y+font.height-1);
//  for(i = 0; i < 2*(font.height); i+=2)//*4
//   {
//       b = font.data[(ch - 32) * 2*font.height + i];
//       c = font.data[(ch - 32) * 2*font.height + i+1];
//       for(j = 0; j < 16; j++)  // width_tmp
//       {
//           if((b << j) & 0x8000)  {
//               uint8_t data[] = { color >> 8, color & 0xFF };
//               bru_LCD_Write_Data_lib(data, sizeof(data));
//           } else {
//               uint8_t data[] = { bgcolor >> 8, bgcolor & 0xFF };
//               bru_LCD_Write_Data_lib(data, sizeof(data));
//           }
//       }
//       k++;
//       k++;
//       for(j = 0; j < 8; j++)
//       {
//           if((c << j) & 0x8000)  {
//               uint8_t data[] = { color >> 8, color & 0xFF };
//               bru_LCD_Write_Data_lib(data, sizeof(data));
//           } else {
//               uint8_t data[] = { bgcolor >> 8, bgcolor & 0xFF };
//               bru_LCD_Write_Data_lib(data, sizeof(data));
//           }
//       }
//       k++;
//       k++;
//   }
//}
//void bru_LCD_WriteChar_18Arial_R(uint16_t x, uint16_t y, char ch, bru_LCD_FontDef font, uint16_t color, uint16_t bgcolor, uint8_t font_w) // for Arial 24
//{
//  uint32_t i = 0;
//  uint32_t b = 0;
//  uint32_t j = 0;
//  uint32_t c = 0;
//  uint32_t buf_cnt = 0;
//
//  bru_LCD_SetPos(x, y, x+font.width-1, y+font.height-1);
//
//  SPI_close(SpiHandler);
//  SpiParams->dataSize = 16;
//  SpiHandler = SPI_open(CONFIG_LCD_SPI, SpiParams);
//
//  for(i = 0; i < 2*(font.height); i+=2)//*4
//  {
//       b = font.data[(ch - 32) * 2*font.height + i];
//       c = font.data[(ch - 32) * 2*font.height + i+1];
//       for(j = 0; j < 16; j++)
//       {
//           if((b << j) & 0x8000)
//           {
//               data_font[buf_cnt] = color;
//               buf_cnt++;
////               data_font[buf_cnt] = (color >> 8);
////               buf_cnt++;
////               data_font[buf_cnt] = (color & 0xFF);
////               buf_cnt++;
//           }
//           else
//           {
//               data_font[buf_cnt] = bgcolor;
//               buf_cnt++;
////               data_font[buf_cnt] = (bgcolor >> 8);
////               buf_cnt++;
////               data_font[buf_cnt] = (bgcolor & 0xFF);
////               buf_cnt++;
//           }
//       }
//       if((buf_cnt/2) > FONT_ARR_SIZE - 32)
//       {
//           bru_LCD_Write_Data_lib_16(data_font, buf_cnt/2);
//           buf_cnt = 0;
//       }
//       for(j = 0; j < 8; j++)
//       {
//           if((c << j) & 0x8000)
//           {
//               data_font[buf_cnt] = color;
//               buf_cnt++;
////               data_font[buf_cnt] = (color >> 8);
////               buf_cnt++;
////               data_font[buf_cnt] = (color & 0xFF);
////               buf_cnt++;
//           }
//           else
//           {
//               data_font[buf_cnt] = bgcolor;
//               buf_cnt++;
////               data_font[buf_cnt] = (bgcolor >> 8);
////               buf_cnt++;
////               data_font[buf_cnt] = (bgcolor & 0xFF);
////               buf_cnt++;
//           }
//       }
//       if(buf_cnt > FONT_ARR_SIZE - 32)
//       {
//           bru_LCD_Write_Data_lib_16(data_font, buf_cnt);
//           buf_cnt = 0;
//       }
//   }
//   if(buf_cnt > 0)
//   {
//       bru_LCD_Write_Data_lib_16(data_font, buf_cnt);
//        buf_cnt = 0;
//   }
//   SPI_close(SpiHandler);
//   SpiParams->dataSize = 8;
//   SpiHandler = SPI_open(CONFIG_LCD_SPI, SpiParams);
//}
void bru_LCD_WriteChar_18Arial_R(uint16_t x, uint16_t y, char ch, bru_LCD_FontDef font, uint16_t color, uint16_t bgcolor, uint8_t font_w) // for Arial 24
{
  uint32_t i = 0;
  uint32_t b = 0;
  uint32_t j = 0;
  uint32_t c = 0;
  uint32_t buf_cnt = 0;

  bru_LCD_SetPos(x, y, x+font.width-1, y+font.height-1);

 
  for(i = 0; i < 2*(font.height); i+=2)//*4
  {
       b = font.data[(ch - 32) * 2*font.height + i];
       c = font.data[(ch - 32) * 2*font.height + i+1];
       for(j = 0; j < 16; j++)
       {
           if((b << j) & 0x8000)
               data_font[buf_cnt] = color;
           else
               data_font[buf_cnt] = bgcolor;
           buf_cnt++;
       }
       if(buf_cnt > (FONT_ARR_SIZE - 16))
       {
           bru_LCD_Write_Data_lib_16(data_font, buf_cnt);
           buf_cnt = 0;
       }
       for(j = 0; j < 8; j++)
       {
           if((c << j) & 0x8000)
               data_font[buf_cnt] = color;
           else
               data_font[buf_cnt] = bgcolor;
           buf_cnt++;
       }
       if(buf_cnt > (FONT_ARR_SIZE - 16))
       {
           bru_LCD_Write_Data_lib_16(data_font, buf_cnt);
           buf_cnt = 0;
       }
   }
   if(buf_cnt > 0)
   {
       bru_LCD_Write_Data_lib_16(data_font, buf_cnt);
       buf_cnt = 0;
   }
  
}

void bru_LCD_WriteString_18Arial_R(uint16_t x, uint16_t y, const char* str, bru_LCD_FontDef font, uint16_t color, uint16_t bgcolor)
{
    uint8_t f_widht;
    while(*str) {
        f_widht = 0;
        f_widht = bru_LCD_FontCNT(*str, font);
        if(x + font.width >= ST7735_WIDTH) {
            x = 0;
            y += font.height;
            if(y + font.height >= ST7735_HEIGHT) {
                break;
            }
            if(*str == ' ') {
                // skip spaces in the beginning of the new line
                str++;
                continue;
            }
        }
        bru_LCD_WriteChar_18Arial_R(x, y, *str, font, color, bgcolor, f_widht);
        x += f_widht;
        str++;
    }
}

void bru_LCD_LinesForButtons(uint16_t color){
    bru_LCD_DrawLine(20,185,220,185,0xffff);
    bru_LCD_DrawLine(120,185,120,240,0xffff);
}

void bru_LCD_Draw_buttons_ENG(const char* l_button,const char* r_button){
    if(compare_string((const uint8_t*)l_button,(const uint8_t*)"OK") == 0){
        bru_LCD_WriteString_16MBArial(85,191,l_button,Font_21X24MB,0xffff,0x0000);
    }
    else if(compare_string((const uint8_t*)l_button,(const uint8_t*)"STOP") == 0){
        bru_LCD_WriteString_16MBArial(54,191,l_button,Font_21X24MB,0xffff,0x0000);
    }
    else{
        bru_LCD_WriteString_16MBArial(46,191,l_button,Font_21X24MB,0xffff,0x0000);
        bru_LCD_WriteString_16MBArial(128,191,r_button,Font_21X24MB,0xffff,0x0000);
    }
}


/** @brief  : draw picture in image buffer
  * @param  : 
  * @retval : None
  */
void lcd_draw_picture(uint8_t x1,uint8_t y1,uint8_t w,uint8_t h,uint8_t *p_buf)
{

	
	bru_LCD_SetPos(x1, y1, x1+w-1, y1+h-1);
		
	uint32_t len=(w)*(h)<<1;
	
	uint8_t remain=len%255;
	uint16_t loop=len-remain;
	uint8_t *p;
	uint8_t *pEnd = p_buf+loop;
	nrf_gpio_pin_set(LCD_DC_PIN);
	for(p=p_buf;p<pEnd;p+=255)
	{
		BSP_spi2_send(p,255);
	}
	if(remain)
	{
		BSP_spi2_send(p,remain);
	}
}

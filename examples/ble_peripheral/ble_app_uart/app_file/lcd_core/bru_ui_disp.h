#ifndef __BRU_UI_DISP_H
#define __BRU_UI_DISP_H

#include "bru_LCD_Driver.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef enum
{
	ENCODE_LEFT,
	ENCODE_RIGHT,
} ENCODE_DIR;

typedef enum
{
	TIME_SEL,
	ALARM_SEL,
} ALARM_TIME_SEL;

typedef enum
{
	ADJ_HOUR_POS,
	ADJ_MINUTE_POS,
} ADJ_TIME_POS;

typedef enum
{
  UI_WINDOW_NULL = 0,
  UI_WINDOW_INTRO_SCREEN,
  UI_WINDOW_HOME_SCREEN,

//    UI_WINDOW_PRESET_MENU_HOME,
//    UI_WINDOW_PRESET_MENU_EDIT_TEMPERATURE,
//    UI_WINDOW_PRESET_MENU_EDIT_TIME,
//    UI_WINDOW_PRESET_MENU_EDIT_WATER_AMOUNT,
//    UI_WINDOW_PRESETS_TO_DEFAULT,

    UI_WINDOW_BREWING_SETUP_MENU_TIME,
    UI_WINDOW_BREWING_SETUP_MENU_TEMPERATURE,
    UI_WINDOW_BREWING_SETUP_MENU_WATER_AMOUNT,
    UI_WINDOW_BREWING_SETUP_MENU_EDIT_TIME,
    UI_WINDOW_BREWING_SETUP_MENU_EDIT_TEMPERATURE,
    UI_WINDOW_BREWING_SETUP_MENU_EDIT_WATER_AMOUNT,

//    UI_WINDOW_MAIN_SETUP_MENU_BREWING_SETUP,
//    UI_WINDOW_MAIN_SETUP_MENU_PRESET_SETUP,
//    UI_WINDOW_MAIN_SETUP_MENU_MACHINE_SETUP,

  UI_WINDOW_BREWING_STATUS_FILLING_UP,
//    UI_WINDOW_BREWING_STATUS_COUNTDOWN,
  UI_WINDOW_BREWING_STATUS_STEEPING,
	UI_WINDOW_BREWING_STEEPING_CANCEL,
  UI_WINDOW_BREWING_STATUS_SERVING,
  UI_WINDOW_BREWING_STATUS_CHAMBER_WASHING,
  UI_WINDOW_BREWING_STATUS_CHAMBER_WASHING_SERVING,
//    UI_WINDOW_BREWING_STATUS_TEA_COOLING,
//    UI_WINDOW_BREWING_STATUS_TEA_COOLING_SERVING,
  UI_WINDOW_BREWING_STATUS_ENJOY,
  UI_WINDOW_BREWING_STATUS_REMOVE_CUP,

  UI_WINDOW_ERROR_MSG_PLACE_CUP,
  UI_WINDOW_ERROR_MSG_CLOSE_LID,
  UI_WINDOW_ERROR_MSG_ADD_WATER,
//    UI_WINDOW_ERROR_MSG_REPLACE_FILTER,
//    UI_WINDOW_ERROR_MSG_CLEAN_THE_MACHINE,
  UI_WINDOW_ERROR_MSG_PLACE_BREWING_CHAMBER,
  UI_WINDOW_ERROR_MSG_ERROR_XXX,
	
	UI_WINDOW_PRESET_STATUS_MENU,
	UI_WINDOW_ALARM_SCREEN,
	UI_WINDOW_DISPENSER_STATUS_MENU,
	UI_WINDOW_DISPENSER_STATUS_DISPENSING,

//    UI_WINDOW_PRE_BREWING_ERRORS_CHECK,
//    UI_WINDOW_PRE_WASHING_ERRORS_CHECK,

//    UI_WINDOW_MACHINE_SETUP_BLE_STATE,
//    UI_WINDOW_MACHINE_SETUP_BLE_STATE_EDIT,
//    UI_WINDOW_MACHINE_SETUP_LANGUAGE,
//    UI_WINDOW_MACHINE_SETUP_LANGUAGE_EDIT,
//    UI_WINDOW_MACHINE_SETUP_CURRENT_TIME,
//    UI_WINDOW_MACHINE_SETUP_CURRENT_TIME_HOURS_EDIT,
//    UI_WINDOW_MACHINE_SETUP_CURRENT_TIME_MINUTES_EDIT,
//    UI_WINDOW_MACHINE_SETUP_UNITS,
//    UI_WINDOW_MACHINE_SETUP_UNITS_EDIT,
//    UI_WINDOW_MACHINE_SETUP_RINSE_CONFIG,
//    UI_WINDOW_MACHINE_SETUP_RINSE_CONFIG_EDIT,
//    UI_WINDOW_MACHINE_SETUP_WATER_FILTER_STATUS,
//    UI_WINDOW_MACHINE_SETUP_WATER_FILTER_STATUS_EDIT,
//    UI_WINDOW_MACHINE_SETUP_MAINTENANCE_STATUS,
//    UI_WINDOW_MACHINE_SETUP_MAINTENANCE_STATUS_EDIT,
//    UI_WINDOW_MACHINE_SETUP_RGB_STATE,
//    UI_WINDOW_MACHINE_SETUP_RGB_STATE_EDIT,
//    UI_WINDOW_MACHINE_SETUP_BUZZER_STATE,
//    UI_WINDOW_MACHINE_SETUP_BUZZER_STATE_EDIT,
//    UI_WINDOW_MACHINE_SETUP_TIME_FORMAT,
//    UI_WINDOW_MACHINE_SETUP_TIME_FORMAT_EDIT,

//    UI_WINDOW_ALARM_SETUP_CURRENT_TIME,
//    UI_WINDOW_ALARM_SETUP_CURRENT_TIME_HOURS_EDIT,
//    UI_WINDOW_ALARM_SETUP_CURRENT_TIME_MINUTES_EDIT,
//    UI_WINDOW_ALARM_SETUP_ALARM_TIME,
//    UI_WINDOW_ALARM_SETUP_ALARM_TIME_HOURS_EDIT,
//    UI_WINDOW_ALARM_SETUP_ALARM_TIME_MINUTES_EDIT,

//    UI_WINDOW_ALARM_SETUP_PRESET_EDIT,


//    UI_WINDOW_BREWING_CANCELING,
//    UI_WINDOW_FIRMWARE_UPDATE,
		UI_WINDOW_TEST_1,
		UI_WINDOW_TEST_2,
		UI_WINDOW_TEST_3,
		UI_WINDOW_TEST_4,
		UI_WINDOW_TEST_5,
		UI_WINDOW_TEST_6,
		UI_WINDOW_TEST_7,
} bru_UI_Window_ID;

typedef enum
{
	b__,
	bOK,
	bSTART,
	bSTOP,
	bBACK,
	bEDIT,
	bNEXT,
	bSAVE,
	bEXIT,
	bMENU,	
} bru_BTN_Label;

void bru_LCD_Update_Screen(bru_UI_Window_ID Window_ID, bool init);

// ========================================================================== //

void bru_BrewingTime_adj_steepint_time_ENG(uint8_t dir);
void bru_BrewingTime_adj_water_temper_ENG(uint8_t dir);
void bru_BrewingTime_adj_water_amount_ENG(uint8_t dir);

void bru_sys_time_alarm_adj_hour_ENG(uint8_t mode,uint8_t dir);
void bru_sys_time_alarm_adj_minute_ENG(uint8_t mode,uint8_t dir);

//========================== WINDOW_SCREENS ================================//

//=====steeping, hot water, amount=================
void bru_LCD_BrewingTime_Steeping_ENG(uint8_t *flag);
void bru_LCD_WaterTemperature_ENG(uint8_t * flag);
void bru_LCD_WaterAmount_ENG(uint8_t * flag);

//=====steeping, hot water, amount edit=================
void bru_LCD_BrewingTime_Steeping_Edit_ENG(uint8_t * flag,const char * menu_name);
void bru_LCD_WaterTemperature_Edit_ENG(uint8_t * flag,const char *menu_name);
void bru_LCD_WaterAmount_Edit_ENG(uint8_t * flag,const char *menu_name);

void bru_LCD_System_Time_ENG(uint8_t *flag,uint8_t mode);
void bru_LCD_Alarm_Preset_ENG(uint8_t *flag);

void bru_LCD_System_Time_Edit_ENG(uint8_t *flag,uint8_t mode,uint8_t adj_pos);

//=======Preset menu=============
void bru_LCD_Preset_1_to_3_Menu_ENG(uint8_t *flag,const char *menu_name);
void bru_LCD_Preset_HotWater_Menu_ENG(uint8_t *flag,const char *menu_name);
void bru_LCD_Preset_Alarm_Menu_ENG(uint8_t *flag,const char *menu_name);

void bru_LCD_Preset_default_presets_Menu_ENG(uint8_t *flag);

//=============================MACHINE SETUP=================================//
void bru_LCD_Bluetooth_State_ENG(uint8_t *flag);
void bru_LCD_Language_ENG(uint8_t *flag);
void bru_LCD_Timeformat_ENG(uint8_t *flag);
void bru_LCD_Units_ENG(uint8_t *flag);
void bru_LCD_Rinse_config_ENG(uint8_t *flag);
void bru_LCD_Water_filter_status_123_liters_ENG(uint8_t *flag);
void bru_LCD_Maintenance_status_ENG(uint8_t *flag);
void bru_LCD_RGB_state_ENG(uint8_t *flag);
void bru_LCD_Buzzer_state_ENG(uint8_t *flag);


void bru_ble_on_off_ENG(void);
void bru_Bluetooth_State_adj_ENG(void);
void bru_LCD_Bluetooth_State_Edit_ENG(uint8_t *flag);

void bru_Timeformat_adj_ENG(void);
void bru_LCD_Timeformat_Edit_ENG(uint8_t *flag);

void bru_Units_adj_ENG(void);
void bru_LCD_Units_Edit_ENG(uint8_t *flag);

void bru_Rinse_adj_ENG(uint8_t dir);
void bru_LCD_Rinse_config_Edit_ENG(uint8_t *flag);

void bru_RGB_State_adj_ENG(void);
void bru_LCD_RGB_state_Edit_ENG(uint8_t *flag);

void bru_Buzzer_State_adj_ENG(void);
void bru_LCD_Buzzer_state_Edit_ENG(uint8_t *flag);
//======== main setup menu ======
void bru_LCD_Main_Menu_Steeping_Setup_ENG(uint8_t* mode);
void bru_LCD_Main_Menu_Preset_Setup_ENG(uint8_t* mode);
void bru_LCD_Main_Menu_Machine_Setup_ENG(uint8_t* mode);

#ifdef __cplusplus
           }
#endif



#endif

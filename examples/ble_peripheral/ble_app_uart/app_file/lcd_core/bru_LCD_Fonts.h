#ifndef BRU_LCD_FONTS_H_
#define BRU_LCD_FONTS_H_

#include "stdint.h"



#ifndef FONTS_STRUCT
    #define FONTS_STRUCT
    typedef struct {
      const uint8_t width;
      uint8_t height;
      const uint16_t *data;
      const uint8_t *data_widht;
    }bru_LCD_FontDef;
#endif

extern const uint8_t btn_black_16x24_icon[];
extern const uint8_t btn_white_16x24_icon[];
extern const uint8_t btn_red_16x24_icon[];	
extern const uint8_t ble_16x24_icon[];
extern const uint8_t oz_unit_48x28_icon[];
extern bru_LCD_FontDef Font_29x41;
extern bru_LCD_FontDef Font_33x45;
extern bru_LCD_FontDef Font_37x52;
extern bru_LCD_FontDef Font_21x24;
extern bru_LCD_FontDef Font_21x30;
extern bru_LCD_FontDef Font_19x22;
extern bru_LCD_FontDef Font_21X24MB;
extern bru_LCD_FontDef Font_24x27;

#endif

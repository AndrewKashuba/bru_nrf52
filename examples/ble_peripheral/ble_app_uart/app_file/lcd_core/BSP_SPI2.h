#ifndef __BSP_SPI2_H_
#define __BSP_SPI2_H_
#include "main.h" 


#ifdef __cplusplus
extern "C" {
#endif

#define RESETB_PIN			03        
#define SPI2_MOSI_PIN		30				
#define SPI2_SCK_PIN		02							
#define LCD_DC_PIN			31								


void BSP_spi2_init(void);
void BSP_spi2_uninit(void);
void BSP_spi2_send(uint8_t *tx_buf,uint32_t len);
void BSP_spi2_receive(uint8_t *rx_buf,uint32_t len);
void BSP_spi2_transfer(uint8_t *tx_buf,uint8_t *rx_buf,uint8_t tx_len,uint8_t rx_len);

//void spi2_cs_pin_en(void);
//void spi2_cs_pin_dis(void);

#ifdef __cplusplus
           }
#endif

#endif








#include "dbprintf.h"
#include "bru_ui_disp.h"
#include "bru_LCD_Driver.h"
#include "target_command.h"
#include "main.h"
#include "bru_version.h"
#include "CtrlUnits.h"
#include "bru_PinchValve.h"
#include "bru_Context.h"

#define FontColor 0xffff // Green - 0x000f, White - 0xffff, Black - 0x0000

static char timeBuff[32] = {0} ;
static char waterAmountBuff[32] = {0} ;
static char temperatureBuff[32] = {0} ;
static char outputBuffer[16] = {0};

static char current_time_Buff[12];
static char alarm_time_Buff[12];
static unix_cal timeStructure = {0};
static unix_cal alarmStructure = {0};

extern bru_Sensor_Status Sensors;
extern bru_Actuators_Status Actuators;

// New =================== User Interface Screens =========================== //

static uint8_t bru_preset_com_menu(bru_UI_Interface_Handle hUserInterface,char *preset_num_Buff,uint8_t *flag,const char *menu_name);

static uint32_t convert_C_to_F(float c)
{
	float temper = c;
	temper *= 1.8f;
	temper += 32.5f; 
	return (uint32_t)temper;
}

static uint32_t convert_ml_to_oz(float ml)
{
	float oz = ml;
	oz *= 0.0338181f;
	oz += 0.5f;
	return (uint32_t)oz;
}

static uint8_t convert_timeformat_24_to_12(uint8_t hour,uint8_t *flag)
{
	if(hour < 12)
	{				 
		if(hour == 0)    // AM
			hour = 12;
		*flag = 0;
	}
	else
	{					
		if(hour != 12)   // PM
			hour -= 12;
		*flag = 1;
	}
	return hour;
}

static void string_format_func(bru_UI_Interface_Handle hUserInterface,char *preset_num_Buff)
{
sprintf(preset_num_Buff,"???");
sprintf(temperatureBuff,"???");
sprintf(timeBuff,"??:??");
sprintf(waterAmountBuff,"???");
}

void bru_LCD_Draw_buttons(const char* l_button, const char* r_button) {
    if(compare_string((const uint8_t*)l_button,(const uint8_t*)"OK") == 0){
        bru_LCD_WriteString_16MBArial(85,191,l_button,Font_21X24MB,0xffff,0x0000);
    }
    else if(compare_string((const uint8_t*)l_button,(const uint8_t*)"STOP") == 0){
        bru_LCD_WriteString_16MBArial(54,191,l_button,Font_21X24MB,0xffff,0x0000);
    }
    else{
        bru_LCD_WriteString_16MBArial(46,191,l_button,Font_21X24MB,0xffff,0x0000);
        bru_LCD_WriteString_16MBArial(128,191,r_button,Font_21X24MB,0xffff,0x0000);
    }
}

////////////////////////////////////////////////////////////////////////////////

static char* GetBtnLabel(bru_BTN_Label lbl)
{
	switch(lbl)
	{
		case b__: return "";
		case bOK: return "OK";
		case bSTART: return "START";
		case bSTOP: return "STOP";
		case bBACK: return "BACK";
		case bEDIT: return "EDIT";
		case bNEXT: return "NEXT";
		case bSAVE: return "SAVE";
		case bEXIT: return "EXIT";
		case bMENU: return "MENU";
		default: return "???";		
	}
}

static void bru_LCD_Draw_Buttons(void) 
{
	uint16_t f_color;
	uint16_t x;
	extern bru_Context BruTask;
	
	switch(BruTask.LeftLabel)
	{
		case bOK: x = 85;	break;
		case bSTOP: x = 54;	break;		
		default: x = 46; break;
	}
	if(BruTask.MainButton == MAIN_BTN_LEFT || BruTask.MainButton == MAIN_BTN_BOTH) 
		f_color = 0xFF9F; //BRU_LCD_MAIN_BTN_COLOUR; //BRU_LCD_WHITE_COLOUR; //BRU_LCD_GREEN_COLOUR;
	else 
		f_color = BRU_LCD_WHITE_COLOUR;

	bru_LCD_WriteString_16MBArial(x,191,GetBtnLabel(BruTask.LeftLabel),Font_21X24MB,f_color,0x0000);
	
	if(BruTask.MainButton == MAIN_BTN_RIGHT || BruTask.MainButton == MAIN_BTN_BOTH) 
		f_color = 0xFF9F; //BRU_LCD_MAIN_BTN_COLOUR; //BRU_LCD_WHITE_COLOUR;	//BRU_LCD_GREEN_COLOUR;
	else 
		f_color = BRU_LCD_WHITE_COLOUR;

	bru_LCD_WriteString_16MBArial(128,191,GetBtnLabel(BruTask.RightLabel),Font_21X24MB,f_color,0x0000);
	
  bru_LCD_LinesForButtons(FontColor);		
}

void bru_LCD_Redraw_Buttons(void) 
{
	bru_LCD_ClearScreen(BRU_LCD_BLACK_COLOUR, 20, 185, 220, 240);
	bru_LCD_Draw_Buttons();
}
	
static char* get_preset_num(void)
{
	bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
	sprintf(outputBuffer, "%01d", hUserInterface->iPresetCurrentIndex);	
	return outputBuffer;
}

static char* get_preset_temp(void)
{
	bru_UI_Interface_Handle  hUserInterface = get_ui_interface();

	if(hUserInterface->presetsArr[hUserInterface->iPresetCurrentIndex].temperature >= BRU_PRESET_TEMPERATURE_MAX)
    sprintf(temperatureBuff, "MAX");
  else if(hUserInterface->presetsArr[hUserInterface->iPresetCurrentIndex].temperature <= BRU_PRESET_TEMPERATURE_MIN)
    sprintf(temperatureBuff, "MIN");
  else if(hUserInterface->hMachineSetup->units == MS_METRIC)
    sprintf(temperatureBuff, "%02d*C", (uint32_t)(hUserInterface->presetsArr[hUserInterface->iPresetCurrentIndex].temperature));
	else 
	{
		uint32_t temper = convert_C_to_F(hUserInterface->presetsArr[hUserInterface->iPresetCurrentIndex].temperature);
		sprintf(temperatureBuff, "%02d*F", temper);
	}
	return temperatureBuff;		
}

static char* get_preset_time(void)
{
	bru_UI_Interface_Handle  hUserInterface = get_ui_interface();

  sprintf(timeBuff, "%02d:%02d", (hUserInterface->presetsArr[hUserInterface->iPresetCurrentIndex].brewingTime)/60, 
																(hUserInterface->presetsArr[hUserInterface->iPresetCurrentIndex].brewingTime)%60);
	return timeBuff;
}

static char* get_preset_water(void)
{
	bru_UI_Interface_Handle  hUserInterface = get_ui_interface();

	if(hUserInterface->presetsArr[hUserInterface->iPresetCurrentIndex].waterAmount >= BRU_PRESET_WATER_AMOUNT_MAX)
		sprintf(waterAmountBuff, " INF "); // MAX
	else if(hUserInterface->presetsArr[hUserInterface->iPresetCurrentIndex].waterAmount <= BRU_PRESET_WATER_AMOUNT_MIN)
		sprintf(waterAmountBuff, "MIN");
	else if(hUserInterface->hMachineSetup->units == MS_METRIC)
		sprintf(waterAmountBuff, "%2dml", (uint32_t)(hUserInterface->presetsArr[hUserInterface->iPresetCurrentIndex].waterAmount));
	else
	{
		uint32_t amount_oz = convert_ml_to_oz(hUserInterface->presetsArr[hUserInterface->iPresetCurrentIndex].waterAmount);
		sprintf(waterAmountBuff, "%2doz", amount_oz);
	}		
	return waterAmountBuff;
}

static void UD(char * msg)
{
	clean_screen_display();
	bru_LCD_WriteString_16MBArial(20,95,msg,Font_21X24MB,0x00F8,0x0000);
}

// New =================== User Interface Screens =========================== //
static void bru_LCD_IntroScreen(void)
{
	clean_screen_display();
  bru_LCD_WriteString_28Arial(50,70,"HELLO",Font_37x52,FontColor,0x0000);
  bru_LCD_WriteString_28Arial(75,122,"BR[ ",Font_37x52,FontColor,0x0000);
	//bru_LCD_WriteString_22Arial(45,170,BRU_APP_VER,Font_29x41,0x000f,0x0000);
}

static void bru_LCD_FillingUp(void)
{
	clean_screen_display();
	bru_LCD_WriteString_28Arial(40,60,"FILLING",Font_37x52,FontColor,0x0000);
	bru_LCD_WriteString_28Arial(89,105,"UP ",Font_37x52,FontColor,0x0000);
	bru_LCD_Draw_Buttons();
}

static void bru_LCD_Serving(void)
{
  clean_screen_display();
	bru_LCD_WriteString_28Arial(23,75,"SERVING",Font_37x52,FontColor,0x0000);
	bru_LCD_Draw_Buttons();  
}

static void bru_LCD_Dispensing(void)
{
  clean_screen_display();
	bru_LCD_WriteString_22Arial(20,85,"DISPENSING", Font_29x41, FontColor, 0x0000);
	bru_LCD_Draw_Buttons();
}

static void bru_LCD_EngoyBRU(void)
{
	clean_screen_display();
	bru_LCD_WriteString_28Arial(47,80,"ENJOY",Font_37x52,FontColor,0x0000);
	bru_LCD_WriteString_28Arial(75,125,"BR[ ",Font_37x52,FontColor,0x0000);
	if(TimeCapture_Result())
		bru_LCD_WriteString_22Arial(80,20,TimeCaptureResult,Font_29x41,FontColor,0x0000); // 170
}

//============================================================================//

static void bru_LCD_Add_Water(void)
{
  clean_screen_display();
	bru_LCD_WriteString_28Arial(75,60,"ADD",Font_37x52,FontColor,0x0000);
	bru_LCD_WriteString_28Arial(47,105,"WATER",Font_37x52,FontColor,0x0000);
	bru_LCD_Draw_buttons("","");
	bru_LCD_LinesForButtons(FontColor);    
}

static void bru_LCD_Place_Brewing_Chamber(void)
{
  clean_screen_display();
  bru_LCD_WriteString_28Arial(50,60,"PLACE",Font_37x52,FontColor,0x0000);
  bru_LCD_WriteString_28Arial(15,105,"CHAMBER",Font_37x52,FontColor,0x0000);
	bru_LCD_Draw_buttons("","");
  bru_LCD_LinesForButtons(FontColor);   
}

static void bru_LCD_Close_Lid(void)
{
  clean_screen_display();
	bru_LCD_WriteString_28Arial(47,60,"CLOSE",Font_37x52,FontColor,0x0000);
	bru_LCD_WriteString_28Arial(90,105,"LID ",Font_37x52,FontColor,0x0000);
	bru_LCD_Draw_buttons("","");
	bru_LCD_LinesForButtons(FontColor);    
}

void bru_LCD_Place_Cup(void)
{    
	clean_screen_display();
	bru_LCD_WriteString_28Arial(50,60,"PLACE",Font_37x52,FontColor,0x0000);
	bru_LCD_WriteString_28Arial(75,105,"CUP ",Font_37x52,FontColor,0x0000);
	bru_LCD_Draw_buttons("","");
	bru_LCD_LinesForButtons(FontColor);
}

void bru_LCD_Remove_Cup(void)
{
	clean_screen_display();
	bru_LCD_WriteString_28Arial(30,60,"REMOVE",Font_37x52,FontColor,0x0000);
	bru_LCD_WriteString_28Arial(75,105,"CUP ",Font_37x52,FontColor,0x0000);
	bru_LCD_Draw_buttons("","");
	bru_LCD_LinesForButtons(FontColor);
}

// ########################################################################## //

//================================Brewing setup============================== //
static void bru_LCD_HomeScreen(void)
{
	bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
	
	clean_screen_display();
	if(hUserInterface->hMachineSetup->bluetoothState == MS_BLUETOOTH_ON)
		lcd_draw_picture(112,24,16,24,(uint8_t*)ble_16x24_icon);
	bru_LCD_WriteString_22Arial(82, 49, get_preset_temp(), Font_29x41, FontColor, 0x0000);
	bru_LCD_WriteString_28Arial(62, 83, get_preset_time(), Font_37x52, FontColor, 0x0000);
	bru_LCD_WriteString_22Arial(71, 132, get_preset_water(), Font_29x41, FontColor, 0x0000);
	bru_LCD_Draw_Buttons();
}

//===========================Brewing status screens=============================//
void bru_LCD_Steeping(bool InitStage)
{	 
	bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
  sprintf(outputBuffer, "%02d:%02d", hUserInterface->iCountdownBrewingTime/60, hUserInterface->iCountdownBrewingTime%60);
	if(InitStage)
	{
		clean_screen_display();		
    bru_LCD_WriteString_28Arial(20,60,"STEEPING",Font_37x52,FontColor,0x0000);
    bru_LCD_WriteString_28Arial(65,105,outputBuffer,Font_37x52,FontColor,0x0000);
		bru_LCD_Draw_Buttons();
	}
  bru_LCD_WriteString_28Arial(65,105,outputBuffer,Font_37x52,FontColor,0x0000);  
	DB_Printf("#Screen 'Steeping %s'\n", outputBuffer); 		
}

void bru_LCD_Steeping_Cancel(void)
{
	clean_screen_display();
	bru_LCD_WriteString_28Arial(10,60,"STEEPING",Font_37x52,FontColor,0x0000);
	bru_LCD_WriteString_28Arial(25,105,"CANCEL",Font_37x52,FontColor,0x0000);
}

void bru_LCD_Error_XXX(void)
{
	bru_UI_Interface_Handle hUserInterface = get_ui_interface();
	sprintf(outputBuffer, "%02d", hUserInterface->errorCode);
	clean_screen_display();
	bru_LCD_WriteString_28Arial(47,60,"ERROR",Font_37x52,FontColor,0x0000);
	bru_LCD_WriteString_28Arial(90,105,outputBuffer,Font_37x52,FontColor,0x0000);
	bru_LCD_Draw_Buttons();
}

void bru_LCD_Chamber_Washing(void)
{
	bru_UI_Interface_Handle hUserInterface = get_ui_interface();
	clean_screen_display();	
	bru_LCD_WriteString_28Arial(60,65,"RINSE",Font_37x52,FontColor,0x0000);
	if(hUserInterface->hMachineSetup->rinseConfiguration == MS_RINSE_TO_CUP) {
		bru_LCD_WriteString_28Arial(30,105,"TO CUP ",Font_37x52,FontColor,0x0000);
		DB_Printf("#Screen 'RINSE TO CUP'\n"); 	
	}	else {
		bru_LCD_WriteString_28Arial(23,105,"TO TRAY",Font_37x52,FontColor,0x0000);
		DB_Printf("#Screen 'RINSE TO TRAY'\n"); 	
	}
	bru_LCD_Draw_Buttons();
}

// ##### Test mode ########################################################## //

void bru_LCD_Test_Screen1(bool InitStage)	// #1 Sensors
{	 
	if(InitStage)
	{
		clean_screen_display();	
		bru_LCD_WriteString_16MBArial(106,0,"#1",Font_21X24MB,FontColor,0x0000);	
		bru_LCD_WriteString_16MBArial(70,24,"SENSORS",Font_21X24MB,FontColor,0x0000);	
		bru_LCD_DrawLine(0,50,240,50,FontColor);
		
		bru_LCD_WriteString_16MBArial(20,54,"Left",Font_21X24MB,FontColor,0x0000);	
		bru_LCD_WriteString_16MBArial(20,79,"Right",Font_21X24MB,FontColor,0x0000);	
		bru_LCD_WriteString_16MBArial(20,104,"Encoder",Font_21X24MB,FontColor,0x0000);	
		bru_LCD_WriteString_16MBArial(20,129,"Lid",Font_21X24MB,FontColor,0x0000);	
		bru_LCD_WriteString_16MBArial(20,153,"Chamber",Font_21X24MB,FontColor,0x0000);	

//    bru_LCD_Draw_buttons("","");
    bru_LCD_LinesForButtons(FontColor);
	}
	
	if (Sensors.LeftButton) 
		lcd_draw_picture(124,54,16,24,(uint8_t*)btn_red_16x24_icon);
	else	
		lcd_draw_picture(124,54,16,24,(uint8_t*)btn_black_16x24_icon);	

	if (Sensors.RightButton) 
		lcd_draw_picture(124,79,16,24,(uint8_t*)btn_red_16x24_icon);	
	else
		lcd_draw_picture(124,79,16,24,(uint8_t*)btn_black_16x24_icon);	

	if (Sensors.EncoderButton) 
		lcd_draw_picture(124,104,16,24,(uint8_t*)btn_red_16x24_icon);	
	else
		lcd_draw_picture(124,104,16,24,(uint8_t*)btn_black_16x24_icon);
	
	if(Sensors.Lid)
		lcd_draw_picture(124,129,16,24,(uint8_t*)btn_red_16x24_icon);
	else
		lcd_draw_picture(124,129,16,24,(uint8_t*)btn_black_16x24_icon);
	
	if(Sensors.BrewingChamber)
		lcd_draw_picture(124,153,16,24,(uint8_t*)btn_red_16x24_icon);
	else
		lcd_draw_picture(124,153,16,24,(uint8_t*)btn_black_16x24_icon);		
}

void bru_LCD_Test_Screen2(bool InitStage)			// #2 UI
{	 
	if(InitStage)
	{
		clean_screen_display();	
		bru_LCD_WriteString_16MBArial(106,0,"#2",Font_21X24MB,FontColor,0x0000);	
		bru_LCD_WriteString_16MBArial(60,24,"INTERFACE",Font_21X24MB,FontColor,0x0000);		
		bru_LCD_DrawLine(0,50,240,50,FontColor);	
	
    bru_LCD_Draw_buttons("Buzzer","LED");
    bru_LCD_LinesForButtons(FontColor);
		bru_LCD_WriteString_16MBArial(20,79,"RGB Led:",Font_21X24MB,FontColor,0x0000);	
	}	
	
//Buzzer
	if (Sensors.LeftButton) 
		bru_LCD_WriteString_16MBArial(20,54,"Buzzer: On",Font_21X24MB,FontColor,0x0000);	
	else 
		bru_LCD_WriteString_16MBArial(20,54,"Buzzer: Off",Font_21X24MB,FontColor,0x0000);	

//RGB Led
	if (Sensors.RightButton)
		lcd_draw_picture(124,79,16,24,(uint8_t*)btn_white_16x24_icon);		
	else
		lcd_draw_picture(124,79,16,24,(uint8_t*)btn_black_16x24_icon);	
}

void bru_LCD_String_16MB(uint16_t x, uint16_t y, const char* str, uint16_t color)
{
	bru_LCD_ClearScreen(BRU_LCD_BLACK_COLOUR, x, y, ST7735_WIDTH, y + 24);
	bru_LCD_WriteString_16MBArial(x,y,str,Font_21X24MB,color,0x0000);	
}

void bru_LCD_Test_Screen3(bool InitStage)			// #3 PV
{	 
	extern bru_PV_unit PV;
	
	if(InitStage)
	{
		clean_screen_display();	
		bru_LCD_WriteString_16MBArial(106,0,"#3",Font_21X24MB,FontColor,0x0000);	
		bru_LCD_WriteString_16MBArial(50,24,"PINCH VALVE",Font_21X24MB,FontColor,0x0000);		
		bru_LCD_DrawLine(0,50,240,50,FontColor);	
	
    bru_LCD_Draw_buttons("OPEN","CLOSE");
    bru_LCD_LinesForButtons(FontColor);
	}	

	switch(PV.UnitStatus)
	{
		case Unknown:
			bru_LCD_String_16MB(20,54,"Status: Unknown",FontColor);	
			break;
		case Open:
			bru_LCD_String_16MB(20,54,"Status: Open",FontColor);	
			break;
		case Close:
			bru_LCD_String_16MB(20,54,"Status: Close",FontColor);	
			break;
		case Failed:
			bru_LCD_String_16MB(20,54,"Status: Error",FontColor);	
			break;
	}		
	
	switch(PV.MotorStatus)
	{
		case Sleep:
			bru_LCD_String_16MB(20,79,"Motor: Sleep",FontColor);	
			break;
		case ToOpen:
			bru_LCD_String_16MB(20,79,"Motor: ToOpen",FontColor);	
			break;
		case ToClose:
			bru_LCD_String_16MB(20,79,"Motor: ToClose",FontColor);	
			break;
	}
	
	sprintf(outputBuffer,"Max current: %d mA", PV.MaxCurrent);		
	bru_LCD_String_16MB(20,104,outputBuffer,FontColor);	
	
	sprintf(outputBuffer,"Info: %s", PV.ErrInfo);
	bru_LCD_String_16MB(20,129,outputBuffer,FontColor);	
	bru_LCD_String_16MB(0,154,"",FontColor);	
}

void bru_LCD_Test_Screen4(bool InitStage)			// #4 IR
{	 
	if(InitStage)
	{
		clean_screen_display();	
		bru_LCD_WriteString_16MBArial(106,0,"#4",Font_21X24MB,FontColor,0x0000);	
		bru_LCD_WriteString_16MBArial(110,24,"IR",Font_21X24MB,FontColor,0x0000);		
		bru_LCD_DrawLine(0,50,240,50,FontColor);	
    bru_LCD_LinesForButtons(FontColor);
	}	
	
	if(Sensors.Cup)
		bru_LCD_WriteString_16MBArial(20,54,"Cup: On place",Font_21X24MB,FontColor,0x0000);	
	else
		bru_LCD_WriteString_16MBArial(20,54,"Cup: Removed",Font_21X24MB,FontColor,0x0000);	

	sprintf(outputBuffer,"Vol: %d mV", Sensors.IR_vol);		
	bru_LCD_WriteString_16MBArial(20,79,outputBuffer,Font_21X24MB,FontColor,0x0000);	
}

void bru_LCD_Test_Screen5(bool InitStage)			// #5 Pump
{	 
	if(InitStage)
	{
		clean_screen_display();	
		bru_LCD_WriteString_16MBArial(106,0,"#5",Font_21X24MB,FontColor,0x0000);	
		bru_LCD_WriteString_16MBArial(90,24,"PUMP",Font_21X24MB,FontColor,0x0000);		
		bru_LCD_DrawLine(0,50,240,50,FontColor);	
	
//    bru_LCD_Draw_buttons("100 ml","INF");
    bru_LCD_LinesForButtons(FontColor);
	}	
	
	if(Sensors.Water)
		bru_LCD_WriteString_16MBArial(20,54,"Water status: OK",Font_21X24MB,FontColor,0x0000);	
	else
		bru_LCD_WriteString_16MBArial(20,54,"Water status: NO",Font_21X24MB,FontColor,0x0000);
}

void bru_LCD_Test_Screen6(bool InitStage)			// #6 Heater
{	 
	if(InitStage)
	{
		clean_screen_display();	
		bru_LCD_WriteString_16MBArial(106,0,"#6",Font_21X24MB,FontColor,0x0000);	
		bru_LCD_WriteString_16MBArial(76,24,"HEATER",Font_21X24MB,FontColor,0x0000);		
		bru_LCD_DrawLine(0,50,240,50,FontColor);	
	
//    bru_LCD_Draw_buttons("100 ml","INF");
    bru_LCD_LinesForButtons(FontColor);
		
		bru_LCD_WriteString_16MBArial(20,54,"Top sensor:",Font_21X24MB,FontColor,0x0000);	
		bru_LCD_WriteString_16MBArial(20,79,"Top temp:",Font_21X24MB,FontColor,0x0000);	
		bru_LCD_WriteString_16MBArial(20,104,"Bot sensor:",Font_21X24MB,FontColor,0x0000);	
		bru_LCD_WriteString_16MBArial(20,129,"Bot temp:",Font_21X24MB,FontColor,0x0000);			
	}		
	
	//Top status:
	if (Sensors.TopSensorStatus)
		bru_LCD_WriteString_16MBArial(146,54,"OK",Font_21X24MB,FontColor,0x0000);	
	else
		bru_LCD_WriteString_16MBArial(146,54,"NC",Font_21X24MB,FontColor,0x0000);	

	//Top temp:
	sprintf(outputBuffer,"%.1f C",Sensors.TopTemperature);
	bru_LCD_String_16MB(146,79,outputBuffer,FontColor);

	//Bot status:
	if (Sensors.BotSensorStatus)
		bru_LCD_WriteString_16MBArial(146,104,"OK",Font_21X24MB,FontColor,0x0000);	
	else
		bru_LCD_WriteString_16MBArial(146,104,"NC",Font_21X24MB,FontColor,0x0000);	

	//Bot temp:
	sprintf(outputBuffer,"%.1f C",Sensors.BotTemperature);
	bru_LCD_String_16MB(146,129,outputBuffer,FontColor);
}

void bru_LCD_Test_Screen7(bool InitStage)			// #7 Water
{	 
	if(InitStage)
	{
		clean_screen_display();	
		bru_LCD_WriteString_16MBArial(106,0,"#7",Font_21X24MB,FontColor,0x0000);	
		bru_LCD_WriteString_16MBArial(80,24,"WATER",Font_21X24MB,FontColor,0x0000);		
		bru_LCD_DrawLine(0,50,240,50,FontColor);	
	
    bru_LCD_Draw_buttons("START","");
    bru_LCD_LinesForButtons(FontColor);
	
	//(45 C / 100 ml; 70 C / 100 ml; 95 C / 100 ml)	
	bru_LCD_WriteString_16MBArial(20,54,"Temp: 45 C",Font_21X24MB,FontColor,0x0000);	
	bru_LCD_WriteString_16MBArial(20,79,"Amount: 100 ml",Font_21X24MB,FontColor,0x0000);	
	}	
		
	if (Actuators.Heater)
		bru_LCD_String_16MB(20,104,"Heater: On",FontColor);	
	else
		bru_LCD_String_16MB(20,104,"Heater: Off",FontColor);	
}

// =========================== Dispenser screens ============================ //

void bru_LCD_Preset_HotWater_Menu(void)
{
		clean_screen_display();	   
		bru_LCD_WriteString_16MBArial(62,34,"HOT WATER",Font_21X24MB,FontColor,0x0000);
		bru_LCD_WriteString_16MBArial(32,58,"DISPENSER MODE",Font_21X24MB,FontColor,0x0000);
		bru_LCD_WriteString_22Arial(132,90,get_preset_temp(), Font_29x41,FontColor,0x0000);
		bru_LCD_WriteString_22Arial(120, 130, get_preset_water(), Font_29x41,FontColor, 0x0000);   
		bru_LCD_Draw_Buttons();
}

void bru_LCD_Preset_1_to_3_Menu(void)
{
		clean_screen_display();	
		bru_LCD_WriteString_16MBArial(28,54,"PRESET",Font_21X24MB,FontColor,0x0000);
		bru_LCD_WriteString_16MBArial(28,78,get_preset_num(),Font_21X24MB,FontColor,0x0000);
		bru_LCD_WriteString_22Arial(132,49,get_preset_temp(), Font_29x41,FontColor,0x0000);
		bru_LCD_WriteString_28Arial(100,83, get_preset_time(), Font_37x52,FontColor, 0x0000);
		bru_LCD_WriteString_22Arial(114,130, get_preset_water(), Font_29x41,FontColor, 0x0000);
		bru_LCD_Draw_Buttons();
}

// =========================== Main preset ============================ //

void bru_LCD_BrewingTime_Steeping(bool InitStage)
{
		if(InitStage) {
			clean_screen_display();
			bru_LCD_Draw_Buttons();
		}
		else
			 bru_LCD_ClearScreen(0x0000, 0, 0, ST7735_WIDTH, 180);
		
	  bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
		sprintf(timeBuff, "%02d:%02d", (hUserInterface->presetsArr[hUserInterface->iPresetCurrentIndex].brewingTime)/60, (hUserInterface->presetsArr[hUserInterface->iPresetCurrentIndex].brewingTime)%60);  
	  bru_LCD_WriteString_18Arial_R(62,60,"STEEPING",Font_24x27,FontColor,0x0000);
		bru_LCD_WriteString_18Arial_R(91,87,"TIME",Font_24x27,FontColor,0x0000);
		bru_LCD_WriteString_28Arial(62, 115, timeBuff, Font_37x52,FontColor, 0x0000);	 
}

void bru_LCD_BrewingTime_Steeping_Edit(bool InitStage)
{
	  bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
    sprintf(timeBuff, "%02d:%02d", (hUserInterface->presetsArr[hUserInterface->iPresetCurrentIndex].brewingTime)/60, (hUserInterface->presetsArr[hUserInterface->iPresetCurrentIndex].brewingTime)%60);
    
		if(InitStage)
		{
			clean_screen_display();
			bru_LCD_WriteString_18Arial_R(62,60,"STEEPING",Font_24x27,FontColor,0x0000);
		  bru_LCD_WriteString_18Arial_R(91,87,"TIME",Font_24x27,FontColor,0x0000);
			bru_LCD_WriteString_28Arial(17, 115, "n", Font_37x52,FontColor, 0x0000);
			bru_LCD_WriteString_28Arial(198, 115, "o", Font_37x52,FontColor, 0x0000);
			bru_LCD_Draw_Buttons();
		}
		bru_LCD_WriteString_28Arial(62, 115, timeBuff, Font_37x52, FontColor, 0x0000);
}


void bru_LCD_WaterTemperature(bool InitStage)
{
	  bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
	  char preset_num_Buff[8];
	  string_format_func(hUserInterface,preset_num_Buff);
    
		if(InitStage)	{
			clean_screen_display();
			bru_LCD_Draw_Buttons();
		}
		else
			 bru_LCD_ClearScreen(0x0000, 0, 0, ST7735_WIDTH, 180);
		
		bru_LCD_WriteString_18Arial_R(77,60,"WATER",Font_24x27,FontColor,0x0000);
		bru_LCD_WriteString_18Arial_R(30,87,"TEMPERATURE",Font_24x27,FontColor,0x0000);
		bru_LCD_WriteString_28Arial(73,115,get_preset_temp(),Font_37x52,FontColor,0x0000);
}

void bru_LCD_WaterTemperature_Edit(bool InitStage)
{
	  bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
    char preset_num_Buff[8];
	  string_format_func(hUserInterface,preset_num_Buff);
		
    if(InitStage)
		{
			clean_screen_display();
			bru_LCD_WriteString_28Arial(17, 115, "n", Font_37x52,FontColor, 0x0000);
			bru_LCD_WriteString_28Arial(198, 115, "o", Font_37x52,FontColor, 0x0000);
			bru_LCD_WriteString_18Arial_R(77,60,"WATER",Font_24x27,FontColor,0x0000);
	  	bru_LCD_WriteString_18Arial_R(30,87,"TEMPERATURE",Font_24x27,FontColor,0x0000);
			bru_LCD_Draw_Buttons();
		}
		else if((hUserInterface->presetsArr[hUserInterface->iPresetCurrentIndex].temperature >= BRU_PRESET_TEMPERATURE_MAX)
			|| (hUserInterface->presetsArr[hUserInterface->iPresetCurrentIndex].temperature <= BRU_PRESET_TEMPERATURE_MIN))
		{
			bru_LCD_ClearScreen(BRU_LCD_BLACK_COLOUR, 152, 115, 200, 158);
		}

		bru_LCD_WriteString_28Arial(73,115,get_preset_temp(),Font_37x52,FontColor,0x0000);
}

void bru_LCD_WaterAmount(bool InitStage)
{	 
	  if(InitStage)
		{
			clean_screen_display();
			bru_LCD_Draw_Buttons();
		}
		else
			 bru_LCD_ClearScreen(0x0000, 0, 0, ST7735_WIDTH, 180);
    
		bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
   
		if(hUserInterface->presetsArr[hUserInterface->iPresetCurrentIndex].waterAmount >= BRU_PRESET_WATER_AMOUNT_MAX)
			sprintf(waterAmountBuff, " INF "); //MAX
		else if(hUserInterface->presetsArr[hUserInterface->iPresetCurrentIndex].waterAmount <= BRU_PRESET_WATER_AMOUNT_MIN)
			sprintf(waterAmountBuff, "MIN ");
    else if(hUserInterface->hMachineSetup->units == MS_METRIC)
			sprintf(waterAmountBuff, "%2dml", (uint32_t)(hUserInterface->presetsArr[hUserInterface->iPresetCurrentIndex].waterAmount));
		else
		{
			uint32_t amount_oz = convert_ml_to_oz(hUserInterface->presetsArr[hUserInterface->iPresetCurrentIndex].waterAmount);
			sprintf(waterAmountBuff, "%2d", amount_oz);			
			lcd_draw_picture(122,128,48,28,(uint8_t*)oz_unit_48x28_icon);
		}
		bru_LCD_WriteString_28Arial(70,115,waterAmountBuff,Font_37x52,FontColor,0x0000);
		bru_LCD_WriteString_18Arial_R(77,60,"WATER",Font_24x27,FontColor,0x0000);
		bru_LCD_WriteString_18Arial_R(68,87,"AMOUNT",Font_24x27,FontColor,0x0000);
}

void bru_LCD_WaterAmount_Edit(bool InitStage)
{
	  if(InitStage)
		{
			clean_screen_display();
			bru_LCD_WriteString_28Arial(17, 115, "n", Font_37x52, FontColor, 0x0000);
			bru_LCD_WriteString_28Arial(198, 115, "o", Font_37x52, FontColor, 0x0000);
		  bru_LCD_WriteString_18Arial_R(77,60,"WATER",Font_24x27,FontColor,0x0000);
		  bru_LCD_WriteString_18Arial_R(68,87,"AMOUNT",Font_24x27,FontColor,0x0000);
			bru_LCD_Draw_Buttons();
		}
    bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
    if(hUserInterface->presetsArr[hUserInterface->iPresetCurrentIndex].waterAmount >= BRU_PRESET_WATER_AMOUNT_MAX)
		{
			bru_LCD_ClearScreen(BRU_LCD_BLACK_COLOUR, 162, 120, 192, 156);
			sprintf(waterAmountBuff, " INF "); //MAX
		}
		else if(hUserInterface->presetsArr[hUserInterface->iPresetCurrentIndex].waterAmount <= BRU_PRESET_WATER_AMOUNT_MIN)
		{
			bru_LCD_ClearScreen(BRU_LCD_BLACK_COLOUR, 148, 120, 192, 156);
			sprintf(waterAmountBuff, "MIN");
		}
    else if(hUserInterface->hMachineSetup->units == MS_METRIC)
		{
			sprintf(waterAmountBuff, "%2dml", (uint32_t)(hUserInterface->presetsArr[hUserInterface->iPresetCurrentIndex].waterAmount));
    }
		else
		{
			bru_LCD_ClearScreen(BRU_LCD_BLACK_COLOUR, 120, 120, 168, 128);
			uint32_t amount_oz = convert_ml_to_oz(hUserInterface->presetsArr[hUserInterface->iPresetCurrentIndex].waterAmount);
			sprintf(waterAmountBuff, "%2d", amount_oz);
			lcd_draw_picture(120,128,48,28,(uint8_t*)oz_unit_48x28_icon);
		}
		bru_LCD_WriteString_28Arial(70,115,waterAmountBuff,Font_37x52,FontColor,0x0000);
}

void bru_LCD_Preset_Alarm_Menu(bool InitStage)
{
    bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
    char preset_num_Buff[8];
	 
	  uint8_t x_cord = 130;

    if((hUserInterface->presetsArr[hUserInterface->iPresetCurrentIndex].waterAmount)> 99)
    {
      x_cord = 114;
    }

		string_format_func(hUserInterface,preset_num_Buff);		
	
	
	  if(InitStage)
		{
			  clean_screen_display();	   
			  bru_LCD_Draw_buttons_ENG("START","EDIT");
			  bru_LCD_LinesForButtons(0xffff);			
		}
    else
			 bru_LCD_ClearScreen(BRU_LCD_BLACK_COLOUR, 32, 36, 236, 78);

    hUserInterface->iCurrentTime = Seconds_get();
		timer_to_cal(hUserInterface->iCurrentTime, &timeStructure);
	 
	 	timer_to_cal(hUserInterface->iAlarmTime, &alarmStructure);
		 
	  if(hUserInterface->hMachineSetup->timeformat == TIMEFORMAT_12)
		{
		  uint8_t timeformat_flag;
	  	uint8_t alarmformat_flag;
			timeStructure.hour = convert_timeformat_24_to_12(timeStructure.hour,&timeformat_flag);	
			sprintf(current_time_Buff, "%02d:%02d %s", timeStructure.hour, timeStructure.min,timeformat_flag ? "PM":"AM");
			alarmStructure.hour = convert_timeformat_24_to_12(alarmStructure.hour,&alarmformat_flag);	
			bru_LCD_WriteString_16MBArial(70,20,current_time_Buff,Font_21X24MB,FontColor,0x0000);
			
			if(alarmformat_flag) // PM
			   bru_LCD_WriteString_16MBArial(50,99,(char const*)"PM",Font_21X24MB,FontColor,0x0000);
			else   // AM
				bru_LCD_WriteString_16MBArial(50,99,(char const*)"AM",Font_21X24MB,FontColor,0x0000);
		}
		else
		{
			sprintf(current_time_Buff, "%02d:%02d", timeStructure.hour, timeStructure.min);	
			bru_LCD_WriteString_16MBArial(90,20,current_time_Buff,Font_21X24MB,FontColor,0x0000);
		}
		sprintf(alarm_time_Buff, "%02d:%02d", alarmStructure.hour, alarmStructure.min);
		bru_LCD_WriteString_16MBArial(28,54,"ALARM",Font_21X24MB,FontColor,0x0000);
	 	bru_LCD_WriteString_22Arial(132,49,get_preset_temp(), Font_29x41, FontColor,0x0000);//131
		bru_LCD_WriteString_28Arial(100, 83, get_preset_time(), Font_37x52, FontColor, 0x0000);
		bru_LCD_WriteString_22Arial(x_cord, 130, get_preset_water(), Font_29x41, FontColor, 0x0000);//110
		bru_LCD_WriteString_16MBArial(28,78,alarm_time_Buff,Font_21X24MB,FontColor,0x0000);		 
}

// ##### Old version ######################################################## //

void bru_BrewingTime_adj_steepint_time_ENG(uint8_t dir)
{
	 bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
	 uint32_t *pTime = &hUserInterface->presetsArr[hUserInterface->iPresetCurrentIndex].brewingTime;
	 if(dir == ENCODE_RIGHT) // right to increase one
	 {
		 if(*pTime < 600)  // 10 minute 
			 *pTime += BRU_PRESET_TIME_STEP1;
		 else
			 *pTime += BRU_PRESET_TIME_STEP2;
		 if(*pTime >= BRU_PRESET_TIME_MAX)
			 *pTime = BRU_PRESET_TIME_MAX;
	 }
	 else
	 {
		 if(*pTime > 600) 
			 *pTime -= BRU_PRESET_TIME_STEP2;
		 else if(*pTime > BRU_PRESET_TIME_STEP1)
			 *pTime -= BRU_PRESET_TIME_STEP1;
	 }
//	DISP_FLAG_T *p = get_disp_flag();
//	p->flush_disp_flag = 1;
}

void bru_BrewingTime_adj_water_temper_ENG(uint8_t dir)
{
	 bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
	 float *pWaterTemp = &hUserInterface->presetsArr[hUserInterface->iPresetCurrentIndex].temperature;
	 if(dir == ENCODE_RIGHT) // right to increase one
	 {
		 if(*pWaterTemp < BRU_PRESET_TEMPERATURE_MAX)  // 10 minute 
			 *pWaterTemp += 5;
	 }
	 else
		 if(*pWaterTemp > BRU_PRESET_TEMPERATURE_MIN) 
			 *pWaterTemp -= 5;
		 
//	DISP_FLAG_T *p = get_disp_flag();
//	p->flush_disp_flag = 1;
}

void bru_BrewingTime_adj_water_amount_ENG(uint8_t dir)
{
	 bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
	 float *pWaterAmount		 = &hUserInterface->presetsArr[hUserInterface->iPresetCurrentIndex].waterAmount;
	 if(dir == ENCODE_RIGHT) // right to increase one
	 {
		 if(*pWaterAmount < BRU_PRESET_WATER_AMOUNT_MAX)  // 10 minute 
			 *pWaterAmount += BRU_PRESET_WATER_AMOUNT_STEP;
	 }
	 else
	 {
		 if(*pWaterAmount > BRU_PRESET_WATER_AMOUNT_MIN) 
			 *pWaterAmount -= BRU_PRESET_WATER_AMOUNT_STEP;
	 }
	 
//	DISP_FLAG_T *p = get_disp_flag();
//	p->flush_disp_flag = 1;
}

////////////////////////////////////////////////////////////////////////////////

//===========================Preset menu========================================//
void bru_LCD_Preset_1_to_3_Menu_ENG(uint8_t *flag,const char *menu_name)
{
	UD("787");
}

void bru_LCD_Preset_HotWater_Menu_ENG(uint8_t *flag,const char *menu_name)
{
	UD("801");    
}

void bru_LCD_Preset_Alarm_Menu_ENG(uint8_t *flag,const char *menu_name)
{
	UD("815");
}

void bru_LCD_Preset_default_presets_Menu_ENG(uint8_t *flag)
{
	UD("884");    
}

void bru_LCD_BrewingTime_Steeping_ENG(uint8_t *flag)
{
		UD("961");
}

void bru_LCD_BrewingTime_Steeping_Edit_ENG(uint8_t * flag,const char * menu_name)
{
		UD("983");
}

void bru_LCD_WaterTemperature_ENG(uint8_t * flag)
{
		UD("1002");
}

void bru_LCD_WaterTemperature_Edit_ENG(uint8_t * flag,const char *menu_name )
{
		UD("1026");
}

void bru_LCD_WaterAmount_ENG(uint8_t * flag)
{
		UD("1053");	 
}

void bru_LCD_WaterAmount_Edit_ENG(uint8_t * flag,const char *menu_name)
{
	UD("1095");
}

////////////////////////////////////////////////////////////////////////////////

void bru_LCD_System_Time_ENG(uint8_t *flag,uint8_t mode)
{	
	if(*flag)
	{
		clean_screen_display();
		bru_LCD_Draw_buttons_ENG("BACK","EDIT");
	  	bru_LCD_LinesForButtons(0xffff);
		  *flag = 0;		 
	}
	else
			 bru_LCD_ClearScreen(0x0000, 30, 30, 236, 110);
	 
	bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
	  if(mode == TIME_SEL)
		{
			bru_LCD_WriteString_18Arial_R(35,88,"CURRENT TIME:",Font_24x27,0xffff,0x0000);
			timer_to_cal(hUserInterface->iCurrentTime, &timeStructure);
	  }
	  else
		{
			bru_LCD_WriteString_18Arial_R(49,88,"ALARM TIME:",Font_24x27,0xffff,0x0000);
			timer_to_cal(hUserInterface->iAlarmTime, &timeStructure);
		}
		
	 
	  if(hUserInterface->hMachineSetup->timeformat == TIMEFORMAT_12)
		{
		  uint8_t timeformat_flag;
	  
			timeStructure.hour = convert_timeformat_24_to_12(timeStructure.hour,&timeformat_flag);	
			sprintf(current_time_Buff, "%02d:%02d%s", timeStructure.hour, timeStructure.min,timeformat_flag ? "PM":"AM");
		  bru_LCD_WriteString_28Arial(37, 118, current_time_Buff, Font_37x52, 0xffff, 0x0000);	
		}
		else
		{
			sprintf(current_time_Buff, "%02d:%02d", timeStructure.hour, timeStructure.min);	
			bru_LCD_WriteString_28Arial(65, 118, current_time_Buff, Font_37x52, 0xffff, 0x0000);	
		}   
}

void bru_sys_time_alarm_adj_hour_ENG(uint8_t mode,uint8_t dir)
{
	uint32_t *pData;
	bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
  if(mode == TIME_SEL)
	{
		pData = &hUserInterface->iCurrentTime;
	}
	else
	{
		pData = &hUserInterface->iAlarmTime;
	}
	timer_to_cal(*pData, &timeStructure);
	if(dir == ENCODE_RIGHT)
	{
		if(++timeStructure.hour > 23)
		{
			timeStructure.hour = 0;
		}
	}
	else
	{
		if(timeStructure.hour == 0)
		{
			timeStructure.hour = 23;
		}
		else
		{
			timeStructure.hour--;
		}
	}
	timeStructure.sec = 0;
	*pData = cal_to_timer(&timeStructure);
	DISP_FLAG_T *p = get_disp_flag();
	p->flush_disp_flag = 1;
}



void bru_sys_time_alarm_adj_minute_ENG(uint8_t mode,uint8_t dir)
{
	uint32_t *pData;
	bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
  if(mode == TIME_SEL)
	{
		pData = &hUserInterface->iCurrentTime;
	}
	else
	{
		pData = &hUserInterface->iAlarmTime;
	}
	timer_to_cal(*pData, &timeStructure);
	if(dir == ENCODE_RIGHT)
	{
		if(++timeStructure.min > 59)
		{
			timeStructure.min = 0;
		}
	}
	else
	{
		if(timeStructure.min == 0)
		{
			timeStructure.min = 59;
		}
		else
		{
			timeStructure.min--;
		}
	}
	timeStructure.sec = 0;
	*pData = cal_to_timer(&timeStructure);
	DISP_FLAG_T *p = get_disp_flag();
	p->flush_disp_flag = 1;
}

void bru_LCD_System_Time_Edit_ENG(uint8_t *flag,uint8_t mode,uint8_t adj_pos)
{
	
	 if(*flag)
	 {
		 	clean_screen_display();
		  bru_LCD_Draw_buttons_ENG("","SAVE");
	  	bru_LCD_LinesForButtons(0xffff);
		 	bru_LCD_WriteString_18Arial_R(80,96,"- EDIT -",Font_24x27,0xffff,0x0000);
			if(mode == TIME_SEL)
			{
				bru_LCD_WriteString_18Arial_R(35,68,"CURRENT TIME:",Font_24x27,0xffff,0x0000);		
			}
			else
			{
				bru_LCD_WriteString_18Arial_R(49,68,"ALARM TIME:",Font_24x27,0xffff,0x0000);	
			}
		  *flag = 0;
	 }
	 
	  bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
	  if(mode == TIME_SEL)
		{
			timer_to_cal(hUserInterface->iCurrentTime, &timeStructure);
	  }
	  else
		{	
			timer_to_cal(hUserInterface->iAlarmTime, &timeStructure);
		}
	
	 
	  if(hUserInterface->hMachineSetup->timeformat == TIMEFORMAT_12)
		{
		  uint8_t timeformat_flag;
	  
			timeStructure.hour = convert_timeformat_24_to_12(timeStructure.hour,&timeformat_flag);	
			if(ADJ_HOUR_POS == adj_pos)
				sprintf(current_time_Buff, "%02d:  %s", timeStructure.hour,timeformat_flag ? "PM":"AM");
			else
				sprintf(current_time_Buff, "  :%02d%s", timeStructure.min,timeformat_flag ? "PM":"AM");	
		  bru_LCD_WriteString_28Arial(37, 118, current_time_Buff, Font_37x52, 0xffff, 0x0000);			
		}
		else
		{
			if(ADJ_HOUR_POS == adj_pos)
				sprintf(current_time_Buff, "%02d:  ", timeStructure.hour);
			else
				sprintf(current_time_Buff, "  :%02d", timeStructure.min);				
			bru_LCD_WriteString_28Arial(65, 118, current_time_Buff, Font_37x52, 0xffff, 0x0000);	
		}    
}

void bru_LCD_Alarm_Preset_ENG(uint8_t *flag)
{
 if(*flag)
 {
		clean_screen_display();
		bru_LCD_Draw_buttons_ENG("BACK","EDIT");
		bru_LCD_LinesForButtons(0xffff);
		*flag = 0;
 }
 else
		 bru_LCD_ClearScreen(0x0000, 0, 0, ST7735_WIDTH, 180);
 
	bru_LCD_WriteString_18Arial_R(32,88,"ALARM PRESET",Font_24x27,0xffff,0x0000); 
}

//==============================================================================//
static uint8_t bru_preset_com_menu(bru_UI_Interface_Handle hUserInterface,char *preset_num_Buff,uint8_t *flag,const char *menu_name)
{
	uint8_t x_cord = 130;

	if((hUserInterface->presetsArr[hUserInterface->iPresetCurrentIndex].waterAmount)> 99)
	{
		x_cord = 114;
	}

	string_format_func(hUserInterface,preset_num_Buff);		
	
	if(*flag)
	{
		clean_screen_display();
		bru_LCD_Draw_buttons_ENG(menu_name,"EDIT");
		bru_LCD_LinesForButtons(0xffff);
		*flag = 0;
	}
	else
	{
		 bru_LCD_ClearScreen(0x0000, 0, 0, ST7735_WIDTH, 180);
	}
	return x_cord;
}

//#######################################################################################################

//=============================MACHINE SETUP=================================//
void bru_LCD_Bluetooth_State_ENG(uint8_t *flag)
{
	if(*flag)
	{
		clean_screen_display();
		bru_LCD_Draw_buttons_ENG("BACK","EDIT");
		bru_LCD_LinesForButtons(0xffff);
		*flag = 0;
	}
	else
	{
		bru_LCD_ClearScreen(BRU_LCD_BLACK_COLOUR, 0, 40, 200, 170);
	}
	
	bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
		
	bru_LCD_WriteString_18Arial_R(40,60,"BLUETOOTH",Font_24x27,0xffff,0x0000);
	bru_LCD_WriteString_18Arial_R(80,96,"STATE:",Font_24x27,0xffff,0x0000);
	

	if(hUserInterface->hMachineSetup->bluetoothState == MS_BLUETOOTH_ON)
		bru_LCD_WriteString_18Arial_R(96,137,"ON",Font_24x27,0xffff,0x0000);
	else
		bru_LCD_WriteString_18Arial_R(96,137,"OFF",Font_24x27,0xffff,0x0000);   
}

void bru_ble_on_off_ENG(void)
{
	 bru_UI_Interface_Handle  hUserInterface = get_ui_interface();

	 if(hUserInterface->hMachineSetup->bluetoothState == MS_BLUETOOTH_OFF)
	 {
		 advertising_stop();
	 }
	 else
	 {
		 advertising_start();
	 }
}

void bru_Bluetooth_State_adj_ENG(void)
{
	 bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
	
	   if(hUserInterface->hMachineSetup->bluetoothState == MS_BLUETOOTH_OFF)
		 {
			 hUserInterface->hMachineSetup->bluetoothState = MS_BLUETOOTH_ON;
		 }
		 else
		 {
			  hUserInterface->hMachineSetup->bluetoothState = MS_BLUETOOTH_OFF;
		 }
		 
	DISP_FLAG_T *p = get_disp_flag();
	p->flush_disp_flag = 1;
}

void bru_LCD_Bluetooth_State_Edit_ENG(uint8_t *flag)
{	
	if(*flag)
	{
		clean_screen_display();
		bru_LCD_Draw_buttons_ENG("","SAVE");
		bru_LCD_LinesForButtons(0xffff);
		bru_LCD_WriteString_18Arial_R(40,60,"BLUETOOTH",Font_24x27,0xffff,0x0000);
		bru_LCD_WriteString_18Arial_R(80,96,"STATE:",Font_24x27,0xffff,0x0000);
		bru_LCD_WriteString_28Arial(17, 125, "n", Font_37x52, 0xffff, 0x0000);
		bru_LCD_WriteString_28Arial(190, 125, "o", Font_37x52, 0xffff, 0x0000);
		*flag = 0;
	}

	bru_UI_Interface_Handle  hUserInterface = get_ui_interface();	

	if(hUserInterface->hMachineSetup->bluetoothState == MS_BLUETOOTH_ON)
			bru_LCD_WriteString_18Arial_R(96,137,"ON ",Font_24x27,0xffff,0x0000);
	else
			bru_LCD_WriteString_18Arial_R(96,137,"OFF",Font_24x27,0xffff,0x0000);
}

void bru_LCD_Language_ENG(uint8_t *flag)
{
	 if(*flag)
		{
			clean_screen_display();
			bru_LCD_Draw_buttons_ENG("BACK","EDIT");
			bru_LCD_LinesForButtons(0xffff);
			*flag = 0;
		}
		else
    {
			bru_LCD_ClearScreen(BRU_LCD_BLACK_COLOUR, 0, 40, 200, 170);
		}			
	   bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
		
   
      
		bru_LCD_WriteString_18Arial_R(51,85,"LANGUAGE:",Font_24x27,0xffff,0x0000);
		if(hUserInterface->hMachineSetup->language == MS_ENGLISH)
		{
				bru_LCD_WriteString_18Arial_R(93,137,"ENG",Font_24x27,0xffff,0x0000);
		}
////		else if(hUserInterface->hMachineSetup->language == MS_FRENCH)
////		{
////				bru_LCD_WriteString_18Arial_R(93,137,"FRA",Font_24x27,0xffff,0x0000);
////		}
////		else if(hUserInterface->hMachineSetup->language == MS_DEUTCH)
////		{
////				bru_LCD_WriteString_18Arial_R(93,137,"DEU",Font_24x27,0xffff,0x0000);
////		}
////		else
////		{
////				bru_LCD_WriteString_18Arial_R(93,137,"ESP",Font_24x27,0xffff,0x0000);
////    }
}

void bru_LCD_Timeformat_ENG(uint8_t *flag)
{
	  if(*flag)
		{
			clean_screen_display();
			bru_LCD_Draw_buttons_ENG("BACK","EDIT");
			bru_LCD_LinesForButtons(0xffff);
			*flag = 0;
		}
		else
    {
			bru_LCD_ClearScreen(BRU_LCD_BLACK_COLOUR, 0, 40, 220, 170);
		}			
	   bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
		
   
      
		bru_LCD_WriteString_18Arial_R(40,85,"TIME FORMAT:",Font_24x27,0xffff,0x0000);
		if(hUserInterface->hMachineSetup->timeformat == TIMEFORMAT_12)
		{
				bru_LCD_WriteString_18Arial_R(93,137,"12H",Font_24x27,0xffff,0x0000);
		}
		else
		{
				bru_LCD_WriteString_18Arial_R(93,137,"24H",Font_24x27,0xffff,0x0000);
	  }
}

void bru_Timeformat_adj_ENG(void)
{
	 bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
	
	
	   if(hUserInterface->hMachineSetup->timeformat == TIMEFORMAT_24)
		 {
			 hUserInterface->hMachineSetup->timeformat = TIMEFORMAT_12;
		 }
		 else
		 {
			  hUserInterface->hMachineSetup->timeformat = TIMEFORMAT_24;
		 }
		 
	DISP_FLAG_T *p = get_disp_flag();
	p->flush_disp_flag = 1;
}

void bru_LCD_Timeformat_Edit_ENG(uint8_t *flag)
{
	  if(*flag)
		{
			clean_screen_display();

			
			  bru_LCD_Draw_buttons_ENG("","SAVE");
        bru_LCD_LinesForButtons(0xffff);
	      bru_LCD_WriteString_18Arial_R(40,85,"TIME FORMAT:",Font_24x27,0xffff,0x0000);
				bru_LCD_WriteString_28Arial(17, 125, "n", Font_37x52, 0xffff, 0x0000);
        bru_LCD_WriteString_28Arial(190, 125, "o", Font_37x52, 0xffff, 0x0000);
			*flag = 0;
		}
					
	   bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
		
		if(hUserInterface->hMachineSetup->timeformat == TIMEFORMAT_12)
		{
				bru_LCD_WriteString_18Arial_R(93,137,"12H",Font_24x27,0xffff,0x0000);
		}
		else
		{
				bru_LCD_WriteString_18Arial_R(93,137,"24H",Font_24x27,0xffff,0x0000);
	  }
}


void bru_LCD_Units_ENG(uint8_t *flag)
{
	  if(*flag)
		{
			clean_screen_display();
			bru_LCD_Draw_buttons_ENG("BACK","EDIT");
			bru_LCD_LinesForButtons(0xffff);
			*flag = 0;
		}
		else
    {
			bru_LCD_ClearScreen(BRU_LCD_BLACK_COLOUR, 0, 40, 220, 170);
		}			
		 bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
   
       
      bru_LCD_WriteString_18Arial_R(77,80,"UNITS:",Font_24x27,0xffff,0x0000);
			if(hUserInterface->hMachineSetup->units == MS_METRIC)
			{
				bru_LCD_WriteString_18Arial_R(72,137,"METRIC",Font_24x27,0xffff,0x0000);
			}
			else
				bru_LCD_WriteString_18Arial_R(60,137,"IMPERIAL",Font_24x27,0xffff,0x0000);
}

void bru_Units_adj_ENG(void)
{
	bru_UI_Interface_Handle  hUserInterface = get_ui_interface();

	if(hUserInterface->hMachineSetup->units == MS_METRIC)
	 hUserInterface->hMachineSetup->units = MS_IMPERIAL;
	else
		hUserInterface->hMachineSetup->units = MS_METRIC;
		 
	DISP_FLAG_T *p = get_disp_flag();
	p->flush_disp_flag = 1;
}

void bru_LCD_Units_Edit_ENG(uint8_t *flag)
{
 if(*flag)
	{
		clean_screen_display();
		bru_LCD_Draw_buttons_ENG("","SAVE");
		bru_LCD_LinesForButtons(0xffff);
		bru_LCD_WriteString_18Arial_R(77,80,"UNITS:",Font_24x27,0xffff,0x0000);
		bru_LCD_WriteString_28Arial(17, 125, "n", Font_37x52, 0xffff, 0x0000);
		bru_LCD_WriteString_28Arial(190, 125, "o", Font_37x52, 0xffff, 0x0000);
		*flag = 0;
	}
	
	 bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
 
		if(hUserInterface->hMachineSetup->units == MS_METRIC)
				bru_LCD_WriteString_18Arial_R(72,137,"METRIC",Font_24x27,0xffff,0x0000);
		else
				bru_LCD_WriteString_18Arial_R(60,137,"IMPERIAL",Font_24x27,0xffff,0x0000);
}

void bru_LCD_Rinse_config_ENG(uint8_t *flag)
{
	  if(*flag)
		{
			clean_screen_display();
			bru_LCD_Draw_buttons_ENG("BACK","EDIT");
			bru_LCD_LinesForButtons(0xffff);
			*flag = 0;
		}
		else
    {
			bru_LCD_ClearScreen(BRU_LCD_BLACK_COLOUR, 0, 40, 220, 170);
		}			
	  bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
			
		bru_LCD_WriteString_18Arial_R(83,60,"RINSE",Font_24x27,0xffff,0x0000);
		bru_LCD_WriteString_18Arial_R(70,85,"CONFIG:",Font_24x27,0xffff,0x0000);
		if(hUserInterface->hMachineSetup->rinseConfiguration == MS_RINSE_TO_CUP)
		{
				bru_LCD_WriteString_18Arial_R(94,137,"CUP",Font_24x27,0xffff,0x0000);
		}
		else if(hUserInterface->hMachineSetup->rinseConfiguration == MS_RINSE_TO_TRAY)
		{
				bru_LCD_WriteString_18Arial_R(84,137,"TRAY",Font_24x27,0xffff,0x0000);
		}
		else
		{
			bru_LCD_WriteString_18Arial_R(84,137,"OFF",Font_24x27,0xffff,0x0000);
		}
    
}

void bru_Rinse_adj_ENG(uint8_t dir)
{
	 bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
	 uint8_t temp = hUserInterface->hMachineSetup->rinseConfiguration;
	 if(dir)  // right
	 {
		 if(++temp > MS_RINSE_OFF)
		 {
			 temp = MS_RINSE_TO_CUP;
		 }
	 }
	 else
	 {
		 if(temp == MS_RINSE_TO_CUP)
		 {
			 temp = MS_RINSE_OFF;
		 }
		 else
		 {
			 temp--;
		 }
	 }
	  
	hUserInterface->hMachineSetup->rinseConfiguration = (bru_MS_Rinse_Config)temp;	 
	DISP_FLAG_T *p = get_disp_flag();
	p->flush_disp_flag = 1;
}

void bru_LCD_Rinse_config_Edit_ENG(uint8_t *flag)
{
	 if(*flag)
		{
			clean_screen_display();
			bru_LCD_Draw_buttons_ENG("","SAVE");
			bru_LCD_LinesForButtons(0xffff);
			bru_LCD_WriteString_18Arial_R(83,60,"RINSE",Font_24x27,0xffff,0x0000);
		  bru_LCD_WriteString_18Arial_R(70,85,"CONFIG:",Font_24x27,0xffff,0x0000);
			bru_LCD_WriteString_28Arial(17, 125, "n", Font_37x52, 0xffff, 0x0000);
			bru_LCD_WriteString_28Arial(190, 125, "o", Font_37x52, 0xffff, 0x0000);
			*flag = 0;
		}
		
	  
	  bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
			
		
		if(hUserInterface->hMachineSetup->rinseConfiguration == MS_RINSE_TO_CUP)
		{
				bru_LCD_WriteString_18Arial_R(77,137," CUP ",Font_24x27,0xffff,0x0000);
		}
		else if(hUserInterface->hMachineSetup->rinseConfiguration == MS_RINSE_TO_TRAY)
		{
				bru_LCD_WriteString_18Arial_R(84,137,"TRAY",Font_24x27,0xffff,0x0000);
		}
		else
		{
			  bru_LCD_WriteString_18Arial_R(84,137,"OFF ",Font_24x27,0xffff,0x0000);
		}    
}

void bru_LCD_Water_filter_status_123_liters_ENG(uint8_t *flag)
{
	 if(*flag)
		{
			clean_screen_display();
			bru_LCD_Draw_buttons_ENG("BACK","EDIT");
			bru_LCD_LinesForButtons(0xffff);
			*flag = 0;
		}
		else
    {
			bru_LCD_ClearScreen(BRU_LCD_BLACK_COLOUR, 0, 40, 200, 170);
		}			
	
		bru_LCD_WriteString_18Arial_R(79,60,"WATER",Font_24x27,0xffff,0x0000);
		bru_LCD_WriteString_18Arial_R(23,85,"FILTER STATUS:",Font_24x27,0xffff,0x0000);
		bru_LCD_WriteString_18Arial_R(51,120,"123 LITERS",Font_24x27,0xffff,0x0000);
		bru_LCD_WriteString_18Arial_R(92,145,"LEFT",Font_24x27,0xffff,0x0000);
    
}


void bru_LCD_Maintenance_status_ENG(uint8_t *flag)
{
	 if(*flag)
		{
			clean_screen_display();
			bru_LCD_Draw_buttons_ENG("BACK","EDIT");
			bru_LCD_LinesForButtons(0xffff);
			*flag = 0;
		}
		else
    {
			bru_LCD_ClearScreen(BRU_LCD_BLACK_COLOUR, 0, 40, 220, 170);
		}	
  
			bru_LCD_WriteString_18Arial_R(30,60,"MAINTENANCE",Font_24x27,0xffff,0x0000);
			bru_LCD_WriteString_18Arial_R(70,85,"STATUS:",Font_24x27,0xffff,0x0000);
			bru_LCD_WriteString_18Arial_R(51,120,"123 LITERS",Font_24x27,0xffff,0x0000);
			bru_LCD_WriteString_18Arial_R(92,145,"LEFT",Font_24x27,0xffff,0x0000);
}

void bru_LCD_RGB_state_ENG(uint8_t *flag)
{
	  if(*flag)
		{
			clean_screen_display();
			bru_LCD_Draw_buttons_ENG("BACK","EDIT");
			bru_LCD_LinesForButtons(0xffff);
			*flag = 0;
		}
		else
    {
			bru_LCD_ClearScreen(BRU_LCD_BLACK_COLOUR, 0, 40, 200, 170);
		}	
		 bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
   
       
			bru_LCD_WriteString_18Arial_R(94,60,"RGB",Font_24x27,0xffff,0x0000);
			bru_LCD_WriteString_18Arial_R(78,85,"STATE:",Font_24x27,0xffff,0x0000);
			if((!hUserInterface->hMachineSetup->redLed) && (!hUserInterface->hMachineSetup->greenLed) && (!hUserInterface->hMachineSetup->blueLed))
			{
  			bru_LCD_WriteString_18Arial_R(94,132,"OFF",Font_24x27,0xffff,0x0000);
			}
			else
			{
					bru_LCD_WriteString_18Arial_R(94,132,"ON",Font_24x27,0xffff,0x0000);
      }
}

void bru_RGB_State_adj_ENG(void)
{
	 bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
	
	
	  if((!hUserInterface->hMachineSetup->redLed) && (!hUserInterface->hMachineSetup->greenLed) && (!hUserInterface->hMachineSetup->blueLed))
		{
			hUserInterface->hMachineSetup->redLed = 255;
			hUserInterface->hMachineSetup->greenLed = 255;
			hUserInterface->hMachineSetup->blueLed = 255;	
		}
		else
		{
			hUserInterface->hMachineSetup->redLed = 0;
			hUserInterface->hMachineSetup->greenLed = 0;
			hUserInterface->hMachineSetup->blueLed = 0;	
		}
		set_rgb_duty(hUserInterface->hMachineSetup->redLed,hUserInterface->hMachineSetup->greenLed,hUserInterface->hMachineSetup->blueLed);
		
	DISP_FLAG_T *p = get_disp_flag();
	p->flush_disp_flag = 1;
}

void bru_LCD_RGB_state_Edit_ENG(uint8_t *flag)
{
	
	 if(*flag)
		{
			clean_screen_display();
			bru_LCD_Draw_buttons_ENG("","SAVE");
			bru_LCD_LinesForButtons(0xffff);
			bru_LCD_WriteString_18Arial_R(94,60,"RGB",Font_24x27,0xffff,0x0000);
			bru_LCD_WriteString_18Arial_R(78,85,"STATE:",Font_24x27,0xffff,0x0000);
			bru_LCD_WriteString_28Arial(17, 125, "n", Font_37x52, 0xffff, 0x0000);
			bru_LCD_WriteString_28Arial(190, 125, "o", Font_37x52, 0xffff, 0x0000);
			*flag = 0;
		}
	 
		 bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
   
       
			
			if((!hUserInterface->hMachineSetup->redLed) && (!hUserInterface->hMachineSetup->greenLed) && (!hUserInterface->hMachineSetup->blueLed))
			{
  			bru_LCD_WriteString_18Arial_R(94,132,"OFF",Font_24x27,0xffff,0x0000);
			}
			else
			{
					bru_LCD_WriteString_18Arial_R(94,132,"ON ",Font_24x27,0xffff,0x0000);
      }
}

void bru_LCD_Buzzer_state_ENG(uint8_t *flag)
{
	  if(*flag)
		{
			clean_screen_display();
			bru_LCD_Draw_buttons_ENG("BACK","EDIT");
			bru_LCD_LinesForButtons(0xffff);
			*flag = 0;
		}
		else
    {
			bru_LCD_ClearScreen(BRU_LCD_BLACK_COLOUR, 0, 40, 200, 170);
		}	
		 bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
   
       
			bru_LCD_WriteString_18Arial_R(74,60,"BUZZER",Font_24x27,0xffff,0x0000);
			bru_LCD_WriteString_18Arial_R(78,85,"STATE:",Font_24x27,0xffff,0x0000);
			if(hUserInterface->hMachineSetup->buzzerState == MS_BUZZER_OFF)
			{
  			bru_LCD_WriteString_18Arial_R(94,132,"OFF",Font_24x27,0xffff,0x0000);
			}
			else
			{
					bru_LCD_WriteString_18Arial_R(94,132,"ON",Font_24x27,0xffff,0x0000);
      }
}


void bru_Buzzer_State_adj_ENG(void)
{
	   bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
	
	
	   if(hUserInterface->hMachineSetup->buzzerState == MS_BUZZER_OFF)
		 {
			 hUserInterface->hMachineSetup->buzzerState = MS_BUZZER_ON;
		 }
		 else
		 {
			  hUserInterface->hMachineSetup->buzzerState = MS_BUZZER_OFF;
		 }
		 
	DISP_FLAG_T *p = get_disp_flag();
	p->flush_disp_flag = 1;
}


void bru_LCD_Buzzer_state_Edit_ENG(uint8_t *flag)
{
	  
	  if(*flag)
		{
			clean_screen_display();
			bru_LCD_Draw_buttons_ENG("","SAVE");
			bru_LCD_LinesForButtons(0xffff);
			bru_LCD_WriteString_18Arial_R(74,60,"BUZZER",Font_24x27,0xffff,0x0000);
			bru_LCD_WriteString_18Arial_R(78,85,"STATE:",Font_24x27,0xffff,0x0000);
			bru_LCD_WriteString_28Arial(17, 125, "n", Font_37x52, 0xffff, 0x0000);
			bru_LCD_WriteString_28Arial(190, 125, "o", Font_37x52, 0xffff, 0x0000);
			*flag = 0;
		}
		
		 bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
   		
		if(hUserInterface->hMachineSetup->buzzerState == MS_BUZZER_OFF)
		{
			bru_LCD_WriteString_18Arial_R(94,132,"OFF",Font_24x27,0xffff,0x0000);
		}
		else
		{
				bru_LCD_WriteString_18Arial_R(94,132,"ON ",Font_24x27,0xffff,0x0000);
		}
}


//===========================Main setup menu=============================//
/**
* @brieaf steeping setup
*/
void bru_LCD_Main_Menu_Steeping_Setup_ENG(uint8_t* mode)
{
   
	      if(*mode)
				{
					clean_screen_display();
					bru_LCD_Draw_buttons_ENG("BACK","OK");
          bru_LCD_LinesForButtons(0xffff);
					bru_LCD_WriteString_18Arial_R(81,115,"SETUP",Font_24x27,0xffff,0x0000);
					*mode = 0;
				}
				else
				{
					bru_LCD_ClearScreen(BRU_LCD_BLACK_COLOUR, 0, 88, 239, 88+27);
				}
        bru_LCD_WriteString_18Arial_R(62,88,"STEEPING",Font_24x27,0xffff,0x0000);
       
}

/**
* @brieaf preset setup
*/
void bru_LCD_Main_Menu_Preset_Setup_ENG(uint8_t* mode)
{
    
	    if(*mode)
			{
				clean_screen_display();
				bru_LCD_Draw_buttons_ENG("BACK","OK");
				bru_LCD_LinesForButtons(0xffff);
				bru_LCD_WriteString_18Arial_R(81,115,"SETUP",Font_24x27,0xffff,0x0000);
				*mode = 0;
			}
			else
			{
				bru_LCD_ClearScreen(BRU_LCD_BLACK_COLOUR, 0, 88, 239, 88+27);
			}
			bru_LCD_WriteString_18Arial_R(72,88,"PRESET",Font_24x27,0xffff,0x0000);
		
}

/**
* @brieaf machine setup
*/
void bru_LCD_Main_Menu_Machine_Setup_ENG(uint8_t* mode)
{   
	    if(*mode)
			{
				clean_screen_display();
				bru_LCD_Draw_buttons_ENG("BACK","OK");
				bru_LCD_LinesForButtons(0xffff);
				bru_LCD_WriteString_18Arial_R(81,115,"SETUP",Font_24x27,0xffff,0x0000);
				*mode = 0;
			}
			else
			{
				bru_LCD_ClearScreen(BRU_LCD_BLACK_COLOUR, 0, 88, 239, 88+27);
			}
			bru_LCD_WriteString_18Arial_R(65,88,"MACHINE",Font_24x27,0xffff,0x0000);   
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void bru_LCD_Update_Screen(bru_UI_Window_ID Window_ID, bool InitStage)
{
  switch(Window_ID)
  {
    case UI_WINDOW_INTRO_SCREEN:
			DB_Printf("#Screen 'HELLO BRU'\n");
      bru_LCD_IntroScreen();
      break;
    case UI_WINDOW_HOME_SCREEN:
			DB_Printf("#Screen 'HOME'\r\n");
      bru_LCD_HomeScreen();
      break;
    case UI_WINDOW_BREWING_STATUS_ENJOY:
			DB_Printf("#Screen 'ENJOY BRU'\n");
      bru_LCD_EngoyBRU();
      break;
		case UI_WINDOW_BREWING_STATUS_FILLING_UP:
			DB_Printf("#Screen 'FILLING UP'\n"); 	
			bru_LCD_FillingUp();
		  break;
    case UI_WINDOW_BREWING_STATUS_STEEPING:
			bru_LCD_Steeping(InitStage);
      break;
		case UI_WINDOW_BREWING_STATUS_CHAMBER_WASHING:
			bru_LCD_Chamber_Washing();
			break;
		case UI_WINDOW_BREWING_STEEPING_CANCEL:
			DB_Printf("#Screen 'STEEPING CANCEL'\n"); 	
			bru_LCD_Steeping_Cancel();
			break;
    case UI_WINDOW_BREWING_STATUS_SERVING:
			DB_Printf("#Screen 'SERVING'\n"); 	
      bru_LCD_Serving();
      break;
    case UI_WINDOW_ERROR_MSG_ADD_WATER:
			DB_Printf("#Screen 'ADD WATER'\r\n"); 	
			bru_LCD_Add_Water();
      break;    
		case UI_WINDOW_ERROR_MSG_CLOSE_LID:
			DB_Printf("#Screen 'CLOSE LID'\r\n");
      bru_LCD_Close_Lid();
      break;
	  case UI_WINDOW_ERROR_MSG_PLACE_CUP:
			DB_Printf("#Screen 'PLACE CUP'\r\n"); 	
			bru_LCD_Place_Cup();
			break;
		case UI_WINDOW_BREWING_STATUS_REMOVE_CUP:
			DB_Printf("#Screen 'REMOVE CUP'\r\n");
			bru_LCD_Remove_Cup();
			break;
		case UI_WINDOW_ERROR_MSG_PLACE_BREWING_CHAMBER:
			DB_Printf("#Screen 'PLACE CHAMBER'\r\n"); 	
			bru_LCD_Place_Brewing_Chamber();
			break;
    case UI_WINDOW_ERROR_MSG_ERROR_XXX:
			DB_Printf("#Screen 'ERROR %s'\n", outputBuffer); 	
      bru_LCD_Error_XXX();
      break;		
		case UI_WINDOW_DISPENSER_STATUS_MENU:
			bru_LCD_Preset_HotWater_Menu();
      break;
		case UI_WINDOW_DISPENSER_STATUS_DISPENSING:	
			DB_Printf("#Screen 'DISPENSING'\n");
			bru_LCD_Dispensing();
      break;
		case UI_WINDOW_PRESET_STATUS_MENU:
			bru_LCD_Preset_1_to_3_Menu();
      break;
		case UI_WINDOW_ALARM_SCREEN:
			bru_LCD_Preset_Alarm_Menu(InitStage);
			break;		
		case UI_WINDOW_BREWING_SETUP_MENU_TIME:
			bru_LCD_BrewingTime_Steeping(InitStage);
			break;
		case UI_WINDOW_BREWING_SETUP_MENU_EDIT_TIME:
			bru_LCD_BrewingTime_Steeping_Edit(InitStage);
			break;
		case UI_WINDOW_BREWING_SETUP_MENU_TEMPERATURE:
			bru_LCD_WaterTemperature(InitStage);
			break;
		case UI_WINDOW_BREWING_SETUP_MENU_EDIT_TEMPERATURE:
			bru_LCD_WaterTemperature_Edit(InitStage);
			break;
		case UI_WINDOW_BREWING_SETUP_MENU_WATER_AMOUNT:
			bru_LCD_WaterAmount(InitStage);
			break;
		case UI_WINDOW_BREWING_SETUP_MENU_EDIT_WATER_AMOUNT:
			bru_LCD_WaterAmount_Edit(InitStage);
			break;
		

    case UI_WINDOW_TEST_1:
      bru_LCD_Test_Screen1(InitStage);
      break;
    case UI_WINDOW_TEST_2:
      bru_LCD_Test_Screen2(InitStage);
      break;
    case UI_WINDOW_TEST_3:
      bru_LCD_Test_Screen3(InitStage);
      break;
    case UI_WINDOW_TEST_4:
      bru_LCD_Test_Screen4(InitStage);
      break;
    case UI_WINDOW_TEST_5:
      bru_LCD_Test_Screen5(InitStage);
      break;
    case UI_WINDOW_TEST_6:
      bru_LCD_Test_Screen6(InitStage);
      break;
    case UI_WINDOW_TEST_7:
      bru_LCD_Test_Screen7(InitStage);
      break;		
		
		default:
			DB_Printf("WARNING! Unknown Window_ID!\r\n");
			break;
  }
//    hUserInterface->lastWindowID = hUserInterface->currentWindowID;
}

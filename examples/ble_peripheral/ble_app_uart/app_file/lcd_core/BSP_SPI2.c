#include "BSP_SPI2.h"
#include "nrf_drv_spi.h"
#include "nrf_log.h"
#include "nrf_gpio.h"
#define SPI_INSTANCE  2/**< SPI instance index. */


//static volatile bool spi_xfer_done;  /**< Flag used to indicate that SPI instance completed the transfer. */
static bool init_flag=0;


static const nrf_drv_spi_t spi = NRF_DRV_SPI_INSTANCE(SPI_INSTANCE);  /**< SPI instance. */
//void spi2_cs_pin_en()
//{
//	nrf_gpio_pin_clear(SPI2_SS_PIN);
//}
//void spi2_cs_pin_dis()
//{
//	nrf_gpio_pin_set(SPI2_SS_PIN);	
//}
/**
 * @brief SPI user event handler.
 * @param event
 */
////static void spi2_event_handler(nrf_drv_spi_evt_t const * p_event,
////                       void *                    p_context)
////{
////    spi_xfer_done = true;
//////    NRF_LOG_INFO("Transfer completed.");
////  
////		spi2_cs_pin_dis();
////}

/**
* @brieaf spi 2 initial for LCD 
*/
void BSP_spi2_init()
{
	if(init_flag)
		return;
	init_flag=1;
	nrf_drv_spi_config_t spi_config = NRF_DRV_SPI_DEFAULT_CONFIG;
	spi_config.ss_pin   = NRF_DRV_SPI_PIN_NOT_USED;//SPI1_SS_PIN;//
	spi_config.miso_pin = NRF_DRV_SPI_PIN_NOT_USED;//SPI2_MISO_PIN;
	spi_config.mosi_pin = SPI2_MOSI_PIN;
	spi_config.sck_pin  = SPI2_SCK_PIN;
	spi_config.frequency=NRF_DRV_SPI_FREQ_8M;//NRF_DRV_SPI_FREQ_8M;//NRF_DRV_SPI_FREQ_125K
	APP_ERROR_CHECK(nrf_drv_spi_init(&spi, &spi_config, NULL, NULL));
	
	nrf_gpio_cfg_output(LCD_DC_PIN);
//	nrf_gpio_cfg_output(SPI2_SS_PIN);
//	spi2_cs_pin_dis();
	
}

/**
* @brieaf spi 2 de-initial
*/
void BSP_spi2_uninit()
{
	if(init_flag==0)
		return;
	init_flag=0;
	nrf_drv_spi_uninit(&spi);
}
void BSP_spi2_transfer(uint8_t *tx_buf,uint8_t *rx_buf,uint8_t tx_len,uint8_t rx_len)//1最底层封装可收发
{
	if(init_flag==0)
		return;
	// Reset rx buffer and transfer done flag
//	memset(rx_buf, 0, len);
	uint8_t tx_temp_buf[256];
	memcpy(tx_temp_buf,tx_buf,tx_len);
//	spi2_cs_pin_en();
//	spi_xfer_done = false;

	APP_ERROR_CHECK(nrf_drv_spi_transfer(&spi, tx_temp_buf, tx_len, rx_buf, rx_len));
//	spi2_cs_pin_dis();
//	while (!spi_xfer_done)
//	{
//			__WFE();
//	}
}
/**
* @brieaf spi2 send data
*/
static void BSP_spi2_tx(uint8_t *tx_buf,uint8_t len)
{

	BSP_spi2_transfer(tx_buf,NULL,len,0);
}

/**
* @brieaf spi2 receive data
*/
static void BSP_spi2_rx(uint8_t *rx_buf,uint8_t len)
{

	BSP_spi2_transfer(NULL,rx_buf,0,len);
}

/**
* @brieaf spi2 send data interface 
*/ 
void BSP_spi2_send(uint8_t *tx_buf,uint32_t len)
{
	uint32_t count=len/255;
	uint32_t send_len=0;
	for(uint32_t c=0;c<count;c++)
	{
		BSP_spi2_tx(tx_buf,255);
		tx_buf+=255;
		send_len+=255;
	}
	send_len=len-send_len;
	BSP_spi2_tx(tx_buf,send_len);
}

/**
* @brieaf spi2 receive data interface
*/
void BSP_spi2_receive(uint8_t *rx_buf,uint32_t len)
{
	uint32_t count=len/255;
	uint32_t receive=0;
	for(uint32_t c=0;c<count;c++)
	{
		BSP_spi2_rx(rx_buf,255);
		rx_buf+=255;
		receive+=255;
	}
	receive=len-receive;
	BSP_spi2_rx(rx_buf,receive);
}

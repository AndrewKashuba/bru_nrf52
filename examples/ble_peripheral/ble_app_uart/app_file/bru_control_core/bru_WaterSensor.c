/*
 * 	bru_WaterSensor.c
 *	WATER SENSOR
 *
 *  Created on: Dec 14, 2021
 *      Author: Andrew Kashuba
 */
 
/***********************************************************************
*           Include Files  
*************************************************************************/
#include "bru_WaterSensor.h"
#include "nrf_gpio.h"
#include "event_queue_c.h" 
#include "CtrlUnits.h"

/***********************************************************************
*           Macro Define  
*************************************************************************/
#define BRU_WATER_SENSOR_OUT_PIN		28
#define TICK_QUANTITY 8

/***********************************************************************
*           Type Define  
*************************************************************************/
//typedef enum
//{
//    NoConnected = 0,
//		No,
//    Ok
//} WaterSensorStatus;

/***********************************************************************
*           Static Function Declarations  
*************************************************************************/

/***********************************************************************
*          	Extern Variable Declarations 
*************************************************************************/
//extern uint8_t first_start_pump_or_no_water_flag;
extern bru_Sensor_EventMap EventMap;

/***********************************************************************
*          	Extern Function Declarations	
*************************************************************************/
extern bool evt_queue_Post(gevent_t* pEvent);

/***********************************************************************
*           Global Variable Define  
*************************************************************************/

/***********************************************************************
*           Static Variable Define 
*************************************************************************/
static bool water_status;
static bool last_water_status;
static uint16_t tick_counter;

/***********************************************************************
*                                   end
*************************************************************************/

void bru_Water_Sensor_Init(void)
{
	nrf_gpio_cfg_input(BRU_WATER_SENSOR_OUT_PIN,NRF_GPIO_PIN_NOPULL);
//	nrf_gpio_cfg_input(BRU_WATER_SENSOR_OUT_PIN,GPIO_PIN_CNF_PULL_Pullup);
	
	if(nrf_gpio_pin_read(BRU_WATER_SENSOR_OUT_PIN)) {
		tick_counter = 0;
		water_status = false;
	}
	else {		
		tick_counter = TICK_QUANTITY;
		water_status = true;
	}
	last_water_status = water_status;
}

bool bru_Water_Status(void)
{
	gevent_t evt;
	
	if(nrf_gpio_pin_read(BRU_WATER_SENSOR_OUT_PIN)) {
		if(tick_counter > 0) tick_counter--;
	}
	else {
		if(tick_counter < TICK_QUANTITY) tick_counter++;
	}
			
	if(tick_counter == 0) water_status = false;
	if(tick_counter == TICK_QUANTITY) water_status = true;
	
	if(!last_water_status && water_status) {
		evt.event = EVENT_WATER_OK;
		if (EventMap.water_enb) evt_queue_Post(&evt);	
	}

	if(last_water_status && !water_status) {
		evt.event = EVENT_WATER_NO;
		if (EventMap.water_enb) evt_queue_Post(&evt);	
	}		
	
	last_water_status = water_status;
	return water_status;		
}


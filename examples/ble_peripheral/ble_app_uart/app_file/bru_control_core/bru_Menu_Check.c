/*************************************************
	Copyright ? ShenZhen Chileaf Electronic Co., Ltd. 2020-2023. All rights reserved. 
	@file		bru_Multiplexer.c
	
	@version	V1.0
	@date			2021-07-05
	@author		WWang
	@brief		establish

*************************************************/

/***********************************************************************
*           Include Files  
************************************************************************
*/
#include "bru_Menu_Check.h"
#include "bru_WaterSensor.h"
#include "bru_Multiplexer_Check.h"
#include "bru_BrewingProcess.h"
#include "bru_Relay.h"
#include "target_command.h"
#include "BSP_menu_info.h"
#include "bru_PinchValve.h"
#include "bru_Pump.h"
#include "bru_pid_core.h"
#include "nrf_log.h"
#include "pwm_pump_core.h"
#include "dbprintf.h"
#include "CtrlUnits.h"

/***********************************************************************
*           Macro Define  
************************************************************************
*/

/***********************************************************************
*           Type Define  
************************************************************************
*/

/***********************************************************************
*           Static Function Declarations  
************************************************************************
*/

/***********************************************************************
*          	Extern Variable Declarations 
************************************************************************
*/
extern bru_Sensor_Status Sensors;

/***********************************************************************
*          	Extern Function Declarations	
************************************************************************
*/

/***********************************************************************
*           Global Variable Define  
************************************************************************
*/
uint8_t current_check_procedure = BRU_ERROR_CHECK_NONE;
/***********************************************************************
*           Static Variable Define  
************************************************************************
*/
static uint8_t TurnOnWater = false;   //true:打开，false:关闭
static uint32_t CheckTimerValue = 0;
static uint8_t Btn_Serving_Stop_flag = false; //true:按下按键停止加热
/***********************************************************************
*                                   end
************************************************************************
*/
static void bru_Check_While_Self_Testing_Start(void  *evt)
{
	static uint8_t is_error = 0;
	bru_UI_Interface_Handle hUserInterface = get_ui_interface();
	
//	if(!bru_Water_Check()) 
//	{
//		NRF_LOG_INFO("bru_Water_Check() = %d",bru_Water_Check());
//		set_timer_value(current_check_procedure, 10, NULL);
//		goto_pre_add_water_disp();
//		return;
//	}
	
	NRF_LOG_INFO("bru_Water_Check IS TRUE = %d, %d",bru_Pinch_Valve_Is_Close(),bru_Pinch_Valve_Is_Open());
	if(bru_Pinch_Valve_Is_Close())
	{
		is_error = 0;
		bru_NTC_Start_Check_Cup_Sampling();
	}
	
	else if(bru_Pinch_Valve_Is_Open()){
		Pinch_Valve(ToClose);
		if(bru_Pinch_Valve_Is_Open())
		{
			hUserInterface->errorCode = UI_ERROR_CODE_03;
			if(is_error == 1) goto_pre_error_code_disp();
			set_timer_value(current_check_procedure, 10, NULL);
		}else{
			set_timer_value(current_check_procedure, 5, NULL);	
		}
		is_error = 1;
	}
	
	else if(!(bru_Pinch_Valve_Is_Close())&&(!bru_Pinch_Valve_Is_Open()))
	{
		Pinch_Valve(ToClose);
		if(bru_Pinch_Valve_Is_Open())
		{
			hUserInterface->errorCode = UI_ERROR_CODE_03;
			if(is_error == 1) goto_pre_error_code_disp();
			set_timer_value(current_check_procedure, 10, NULL);
		}else{
			set_timer_value(current_check_procedure, 5, NULL);		
		}
		is_error = 1;
	}
}	

static void bru_Check_While_Pre_Brewing_Start(void  *evt)
{
	static uint8_t is_error = 0;
	bru_UI_Interface_Handle hUserInterface = get_ui_interface();
	
	if(!Sensors.Water) 
	{
	DB_Printf("### Water check\r\n"); 	
		set_timer_value(current_check_procedure, 10, NULL);
		goto_pre_add_water_disp();
	}
	
	else if(bru_Pinch_Valve_Is_Close())
	{
		DB_Printf("### STEP1: PV close\r\n"); 	
		is_error = 0;
		bru_NTC_Start_Check_Cup_Sampling();
	}
	
	else if(bru_Pinch_Valve_Is_Open()){
	DB_Printf("### PV open\r\n"); 	
		Pinch_Valve(ToClose);
		if(bru_Pinch_Valve_Is_Open()) {
			hUserInterface->errorCode = UI_ERROR_CODE_03;
			if(is_error == 1) goto_pre_error_code_disp();
			set_timer_value(current_check_procedure, 10, NULL);
		}else{
			set_timer_value(current_check_procedure, 1, NULL);
		}
		is_error = 1;
	}
	
	else if(!(bru_Pinch_Valve_Is_Close())&&(!bru_Pinch_Valve_Is_Open()))
	{
	DB_Printf("### PV !close & !open\r\n"); 		
		Pinch_Valve(ToClose);
		if(bru_Pinch_Valve_Is_Open()) {
			hUserInterface->errorCode = UI_ERROR_CODE_03;
			if(is_error == 1) goto_pre_error_code_disp();
			set_timer_value(current_check_procedure, 10, NULL);
		}else{
			set_timer_value(current_check_procedure, 1, NULL);
		}
		is_error = 1;
	}
}

static void bru_Check_While_Filling_Up_Start(void  *evt)
{
//	if(!bru_Water_Check()) {
	if(!true) {
		set_timer_value(current_check_procedure, 10, NULL);
		goto_pre_add_water_disp();
		bru_Brewing_Process_Pause();
	} else {
		bru_NTC_Start_Lid_Get_Position_Sampling();
	}
}	

static void bru_Check_While_Canceling_Brewing_Start(void  *evt)
{
	bru_NTC_Start_Check_Cup_Sampling();
}

static void bru_Check_While_Pre_Serving_Start(void  *evt)
{
	bru_NTC_Start_Check_Cup_Sampling();
}

static void bru_Check_While_Enjoy_Start(void  *evt)
{
	bru_UI_Interface_Handle hUserInterface = get_ui_interface();
	current_check_procedure = BRU_ERROR_CHECK_NONE;
	
	DB_Printf("### STEP9: Enjoy Start\r\n"); 

	bru_Pump_Stop();
	if((hUserInterface->hMachineSetup->rinseConfiguration == MS_RINSE_TO_TRAY) && \
		(hUserInterface->bChamberWashingFlag == true)) {
			hUserInterface->bChamberWashingFlag = false;
			bru_Menu_Check_Procedure(BRU_ERROR_CHECK_WHILE_REMOVE_CUP);
		} else {
			hUserInterface->bChamberWashingFlag = true;
			goto_brewing_enjoy_5_disp();
		}
	set_rgb_duty(0,0,0); //ak	
}

static void bru_Check_While_Remove_Cup_Start(void  *evt)
{
	bru_NTC_Start_Lid_Get_Position_Sampling();
}

static void bru_Check_Pre_Washing_Start(void  *evt)
{
	bru_NTC_Start_Lid_Get_Position_Sampling();
}

static void bru_Check_Hot_Water_Dispancer_Start(void  *evt)
{
	bru_NTC_Start_Check_Cup_Sampling();
}

static void bru_Check_While_Serving_Have_Water_Start(void  *evt)
{
	bru_NTC_Start_Check_Cup_Sampling();
}

static void bru_Check_While_Cancel_Have_Water_Start(void  *evt)
{
	bru_NTC_Start_Check_Cup_Sampling();
}

static void bru_Check_While_Enjoy_Have_Water_Start(void  *evt)
{
	bru_NTC_Start_Check_Cup_Sampling();
}

uint8_t setCheckTimerValue(void)
{
	if(getWaterVolume() > 400)
	{
		CheckTimerValue = 11 * TIMER_CALL_HZ;
	}else if(getWaterVolume() > 350)
	{
		CheckTimerValue = 10 * TIMER_CALL_HZ;
	}else if(getWaterVolume() > 300)
	{
		CheckTimerValue = 9 * TIMER_CALL_HZ;
	}else if(getWaterVolume() > 250)
	{
		CheckTimerValue = 8 * TIMER_CALL_HZ;
	}else if(getWaterVolume() > 200)
	{
		CheckTimerValue = 7 * TIMER_CALL_HZ;
	}else if(getWaterVolume() > 150)
	{
		CheckTimerValue = 6 * TIMER_CALL_HZ;
	}else if(getWaterVolume() > 100)
	{
		CheckTimerValue = 5 * TIMER_CALL_HZ;
	}else if(getWaterVolume() > 50)
	{
		CheckTimerValue = 4 * TIMER_CALL_HZ;
	}else 
	{
		CheckTimerValue = 3 * TIMER_CALL_HZ;
	}
	
	return CheckTimerValue/TIMER_CALL_HZ;
}

static void bru_Check_Serving_Chamber_have_Water(void  *evt)
{
	bru_UI_Interface_Handle hUserInterface = get_ui_interface();
	current_check_procedure = BRU_ERROR_CHECK_NONE;
	
	if(hUserInterface->iPresetCurrentIndex == BRU_HOT_WATER_DISPENSER_INDEX)
	{
		goto check_enjoy;
	}
	
	if(hUserInterface->hMachineSetup->rinseConfiguration == MS_RINSE_TO_CUP)
	{
		bru_Menu_Check_Procedure(BRU_ERROR_CHECK_WHILE_PRE_WASHING);
	}else if(hUserInterface->hMachineSetup->rinseConfiguration == MS_RINSE_TO_TRAY)
	{
		hUserInterface->bChamberWashingFlag = false;
		bru_Menu_Check_Procedure(BRU_ERROR_CHECK_WHILE_REMOVE_CUP);
	}else{
check_enjoy:
		
		hUserInterface->bBrewingProcessFlag = false;
		hUserInterface->bChamberWashingFlag = false;
		bru_Menu_Check_Procedure(BRU_ERROR_CHECK_WHILE_ENJOY);
	}
}

void bru_Serving_Chamber_have_Water_Open(void)
{
	if(Btn_Serving_Stop_flag) return;
	if(TurnOnWater == true) return;
	TurnOnWater = true;
	
	Pinch_Valve(ToOpen);
	set_timer_value(BRU_CHECK_SERVING_CHAMBER_HAVE_WATER_ID, CheckTimerValue, NULL);
}

void bru_Serving_Chamber_have_Water_Close(void)
{
	TIMER_INFO_T  *timer;
	Btn_Serving_Stop_flag = false;
	if(TurnOnWater == false) return;
	TurnOnWater = false;
	 
	Pinch_Valve(ToClose);
	timer = find_timer(BRU_CHECK_SERVING_CHAMBER_HAVE_WATER_ID);
	CheckTimerValue = timer->timer_value;
	set_timer_value(BRU_CHECK_SERVING_CHAMBER_HAVE_WATER_ID, 0, NULL);
}

void bru_Serving_Chamber_have_Water_Btn_Stop(void)
{
	TIMER_INFO_T  *timer;
	Btn_Serving_Stop_flag = true;
	TurnOnWater = false;
	 
	Pinch_Valve(ToClose);
	timer = find_timer(BRU_CHECK_SERVING_CHAMBER_HAVE_WATER_ID);
	CheckTimerValue = timer->timer_value;
	set_timer_value(BRU_CHECK_SERVING_CHAMBER_HAVE_WATER_ID, 0, NULL);
}

void bru_Cancel_Chamber_have_Water_Open(void)
{
	if(TurnOnWater == true) return;
	TurnOnWater = true;
	
	Pinch_Valve(ToOpen);
	set_timer_value(get_steeping_cancel_timer_id(), CheckTimerValue, NULL);
}

void bru_Cancel_Chamber_have_Water_Check_Over(void)
{
	bru_UI_Interface_Handle hUserInterface = get_ui_interface();
	hUserInterface->bBrewingProcessFlag = false;
	current_check_procedure = BRU_ERROR_CHECK_NONE;
}

void bru_Cancel_Chamber_have_Water_Close(void)
{
	TIMER_INFO_T  *timer;
	if(TurnOnWater == false) return;
	TurnOnWater = false;
	 
	Pinch_Valve(ToClose);
	timer = find_timer(get_steeping_cancel_timer_id());
	CheckTimerValue = timer->timer_value;
	set_timer_value(get_steeping_cancel_timer_id(), 0, NULL);
}

void bru_Enjoy_Chamber_have_Water_Open(void)
{
	if(TurnOnWater == true) return;
	TurnOnWater = true;
	
	bru_Pump_Raw_Set(0);
	Pinch_Valve(ToOpen);
	set_timer_value(BRU_ERROR_CHECK_WHILE_ENJOY, CheckTimerValue, NULL);
}

void bru_Enjoy_Chamber_have_Water_Close(void)
{
	TIMER_INFO_T  *timer;
	if(TurnOnWater == false) return;
	TurnOnWater = false;
	
	DB_Printf("### STEP5: Water Close\r\n");  
	bru_Pump_Stop();
	Pinch_Valve(ToClose);
	timer = find_timer(BRU_ERROR_CHECK_WHILE_ENJOY);
	CheckTimerValue = timer->timer_value;
	set_timer_value(BRU_ERROR_CHECK_WHILE_ENJOY, 0, NULL);
}

void bru_Cancel_Washing_Evt(void)
{
	bru_UI_Interface_Handle hUserInterface = get_ui_interface();
	
	DB_Printf("### STEP4: Cancel\r\n");  
	Pinch_Valve(ToOpen);
	bru_Pump_Stop();
	TurnOnWater = false;
	hUserInterface->bChamberWashingFlag = true;
	current_check_procedure = BRU_ERROR_CHECK_NONE;
	set_timer_value(BRU_ERROR_CHECK_WHILE_ENJOY, 0, NULL);
	goto_home_disp();
}

void bru_Menu_Check_Init(void)
{
	bru_Brewing_Process_Init();
	
	register_timer(BRU_ERROR_CHECK_WHILE_SELF_TESTING,bru_Check_While_Self_Testing_Start);
	register_timer(BRU_ERROR_CHECK_WHILE_PRE_BREWING,bru_Check_While_Pre_Brewing_Start);
	register_timer(BRU_ERROR_CHECK_WHILE_FILLING_UP,bru_Check_While_Filling_Up_Start);
	register_timer(BRU_ERROR_CHECK_WHILE_CANCELING_BREWING,bru_Check_While_Canceling_Brewing_Start);
	register_timer(BRU_ERROR_CHECK_WHILE_PRE_SERVING,bru_Check_While_Pre_Serving_Start);
	register_timer(BRU_ERROR_CHECK_WHILE_REMOVE_CUP,bru_Check_While_Remove_Cup_Start);
	register_timer(BRU_ERROR_CHECK_WHILE_ENJOY,bru_Check_While_Enjoy_Start);
	register_timer(BRU_ERROR_CHECK_WHILE_PRE_WASHING,bru_Check_Pre_Washing_Start);
	register_timer(BRU_ERROR_CHECK_WHILE_HOT_WATER_DISPANCER,bru_Check_Hot_Water_Dispancer_Start);
	register_timer(BRU_ERROR_CHECK_WHILE_SERVING_HAVE_WATER,bru_Check_While_Serving_Have_Water_Start);
	register_timer(BRU_ERROR_CHECK_WHILE_CANCEL_HAVE_WATER,bru_Check_While_Cancel_Have_Water_Start);
	register_timer(BRU_ERROR_CHECK_WHILE_ENJOY_HAVE_WATER,bru_Check_While_Enjoy_Have_Water_Start);
	
	register_timer(BRU_CHECK_SERVING_CHAMBER_HAVE_WATER_ID,bru_Check_Serving_Chamber_have_Water);
}

void bru_Menu_Check_Procedure(uint8_t procedure)
{
	current_check_procedure = procedure;
	NRF_LOG_INFO("current_check_procedure = %d",current_check_procedure);
	switch(current_check_procedure)
	{
		case BRU_ERROR_CHECK_NONE:
			break;
		case BRU_ERROR_CHECK_WHILE_SELF_TESTING:
			//set_rgb_duty(100,100,100);
			bru_Check_While_Self_Testing_Start(NULL);
			break;
		case BRU_ERROR_CHECK_WHILE_PRE_BREWING:
			set_rgb_duty(100,100,100);
			bru_Check_While_Pre_Brewing_Start(NULL);
			break;
		case BRU_ERROR_CHECK_WHILE_FILLING_UP:
			bru_Check_While_Filling_Up_Start(NULL);
			break;
		case BRU_ERROR_CHECK_WHILE_CANCELING_BREWING:
			bru_Check_While_Canceling_Brewing_Start(NULL);
			break;
		case BRU_ERROR_CHECK_WHILE_PRE_SERVING:
			bru_Check_While_Pre_Serving_Start(NULL);
			break;
		case BRU_ERROR_CHECK_WHILE_PRE_WASHING:
//ak			bru_Check_Pre_Washing_Start(NULL); 
			bru_Check_While_Enjoy_Start(NULL);	//ak tmp rev	
			break;
		case BRU_ERROR_CHECK_WHILE_REMOVE_CUP:
			bru_Check_While_Remove_Cup_Start(NULL);
			break;
		case BRU_ERROR_CHECK_WHILE_ENJOY:
			bru_Check_While_Enjoy_Start(NULL);
			break;
		case BRU_ERROR_CHECK_WHILE_HOT_WATER_DISPANCER:
			bru_Check_Hot_Water_Dispancer_Start(NULL);
			break;
		case BRU_ERROR_CHECK_WHILE_SERVING_HAVE_WATER:
			TurnOnWater = true;
			setCheckTimerValue();
			set_timer_value(BRU_CHECK_SERVING_CHAMBER_HAVE_WATER_ID, CheckTimerValue, NULL);
			bru_Check_While_Serving_Have_Water_Start(NULL);
			break;
		case BRU_ERROR_CHECK_WHILE_CANCEL_HAVE_WATER:
			TurnOnWater = true;
			bru_Check_While_Cancel_Have_Water_Start(NULL);
			break;
		case BRU_ERROR_CHECK_WHILE_ENJOY_HAVE_WATER:
			TurnOnWater = true;
			bru_Check_While_Enjoy_Have_Water_Start(NULL);
			break;
		default:
			break;
	}
}

void start_or_stop_operation(uint8_t index,uint8_t stsp)
{
	bru_UI_Interface_Handle p = get_ui_interface();
	
	p->iPresetCurrentIndex = index;
	
	if(stsp == 0)		//stop_operation
	{
		if(p->bBrewingProcessFlag == false) 
		{
			if(index == 5) bru_alarm_cancel_brewing();
			return;
		}
		
		if(p->bBrewingStartFlag)
		{
			bru_Brewing_Process_Stop();
		}else if((current_check_procedure == BRU_ERROR_CHECK_WHILE_REMOVE_CUP) || \
						(current_check_procedure == BRU_ERROR_CHECK_WHILE_ENJOY_HAVE_WATER))
		{
			bru_Cancel_Washing_Evt();
		}else if(current_check_procedure == BRU_ERROR_CHECK_WHILE_SERVING_HAVE_WATER)
		{
			bru_Serving_Chamber_have_Water_Btn_Stop();
		}else{
			bru_Menu_Check_Procedure(BRU_ERROR_CHECK_WHILE_CANCELING_BREWING);
		}
		
	}else{
		if(index == 5)		//定时加热
		{
			bru_alarm_start_brewing();
		}else{
			bru_Menu_Check_Procedure(BRU_ERROR_CHECK_WHILE_PRE_BREWING);
		}
	}
}




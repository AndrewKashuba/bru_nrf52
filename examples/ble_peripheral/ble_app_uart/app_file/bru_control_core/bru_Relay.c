/*************************************************
	Copyright ? ShenZhen Chileaf Electronic Co., Ltd. 2020-2023. All rights reserved. 
	@file		bru_Relay.c
	
	@version	V1.0
	@date			2021-07-05
	@author		WWang
	@brief		establish

*************************************************/

/***********************************************************************
*           Include Files  
************************************************************************
*/
//#include "bru_Relay.h"
//#include "nrf_gpio.h"
//#include "app_timer.h"
//#include "nrf_drv_gpiote.h"
//#include "gpiote_drv_core.h"

//#define NRF_LOG_MODULE_NAME Relay

//#define NRF_LOG_LEVEL       3
//#define NRF_LOG_INFO_COLOR  3

//#include "nrf_log.h"
//NRF_LOG_MODULE_REGISTER();
/***********************************************************************
*           Macro Define  
************************************************************************
*/
//#define BRU_RELAY_CONTROL_PIN				25
//#define BRU_RELAY_ZERO_CROSS_PIN		27

/***********************************************************************
*           Type Define  
************************************************************************
*/

/***********************************************************************
*           Static Function Declarations   
************************************************************************
*/

/***********************************************************************
*          	Extern Variable Declarations 
************************************************************************
*/

/***********************************************************************
*          	Extern Function Declarations	
************************************************************************
*/

/***********************************************************************
*           Global Variable Define   
************************************************************************
*/

/***********************************************************************
*           Static Variable Define  
************************************************************************
*/
//static volatile bool bRelayState = false;		//Current relay status, false: off, true: on
/***********************************************************************
*                                   end
************************************************************************
*/

//void bru_Relay_Init(void)
//{
//	nrf_gpio_cfg_output(BRU_RELAY_CONTROL_PIN);
//}

//void bru_Relay_ON(void)
//{
//	bRelayState = true;
//	nrf_gpio_pin_set(BRU_RELAY_CONTROL_PIN);
//}

//void bru_Relay_OFF(void)
//{
//	bRelayState = false;
//	nrf_gpio_pin_clear(BRU_RELAY_CONTROL_PIN);
//}




/*
 * 	bru_PinchValve.c
 *	PINCH VALVE
 *
 *  Created on: Dec 6, 2021
 *      Author: Andrew Kashuba
 */

/***********************************************************************
*           Include Files 
*************************************************************************/
#include "dbprintf.h"
#include "bru_version.h"
#include "bru_PinchValve.h"
#include "nrf_gpio.h"
#include "event_queue_c.h" 
#include "bru_Context.h"

/***********************************************************************
*           Macro Define 
*************************************************************************/
#define BRU_PINCH_VALVE_IN_1_PIN				19
#define BRU_PINCH_VALVE_IN_2_PIN				18	
#define BRU_PINCH_VALVE_OPEN_POS_PIN		6
#define BRU_PINCH_VALVE_CLOSE_POS_PIN		7	

/*************** Pin Status *****************/
#define BRU_SWITCH_OPEN                 1
#define BRU_SWITCH_CLOSE                0

/*************** timer *****************/
#define LIMIT_TIME_PROCESS	(3000 / TASK_TICK_INTERVAL)
#define PROCESS_START_INTERVAL (9 * LIMIT_TIME_PROCESS / 10)

#define DEF_ErrorNo_Info						("")
#define DEF_Error10_Info						("PV Motor NC!")
#define DEF_Error11_Info						("Swap switches")
#define DEF_Error12_Info						("Reductor err!")
#define DEF_Error14_Info						("Open Switch!")
#define DEF_Error15_Info						("Close Switch!")
#define DEF_Error16_Info						("Check nozzle!")

/***********************************************************************
*           Type Define 
*************************************************************************/

/***********************************************************************
*           Static Function Declarations   
*************************************************************************/
static void Pinch_Valve_Done(PVUnitStatus PV_ToState);
	
/***********************************************************************
*          	Extern Variable Declarations 
*************************************************************************/
extern bool fTestMode;
extern bru_Sensor_Status Sensors;
extern bru_Sensor_EventMap EventMap;

/***********************************************************************
*          	Extern Function Declarations	
*************************************************************************/
extern bool evt_queue_Post(gevent_t* pEvent);

/***********************************************************************
*           Global Variable Define 
*************************************************************************/
bru_PV_unit PV;

/***********************************************************************
*           Static Variable Define  
*************************************************************************/
static PVMotorStatus PVTargetStatus = Sleep;
static bool StartTime = false;
static bool NozzleClose = false;
static uint32_t PVCountdownTime = 0;

/************************************************************************/

void bru_Pinch_Valve_Init(void)
{
	nrf_gpio_cfg_output(BRU_PINCH_VALVE_IN_1_PIN);
	nrf_gpio_cfg_output(BRU_PINCH_VALVE_IN_2_PIN);
	bru_Pinch_Valve_Set_Direction(PINCH_VALVE_TO_SLEEP);
	
	nrf_gpio_cfg_input(BRU_PINCH_VALVE_OPEN_POS_PIN,NRF_GPIO_PIN_NOPULL);
	nrf_gpio_cfg_input(BRU_PINCH_VALVE_CLOSE_POS_PIN,NRF_GPIO_PIN_NOPULL);

	PV.ErrInfo = DEF_ErrorNo_Info;	
}

void Pinch_Valve(PVMotorStatus PV_ToState)
{
	gevent_t evt;	
	
	PVTargetStatus = PV_ToState;
	PV.MotorStatus = PV_ToState;
	if(PV_ToState == Sleep) {
		PVCountdownTime = 0;
		bru_Pinch_Valve_Set_Direction(PINCH_VALVE_TO_SLEEP);
		bru_Pinch_Valve_Set_Direction(PINCH_VALVE_TO_BRAKE);
		return;
	}	
	
	PV.Error = PV_ERROR_NONE;
	PV.ErrInfo = DEF_ErrorNo_Info;
	PV.MaxCurrent = 0;
	if(EventMap.pv_begin_enb) {
		evt.event = EVENT_PV_BEGIN;
		evt_queue_Post(&evt);	
	}				

	PV.UnitStatus = Unknown;
	NozzleClose = false;
	if(PV_ToState == ToOpen) {
		if(!Sensors.Water) {
			DB_Printf("WARNING! Hard code error 25!\n");
			return;
		}
		if(bru_Pinch_Valve_Is_Open()) {
			Pinch_Valve_Done(Open);
			return;
		} else {		
			bru_Pinch_Valve_Set_Direction(PINCH_VALVE_TO_OPEN);
			DB_Printf("Opening Pinch Valve\n");		
		}
	}
	else if(PV_ToState == ToClose) {
		if(bru_Pinch_Valve_Is_Close()) {
			Pinch_Valve_Done(Close);			
			return;
		} else {
			bru_Pinch_Valve_Set_Direction(PINCH_VALVE_TO_CLOSE);
			DB_Printf("Closing Pinch Valve\n");			
		}
	}
	
	PVCountdownTime = LIMIT_TIME_PROCESS;
	StartTime = true;	
}

bool bru_Pinch_Valve_Is_Open(void)
{
	if(nrf_gpio_pin_read(BRU_PINCH_VALVE_OPEN_POS_PIN))
		return BRU_SWITCH_CLOSE;
	else
		return BRU_SWITCH_OPEN;
}

bool bru_Pinch_Valve_Is_Close(void)
{
	if(NozzleClose) return BRU_SWITCH_CLOSE;
	
	if(nrf_gpio_pin_read(BRU_PINCH_VALVE_CLOSE_POS_PIN))
		return BRU_SWITCH_CLOSE;
	else
		return BRU_SWITCH_OPEN;
}

void bru_Pinch_Valve_Set_Direction(Bru_Pinch_Valve_Direction_Type direction)
{
	switch(direction) {
		case PINCH_VALVE_TO_OPEN:
			nrf_gpio_pin_clear(BRU_PINCH_VALVE_IN_1_PIN);
			nrf_gpio_pin_set(BRU_PINCH_VALVE_IN_2_PIN);
			break;
		case PINCH_VALVE_TO_CLOSE:
			nrf_gpio_pin_set(BRU_PINCH_VALVE_IN_1_PIN);
			nrf_gpio_pin_clear(BRU_PINCH_VALVE_IN_2_PIN);
			break;
		case PINCH_VALVE_TO_BRAKE:
			nrf_gpio_pin_set(BRU_PINCH_VALVE_IN_1_PIN);
			nrf_gpio_pin_set(BRU_PINCH_VALVE_IN_2_PIN);
			break;
		case PINCH_VALVE_TO_SLEEP:
			nrf_gpio_pin_clear(BRU_PINCH_VALVE_IN_1_PIN);
			nrf_gpio_pin_clear(BRU_PINCH_VALVE_IN_2_PIN);
			break;
	}
}

void get_pinch_current(void *data)
{
	PV.Current = *((int16_t *)(data)) * 3250 / 4096 * 2;
	if(PV.Current < 5) PV.Current = 0;
	if ((!StartTime) && (PV.MotorStatus != Sleep)) {
		if(PV.MaxCurrent < PV.Current) PV.MaxCurrent = PV.Current;
		DB_Printf("PV current sensor: %d mA\n", PV.Current);	
	}
}

// -----------------------------------------------------------------------------

static void Pinch_Valve_Done(PVUnitStatus PV_ToState)
{
	gevent_t evt;	
	
	PV.UnitStatus = PV_ToState;

	if(PV_ToState == Open) {
		if(EventMap.pv_open_enb) {
			evt.event = EVENT_PV_OPEN;
			evt_queue_Post(&evt);				
		}		
		DB_Printf("Opened Pinch Valve\n");		
	}
	
	else if(PV_ToState == Close) {
		if(EventMap.pv_close_enb) {
			evt.event = EVENT_PV_CLOSE;
			evt_queue_Post(&evt);				
		}	
		if (NozzleClose)
			DB_Printf("Closed Pinch Valve (nozzle is ok)\n");				
		else
			DB_Printf("Closed Pinch Valve (no nozzle)\n");			
	}
	
	Pinch_Valve(Sleep);
}

static void Pinch_Valve_Error(bru_PV_Error_Code Error)
{
	gevent_t evt;	

	Pinch_Valve(Sleep);
	PV.UnitStatus = Failed;
	PV.Error = Error;
	if(EventMap.pv_error_enb) {
		bru_UI_Interface_Handle hUserInterface = get_ui_interface();
		hUserInterface->errorCode = (int) Error;
		evt.event = EVENT_PV_ERROR;
		evt_queue_Post(&evt);
	}
	
	switch(Error)	{
		case PV_ERROR_10:
			DB_Printf("ERROR! PV motor don't connected!\n");
			PV.ErrInfo = DEF_Error10_Info;
			break;		
		case PV_ERROR_11:
			DB_Printf("ERROR! Swaped PV Pos_Lim_Switches!\n");
			PV.ErrInfo = DEF_Error11_Info;
			break;
		case PV_ERROR_12:		
			DB_Printf("ERROR! Destroed PV reductor!\n");
			PV.ErrInfo = DEF_Error12_Info;
			break;
		case PV_ERROR_14:		
			DB_Printf("ERROR! 14!\n");									// - Open Pos_Lim_Switch error ?
			PV.ErrInfo = DEF_Error14_Info;
			break;
		case PV_ERROR_15:		
			DB_Printf("ERROR! 15!\n");									// - Close Pos_Lim_Switch error ?
			PV.ErrInfo = DEF_Error15_Info;
			break;
		case PV_ERROR_16:
			DB_Printf("WARNING! Check nozzle!\n");			// check nozzle - Close Pos_Lim_Switch
			PV.ErrInfo = DEF_Error16_Info;
			break;		
		default: 
			break;		
	}
}

void bru_PV_Loop(void)
{
	if (PVCountdownTime != 0)	{
		
		PVCountdownTime--;
		if (StartTime && (PVCountdownTime < PROCESS_START_INTERVAL))
			StartTime = false;
		
		// --- Limit time process --------------------------------------------------
		if (PVCountdownTime == 0)	{	

			if(PV.MaxCurrent == 0)
				Pinch_Valve_Error(PV_ERROR_10);						// 10 - motor don't connected	

			else if(PVTargetStatus == ToOpen) {
				DB_Printf("WARNING! Timer Open Pinch Valve\n");
				Pinch_Valve_Error(PV_ERROR_14);						// 14 - check Open Pos_Lim_Switch
			}
		
			else if(PVTargetStatus == ToClose) {
				DB_Printf("WARNING! Timer Close Pinch Valve\n");
				if (fTestMode) {	
					if((5 < PV.MaxCurrent) && (PV.MaxCurrent < 100))
						Pinch_Valve_Error(PV_ERROR_12);					// 12 - destroed PV reductor
				}	
				else				
					Pinch_Valve_Done(Close); //!!! tmp
			}

			Pinch_Valve(Sleep);
		}
		// --- End Limit time process ----------------------------------------------
			
		if(!StartTime && (PV.Current > 0)) {
			
			if(PVTargetStatus == Open) {
				if (bru_Pinch_Valve_Is_Open()) 						// check limit_switch
					Pinch_Valve_Done(Open);			
				if (fTestMode) {
					if (bru_Pinch_Valve_Is_Close())
						Pinch_Valve_Error(PV_ERROR_11);					// 11 - swaped Pos_Lim_Switches				
				}
			}
			else if(PVTargetStatus == Close) {
				if (PV.MaxCurrent > 200) {
					NozzleClose = true;
					Pinch_Valve_Done(Close);
				}
				else {			
					if (bru_Pinch_Valve_Is_Close())	
//						Pinch_Valve_Error(PV_ERROR_16);				// 16 - check nozzle
						Pinch_Valve_Done(Close);
					if (fTestMode) {	
						if (bru_Pinch_Valve_Is_Open())
							Pinch_Valve_Error(PV_ERROR_11);				// 11 - swaped Pos_Lim_Switches
					}					
				}			
			}
		}
	}
}


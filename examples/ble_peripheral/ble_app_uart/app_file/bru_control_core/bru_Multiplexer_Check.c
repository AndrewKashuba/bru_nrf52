/*************************************************
	Copyright ? ShenZhen Chileaf Electronic Co., Ltd. 2020-2023. All rights reserved. 
	@file		bru_Multiplexer.c
	
	@version	V1.0
	@date			2021-07-05
	@author		WWang
	@brief		establish

*************************************************/

/***********************************************************************
*           Include Files  
************************************************************************
*/
#include "BSP_ADC.h"
//#include "bru_Multiplexer_Check.h"
//#include "bru_PinchValve.h"
//#include "bru_BrewingProcess.h"
//#include "nrf_log.h"
//#include <math.h>
//#include "bru_Menu_Check.h"
//#include "target_command.h"
//#include "bru_data.h"
//#include "BSP_menu_info.h"
//#include "bru_Pump.h"
//#include "dbprintf.h"

/***********************************************************************
*           Macro Define  
************************************************************************
*/

/***********************************************************************
*           Type Define  
************************************************************************
*/

/***********************************************************************
*           Static Function Declarations  
************************************************************************
*/

/***********************************************************************
*          	Extern Variable Declarations 
************************************************************************
*/

/***********************************************************************
*          	Extern Function Declarations	
************************************************************************
*/
//extern void bru_PID_Algorithm_Handler(float temperature);
/***********************************************************************
*           Global Variable Define  
************************************************************************
*/

/***********************************************************************
*           Static Variable Define  
************************************************************************
*/
//static const float Rb  = 15000;        //Ballast resistor from the button branch of NTC 42.2

//static const uint32_t R2T[140] = {
//	544435,516236,489694,464701,441157,418969,398049,378317,359698,342123,
//	325526,309233,293865,279364,265676,252750,240539,228999,218089,207770,
//	198008,188768,180020,171734,163883,156442,149387,142696,136347,130321,
//	124600,119167,114005,109099,104435,100000,95667,91549,87634,83911,
//	80369,76998,73789,70734,67824,65051,62409,59890,57487,55196,
//	53010,50923,48932,47029,45213,43477,41818,40232,38715,37265,
//	35877,34519,33221,31978,30789,29652,28563,27520,26521,25564,
//	24647,23768,22926,22118,21343,20599,19886,19201,18544,17913,
//	17307,16724,16165,15627,15110,14613,14135,13676,13234,12808,
//	12398,12004,11625,11259,10907,10568,10233,9911,9600,9301,
//	9013,8736,8468,8210,7961,7721,7489,7266,7050,6842,
//	6641,6446,6257,6074,5898,5728,5563,5404,5251,5102,
//	4959,4820,4686,4556,4431,1309,4192,4078,3968,3861,
//	3758,3658,3561,3467,3377,3289,3202,3117,3036,2957
//};


///**
//* @brieaf calc temperature 
//*/
//float calc_temperature_value(void *data)
//{
//	float temperature = 0;
//	int16_t adc_temp = *((int16_t *)(data));
//	if(adc_temp < 271)  return -10.0f;
//	uint32_t Rntc = ((float)4096/adc_temp-1)*Rb;
//	uint8_t i;
//	for(i=0; i<140; i++)
//	{
//		if(Rntc >= R2T[i]) break;
//	}
//	if(i == 0) temperature = -10.0f;
//	else if(i == 140) temperature = 130.0f;
//	else	temperature = (i-10)-(float)(Rntc-R2T[i])/(R2T[i-1]-R2T[i]);
//	return temperature;
//}

/***********************************************************************
*                                   end
************************************************************************
*/
///**
//① data/4096 = vol/Vcc  ---> vol = data * Vcc / 4096
//② (vol/4)/(Vcc/4) = Rb/(Rb+Rntc) --->vol =  Rb*Vcc/(Rb+Rntc)
//①+② ---> data * Vcc / 4096 = Rb*Vcc/(Rb+Rntc)
//		---->Rntc = (4096/data-1)*Rb 
//*/
//void get_ntc_bottom(void *data)
//{
//	
//	float temperature = calc_temperature_value(data);
//	static char buf[32];
//	sprintf(buf,"%.1f",temperature);
//	DB_Printf("bot %s,%d\r\n",buf,*((int16_t *)data));
//	
////	bru_UI_Interface_Handle hUserInterface = get_ui_interface();
////	
////	if(temperature < -5.0)
////	{
////		set_timer_value(current_check_procedure, 10, NULL);
////		hUserInterface->errorCoder = UI_ERROR_CODE_02;
////	}else{
////		switch(current_check_procedure)
////		{
////			case BRU_ERROR_CHECK_WHILE_SELF_TESTING:
////				current_check_procedure = BRU_ERROR_CHECK_NONE;
////				goto_home_from_check();
////				break;
////			default:
////				break;
////		}
////	}
//}

//void get_ntc_top(void *data)
//{
//	uint32_t time_ms = 0;
//	static uint8_t overheat_protection_times = 0;
//	float temperature = calc_temperature_value(data);

//	static char buf[32];
//	sprintf(buf,"%.1f",temperature);
//	DB_Printf("top %s,%d\r\n",buf,*((int16_t *)data));
//	
////	bru_UI_Interface_Handle hUserInterface = get_ui_interface();
////	
////	if(temperature < -5.0)
////	{
////		set_timer_value(current_check_procedure, 10, NULL);
////		hUserInterface->errorCoder = UI_ERROR_CODE_01;
////	}else{
////		bru_NTC_Start_Temperature_BOT_Sampling();
////	}
////	if(temperature > 101.0f)
////	{
////		overheat_protection_times += 10;
////	}
////	if(temperature > 100.5f)
////	{
////		if(++overheat_protection_times > 20)
////		{
////			bru_Brewing_Process_Stop();
////			return;
////		}
////	} else {
////		overheat_protection_times = 0;
////	}
////	
////	if(g_start_preheat)
////	{
////		g_start_preheat = 0;
////		if(get_waterTemperatureRequired() < temperature) time_ms = 0;
////		/*
////		4180:  4.18*1000  4.18是水的比热容，1000是s转成ms
////		1200:  加热器功率是1200w
////		0.96： 加热器热效率
////		*/
////		else time_ms = (get_waterTemperatureRequired()-temperature)*4180*12/1200/0.96;
////		start_preheat_time(time_ms);
////	} else {
////		bru_PID_Algorithm_Handler(temperature);
////	}
//}

//#define CUP_LIMIT_VOLTAGE    (2300)      // 2.3V
//#define CUP_LIMIT_ADC_VALUE(VOL_IN_MV)  (VOL_IN_MV*4096/3250)
//void get_ir_sensor(void *data)
//{
//	static uint8_t move_cup_flag = 0;
//	bru_UI_Interface_Handle hUserInterface = get_ui_interface();
//	int16_t ir_adc = *((int16_t *)(data));
//  DB_Printf("get_ir_sensor *data = %d\r\n",ir_adc);
//	
//	if(ir_adc < CUP_LIMIT_ADC_VALUE(CUP_LIMIT_VOLTAGE))
//	{
//		DB_Printf("no place cup\r\n");
//		// no place cup
//		if((BRU_ERROR_CHECK_WHILE_REMOVE_CUP != current_check_procedure)&&\
//			(BRU_ERROR_CHECK_WHILE_PRE_WASHING != current_check_procedure)&&\
//			(BRU_ERROR_CHECK_WHILE_SERVING_HAVE_WATER != current_check_procedure)&&\
//			(BRU_ERROR_CHECK_WHILE_CANCEL_HAVE_WATER != current_check_procedure)&&\
//			(BRU_ERROR_CHECK_WHILE_ENJOY_HAVE_WATER != current_check_procedure))
//		{
//			set_timer_value(current_check_procedure, 10, NULL);
//			goto_pre_place_cup_disp();
//		}
//		switch(current_check_procedure)
//		{
//			case BRU_ERROR_CHECK_WHILE_PRE_SERVING:
//				bru_Pinch_Valve_Close();
//				break;
//			case BRU_ERROR_CHECK_WHILE_REMOVE_CUP:
//				bru_Pinch_Valve_Open();
//				bru_Pump_Raw_Set(0);
//				goto_brewing_washing_5_disp();
//				set_timer_value(BRU_ERROR_CHECK_WHILE_ENJOY, 200, NULL);
//				break;
//			case BRU_ERROR_CHECK_WHILE_PRE_WASHING:
//				move_cup_flag = 1;
//				set_timer_value(current_check_procedure, 1, NULL);
//				break;
//			case BRU_ERROR_CHECK_WHILE_SERVING_HAVE_WATER:
//				bru_Serving_Chamber_have_Water_Close();
//				set_timer_value(current_check_procedure, 1, NULL);
//				break;
//			case BRU_ERROR_CHECK_WHILE_CANCEL_HAVE_WATER:
//				bru_Cancel_Chamber_have_Water_Close();
//				set_timer_value(current_check_procedure, 1, NULL);
//				break;
//			case BRU_ERROR_CHECK_WHILE_ENJOY_HAVE_WATER:
//				bru_Enjoy_Chamber_have_Water_Close();
//				set_timer_value(current_check_procedure, 1, NULL);
//				break;
//		}
//	}
//	else 
//	{
//		DB_Printf("place cup\r\n");
//		// placed cup
//		switch(current_check_procedure)
//		{
//			case BRU_ERROR_CHECK_WHILE_SELF_TESTING:
//				bru_NTC_Start_Temperature_TOP_Sampling();
//				break;
//			case BRU_ERROR_CHECK_WHILE_PRE_BREWING:
//				DB_Printf("### STEP2: place cup\r\n"); 	
//				bru_NTC_Start_Chamber_Get_Position_Sampling();
//				break;
//			case BRU_ERROR_CHECK_WHILE_PRE_SERVING:
//				current_check_procedure = BRU_ERROR_CHECK_NONE;
//				bru_Pinch_Valve_Open();
//				goto_brewing_serving_5_disp();
//				break;
//			case BRU_ERROR_CHECK_WHILE_CANCELING_BREWING:
//				bru_Pinch_Valve_Open();
//				current_check_procedure = BRU_ERROR_CHECK_NONE;
//				goto_brewing_steeping_cancel_5_disp();
//				bru_Menu_Check_Procedure(BRU_ERROR_CHECK_WHILE_CANCEL_HAVE_WATER);
//				break;
//			case BRU_ERROR_CHECK_WHILE_REMOVE_CUP:
//				set_timer_value(current_check_procedure, 10, NULL);
//				break;
//			case BRU_ERROR_CHECK_WHILE_PRE_WASHING:
//				if(move_cup_flag == 0)	
//				{
//					set_timer_value(current_check_procedure, 10, NULL);
//					break;
//				}
//				move_cup_flag = 0;
//				bru_Pinch_Valve_Open();
//				bru_Pump_Raw_Set(0);
//				goto_brewing_washing_5_disp();
//				set_timer_value(BRU_ERROR_CHECK_WHILE_ENJOY, 200, NULL);
//				bru_Menu_Check_Procedure(BRU_ERROR_CHECK_WHILE_ENJOY_HAVE_WATER);
//				break;
//			case BRU_ERROR_CHECK_WHILE_HOT_WATER_DISPANCER:
//				bru_Pinch_Valve_Open();
//				current_check_procedure = BRU_ERROR_CHECK_NONE;
//				bru_Menu_Check_Procedure(BRU_ERROR_CHECK_WHILE_SERVING_HAVE_WATER);
//				break;
//			case BRU_ERROR_CHECK_WHILE_SERVING_HAVE_WATER:
//				bru_Serving_Chamber_have_Water_Open();
//				set_timer_value(current_check_procedure, 1, NULL);
//				break;
//			case BRU_ERROR_CHECK_WHILE_CANCEL_HAVE_WATER:
//				bru_Cancel_Chamber_have_Water_Open();
//				set_timer_value(current_check_procedure, 1, NULL);
//				break;
//			case BRU_ERROR_CHECK_WHILE_ENJOY_HAVE_WATER:
//				bru_Enjoy_Chamber_have_Water_Open();
//				set_timer_value(current_check_procedure, 1, NULL);
//				break;
//			default:
//				break;
//		}
//	}
//}

//void get_lid_position(void *data)
//{
//	bru_UI_Interface_Handle hUserInterface = get_ui_interface();
//	if(*((int16_t *)(data)) < 100)
//	{
//		set_timer_value(current_check_procedure, 10, NULL);
//		goto_pre_close_lid_disp();
//		switch(current_check_procedure)
//		{
//			case BRU_ERROR_CHECK_WHILE_FILLING_UP:
//				bru_Brewing_Process_Pause();
//				break;
//			default:
//				break;
//		}
//	}else if(*((int16_t *)(data)) >= 3700){
//		switch(current_check_procedure)
//		{
//			case BRU_ERROR_CHECK_WHILE_PRE_BREWING:
//				DB_Printf("### STEP3: Start Filling Up\r\n"); 
//				current_check_procedure = BRU_ERROR_CHECK_NONE;
//				bru_NTC_Start_Chamber_Get_Position_Sampling();
//				bru_Brewing_Process_Start(hUserInterface->presetsArr[hUserInterface->iPresetCurrentIndex].waterAmount,\
//																	hUserInterface->presetsArr[hUserInterface->iPresetCurrentIndex].temperature);
//				goto_filling_up_5_disp();
//				bru_Menu_Check_Procedure(BRU_ERROR_CHECK_WHILE_FILLING_UP);
//				break;
//			case BRU_ERROR_CHECK_WHILE_FILLING_UP:
//				if(hUserInterface->bBrewingProcessFlag)                        
//				{
//					set_timer_value(current_check_procedure, 10, NULL);
//					
//					if(!hUserInterface->bBrewingStartFlag)
//					{
//						goto_filling_up_5_disp();
//						bru_Brewing_Process_Resume();
//					}
//				}
//				break;
//			case BRU_ERROR_CHECK_WHILE_REMOVE_CUP:
//				bru_NTC_Start_Check_Cup_Sampling();
//				break;
//			case BRU_ERROR_CHECK_WHILE_PRE_WASHING:
//				bru_NTC_Start_Check_Cup_Sampling();
//				break;
//			default:
//				break;
//		}
//	}
//	DB_Printf("get_lid_position: *data = %d\r\n", *((int16_t *)(data)));
//}

//void get_brewing_chamber(void *data)
//{
//	if(*((int16_t *)(data)) >= 3700)
//	{
//		set_timer_value(current_check_procedure, 10, NULL);
//		goto_pre_place_chamber_disp();
//	}else if(*((int16_t *)(data)) < 100){
//		bru_NTC_Start_Lid_Get_Position_Sampling();
//	}
//	DB_Printf("get_brewing_chamber: *data = %d\r\n", *((int16_t *)(data)));
//}

void bru_NTC_Start_Temperature_BOT_Sampling(void)
{
	start_multiplex_messurement(MULT_NTC_BOTTOM);
}

void bru_NTC_Start_Temperature_TOP_Sampling(void)
{
	start_multiplex_messurement(MULT_NTC_TOP);
}

void bru_NTC_Start_Temperature_PROT_Sampling(void)
{
	start_multiplex_messurement(MULT_NTC_PROTECTION);
}

void bru_NTC_Start_Check_Cup_Sampling(void)
{
	start_multiplex_messurement(MULT_IR_SENSOR);
}

void bru_NTC_Start_Lid_Get_Position_Sampling(void)
{
	start_multiplex_messurement(MULT_LID_POSITION);
}

void bru_NTC_Start_Chamber_Get_Position_Sampling(void)
{
	start_multiplex_messurement(MULT_BREWING_CHAMBER);
}

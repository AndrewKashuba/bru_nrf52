/*************************************************
	Copyright ? ShenZhen Chileaf Electronic Co., Ltd. 2020-2023. All rights reserved. 
	@file		bru_Multiplexer.H
	
	@version	V1.0
	@date			2021-07-05
	@author		WWang
	@brief		establish

*************************************************/

#ifndef __BRU_MULTIPLEXER_CKECK_H__
#define __BRU_MULTIPLEXER_CKECK_H__
/***********************************************************************
*           Basic Header Files  
************************************************************************
*/
#include "stdint.h"
#include "stdbool.h"
/***********************************************************************
*           Global Macro Define  
************************************************************************
*/

/***********************************************************************
*           Global Type Define 
************************************************************************
*/

/***********************************************************************
*           Global Variable Declarations   
************************************************************************
*/

/***********************************************************************
*           Global Function Declarations 
************************************************************************
*/
extern void bru_NTC_Start_Temperature_BOT_Sampling(void);
extern void bru_NTC_Start_Temperature_TOP_Sampling(void);
extern void bru_NTC_Start_Temperature_PROT_Sampling(void);
extern void bru_NTC_Start_Check_Cup_Sampling(void);
extern void bru_NTC_Start_Lid_Get_Position_Sampling(void);
extern void bru_NTC_Start_Chamber_Get_Position_Sampling(void);
extern float calc_temperature_value(void *data);
/***********************************************************************
*          end
************************************************************************
*/
#endif /* #ifndef __BRU_MULTIPLEXER_CKECK_H__ */

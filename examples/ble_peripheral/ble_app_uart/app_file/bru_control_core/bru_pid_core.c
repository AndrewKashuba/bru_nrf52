#include "bru_pid_core.h"
#include "target_command.h"
#include "BSP_ADC.h"
#include "bru_Multiplexer_Check.h"


#define PID_TIMER_ID       (26345)
#define PID_TIMER_VALUE    (TIMER_CALL_HZ/20)

static pos_pid_t pos_pid = {0};

static bool pid_sampling_flag = false;


/**
 * position PID realize
 * U(k) = Kp*(E(k)+Ki*��E(j)+Kd*[E(k)-E(k-1)]) 
 *
 * @param set_temp		temprature set by user
 * @param actual_temp	temprature realtime monitor
 *
 * @return the calculate pulse-width
 */
int32_t pos_pid_realize(uint32_t set_temp, float actual_temp)
{
//	pos_pid_set_param(pos_pid, set_temp);
	pos_pid.set_temp 	= set_temp;
	pos_pid.actual_temp = actual_temp;
	pos_pid.err 		= pos_pid.set_temp * 1.0  - pos_pid.actual_temp;
	pos_pid.integral	+= pos_pid.err;
	pos_pid.pw 			= (int32_t)(((pos_pid.kp * pos_pid.err \
						+ pos_pid.ki * pos_pid.integral \
						+ pos_pid.kd * (pos_pid.err - pos_pid.err_last)) * 10));
	pos_pid.err_last = pos_pid.err;
	
	if(pos_pid.pw > 690)
	{
		pos_pid.pw = 690;
	}
	else if(pos_pid.pw < 0)
	{
		pos_pid.pw = 0;
	}

	return pos_pid.pw;
}


/**
* @brieaf PID control handler every 50ms call it
*/
static void pid_control_timer_handler(void *p)
{	
	  TIMER_INFO_T *timer = p;
	  timer->timer_value = PID_TIMER_VALUE;
	// TODO this to add code 
	
	bru_NTC_Start_Temperature_TOP_Sampling();
	
}

/**
* @brieaf start pid adjuctment
*/
void start_pid_adjust(void)
{	
	set_timer_value(PID_TIMER_ID,1,NULL);
	pid_sampling_flag = true;
}

/**
* @brieaf stop pid adjuctment
*/
void stop_pid_adjust(void)
{
	set_timer_value(PID_TIMER_ID,0,NULL);
	pid_sampling_flag = false;	
}

/**
* @brieaf get pid work status
*/
bool get_pid_status(void)
{
	return pid_sampling_flag;
}

/**
* @brieaf config pid params
*/
//void bru_pid_params(float waterTemperatureRequired)
//{
//	pos_pid.err			=	0.0;
//	pos_pid.err_last	= 	0.0;
//	pos_pid.integral	=  0.0;
//	
//	if(waterTemperatureRequired > 97.5)
//	{
//		pos_pid.kp			=	  0.8f;
//		pos_pid.ki			= 	0.022f;
//		pos_pid.kd			= 	0.001f;
//	}else if(waterTemperatureRequired > 92.5)
//	{
//		pos_pid.kp			=	  0.92f;
//		pos_pid.ki			= 	0.015f;
//		pos_pid.kd			= 	0.000f;
//	}else if(waterTemperatureRequired > 87.5)
//	{
//		pos_pid.kp			=	  1.02f;
//		pos_pid.ki			= 	0.018f;
//		pos_pid.kd			= 	0.000f;
//	}else if(waterTemperatureRequired > 82.5)
//	{
//		pos_pid.kp			=	  1.05f;
//		pos_pid.ki			= 	0.018f;
//		pos_pid.kd			= 	0.005f;
//	}else if(waterTemperatureRequired > 77.5)
//	{
//		pos_pid.kp			=	  1.10f;
//		pos_pid.ki			= 	0.022f;
//		pos_pid.kd			= 	0.02f;
//	}else if(waterTemperatureRequired > 72.5)
//	{
//		pos_pid.kp			=	  1.20f;
//		pos_pid.ki			= 	0.022f;
//		pos_pid.kd			= 	0.005f;
//	}else if(waterTemperatureRequired > 67.5)
//	{
//		pos_pid.kp			=	  1.30f;
//		pos_pid.ki			= 	0.02f;
//		pos_pid.kd			= 	0.03f;
//	}else if(waterTemperatureRequired > 62.5)
//	{
//		pos_pid.kp			=	  1.60f;
//		pos_pid.ki			= 	0.15f;
//		pos_pid.kd			= 	0.02f;
//	}else
//	{
//		pos_pid.kp			=	  1.60f;
//		pos_pid.ki			= 	0.15f;
//		pos_pid.kd			= 	0.02f;
//	}
//}
void bru_pid_params(float waterTemperatureRequired)
{
	pos_pid.err			=	0.0;
	pos_pid.err_last	= 	0.0;
	pos_pid.integral	=  0.0;
	
	if(waterTemperatureRequired > 97.5)
	{
		pos_pid.kp			=	  0.7f;
		pos_pid.ki			= 	0.0195f;
		pos_pid.kd			= 	8.000f;
	}else if(waterTemperatureRequired > 92.5)
	{
		pos_pid.kp			=	  0.72f;
		pos_pid.ki			= 	0.024f;
		pos_pid.kd			= 	5.000f;
	}else if(waterTemperatureRequired > 87.5)
	{
		pos_pid.kp			=	  0.77f;
		pos_pid.ki			= 	0.021f;
		pos_pid.kd			= 	5.000f;
	}else if(waterTemperatureRequired > 82.5)
	{
		pos_pid.kp			=	  0.78f;
		pos_pid.ki			= 	0.021f;
		pos_pid.kd			= 	5.00f;
	}else if(waterTemperatureRequired > 77.5)
	{
		pos_pid.kp			=	  0.88f;
		pos_pid.ki			= 	0.024f;
		pos_pid.kd			= 	5.00f;
	}else if(waterTemperatureRequired > 72.5)
	{
		pos_pid.kp			=	  0.90f;
		pos_pid.ki			= 	0.028f;
		pos_pid.kd			= 	8.00f;
	}else if(waterTemperatureRequired > 67.5)
	{
		pos_pid.kp			=	  0.95f;
		pos_pid.ki			= 	0.04f;
		pos_pid.kd			= 	8.00f;
	}else if(waterTemperatureRequired > 62.5)
	{
		pos_pid.kp			=	  1.00f;
		pos_pid.ki			= 	0.055f;
		pos_pid.kd			= 	10.00f;
	}else if(waterTemperatureRequired > 57.5)
	{
		pos_pid.kp			=	  1.00f;
		pos_pid.ki			= 	0.07f;
		pos_pid.kd			= 	15.00f;
	}else
	{
		pos_pid.kp			=	  1.00f;
		pos_pid.ki			= 	0.07f;
		pos_pid.kd			= 	15.00f;
	}
}

/**
* @brieaf PID init
*/
void bru_PID_Init(void)
{
	register_timer(PID_TIMER_ID,pid_control_timer_handler);
}

float Calculate_Current_Water_Volume(float pidFeedback)
{
	if(pidFeedback > 725)
	{
		return 50/363.90;
	}else if(pidFeedback > 675)
	{
		return 50/247.52;
	}else if(pidFeedback > 625)
	{
		return 50/188.84;
	}else if(pidFeedback > 575)
	{
		return 50/153.24;
	}else if(pidFeedback > 525)
	{
		return 50/125.30;
	}else if(pidFeedback > 475)
	{
		return 50/113.90;
	}else if(pidFeedback > 425)
	{
		return 50/98.76;
	}else if(pidFeedback > 375)
	{
		return 50/87.94;
	}else if(pidFeedback > 325)
	{
		return 50/80.0;
	}else if(pidFeedback > 275)
	{
		return 50/74.70;
	}else if(pidFeedback > 225)
	{
		return 50/69.2;
	}else if(pidFeedback > 175)
	{
		return 50/64.26;
	}else if(pidFeedback > 125)
	{
		return 50/60.20;
	}else if(pidFeedback > 75)
	{
		return 50/57.36;
	}else if(pidFeedback > 25)
	{
		return 50/54.00;
	}else 
	{
		return 50/50.34;
	}
}
//float Calculate_Current_Water_Volume(float pidFeedback)
//{
//	if(pidFeedback > 725)
//	{
//		return 50/363.90;
//	}else if(pidFeedback > 675)
//	{
//		return 50/257.52;
//	}else if(pidFeedback > 625)
//	{
//		return 50/198.84;
//	}else if(pidFeedback > 575)
//	{
//		return 50/163.24;
//	}else if(pidFeedback > 525)
//	{
//		return 50/133.30;
//	}else if(pidFeedback > 475)
//	{
//		return 50/118.90;
//	}else if(pidFeedback > 425)
//	{
//		return 50/103.76;
//	}else if(pidFeedback > 375)
//	{
//		return 50/92.94;
//	}else if(pidFeedback > 325)
//	{
//		return 50/85.0;
//	}else if(pidFeedback > 275)
//	{
//		return 50/78.70;
//	}else if(pidFeedback > 225)
//	{
//		return 50/73.2;
//	}else if(pidFeedback > 175)
//	{
//		return 50/68.26;
//	}else if(pidFeedback > 125)
//	{
//		return 50/64.20;
//	}else if(pidFeedback > 75)
//	{
//		return 50/60.36;
//	}else if(pidFeedback > 25)
//	{
//		return 50/57.00;
//	}else 
//	{
//		return 50/52.34;
//	}
//}
//float Calculate_Current_Water_Volume(float pidFeedback)
//{
//	if(pidFeedback > 725)
//	{
//		return 50/358.90;
//	}else if(pidFeedback > 675)
//	{
//		return 50/252.52;
//	}else if(pidFeedback > 625)
//	{
//		return 50/193.84;
//	}else if(pidFeedback > 575)
//	{
//		return 50/158.24;
//	}else if(pidFeedback > 525)
//	{
//		return 50/128.30;
//	}else if(pidFeedback > 475)
//	{
//		return 50/113.90;
//	}else if(pidFeedback > 425)
//	{
//		return 50/97.76;
//	}else if(pidFeedback > 375)
//	{
//		return 50/84.94;
//	}else if(pidFeedback > 325)
//	{
//		return 50/77.0;
//	}else if(pidFeedback > 275)
//	{
//		return 50/72.70;
//	}else if(pidFeedback > 225)
//	{
//		return 50/67.2;
//	}else if(pidFeedback > 175)
//	{
//		return 50/60.26;
//	}else if(pidFeedback > 125)
//	{
//		return 50/58.20;
//	}else if(pidFeedback > 75)
//	{
//		return 50/55.36;
//	}else if(pidFeedback > 25)
//	{
//		return 50/52.00;
//	}else 
//	{
//		return 50/47.34;
//	}
//}
//float Calculate_Current_Water_Volume(float pidFeedback)
//{
//	if(pidFeedback > 725)
//	{
//		return 50/245.58;
//	}else if(pidFeedback > 675)
//	{
//		return 50/183.19;
//	}else if(pidFeedback > 625)
//	{
//		return 50/148.65;
//	}else if(pidFeedback > 575)
//	{
//		return 50/124.91;
//	}else if(pidFeedback > 525)
//	{
//		return 50/108.54;
//	}else if(pidFeedback > 475)
//	{
//		return 50/96.78;
//	}else if(pidFeedback > 425)
//	{
//		return 50/86.12;
//	}else if(pidFeedback > 375)
//	{
//		return 50/78.58;
//	}else if(pidFeedback > 325)
//	{
//		return 50/71.41;
//	}else if(pidFeedback > 275)
//	{
//		return 50/66.52;
//	}else if(pidFeedback > 225)
//	{
//		return 50/61.51;
//	}else if(pidFeedback > 175)
//	{
//		return 50/57.59;
//	}else if(pidFeedback > 125)
//	{
//		return 50/53.37;
//	}else if(pidFeedback > 75)
//	{
//		return 50/48.57;
//	}else if(pidFeedback > 25)
//	{
//		return 50/47.47;
//	}else 
//	{
//		return 50/46.14;
//	}
//}

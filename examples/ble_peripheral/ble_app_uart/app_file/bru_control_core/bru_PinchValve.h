/*
 * 	bru_PinchValve.h
 *	PINCH VALVE
 *
 *  Created on: Dec 6, 2021
 *      Author: Andrew Kashuba
 */

#ifndef __BRU_PINCH_VALVE_H__
#define __BRU_PINCH_VALVE_H__

/***********************************************************************
*           Basic Header Files  
*************************************************************************/
#include "stdint.h"
#include "stdbool.h"

/***********************************************************************
*           Global Macro Define  
*************************************************************************/

/***********************************************************************
*           Global Type Define  
*************************************************************************/
typedef enum
{
    PINCH_VALVE_TO_OPEN   = 0,
    PINCH_VALVE_TO_CLOSE  = 1,
		PINCH_VALVE_TO_SLEEP  = 2,
		PINCH_VALVE_TO_BRAKE	= 3
} Bru_Pinch_Valve_Direction_Type;

typedef enum
{
    PV_ERROR_NONE = 0,
		PV_ERROR_10 = 10,										// PV motor don't connected
		PV_ERROR_11 = 11,										// PV swaped Pos_Lim_Switches
		PV_ERROR_12 = 12,										// destroed reductor of PV	
		PV_ERROR_14 = 14,										// check Open Pos_Lim_Switch
		PV_ERROR_15 = 15,										// check Close Pos_Lim_Switch OR current limit level
		PV_ERROR_16 = 16										// PV check nozzle (Close Switch on)
} bru_PV_Error_Code;

typedef enum
{
	Sleep,
	ToOpen,
	ToClose
} PVMotorStatus;

typedef enum
{
	Unknown,
	Open,
	Close,
	Failed
} PVUnitStatus;

typedef struct 
{
	PVMotorStatus MotorStatus;					// PV Status (Sleep, ToOpen, ToClose)
	char *MotorStsInfo;
	PVUnitStatus UnitStatus;						// PV Status (Unknown, Open, Close, Failed)
	char *UnitStsInfo;
	bru_PV_Error_Code Error;
	char *ErrInfo;
	int16_t	Current;										// PV_MotorCurrent (analog)	
	int16_t	MaxCurrent;
} bru_PV_unit;

/***********************************************************************
*           Global Variable Declarations   
*************************************************************************/

/***********************************************************************
*           Global Function Declarations   
*************************************************************************/
extern void bru_Pinch_Valve_Init(void);
extern void bru_PV_Loop(void);
extern void Pinch_Valve(PVMotorStatus);

extern void bru_Pinch_Valve_Set_Direction(Bru_Pinch_Valve_Direction_Type);
extern bool bru_Pinch_Valve_Is_Open(void);
extern bool bru_Pinch_Valve_Is_Close(void);

#endif /* #ifndef __BRU_PINCH_VALVE_H__ */

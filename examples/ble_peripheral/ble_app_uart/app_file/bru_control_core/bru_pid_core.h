#ifndef __BRU_PID_CORE_H
#define __BRU_PID_CORE_H

#include "bru_Pump.h"
#ifdef __cplusplus
extern "C" {
#endif


/**
* @brieaf PID control info.
*/ 
typedef struct position_pid
{
	uint32_t set_temp;  	//设定温度值
	float actual_temp;	//实际温度值
	float err; 			//偏差值
	float err_last; 	//上一个偏差值
	float kp, ki, kd;	//比例、积分、微分系数
	float integral;		//积分值
	int32_t pw;		//pwm`s pulse width
}pos_pid_t, *p_pos_pid_t;





//void bru_PID_Init(void);
int32_t pos_pid_realize(uint32_t set_temp, float actual_temp);
void start_pid_adjust(void);
void stop_pid_adjust(void);
bool get_pid_status(void);
void bru_pid_params(float waterTemperatureRequired);
float Calculate_Current_Water_Volume(float pidFeedback);

#ifdef __cplusplus
}
#endif


#endif


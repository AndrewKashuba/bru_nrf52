/*************************************************
	Copyright ? ShenZhen Chileaf Electronic Co., Ltd. 2020-2023. All rights reserved. 
	@file		bru_BrewingProcess.H
	
	@version	V1.0
	@date			2021-07-05
	@author		WWang
	@brief		establish

*************************************************/

#ifndef __BRU_BREWING_PROCESS_H__
#define __BRU_BREWING_PROCESS_H__
/***********************************************************************
*           Basic Header Files 
************************************************************************
*/
#include "stdint.h"
#include "stdbool.h"
/***********************************************************************
*           Global Macro Define 
************************************************************************
*/
#define BRU_PID_MANUAL       0
#define BRU_PID_AUTOMATIC    1
#define BRU_PID_DIRECT       0
#define BRU_PID_REVERSE      1
#define BRU_PID_P_ON_M       0
#define BRU_PID_P_ON_E       1
/***********************************************************************
*           Global Type Define 
************************************************************************
*/

/***********************************************************************
*           Global Variable Declarations  
************************************************************************
*/
extern uint8_t g_start_preheat;
/***********************************************************************
*           Global Function Declarations   
************************************************************************
*/


/**
 * @brief Start heating.
 *
 * @param[in]	volume 					water Volume Required
 * @param[in]	temperature 		water Temperature Required
 */
extern void bru_Brewing_Process_Start(float volume, float temperature);

/**
 * @brief Resume heating.
 */
extern void bru_Brewing_Process_Resume(void);

/**
 * @brief Pause heating
 */
extern void bru_Brewing_Process_Pause(void);

/**
 * @brief Stop heating.
 */
extern void bru_Brewing_Process_Stop(void);

extern void bru_Brewing_Process_Init(void);
extern float getWaterVolume(void);
extern void start_preheat_time(uint16_t time_ms);
extern float get_waterTemperatureRequired(void);
/***********************************************************************
*          end
************************************************************************
*/
#endif /* #ifndef __BRU_BREWING_PROCESS_H__ */

/*************************************************
	Copyright ? ShenZhen Chileaf Electronic Co., Ltd. 2020-2023. All rights reserved. 
	@file		bru_Multiplexer.H
	
	@version	V1.0
	@date			2021-07-05
	@author		WWang
	@brief		establish

*************************************************/

#ifndef __BRU_MENU_CKECK_H__
#define __BRU_MENU_CKECK_H__
/***********************************************************************
*           Basic Header Files  
************************************************************************
*/
#include "stdint.h"
#include "stdbool.h"
#include "bru_BrewingProcess.h"
/***********************************************************************
*           Global Macro Define  
************************************************************************
*/
#define BRU_ERROR_CHECK_NONE                                         (0x0)
#define BRU_ERROR_CHECK_WHILE_SELF_TESTING                           (0x1)
#define BRU_ERROR_CHECK_WHILE_PRE_BREWING                            (0x2)
#define BRU_ERROR_CHECK_WHILE_FILLING_UP                             (0x3)
#define BRU_ERROR_CHECK_WHILE_PRE_SERVING                            (0x4)
#define BRU_ERROR_CHECK_WHILE_REMOVE_CUP                             (0x5)
#define BRU_ERROR_CHECK_WHILE_ENJOY	                                 (0x6)
#define BRU_ERROR_CHECK_WHILE_CANCELING_BREWING                      (0x7)
#define BRU_ERROR_CHECK_WHILE_PRE_WASHING                            (0x8)
#define BRU_ERROR_CHECK_WHILE_HOT_WATER_DISPANCER                		 (0x9)
#define BRU_ERROR_CHECK_WHILE_SERVING_HAVE_WATER                     (0x10)
#define BRU_ERROR_CHECK_WHILE_CANCEL_HAVE_WATER                      (0x11)
#define BRU_ERROR_CHECK_WHILE_ENJOY_HAVE_WATER                       (0x12)

#define BRU_CHECK_SERVING_CHAMBER_HAVE_WATER_ID											 (0x100)
/***********************************************************************
*           Global Type Define 
************************************************************************
*/

/***********************************************************************
*           Global Variable Declarations   
************************************************************************
*/
extern uint8_t current_check_procedure;
/***********************************************************************
*           Global Function Declarations 
************************************************************************
*/
extern void bru_Menu_Check_Init(void);
extern void bru_Menu_Check_Procedure(uint8_t procedure);

extern uint8_t setCheckTimerValue(void);

extern void bru_Serving_Chamber_have_Water_Open(void);
extern void bru_Serving_Chamber_have_Water_Close(void);
extern void bru_Serving_Chamber_have_Water_Btn_Stop(void);

extern void bru_Cancel_Chamber_have_Water_Close(void);
extern void bru_Cancel_Chamber_have_Water_Open(void);
extern void bru_Cancel_Chamber_have_Water_Check_Over(void);

extern void bru_Enjoy_Chamber_have_Water_Open(void);
extern void bru_Enjoy_Chamber_have_Water_Close(void);

extern void bru_Cancel_Washing_Evt(void);
/***********************************************************************
*          end
************************************************************************
*/
#endif /* #ifndef __BRU_MENU_CKECK_H__ */

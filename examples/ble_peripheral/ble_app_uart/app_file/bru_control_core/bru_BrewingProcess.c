/*************************************************
	Copyright ? ShenZhen Chileaf Electronic Co., Ltd. 2020-2023. All rights reserved. 
	@file		bru_BrewingProcess.c
	
	@version	V1.0
	@date			2021-07-05
	@author		WWang
	@brief		establish

*************************************************/

/***********************************************************************
*           Include Files 
************************************************************************
*/
#include "bru_BrewingProcess.h"
#include "bru_Pump.h"
#include "bru_Relay.h"
#include "app_timer.h"
#include "bru_data.h"
#include "BSP_ADC.h"
#include "bru_Multiplexer_Check.h"
#include "bru_Menu_Check.h"
#include "BSP_menu_info.h"
#include "bru_pid_core.h"
#include "dbprintf.h"

#define NRF_LOG_MODULE_NAME BrewingProcess

#define NRF_LOG_LEVEL       3
#define NRF_LOG_INFO_COLOR  3

#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();

/***********************************************************************
*           Macro Define 
************************************************************************
*/
/*************** timer *****************/
#define TICK_PID_INTERVAL   APP_TIMER_TICKS(50)
/***********************************************************************
*           Type Define  
************************************************************************
*/

/***********************************************************************
*           Static Function Declarations   
************************************************************************
*/
static void stop_pump_water_timer_handler(void *p);
static void stop_pump_water_timer(void);
/***********************************************************************
*          	Extern Variable Declarations 
************************************************************************
*/

/***********************************************************************
*          	Extern Function Declarations	
************************************************************************
*/
uint8_t first_start_pump_or_no_water_flag = 1; //1:表示第一次开机或者没有水的时候，需要抽水1s，防止烧坏加热器
/***********************************************************************
*           Global Variable Define  
************************************************************************
*/
uint8_t g_start_preheat = 0;
/***********************************************************************
*           Static Variable Define   
************************************************************************
*/


//Brewing_Process
static float topNtcTemperature = 0.0f;
static float waterTemperatureRequired;
static float waterVolume;
static float waterVolumeRequired;
static float pidFeedback;



/***********************************************************************
*                                   end
************************************************************************
*/


void bru_PID_Algorithm_Handler(float temperature)
{
	if(!get_pid_status()) return;		//Prevent entering PID commissioning when collecting temperature in other places

	bru_UI_Interface_Handle hUserInterface = get_ui_interface();
	
	topNtcTemperature = temperature;
	if(waterTemperatureRequired >= BRU_PRESET_TEMPERATURE_MIN)
	{
		pidFeedback = pos_pid_realize(waterTemperatureRequired,topNtcTemperature);
	}else{
		pidFeedback = 0;
	}
	
	bru_Pump_Raw_Set((int)pidFeedback);
	NRF_LOG_INFO("pidFeedback = %d", (int32_t)pidFeedback);

	if(waterVolume > waterVolumeRequired)
	{
		DB_Printf("### STEP81: Stop Filling up\r\n"); 

		stop_pump_water_timer();
		bru_Relay_OFF();
		stop_pid_adjust();
	
		current_check_procedure = BRU_ERROR_CHECK_NONE;
		hUserInterface->bBrewingStartFlag = false;
		
		if(hUserInterface->iPresetCurrentIndex == BRU_HOT_WATER_DISPENSER_INDEX)
		{
			hUserInterface->bChamberWashingFlag = FALSE;
//			bru_Menu_Check_Procedure(BRU_ERROR_CHECK_WHILE_HOT_WATER_DISPANCER);
			bru_Menu_Check_Procedure(BRU_ERROR_CHECK_WHILE_PRE_SERVING);
		}else{
			goto_brewing_steeping_5_disp();
		}
	}
	
//	waterVolume += Calculate_Current_Water_Volume(pidFeedback);
	waterVolume += Calculate_Current_Water_Volume(pidFeedback);
}

/***********************************************************************
										Brewing_Process
***********************************************************************/

#define PUMP_WATER_START_TIMER_ID         22211
#define PUMP_WATER_STOP_TIMER_ID          22212
#define	START_PREHEAT_TIMER_ID						22213
#define PUMP_WATER_START_TIMER_VALUE      (TIMER_CALL_HZ*1)
#define PUMP_WATER_STOP_TIMER_VALUE       (10)

static void start_pump_water_timer_handler(void *p)
{
	DB_Printf("### STEP7: ???\r\n");  
	bru_Pump_Stop();
	g_start_preheat = 1;
	bru_NTC_Start_Temperature_TOP_Sampling();
}

static void stop_pump_water_timer_handler(void *p)
{
	DB_Printf("### STEP8: Stop by timer\r\n");  
	bru_Pump_Stop();
}

void start_preheat_time_handler(void *p)
{
	start_pid_adjust();
}

void start_preheat_time(uint16_t time_ms)
{
	bru_Relay_ON();
	register_timer(START_PREHEAT_TIMER_ID,start_preheat_time_handler);
	set_timer_value(START_PREHEAT_TIMER_ID, time_ms/10,NULL);
}

void start_pump_water_timer(void)
{
	first_start_pump_or_no_water_flag = 0;
	set_timer_value(PUMP_WATER_START_TIMER_ID,PUMP_WATER_START_TIMER_VALUE,NULL);
}

static void stop_pump_water_timer(void)
{
	set_timer_value(PUMP_WATER_STOP_TIMER_ID,PUMP_WATER_STOP_TIMER_VALUE,NULL);
}

void bru_Brewing_Process_Init(void)
{
	register_timer(PUMP_WATER_START_TIMER_ID,start_pump_water_timer_handler);
	register_timer(PUMP_WATER_STOP_TIMER_ID,stop_pump_water_timer_handler);
}

void bru_Brewing_Process_Start(float volume, float temperature)
{
	bru_UI_Interface_Handle hUserInterface = get_ui_interface();
	
	hUserInterface->bBrewingStartFlag = true;
	hUserInterface->bBrewingProcessFlag = true;
	
	temperature += 1;  //warnning:前面会注入管子部分水量，需要将其中和一下，提高一下温度
	if(temperature > 98.0f) temperature = 98.0f;
	waterTemperatureRequired = temperature;
	waterVolumeRequired = volume;
	
	bru_pid_params(waterTemperatureRequired);
	if(first_start_pump_or_no_water_flag)
	{
		bru_Pump_Start();
		waterVolume = 1000/113.90;//0.0;   //开局先用百分之五十 PWM抽水1s钟
		start_pump_water_timer();
	}
	else
	{
		waterVolume = 0;
		start_pump_water_timer_handler(NULL);
	}
}

void bru_Brewing_Process_Resume(void)
{
	bru_UI_Interface_Handle hUserInterface = get_ui_interface();
	
	if(hUserInterface->bBrewingStartFlag) return;
	hUserInterface->bBrewingStartFlag = true;
	
	if(first_start_pump_or_no_water_flag)
	{
		bru_Pump_Start();
		waterVolume += 1000/118.90;//0.0;   //开局先用百分之五十 PWM抽水1s钟
		start_pump_water_timer();
	}
	else
	{
		start_pump_water_timer_handler(NULL);
	}
	
}

void bru_Brewing_Process_Pause(void)
{
	bru_UI_Interface_Handle hUserInterface = get_ui_interface();
	
	if(!hUserInterface->bBrewingStartFlag) return;
	hUserInterface->bBrewingStartFlag = false;
	
	stop_pump_water_timer();
	bru_Relay_OFF();
	
	stop_pid_adjust();
}

void bru_Brewing_Process_Stop(void)
{
	bru_UI_Interface_Handle hUserInterface = get_ui_interface();
	
	hUserInterface->bBrewingStartFlag = false;
	
	stop_pump_water_timer();
	bru_Relay_OFF();
	
	stop_pid_adjust();
	
	bru_Menu_Check_Procedure(BRU_ERROR_CHECK_WHILE_CANCELING_BREWING);
}

float getWaterVolume(void)
{
	return waterVolume;
}

float get_waterTemperatureRequired(void)
{
	return waterTemperatureRequired;
}


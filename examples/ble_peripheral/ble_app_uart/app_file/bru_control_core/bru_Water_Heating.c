/*
 * %AppCore.c
 * Event, Logic, Task, State Machine, ...
 *
 *  Created on: Dec 01, 2021
 *      Author: Andrew Kashuba
 */

/***********************************************************************
*           Include Files  
***********************************************************************/
#include <stdint.h>
#include <stdbool.h>
#include "dbprintf.h"
#include "bru_Pump.h"
#include "bru_Relay.h"
#include "bru_Context.h"
#include "CtrlUnits.h"

/***********************************************************************
*           Macro Define  
***********************************************************************/

/***********************************************************************
*           Type Define  
***********************************************************************/

/***********************************************************************
*           Static Function Declarations  
***********************************************************************/
static void bru_PID_Initialization(void);
//static void bru_pid_params(float waterTemperatureRequired);

/***********************************************************************
*          	Extern Variable Declarations 
***********************************************************************/
extern bru_Sensor_Status Sensors;

/***********************************************************************
*          	Extern Function Declarations	
***********************************************************************/

/***********************************************************************
*           Global Variable Define  
***********************************************************************/

/***********************************************************************
*           Static Variable Define 
***********************************************************************/
static bool PID_enb = false;
static float waterTemperatureRequired;
static float waterVolumeRequired;
static float waterVolume;

static float Input;
static float Output;
static float Setpoint;
static float outputSum;
static float lastInput;
static float kp;
static float ki;
static float kd;
static float outMin, outMax;
static float AK;

static bool  pOnE = false;
static bool  pOnM = false;
static float pOnEKp;
static float pOnMKp;

typedef struct {
    float                   Kp;
    float                   Ki;
    float                   Kd;
    float                   PID_mode_ratio;
    float                   pidPeriod;
    uint8_t                 pwmMaxLim;
    uint8_t                 pwmMinLim;
} bru_PID_Adj_Param;

static bru_PID_Adj_Param pidParams;


//### Water prepare ############################################################

// Water prepare procedures ****************************************************
void bru_Water_Prepare_Start(float volume, float temperature)
{
	extern bool fFirstCup;
	
  waterTemperatureRequired = temperature;
  waterVolumeRequired = volume;
  waterVolume = 0.0;
	
	// waterVolumeRequired correction
	if(fFirstCup) waterVolumeRequired += 50;
	
	// !!!! temp solution just for 6 Dec 2021
	//40	45		50		55		60		65		70		75		80		85		90		95		100
	//1.2	1.21	1.22	1.24	1.26	1.28	1.3		1.33	1.36	1.42	1.53	1.67	1.84

    if(0 <= temperature && temperature <= 40)
        AK = 1.2;
    else if(40 < temperature && temperature <= 45)
        AK = 1.21;
    else if(45 < temperature && temperature <= 50)
        AK = 1.22;
    else if(50 < temperature && temperature <= 55)
        AK = 1.24;
    else if(55 < temperature && temperature <= 60)
        AK = 1.26;
    else if(60 < temperature && temperature <= 65)
        AK = 1.28;
    else if(65 < temperature && temperature <= 70)
        AK = 1.3;
    else if(70 < temperature && temperature <= 75)
        AK = 1.33;
    else if(75 < temperature && temperature <= 80)
        AK = 1.36;
    else if(80 < temperature && temperature <= 85)
        AK = 1.42;
    else if(85 < temperature && temperature <= 90)
        AK = 1.53;
    else if(90 < temperature && temperature <= 95)
        AK = 1.67;
    else // 95+
        AK = 1.84; 
		
	bru_PID_Initialization();
  bru_Pump_Start();
	PID_enb = true;		
  if(waterTemperatureRequired != BRU_PRESET_TEMPERATURE_MIN) {
		bru_Relay_ON();
	}
}

void bru_Water_Prepare_Pause(void)
{
  bru_Relay_OFF();
	PID_enb = false;
  bru_Pump_Stop();
}

void bru_Water_Prepare_Resume(void)
{
	bru_Pump_Start();
	if(waterTemperatureRequired != BRU_PRESET_TEMPERATURE_MIN) {
		PID_enb = true;		
		bru_Relay_ON();
	}
}

void bru_Water_Prepare_Stop(void)
{
	bru_Relay_OFF();
	PID_enb = false;
	bru_Pump_Stop();
}

// PID Controller --------------------------------------------------------------

float bru_PID_Compute(float currentTemperature, float setTemperature)
{
    float error;
    float dInput;

    Input = currentTemperature;
    Setpoint = setTemperature;

    /*Compute all the working error variables*/
    error = Setpoint - Input;
    dInput = (Input - lastInput);
    outputSum += (ki * error);

    /*Add Proportional on Measurement, if P_ON_M is specified*/
    if(pOnM) outputSum -= pOnMKp * dInput;

    if(outputSum > outMax) outputSum = outMax;
    else if(outputSum < outMin) outputSum = outMin;

    /*Add Proportional on Error, if P_ON_E is specified*/
    if(pOnE) Output = pOnEKp * error;
    else Output = 0;

    /*Compute Rest of PID Output*/
    Output += outputSum - kd * dInput;

    if(Output > outMax) Output = outMax;
    else if(Output < outMin) Output = outMin;

    /*Remember some variables for next time*/
    lastInput = Input;
		
    return Output;
}

void bru_PID_SetOutputLimits(float Min, float Max)
{
    if(Min > Max) return;
    outMin = Min;
    outMax = Max;

    if(Output >= outMax) Output = outMax;
    else if(Output <= outMin) Output = outMin;

    if(outputSum >= outMax) outputSum = outMax;
    else if(outputSum <= outMin) outputSum = outMin;
}

static void bru_PID_Initialization(void)
{
    float SampleTimeInSec;

		pidParams.Kp = 0.80f;
    pidParams.Ki = 0.35f;
    pidParams.Kd = 0.80f;
		//bru_pid_params(waterTemperatureRequired);

		pidParams.pwmMinLim = 25;
    pidParams.pwmMaxLim = 100;
    pidParams.PID_mode_ratio = 0.0;             // if 0 - pOn_M     if 1 - pOn_E
    pidParams.pidPeriod = 250.0;

    pOnE = pidParams.PID_mode_ratio > 0; //some p on error is desired;
    pOnM = pidParams.PID_mode_ratio < 1; //some p on measurement is desired;

    SampleTimeInSec = pidParams.pidPeriod / 1000.0;
		kp = 0 - pidParams.Kp;
    ki = 0 - pidParams.Ki * SampleTimeInSec;
    kd = 0 - pidParams.Kd / SampleTimeInSec;

    pOnEKp = pidParams.PID_mode_ratio * kp;
    pOnMKp = (1 - pidParams.PID_mode_ratio) * kp;

    bru_PID_SetOutputLimits(pidParams.pwmMinLim, pidParams.pwmMaxLim);
}

bool bru_Pid_Step(void)
{
	static float waterVolumeThreshold;
	static float pidFeedback;
	static char buf[32];
	
	if(PID_enb) {
    if(waterTemperatureRequired != BRU_PRESET_TEMPERATURE_MIN)
      pidFeedback = bru_PID_Compute(Sensors.TopTemperature, waterTemperatureRequired);
    else
      pidFeedback = 100.0;

		bru_Pump_Raw_Set((int)pidFeedback);

		waterVolume += (-0.0034 * pidFeedback * pidFeedback + 2.5 * pidFeedback - 4.3377) / (40.0 * AK);
			
    if(0 <= waterVolumeRequired && waterVolumeRequired <= 50)         // 25 ml ->
        waterVolumeThreshold = 5.0;
    else if(50 < waterVolumeRequired && waterVolumeRequired <= 100)   // 75 ml -> 
        waterVolumeThreshold = 7.0;
    else if(100 < waterVolumeRequired && waterVolumeRequired <= 150)  // 125 ml -> 4oz
        waterVolumeThreshold = 10.0;
    else if(150 < waterVolumeRequired && waterVolumeRequired <= 200)  // 175 ml -> 6oz
        waterVolumeThreshold = 20.0;
    else if(200 < waterVolumeRequired && waterVolumeRequired <= 250)  // 225 ml -> 8oz
        waterVolumeThreshold = 22.5;
    else if(250 < waterVolumeRequired && waterVolumeRequired <= 300)  // 275 ml -> 10oz
        waterVolumeThreshold = 25.0;
    else if(300 < waterVolumeRequired && waterVolumeRequired <= 350)  // 325 ml -> 12 oz,
        waterVolumeThreshold = 32.5;
    else if(350 < waterVolumeRequired && waterVolumeRequired <= 400)    // 375 ml -> 14 oz
        waterVolumeThreshold = 35.0;
    else // 400 and more
        waterVolumeThreshold = 35.0;                                    // 450 ml -> 16 oz

		sprintf(buf,"%.1f, ml: %.1f",Sensors.TopTemperature, waterVolume);
		DB_Printf("temp: %s, pid: %02d\n",buf, (int)pidFeedback);	
		
		if((waterVolume > (waterVolumeRequired)) && 
			 (waterVolumeRequired < BRU_PRESET_WATER_AMOUNT_INF_ML)) 
					bru_Relay_OFF();
		
    if((waterVolume > (waterVolumeRequired + waterVolumeThreshold)) && 
			 (waterVolumeRequired < BRU_PRESET_WATER_AMOUNT_INF_ML))
      return true;
	}
	return false;
}

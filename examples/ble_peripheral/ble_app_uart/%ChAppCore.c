// Event, Logic, Task, State Machine, ...

#include <stdint.h>
#include <stdbool.h>
#include "dbprintf.h"
#include "target_command.h"
#include "nrf_pwr_mgmt.h"
#include "app_timer.h"
#include "BSP_menu_info.h"
//#include "pwm_pump_core.h"

//----- static variables -----------------------------------------------------//
bool fChAppCoreActivity;
uint8_t flashId;

//------ Defined Function ------------------------------------------------//
bool ChCoreUnits_Init(void);
static void tick_32hz_timeout_handler(void * p_context);


// ##### FreeRTOS section ######################################################

#include "FreeRTOS.h"
#include "task.h"

TaskHandle_t  ch_core_task_handle;   /**< Reference to LED0 toggling FreeRTOS task. */

void ch_core_task_function (void * pvParameter);

bool ChCoreUnits_Init(void){
	/* Create task for ChCoreUnits with priority set to 1 */
  xTaskCreate(ch_core_task_function, "ChAppCore", configMINIMAL_STACK_SIZE + 200, NULL, 1, &ch_core_task_handle);
	return true;	
}

void ch_core_task_function (void * pvParameter)
{
	// --------------------- Hardware & Firmware Initialize ------------------- //
#if INNER_FLASH_ENABLED
    BSP_InnerFlash_Init();
#else
	  flashId = flash_init();   					// outer flash initial	  
#endif
	
		bru_Menu_Check_Init(); 
	  bru_UI_Init();
		flash_data_init(flashId);
	  
    init_key();      
		backlight_init();  
		
		fChAppCoreActivity = true;
		DB_Printf("Chine Application Core started!\r\n"); 
		
		//-------------------------------- Main Loop -----------------------------//
    for (;;)
    {		
			  while((lpGetMsg = GetMessage()) != NULL) //get message
            OnCmdMsg(lpGetMsg->msg,lpGetMsg->lpMsgEn,lpGetMsg->extraData); //translate and excute message
				
				tick_32hz_timeout_handler(NULL);
				vTaskDelay(10);
				
//      idle_state_handle();   // idle mode
    }		
}

// ##### Bru old AppCore #######################################################

static void tick_32hz_timeout_handler(void * p_context)
{
    UNUSED_PARAMETER(p_context);
	  
	  static uint8_t cnt;
	  static uint8_t tick_32hz_flag = 0;
		if(tick_32hz_flag == 0) {
			tick_32hz_flag = 1;
			PostMessage((void*)&command_32hz_timer_evt,(void*)&tick_table,&tick_32hz_flag);
		}

	  if(++cnt == TIMER_CALL_HZ) {
			cnt = 0;
		  utc_running_seconds();			
		}
}

// ##### Bru AppCore ###########################################################


// === End file  ========================================

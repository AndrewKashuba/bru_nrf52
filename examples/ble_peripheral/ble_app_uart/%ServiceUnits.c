// Memory, DbgMsg, Log, RGBLED, ...

#include <stdint.h>
#include <stdlib.h>
#include <stddef.h>
#include "nrf_gpio.h"
#include "dbprintf.h"
#include "io_controller_c.h"
#include "app_def.h"
#include "main.h"
#include "AppCore.h"
#include "CtrlUnits.h"
#include "ServiceUnits.h"

#include "at_mobile_c.h"

io_controller_c IOexpand;
led_state_t _curLedState;

extern void _APP_InitTimer(uint8_t, uint32_t);


bool ServiceUnits_Init(void){
	extern i2c_controller_c i2cObj;
	
	// ********************** PCA9574 PORT Ouput Setting *************************** //
	IOexpand.Reinit(&i2cObj);
	IOexpand.SetMode(true);      // all pin was set as output.
	IOexpand.TurnOnSystem();
	IOexpand.LED_OFF();
	IOexpand.W_LED_ON();
	
return true;	
}

bool ServiceUnits_Shutdown(void){
	LED_SetState(LED_STATE_OFF);
return true;	
}

// ##### Service Units #########################################################

// === Port Pin Managing =======================================================

void PortCmnd(IOexp_state Mode)
{
		switch (Mode){
			case LED_OFF: IOexpand.LED_OFF(); break;
			case R_LED_ON: IOexpand.R_LED_ON(); break;
			case G_LED_ON: IOexpand.G_LED_ON(); break;
			case B_LED_ON: IOexpand.B_LED_ON(); break;
			case Y_LED_ON: IOexpand.Y_LED_ON(); break;
			case M_LED_ON: IOexpand.M_LED_ON(); break;
			case C_LED_ON: IOexpand.C_LED_ON(); break;
			case W_LED_ON: IOexpand.W_LED_ON(); break;
			case TurnOnSelfHold: IOexpand.TurnOnSystem();	break;
			case TurnOffSelfHold: IOexpand.TurnOffSystem(); break;
			case SetMobilePwrPinOn: IOexpand.SetMobilePwrOnPin(true); break;
			case SetMobilePwrPinOff: IOexpand.SetMobilePwrOnPin(false); break;
			case SetMobileResetPinOn: IOexpand.SetMobileResetPin(true); break;
			case SetMobileResetPinOff: IOexpand.SetMobileResetPin(false); break;
			//default: IOexpand.LED_OFF();			
		}	
}

// === RGB LED =================================================================

// ----------------- LED Indicate Setting -------------------- //
// 8 = 1s, 16 = 2s, 24 = 3s, 32 = 4s, 40 = 5s, 48 = 6s, 56 = 7s, 64 = 8s, 72 = 9s, 80 = 10s 
// 1 tick = LED_NORMAL_TIMEOUT 0.250s

void led_timer_check(uint32_t _tickCount){
	extern battery_state_t _curBatState;
	
	switch (_curLedState) {
		
		case LED_STATE_OFF:	
				IOexpand.LED_OFF();
				break; //LED_STATE_OFF
		
		case LED_STATE_PAIRING_WAIT_NET:
//				_LED_Toggle(LED_RED_PIN);
//				_LED_Toggle(LED_BLUE_PIN);
				break; //LED_STATE_PAIRING_WAIT_NET
		
		case LED_STATE_PAIRING_NET:
				if (_tickCount % 12 == 0){
					IOexpand.LED_OFF();
					IOexpand.R_LED_ON();
				}
				else if (_tickCount % 12 == 1){
					IOexpand.LED_OFF();
					IOexpand.B_LED_ON();
				}
				else if (_tickCount % 12 == 2){
					IOexpand.LED_OFF();
					IOexpand.G_LED_ON();
				}
				break; //LED_STATE_PAIRING_NET
				
		case LED_STATE_UPLOAD:
				if (_tickCount % 160 == 0) {IOexpand.B_LED_ON();}
				else if (_tickCount  % 160 == 8) {IOexpand.LED_OFF();}
				break; //LED_STATE_UPLOAD
		
		case LED_STATE_WORKING:	
//			bat_timer_check();
				break; //LED_STATE_WORKING
				
		case LED_STATE_NO_NET:
				IOexpand.R_LED_ON();
				break; //LED_STATE_NO_NET
	
		case LED_STATE_MA:
				if (_tickCount % 160 == 0) {IOexpand.R_LED_ON();}
				else if (_tickCount % 160 == 2) {IOexpand.LED_OFF();}
				break; //LED_STATE_MA
			
		default: break; //_curLedState
	}
	
	if (_curLedState == LED_STATE_WORKING) {
		
		switch (_curBatState)	{
			case BAT_LOW_STATUS:
					if (_tickCount % 160 == 0) {IOexpand.G_LED_ON();}
					else if (_tickCount % 160 == 2) {IOexpand.R_LED_ON();}
					else if (_tickCount % 160 == 4) {IOexpand.LED_OFF();}
					break; //BAT_LOW_STATUS
				
			case BAT_FULL_CHARGE:
					if (_tickCount % 160 == 0) {IOexpand.G_LED_ON();}
					else if (_tickCount % 160 == 4) {IOexpand.LED_OFF();}
					break; //BAT_FULL_CHARGE
				
			case BAT_MED_CHARGE:
					if (_tickCount % 160 == 0) {IOexpand.G_LED_ON();}
					else if (_tickCount % 160 == 2) {IOexpand.LED_OFF();}
					break; //BAT_MED_CHARGE
				
			default: break; //_curBatState            
		}		
	}	
}

//*****************************************************************************************
/**
  @brief Set LED state
	@param[in] state led blink state
  @return TRUE done success
*/
bool LED_SetState(led_state_t newState){

	IOexpand.LED_OFF();	
	switch (newState){
		case LED_STATE_PAIRING_WAIT_NET:
		
		case LED_STATE_PAIRING_NET:
		
		case LED_STATE_NO_NET:
			IOexpand.R_LED_ON();
			break;		
		
		case LED_STATE_UPLOAD:
			IOexpand.B_LED_ON();
			break;
		
		case LED_STATE_WORKING:
			IOexpand.G_LED_ON();
			break;
		
		case LED_STATE_MA:
			IOexpand.R_LED_ON();
			break;
		
		case LED_STATE_OFF:
			IOexpand.LED_OFF();
			break;		
		
		default: break;
	}
  
  _curLedState = newState;
  
  return true;
};



/*
 * %AppCore.c
 * Event, Logic, Task, State Machine, ...
 *
 *  Created on: Nov 20, 2021
 *      Author: Andrew Kashuba
 */

/***********************************************************************
*           Include Files  
***********************************************************************/
#include <stdint.h>
#include <stdbool.h>
#include "dbprintf.h"
#include "nrf_drv_timer.h"
#include "bru_Context.h"
#include "bru_version.h"
#include "bru_ui_disp.h"
#include "bru_Menu_Check.h"
#include "bru_Pump.h"
#include "bru_PinchValve.h"
#include "bru_Water_Heating.h"
#include "CtrlUnits.h"
#include "BSP_wdt.h"
#include "event_queue_c.h"
#include "BSP_menu_info.h"

/***********************************************************************
*           Macro Define  
***********************************************************************/

/***********************************************************************
*           Type Define  
***********************************************************************/

/***********************************************************************
*           Global Function Declarations  
***********************************************************************/
bool CoreUnits_Init(void);
void CallSubTask(bru_Tasks_List task);
void bru_Context_Next_Task(bru_Tasks_List task);
void bru_Context_Task_Intro(bru_Tasks_List, bru_UI_Window_ID);
void SetupButtons(bru_main_btn, bru_BTN_Label, bru_BTN_Label);
void EncoderClick(void);

/***********************************************************************
*           Static Function Declarations  
***********************************************************************/
static void bru_Context_TaskList_Init(void);
static bru_Context bru_Context_Task(bru_Tasks_List);
static void ReturnSubTask(void);

/***********************************************************************
*          	Extern Variable Declarations 
***********************************************************************/
extern bru_Sensor_Status Sensors;
extern bru_Sensor_EventMap EventMap;

/***********************************************************************
*          	Extern Function Declarations	
***********************************************************************/
extern void GetTaskLogo(bru_Tasks_List);
extern void StartTickGenerator(uint32_t);
extern void AlarmTimerStart(int32_t);
extern void AlarmTimerPause(void);
extern void AlarmTimerResume(void);
extern bool evt_queue_GetEvent(gevent_t*);
extern bool evt_queue_Post(gevent_t* pEvent);

/***********************************************************************
*           Global Variable Define  
***********************************************************************/
bool fAppCoreActivity;
bool fTestMode;
bool fFirstCup = true;
bool fFirstMenuSurf;
bru_Context BruTask;                         // Active Task
bru_Tasks_List LastTask;
_ContextSM ContextSM = Suspend;
event_id_t event;

/***********************************************************************
*           Static Variable Define 
***********************************************************************/
static bru_Context bru_TaskList[TASKS_LIST_END];
static _ContextSM ReturnSM = Suspend;
static bru_Context BruReturnTask; 
static bool BruSubTask = false;
static bru_UI_Window_ID ReturnScreen;
static bru_Sensor_EventMap ReturnSensorEventMap;

const char * msg1 = "T_HomeScreen";
// =============================================================================

// ##### FreeRTOS section ######################################################

#include "FreeRTOS.h"
#include "task.h"

TaskHandle_t  core_task_handle;   /**< Reference to LED0 toggling FreeRTOS task. */

void core_task_function (void * pvParameter);

bool CoreUnits_Init(void){
	/* Create task for AppCoreUnits with priority set to 1 */
  xTaskCreate(core_task_function, "AppCore", configMINIMAL_STACK_SIZE + 200, NULL, 1, &core_task_handle);
	return true;
}

static void core_task_function (void * pvParameter)
{
  UNUSED_PARAMETER(pvParameter);
	extern bool fCtrlUnitsActivity;
	extern bool fBLEUnitsActivity;
	extern bool fChAppCoreActivity;
	
  BSP_WDT_Init();   															// watch dog initial 
  bru_Context_TaskList_Init();
  BruTask = bru_Context_Task(T_Null);
	
	while (!fCtrlUnitsActivity || !fBLEUnitsActivity || !fChAppCoreActivity) {vTaskDelay(10);};
	fAppCoreActivity = true;
	DB_Printf("Bru %s started!\n", BRU_APP_VER);
	
	if(!Sensors.RightButton && !Sensors.EncoderButton) {
		fTestMode = false;
		bru_Context_Task_Intro(T_IntroScreen, UI_WINDOW_HOME_SCREEN);	
		goto_home_disp();
	}
	else 
	{
		fTestMode = true;
		bru_Context_Task_Intro(T_TestScreen_1, UI_WINDOW_HOME_SCREEN);	
	}
	
	//-------------------------------- Main Loop ------------------------------ //
  for (;;)
  {	
		BSP_WDT_Feed();
		
		switch (ContextSM) {
			
			case Initialized:
				ContextSM = Core;
				BruTask.fxnInitTask(BruTask.ArgInit);
				break;
			
			case Core:
				if (BruTask.fxnCoreTask(BruTask.ArgCore)) 
					ContextSM = Finalizing;
				break;
			
			case Finalizing:
				BruTask.fxnFinalTask(BruTask.ArgFin);
				if (BruSubTask) ReturnSubTask();
				break;
			
			default:
				vTaskDelay(10);		
				break;	
		}		
  }
	//-------------------------------- End Main Loop -------------------------- //
}

// ##### AppCore ###############################################################

void PostEvent(event_id_t event_id)
{
	gevent_t evt;
	
	evt.event = event_id;
	evt_queue_Post(&evt);			
}

bool GetEvent(void)
{
	gevent_t evt;
	extern bool evt_queue_GetEvent(gevent_t*);
	
	bool result = evt_queue_GetEvent(&evt);
	event = evt.event;
	return result;
}

void SetButtonMain(bru_main_btn m_btn)
{
	extern void bru_LCD_Redraw_Buttons(void);
	BruTask.MainButton = m_btn;	
	bru_LCD_Redraw_Buttons();
}

void SetupButtons(bru_main_btn m_btn, bru_BTN_Label l_btn, bru_BTN_Label r_btn)
{
	BruTask.MainButton = m_btn;
	BruTask.LeftLabel = l_btn;
	BruTask.RightLabel = r_btn;	
}

void EncoderClick(void)
{
	if(BruTask.MainButton == MAIN_BTN_LEFT || BruTask.MainButton == MAIN_BTN_BOTH)
		PostEvent(EVENT_LEFT_BUTTON_CLICK);
	else if(BruTask.MainButton == MAIN_BTN_RIGHT) 
		PostEvent(EVENT_RIGHT_BUTTON_CLICK);
}

//=== Context Core =============================================================

void UpdateTaskScreen(bru_UI_Window_ID TScreen, bool init)
{
	BruTask.TaskScreen = TScreen;
	bru_LCD_Update_Screen(TScreen, init);
}

void bru_Context_Task_Intro(bru_Tasks_List task, bru_UI_Window_ID Screen_ID)
{
	ReturnScreen = Screen_ID;
	bru_Context_Next_Task(task);
}

static bru_Context bru_Context_Task(bru_Tasks_List task)
{
  for (bru_Tasks_List i = T_Null; i < TASKS_LIST_END; i++)
    if (bru_TaskList[i].TaskName == task)
      return bru_TaskList[i];
  return bru_TaskList[0];
}

void bru_Context_Next_Task(bru_Tasks_List task)
{
	extern void ClearSensorEventMap(void);
	extern char* pTaskLogo;;
	
	LastTask = BruTask.TaskName;
	bru_UI_Window_ID TScreen = BruTask.TaskScreen;
  BruTask = bru_Context_Task(task);
	GetTaskLogo(task);
	DB_Printf("-----> %s <-----\n", pTaskLogo);	
	BruTask.TaskScreen = TScreen;
	SetupButtons(MAIN_BTN_NO, b__, b__);
	ClearSensorEventMap();
	ContextSM = Initialized;
}

void bru_Context_Task_Exit(void)
{	
	UpdateTaskScreen(ReturnScreen, Init);
	BruTask = bru_Context_Task(T_Null);
	ContextSM = Suspend;	
}

void CallSubTask(bru_Tasks_List task)
{
	BruReturnTask = BruTask;
	ReturnSensorEventMap = EventMap;
	ReturnSM = ContextSM;
  BruTask = bru_Context_Task(task);
	BruTask.TaskScreen = BruReturnTask.TaskScreen;
	ContextSM = Initialized;	
	BruSubTask = true;
}

static void ReturnSubTask(void)
{
	UpdateTaskScreen(BruReturnTask.TaskScreen, Init);
	EventMap = ReturnSensorEventMap;
	BruTask = BruReturnTask;
	ContextSM = ReturnSM;
	BruSubTask = false;
}

bru_Tasks_List jmp_to_preset_menu(void)
{
	bru_UI_Interface_Handle hUserInterface = get_ui_interface();

	switch(hUserInterface->iPresetCurrentIndex) {
		case 0: return T_HomeScreen;
		case 1: return T_Preset_1_Menu;	
		case 2: return T_Preset_2_Menu;	
		case 3: return T_Preset_3_Menu;	
		case 4: return T_Dispenser_Menu;
		case 5: return T_Alarm_Menu;
		default: return T_HomeScreen;
	}	
}
	
void msg_unused_event(event_id_t evt)
{
	DB_Printf("WARNING!!! Unused event %2d!\n", (int)evt);
}

//=== SubTasks =================================================================

//--- ST_ADD_WATER -------------------------------------------------------------
static void ADD_WATER_Unit_Init(uint32_t Arg)
{
	UpdateTaskScreen(UI_WINDOW_ERROR_MSG_ADD_WATER, Init);
}

static bool ADD_WATER_Unit_Core(uint32_t Arg)
{
	if (Sensors.Water) DB_Printf("Water Ok!\n"); 
	else vTaskDelay(100);
	return Sensors.Water;
}

//--- ST_PLACE_STEEPING_CHAMBER ------------------------------------------------
static void PLACE_STEEPING_CHAMBER_Unit_Init(uint32_t Arg)
{
	UpdateTaskScreen(UI_WINDOW_ERROR_MSG_PLACE_BREWING_CHAMBER, Init);
}

static bool PLACE_STEEPING_CHAMBER_Unit_Core(uint32_t Arg)
{
	if(Sensors.BrewingChamber) {
		DB_Printf("Steeping chamber ok!\n"); 	
		return true;
	}
	vTaskDelay(100);	
	return false;
}

//--- ST_PLEASE_CUP ------------------------------------------------------------
static void PLEASE_CUP_Unit_Init(uint32_t Arg)
{
	UpdateTaskScreen(UI_WINDOW_ERROR_MSG_PLACE_CUP, Init);
}

static bool PLEASE_CUP_Unit_Core(uint32_t Arg)
{
	vTaskDelay(100);	
	return Sensors.Cup;	
}

//--- ST_REMOVE_CUP ------------------------------------------------------------
static void REMOVE_CUP_Unit_Init(uint32_t Arg)
{
	UpdateTaskScreen(UI_WINDOW_BREWING_STATUS_REMOVE_CUP, Init);
}

static bool REMOVE_CUP_Unit_Core(uint32_t Arg)
{
	vTaskDelay(100);	
	return !Sensors.Cup;	
}

//--- ST_CLOSE_LID -------------------------------------------------------------
static void CLOSE_LID_Unit_Init(uint32_t Arg)
{
	UpdateTaskScreen(UI_WINDOW_ERROR_MSG_CLOSE_LID, Init);
}

static bool CLOSE_LID_Unit_Core(uint32_t Arg)
{
	vTaskDelay(100);	
	return Sensors.Lid;
}

//--- ST_ERROR_xx --------------------------------------------------------------
static void ERROR_xx_Unit_Init(uint32_t Arg)
{
	SetupButtons(MAIN_BTN_LEFT, bEXIT, b__);	
	UpdateTaskScreen(UI_WINDOW_ERROR_MSG_ERROR_XXX, Init);
	EventMap.left_button_enb = true;	
	EventMap.encoder_button_enb = true;
}

static bool ERROR_xx_Unit_Core(uint32_t Arg)
{
	if(GetEvent())
		switch(event)	
		{
			case EVENT_LEFT_BUTTON_CLICK:
			case EVENT_ENCODER_BUTTON_CLICK:
				PostEvent(EVENT_CANCEL);
				return true;
			
			default: msg_unused_event(event);
		}
	return false;
}

//=== Units ====================================================================

//--- T_IntroScreen -------------------------------------------------
static void uIntroScreen_Init(uint32_t Arg)
{
	extern bool fDisplayStartSolution;
	
	vTaskDelay(500); 																					// temp solution!!!
	fDisplayStartSolution = true;															// temp solution!!!
	UpdateTaskScreen(UI_WINDOW_INTRO_SCREEN, Init);
	set_backlight_value(BACKLIGHT_ON_VALUE);
	vTaskDelay(3000);
}

static bool uIntroScreen_Core(uint32_t Arg)
{	
//	bru_UI_Interface_Handle hUserInterface = get_ui_interface();
	
//	if (!Sensors.Water) {																			// Check Water Level
//		CallSubTask(ST_ADD_WATER);
//		return false;
//	}
//	
//	if (!Sensors.TopSensorStatus)	{ 													// Check Top NTC
//		hUserInterface->errorCode = (int) UI_ERROR_CODE_01;
//		CallSubTask(ST_ERROR_xx);
//		return false;
//	}
//	
//	if (!Sensors.BotSensorStatus)	{ 													// Check Bot NTC
//		hUserInterface->errorCode = (int) UI_ERROR_CODE_02;
//		CallSubTask(ST_ERROR_xx);
//		return false;
//	}
	
	if (bru_Pinch_Valve_Is_Open()) return true;								// Initialisation PV
	else {
		if (bru_Pinch_Valve_Is_Close()) {
			if(!Sensors.Cup)
				CallSubTask(ST_PLEASE_CUP);
			else
				Pinch_Valve(ToOpen);
			return true;
		}
		else		
			Pinch_Valve(ToClose);																		
	}	
	return false;
}

static void uIntroScreen_Fin(uint32_t Arg)
{
	bru_Context_Next_Task(T_HomeScreen);
}

//--- T_HomeScreen -------------------------------------------------

static void uHomeScreen_Init(uint32_t Arg)
{
	bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
	hUserInterface->iPresetCurrentIndex = 0;
	SetupButtons(MAIN_BTN_LEFT, bSTART, bMENU);
	UpdateTaskScreen(UI_WINDOW_HOME_SCREEN, Init);
	EventMapSetup(MenuSurfing);
	fFirstMenuSurf = true;
}

static bool uHomeScreen_Core(uint32_t Arg)
{	
	bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
	DISP_FLAG_T *p = get_disp_flag();
	
	if(GetEvent())
		switch(event)	
		{
			case EVENT_ENCODER_TURN_LEFT:
				bru_Context_Next_Task(T_Brewing_Setup_Time);	
				break;
			
			case EVENT_ENCODER_TURN_RIGHT:
				bru_Context_Next_Task(T_Preset_1_Menu);
				break;
			
			case EVENT_LEFT_BUTTON_CLICK:
				bru_Context_Next_Task(T_PreBrewing_Water);			
				break;			

			case EVENT_RIGHT_BUTTON_CLICK:
p->sel_draw_flag_1 = 1;
ContextSM = Suspend;			
hUserInterface->bMenuPresetFlag = 0;
goto_steeping_disp();					
				break;
			
			case EVENT_ENCODER_BUTTON_CLICK:
				EncoderClick();
				break;
			
			default: msg_unused_event(event);			
		}
	else
		vTaskDelay(10);		

	return false;
}

//=== Brewing Setup Menu =======================================================

//--- T_Brewing_Setup_Time -----------------------------------------------------
static void uBrewing_Setup_Time_Init(uint32_t Arg)
{
	if(fFirstMenuSurf)
		SetupButtons(MAIN_BTN_RIGHT, bBACK, bEDIT);
	else 
		SetupButtons(MAIN_BTN_LEFT, bBACK, bEDIT);
	UpdateTaskScreen(UI_WINDOW_BREWING_SETUP_MENU_TIME, Init);
	EventMapSetup(MenuSurfing);
}

static bool uBrewing_Setup_Time_Core(uint32_t Arg)
{
	if(GetEvent())
		switch(event)	
		{
			case EVENT_ENCODER_TURN_LEFT:
				bru_Context_Next_Task(T_Brewing_Setup_Temperature);					
				break;
			
			case EVENT_ENCODER_TURN_RIGHT:
				bru_Context_Next_Task(T_Brewing_Setup_Water);
				fFirstMenuSurf = false;
				break;
			
			case EVENT_LEFT_BUTTON_CLICK:								// BACK
				bru_Context_Next_Task(T_HomeScreen);	
				break;
			
			case EVENT_RIGHT_BUTTON_CLICK:							// EDIT 
				bru_Context_Next_Task(T_Brewing_Setup_Time_Edit);
				break;			

			case EVENT_ENCODER_BUTTON_CLICK:
				EncoderClick();
				break;
			
			default: msg_unused_event(event);	
		}
	else
		vTaskDelay(10);

	return false;
}

//--- T_Brewing_Setup_Temperature ----------------------------------------------
static void uBrewing_Setup_Temp_Init(uint32_t Arg)
{
	if(fFirstMenuSurf)
		SetupButtons(MAIN_BTN_RIGHT, bBACK, bEDIT);
	else 
		SetupButtons(MAIN_BTN_LEFT, bBACK, bEDIT);
	UpdateTaskScreen(UI_WINDOW_BREWING_SETUP_MENU_TEMPERATURE, Init);
	EventMapSetup(MenuSurfing);	
}

static bool uBrewing_Setup_Temp_Core(uint32_t Arg)
{
	if(GetEvent())
		switch(event)	
		{
			case EVENT_ENCODER_TURN_LEFT:
				bru_Context_Next_Task(T_Brewing_Setup_Water);					
				break;
			
			case EVENT_ENCODER_TURN_RIGHT:
				bru_Context_Next_Task(T_Brewing_Setup_Time);
				break;
			
			case EVENT_LEFT_BUTTON_CLICK:								// BACK
				bru_Context_Next_Task(T_HomeScreen);	
				break;
			
			case EVENT_RIGHT_BUTTON_CLICK:							// EDIT 
				bru_Context_Next_Task(T_Brewing_Setup_Temperature_Edit);
				break;			
			
			case EVENT_ENCODER_BUTTON_CLICK:
				EncoderClick();
				break;
			
			default: msg_unused_event(event);	
		}
	else
		vTaskDelay(10);
	
	return false;
}

//--- T_Brewing_Setup_Water ----------------------------------------------------
static void uBrewing_Setup_Water_Init(uint32_t Arg)
{
	if(fFirstMenuSurf)
		SetupButtons(MAIN_BTN_RIGHT, bBACK, bEDIT);
	else 
		SetupButtons(MAIN_BTN_LEFT, bBACK, bEDIT);
	UpdateTaskScreen(UI_WINDOW_BREWING_SETUP_MENU_WATER_AMOUNT, Init);
	EventMapSetup(MenuSurfing);
}

static bool uBrewing_Setup_Water_Core(uint32_t Arg)
{
	if(GetEvent())
		switch(event)	
		{
			case EVENT_ENCODER_TURN_LEFT:
				bru_Context_Next_Task(T_Brewing_Setup_Time);					
				fFirstMenuSurf = false;
				break;
			
			case EVENT_ENCODER_TURN_RIGHT:
				bru_Context_Next_Task(T_Brewing_Setup_Temperature);
				break;
			
			case EVENT_LEFT_BUTTON_CLICK:								// BACK
				bru_Context_Next_Task(T_HomeScreen);	
				break;
			
			case EVENT_RIGHT_BUTTON_CLICK:							// EDIT
				bru_Context_Next_Task(T_Brewing_Setup_Water_Edit);
				break;			

			case EVENT_ENCODER_BUTTON_CLICK:
				EncoderClick();
				break;
			
			default: msg_unused_event(event);	
		}
	else
		vTaskDelay(10);
	
	return false;
}

//--- T_Brewing_Setup_Time_Edit ------------------------------------------------
static void uBrewing_Setup_Time_Edit_Init(uint32_t Arg)
{
	SetupButtons(MAIN_BTN_RIGHT, b__, bSAVE);
	UpdateTaskScreen(UI_WINDOW_BREWING_SETUP_MENU_EDIT_TIME, Init);
	EventMapSetup(MenuSurfing);
}

static bool uBrewing_Setup_Time_Edit_Core(uint32_t Arg)
{
	if(GetEvent())
		switch(event)	
		{
			case EVENT_ENCODER_TURN_LEFT:
				bru_BrewingTime_adj_steepint_time_ENG(ENCODE_LEFT);			
				UpdateTaskScreen(UI_WINDOW_BREWING_SETUP_MENU_EDIT_TIME, Update);
				break;
			
			case EVENT_ENCODER_TURN_RIGHT:
				bru_BrewingTime_adj_steepint_time_ENG(ENCODE_RIGHT);
				UpdateTaskScreen(UI_WINDOW_BREWING_SETUP_MENU_EDIT_TIME, Update);
				break;
			
			case EVENT_RIGHT_BUTTON_CLICK:							// SAVE
				write_record_info_to_flash();
				get_all_preset_info();				
				bru_Context_Next_Task(T_Brewing_Setup_Time);
				break;			

			case EVENT_ENCODER_BUTTON_CLICK:
				EncoderClick();
				break;
			
			default: msg_unused_event(event);	
		}
	else
		vTaskDelay(10);
	
	return false;
}

//--- T_Brewing_Setup_Temperature_Edit -----------------------------------------
static void uBrewing_Setup_Temp_Edit_Init(uint32_t Arg)
{
	SetupButtons(MAIN_BTN_RIGHT, b__, bSAVE);
	UpdateTaskScreen(UI_WINDOW_BREWING_SETUP_MENU_EDIT_TEMPERATURE, Init);
	EventMapSetup(MenuSurfing);
}

static bool uBrewing_Setup_Temp_Edit_Core(uint32_t Arg)
{
	if(GetEvent())
		switch(event)	
		{
			case EVENT_ENCODER_TURN_LEFT:
				bru_BrewingTime_adj_water_temper_ENG(ENCODE_LEFT);
				UpdateTaskScreen(UI_WINDOW_BREWING_SETUP_MENU_EDIT_TEMPERATURE, Update);
				break;
			
			case EVENT_ENCODER_TURN_RIGHT:
				bru_BrewingTime_adj_water_temper_ENG(ENCODE_RIGHT);
				UpdateTaskScreen(UI_WINDOW_BREWING_SETUP_MENU_EDIT_TEMPERATURE, Update);
				break;
			
			case EVENT_RIGHT_BUTTON_CLICK:							// SAVE
				write_record_info_to_flash();
				get_all_preset_info();				
				bru_Context_Next_Task(T_Brewing_Setup_Temperature);
				break;			

			case EVENT_ENCODER_BUTTON_CLICK:
				EncoderClick();
				break;
			
			default: msg_unused_event(event);	
		}
	else
		vTaskDelay(10);
	
	return false;
}

//--- T_Brewing_Setup_WaterAmount_Edit -----------------------------------------
static void uBrewing_Setup_Water_Edit_Init(uint32_t Arg)
{
	SetupButtons(MAIN_BTN_RIGHT, b__, bSAVE);
	UpdateTaskScreen(UI_WINDOW_BREWING_SETUP_MENU_EDIT_WATER_AMOUNT, Init);
	EventMapSetup(MenuSurfing);
}

static bool uBrewing_Setup_Water_Edit_Core(uint32_t Arg)
{
	if(GetEvent())
		switch(event)	
		{
			case EVENT_ENCODER_TURN_LEFT:
				bru_BrewingTime_adj_water_amount_ENG(ENCODE_LEFT);
				UpdateTaskScreen(UI_WINDOW_BREWING_SETUP_MENU_EDIT_WATER_AMOUNT, Update);
				break;
			
			case EVENT_ENCODER_TURN_RIGHT:
				bru_BrewingTime_adj_water_amount_ENG(ENCODE_RIGHT);
				UpdateTaskScreen(UI_WINDOW_BREWING_SETUP_MENU_EDIT_WATER_AMOUNT, Update);
				break;
			
			case EVENT_RIGHT_BUTTON_CLICK:							// SAVE
				write_record_info_to_flash();
				get_all_preset_info();				
				bru_Context_Next_Task(T_Brewing_Setup_Water);
				break;			

			case EVENT_ENCODER_BUTTON_CLICK:
				EncoderClick();
				break;
			
			default: msg_unused_event(event);	
		}
	else
		vTaskDelay(10);
	
	return false;
}

//=== Preset Menu ==============================================================

//--- T_Preset_1_Menu ----------------------------------------------------------
static void uPreset_1_Menu_Init(uint32_t Arg)
{
	bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
	hUserInterface->iPresetCurrentIndex = 1;	
	SetupButtons(MAIN_BTN_LEFT, bSTART, bEDIT);
	UpdateTaskScreen(UI_WINDOW_PRESET_STATUS_MENU, Init);
	EventMapSetup(MenuSurfing);
}

static bool uPreset_1_Menu_Core(uint32_t Arg)
{
	if(GetEvent())
		switch(event)	
		{
			case EVENT_ENCODER_TURN_LEFT:
				bru_Context_Next_Task(T_HomeScreen);					
				break;
			
			case EVENT_ENCODER_TURN_RIGHT:
				bru_Context_Next_Task(T_Preset_2_Menu);
				break;
			
			case EVENT_LEFT_BUTTON_CLICK:								// START
				bru_Context_Next_Task(T_PreBrewing_Water);			
				break;
			
			case EVENT_RIGHT_BUTTON_CLICK:							// EDIT 
				bru_Context_Next_Task(T_Preset_Time_Edit);
				break;			

			case EVENT_ENCODER_BUTTON_CLICK:
				EncoderClick();
				break;
			
			default: msg_unused_event(event);	
		}
	else
		vTaskDelay(10);

	return false;
}

//--- T_Preset_2_Menu ----------------------------------------------------------
static void uPreset_2_Menu_Init(uint32_t Arg)
{
	bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
	hUserInterface->iPresetCurrentIndex = 2;	
	SetupButtons(MAIN_BTN_LEFT, bSTART, bEDIT);
	UpdateTaskScreen(UI_WINDOW_PRESET_STATUS_MENU, Init);
	EventMapSetup(MenuSurfing);
}

static bool uPreset_2_Menu_Core(uint32_t Arg)
{
	if(GetEvent())
		switch(event)	
		{
			case EVENT_ENCODER_TURN_LEFT:
				bru_Context_Next_Task(T_Preset_1_Menu);					
				break;
			
			case EVENT_ENCODER_TURN_RIGHT:
				bru_Context_Next_Task(T_Preset_3_Menu);
				break;
			
			case EVENT_LEFT_BUTTON_CLICK:								// START
				bru_Context_Next_Task(T_PreBrewing_Water);	
				break;
			
			case EVENT_RIGHT_BUTTON_CLICK:							// EDIT 
				bru_Context_Next_Task(T_Preset_Time_Edit);
				break;			

			case EVENT_ENCODER_BUTTON_CLICK:
				EncoderClick();
				break;
			
			default: msg_unused_event(event);	
		}
	else
		vTaskDelay(10);

	return false;
}

//--- T_Preset_3_Menu ----------------------------------------------------------
static void uPreset_3_Menu_Init(uint32_t Arg)
{
	bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
	hUserInterface->iPresetCurrentIndex = 3;	
	SetupButtons(MAIN_BTN_LEFT, bSTART, bEDIT);
	UpdateTaskScreen(UI_WINDOW_PRESET_STATUS_MENU, Init);
	EventMapSetup(MenuSurfing);
}

static bool uPreset_3_Menu_Core(uint32_t Arg)
{
	if(GetEvent())
		switch(event)	
		{
			case EVENT_ENCODER_TURN_LEFT:
				bru_Context_Next_Task(T_Preset_2_Menu);					
				break;
			
			case EVENT_ENCODER_TURN_RIGHT:
				bru_Context_Next_Task(T_Dispenser_Menu);
				break;
			
			case EVENT_LEFT_BUTTON_CLICK:								// START
				bru_Context_Next_Task(T_PreBrewing_Water);			
				break;
			
			case EVENT_RIGHT_BUTTON_CLICK:							// EDIT 
				bru_Context_Next_Task(T_Preset_Time_Edit);
				break;			

			case EVENT_ENCODER_BUTTON_CLICK:
				EncoderClick();
				break;
			
			default: msg_unused_event(event);	
		}
	else
		vTaskDelay(10);

	return false;
}

//--- T_Dispenser_Menu ----------------------------------------------------------
static void uDispenser_Menu_Init(uint32_t Arg)
{
	bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
	hUserInterface->iPresetCurrentIndex = 4;	
	SetupButtons(MAIN_BTN_LEFT, bSTART, bEDIT);
	UpdateTaskScreen(UI_WINDOW_DISPENSER_STATUS_MENU, Init);
	EventMapSetup(MenuSurfing);
}

static bool uDispenser_Menu_Core(uint32_t Arg)
{
	if(GetEvent())
		switch(event)	
		{
			case EVENT_ENCODER_TURN_LEFT:
				bru_Context_Next_Task(T_Preset_3_Menu);					
				break;
			
			case EVENT_ENCODER_TURN_RIGHT:
				bru_Context_Next_Task(T_Alarm_Menu);
				break;
			
			case EVENT_LEFT_BUTTON_CLICK:								// START
				bru_Context_Next_Task(T_Dispenser_Water);	
				break;
			
			case EVENT_RIGHT_BUTTON_CLICK:							// EDIT 
				bru_Context_Next_Task(T_Preset_Temp_Edit);
				break;			

			case EVENT_ENCODER_BUTTON_CLICK:
				EncoderClick();
				break;
			
			default: msg_unused_event(event);	
		}
	else
		vTaskDelay(10);

	return false;
}

//--- T_Alarm_Menu ----------------------------------------------------------
static void uAlarm_Menu_Init(uint32_t Arg)
{
	bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
	hUserInterface->iPresetCurrentIndex = 5;	
	SetupButtons(MAIN_BTN_LEFT, bSTART, bEDIT);
	UpdateTaskScreen(UI_WINDOW_ALARM_SCREEN, Init);
	EventMapSetup(MenuSurfing);
}

static bool uAlarm_Menu_Core(uint32_t Arg)
{
	if(GetEvent())
		switch(event)	
		{
			case EVENT_ENCODER_TURN_LEFT:
				bru_Context_Next_Task(T_Dispenser_Menu);					
				break;
			
			case EVENT_ENCODER_TURN_RIGHT:
				bru_Context_Next_Task(T_HomeScreen);
				break;
			
			case EVENT_LEFT_BUTTON_CLICK:								// START
//				bru_Context_Next_Task();	
				break;
			
			case EVENT_RIGHT_BUTTON_CLICK:							// EDIT 
//				bru_Context_Next_Task(T_Preset_Time_Edit);
				break;			

			case EVENT_ENCODER_BUTTON_CLICK:
				EncoderClick();
				break;
			
			default: msg_unused_event(event);	
		}
	else
		vTaskDelay(10);

	return false;
}

//--- T_Preset_Time_Edit ------------------------------------------------
static void uPreset_Time_Edit_Init(uint32_t Arg)
{
	SetupButtons(MAIN_BTN_RIGHT, b__, bNEXT);
	UpdateTaskScreen(UI_WINDOW_BREWING_SETUP_MENU_EDIT_TIME, Init);
	EventMapSetup(MenuSurfing);
}

static bool uPreset_Time_Edit_Core(uint32_t Arg)
{
	if(GetEvent())
		switch(event)	
		{
			case EVENT_ENCODER_TURN_LEFT:
				bru_BrewingTime_adj_steepint_time_ENG(ENCODE_LEFT);			
				UpdateTaskScreen(UI_WINDOW_BREWING_SETUP_MENU_EDIT_TIME, Update);
				break;
			
			case EVENT_ENCODER_TURN_RIGHT:
				bru_BrewingTime_adj_steepint_time_ENG(ENCODE_RIGHT);
				UpdateTaskScreen(UI_WINDOW_BREWING_SETUP_MENU_EDIT_TIME, Update);
				break;
			
			case EVENT_RIGHT_BUTTON_CLICK:							// NEXT
				bru_Context_Next_Task(T_Preset_Temp_Edit);
				break;			

			case EVENT_ENCODER_BUTTON_CLICK:
				EncoderClick();
				break;
			
			default: msg_unused_event(event);	
		}
	else
		vTaskDelay(10);
	
	return false;
}

//--- T_Preset_Temperature_Edit -----------------------------------------
static void uPreset_Temp_Edit_Init(uint32_t Arg)
{
	SetupButtons(MAIN_BTN_RIGHT, b__, bNEXT);
	UpdateTaskScreen(UI_WINDOW_BREWING_SETUP_MENU_EDIT_TEMPERATURE, Init);
	EventMapSetup(MenuSurfing);
}

static bool uPreset_Temp_Edit_Core(uint32_t Arg)
{
	if(GetEvent())
		switch(event)	
		{
			case EVENT_ENCODER_TURN_LEFT:
				bru_BrewingTime_adj_water_temper_ENG(ENCODE_LEFT);
				UpdateTaskScreen(UI_WINDOW_BREWING_SETUP_MENU_EDIT_TEMPERATURE, Update);
				break;
			
			case EVENT_ENCODER_TURN_RIGHT:
				bru_BrewingTime_adj_water_temper_ENG(ENCODE_RIGHT);
				UpdateTaskScreen(UI_WINDOW_BREWING_SETUP_MENU_EDIT_TEMPERATURE, Update);
				break;
			
			case EVENT_RIGHT_BUTTON_CLICK:							// NEXT
				bru_Context_Next_Task(T_Preset_Water_Edit);
				break;			

			case EVENT_ENCODER_BUTTON_CLICK:
				EncoderClick();
				break;
			
			default: msg_unused_event(event);	
		}
	else
		vTaskDelay(10);
	
	return false;
}

//--- T_Preset_Water_Edit -----------------------------------------
static void uPreset_Water_Edit_Init(uint32_t Arg)
{
	SetupButtons(MAIN_BTN_RIGHT, b__, bSAVE);
	UpdateTaskScreen(UI_WINDOW_BREWING_SETUP_MENU_EDIT_WATER_AMOUNT, Init);
	EventMapSetup(MenuSurfing);
}

static bool uPreset_Water_Edit_Core(uint32_t Arg)
{
	if(GetEvent())
		switch(event)	
		{
			case EVENT_ENCODER_TURN_LEFT:
				bru_BrewingTime_adj_water_amount_ENG(ENCODE_LEFT);
				UpdateTaskScreen(UI_WINDOW_BREWING_SETUP_MENU_EDIT_WATER_AMOUNT, Update);
				break;
			
			case EVENT_ENCODER_TURN_RIGHT:
				bru_BrewingTime_adj_water_amount_ENG(ENCODE_RIGHT);
				UpdateTaskScreen(UI_WINDOW_BREWING_SETUP_MENU_EDIT_WATER_AMOUNT, Update);
				break;
			
			case EVENT_RIGHT_BUTTON_CLICK:							// SAVE
				write_record_info_to_flash();
				get_all_preset_info();				
				bru_Context_Next_Task(jmp_to_preset_menu());
				break;			

			case EVENT_ENCODER_BUTTON_CLICK:
				EncoderClick();
				break;
			
			default: msg_unused_event(event);	
		}
	else
		vTaskDelay(10);
	
	return false;
}

//=== Dispenser Tasks ==========================================================

//--- T_Dispenser_Water --------------------------------------------------------
static bool Dispenser_Water_Unit_Core(uint32_t Arg)
{
	if(Sensors.Water)	return true;
	CallSubTask(ST_ADD_WATER);
	return false;
}

static void Dispenser_Water_Unit_Fin(uint32_t Arg)
{
	bru_Context_Next_Task(T_Dispenser_SChamber);
}

//--- T_Dispenser_SChamber -----------------------------------------------------
static bool Dispenser_SChamber_Unit_Core(uint32_t Arg)
{
	if(Sensors.BrewingChamber) return true;
	CallSubTask(ST_PLACE_STEEPING_CHAMBER);
	return false;
}

static void Dispenser_SChamber_Unit_Fin(uint32_t Arg)
{
	bru_Context_Next_Task(T_Dispenser_Cup);
}

//--- T_Dispenser_Cup ----------------------------------------------------------
static bool Dispenser_Cup_Unit_Core(uint32_t Arg)
{	
	if(Sensors.Cup) return true;
	CallSubTask(ST_PLEASE_CUP);
	return false;
}

static void Dispenser_Cup_Unit_Fin(uint32_t Arg)
{
	bru_Context_Next_Task(T_Dispenser_Lid);
}

//--- T_Dispenser_Lid ----------------------------------------------------------
static bool Dispenser_Lid_Unit_Core(uint32_t Arg)
{	
	if(Sensors.Lid) return true;
	CallSubTask(ST_CLOSE_LID);
	return false;
}

static void Dispenser_Lid_Unit_Fin(uint32_t Arg)
{
	bru_Context_Next_Task(T_Dispenser_PV);
}

//--- T_Dispenser_PV ----------------------------------------------------------
static void Dispenser_PV_Unit_Init(uint32_t Arg)
{
	EventMap.pv_open_enb = true;
	EventMap.pv_error_enb = true;
	EventMap.cancel_enb = true;
	Pinch_Valve(ToOpen);	
}

static bool Dispenser_PV_Unit_Core(uint32_t Arg)
{
	if(GetEvent())
		switch(event)	
		{
			case EVENT_CANCEL:
			case EVENT_PV_OPEN:
				return true;
			
			case EVENT_PV_ERROR:
				CallSubTask(ST_ERROR_xx);
				return false;

			default: msg_unused_event(event);
		}
		
	vTaskDelay(50);	
	return false;
}

static void Dispenser_PV_Unit_Fin(uint32_t Arg)
{
	bru_Context_Next_Task(T_Dispenser_Dispensing);
}

//--- T_Dispenser_Dispensing ---------------------------------------------------
static void Dispenser_Dispensing_Unit_Init(uint32_t Arg)
{
	SetupButtons(MAIN_BTN_LEFT, bSTOP, b__);
	UpdateTaskScreen(UI_WINDOW_DISPENSER_STATUS_DISPENSING, Init);
	EventMap.left_button_enb = true;
	EventMap.encoder_button_enb = true;
	EventMap.water_enb = true;
	EventMap.lid_open_enb = true;
	EventMap.lid_close_enb = true;
	
	bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
	
	DB_Printf("Dispensing: temp: %02d, water: %03d\n",
		(int)hUserInterface->presetsArr[hUserInterface->iPresetCurrentIndex].temperature,
		(int)hUserInterface->presetsArr[hUserInterface->iPresetCurrentIndex].waterAmount);
	
	bru_Water_Prepare_Start(hUserInterface->presetsArr[hUserInterface->iPresetCurrentIndex].waterAmount,\
													hUserInterface->presetsArr[hUserInterface->iPresetCurrentIndex].temperature);	
	set_rgb_duty(100,0,0);
}

static bool Dispenser_Dispensing_Unit_Core(uint32_t Arg)
{
	if (bru_Pid_Step()) return true;
		
	if(GetEvent())
		switch(event)	
		{
			case EVENT_LEFT_BUTTON_CLICK:													// btn Stop
				return true;
			
			case EVENT_WATER_NO:																	// Check Water evt
				bru_Water_Prepare_Pause();
				CallSubTask(ST_ADD_WATER);
				break;
			
			case 	EVENT_WATER_OK:																	// Check Water evt
				bru_Water_Prepare_Resume();
				break;
			
			case EVENT_LID_OPENED:																// Check Lid evt
				bru_Water_Prepare_Pause();
				CallSubTask(ST_CLOSE_LID);
				return false;				
			
			case EVENT_LID_CLOSED:
				bru_Water_Prepare_Resume();
				return false;			

			case EVENT_ENCODER_BUTTON_CLICK:
				EncoderClick();
				break;
			
			default: msg_unused_event(event);
		}

	vTaskDelay(250);	
	return false;
}

static void Dispenser_Dispensing_Unit_Fin(uint32_t Arg)
{
	bru_Water_Prepare_Stop();
	bru_Context_Next_Task(T_Dispenser_Serving);
}

//--- T_Dispenser_Serving ------------------------------------------------------
static void Dispenser_Serving_Unit_Init(uint32_t Arg)
{
	AlarmTimerStart(3000);
}

static bool Dispenser_Serving_Unit_Core(uint32_t Arg)
{
	if(GetEvent())
		switch(event)	
		{
			case EVENT_ALARM_TIMER:
				return true; 										// Serving Done
				
			default: msg_unused_event(event);
		}
	return false;
}

static void Dispenser_Serving_Unit_Fin(uint32_t Arg)
{
	set_rgb_duty(0,0,0);
	bru_Context_Next_Task(T_Dispenser_Menu);	
}

//--- T_Null -------------------------------------------------------------------
void Null_FProc(uint32_t Arg) {}
void Null_Proc(uint32_t Arg) {vTaskDelay(10);}
bool Null_Func(uint32_t Arg) {return true;}

//=== Connecting Units ========================================================
void TaskLink(bru_Tasks_List TaskName, 
							bru_Context_Task_Init_Fxn Init_Fxn,
							bru_Context_Task_Core_Fxn Core_Fxn,
							bru_Context_Task_Final_Fxn Final_Fxn)
{
	bru_TaskList[TaskName].TaskName = TaskName;
  bru_TaskList[TaskName].fxnInitTask = Init_Fxn;
  bru_TaskList[TaskName].fxnCoreTask = Core_Fxn;
  bru_TaskList[TaskName].fxnFinalTask = Final_Fxn;
}	

static void bru_Context_TaskList_Init(void)
{
	extern void TaskLink_Brewing_Init(void);
	extern void TaskLink_Tests_Init(void);
	
  bru_Context Task;

  Task.TaskName = T_Null;
  Task.ArgInit = NULL;
  Task.ArgCore = NULL;
  Task.ArgFin = NULL;
  Task.fxnInitTask = &Null_Proc;
  Task.fxnCoreTask = &Null_Func;
  Task.fxnFinalTask = &Null_Proc;
	Task.TaskScreen = UI_WINDOW_NULL;

  for (bru_Tasks_List i = T_Null; i < TASKS_LIST_END; i++) 
    bru_TaskList[i] = Task;

	// Linking Tasks
	TaskLink(ST_ADD_WATER, &ADD_WATER_Unit_Init, &ADD_WATER_Unit_Core, &Null_Proc);
	TaskLink(ST_PLACE_STEEPING_CHAMBER, &PLACE_STEEPING_CHAMBER_Unit_Init, &PLACE_STEEPING_CHAMBER_Unit_Core, &Null_Proc);
	TaskLink(ST_PLEASE_CUP, &PLEASE_CUP_Unit_Init, &PLEASE_CUP_Unit_Core, &Null_Proc);
	TaskLink(ST_REMOVE_CUP, &REMOVE_CUP_Unit_Init, &REMOVE_CUP_Unit_Core, &Null_Proc);
	TaskLink(ST_CLOSE_LID, &CLOSE_LID_Unit_Init, &CLOSE_LID_Unit_Core, &Null_Proc);
	TaskLink(ST_ERROR_xx, &ERROR_xx_Unit_Init, &ERROR_xx_Unit_Core, &Null_Proc);
	TaskLink(T_IntroScreen, &uIntroScreen_Init, &uIntroScreen_Core, &uIntroScreen_Fin);
	TaskLink(T_HomeScreen, &uHomeScreen_Init, &uHomeScreen_Core, &Null_Proc);

	TaskLink_Brewing_Init();
	
	TaskLink(T_Brewing_Setup_Time, &uBrewing_Setup_Time_Init, &uBrewing_Setup_Time_Core, &Null_Proc);
	TaskLink(T_Brewing_Setup_Time_Edit, &uBrewing_Setup_Time_Edit_Init, &uBrewing_Setup_Time_Edit_Core, &Null_Proc);
	TaskLink(T_Brewing_Setup_Temperature, &uBrewing_Setup_Temp_Init, &uBrewing_Setup_Temp_Core, &Null_Proc);
	TaskLink(T_Brewing_Setup_Temperature_Edit, &uBrewing_Setup_Temp_Edit_Init, &uBrewing_Setup_Temp_Edit_Core, &Null_Proc);
	TaskLink(T_Brewing_Setup_Water, &uBrewing_Setup_Water_Init, &uBrewing_Setup_Water_Core, &Null_Proc);
	TaskLink(T_Brewing_Setup_Water_Edit, &uBrewing_Setup_Water_Edit_Init, &uBrewing_Setup_Water_Edit_Core, &Null_Proc);
	TaskLink(T_Preset_1_Menu, &uPreset_1_Menu_Init, &uPreset_1_Menu_Core, &Null_Proc);
	TaskLink(T_Preset_2_Menu, &uPreset_2_Menu_Init, &uPreset_2_Menu_Core, &Null_Proc);
	TaskLink(T_Preset_3_Menu, &uPreset_3_Menu_Init, &uPreset_3_Menu_Core, &Null_Proc);
	TaskLink(T_Dispenser_Menu, &uDispenser_Menu_Init, &uDispenser_Menu_Core, &Null_Proc);
	TaskLink(T_Alarm_Menu, &uAlarm_Menu_Init, &uAlarm_Menu_Core, &Null_Proc);
	TaskLink(T_Preset_Time_Edit, &uPreset_Time_Edit_Init, &uPreset_Time_Edit_Core, &Null_Proc);
	TaskLink(T_Preset_Temp_Edit, &uPreset_Temp_Edit_Init, &uPreset_Temp_Edit_Core, &Null_Proc);
	TaskLink(T_Preset_Water_Edit, &uPreset_Water_Edit_Init, &uPreset_Water_Edit_Core, &Null_Proc);

//	TaskLink_Dispenser_Init();
	TaskLink(T_Dispenser_Water, &Null_FProc, &Dispenser_Water_Unit_Core, &Dispenser_Water_Unit_Fin);
	TaskLink(T_Dispenser_SChamber, &Null_FProc, &Dispenser_SChamber_Unit_Core, &Dispenser_SChamber_Unit_Fin);
	TaskLink(T_Dispenser_Cup, &Null_FProc, &Dispenser_Cup_Unit_Core, &Dispenser_Cup_Unit_Fin);
	TaskLink(T_Dispenser_Lid, &Null_FProc, &Dispenser_Lid_Unit_Core, &Dispenser_Lid_Unit_Fin);
	TaskLink(T_Dispenser_PV, &Dispenser_PV_Unit_Init, &Dispenser_PV_Unit_Core, &Dispenser_PV_Unit_Fin);
	TaskLink(T_Dispenser_Dispensing, &Dispenser_Dispensing_Unit_Init, &Dispenser_Dispensing_Unit_Core, &Dispenser_Dispensing_Unit_Fin);
	TaskLink(T_Dispenser_Serving, &Dispenser_Serving_Unit_Init, &Dispenser_Serving_Unit_Core, &Dispenser_Serving_Unit_Fin);
	
	TaskLink_Tests_Init();	
}

// === End file  ========================================

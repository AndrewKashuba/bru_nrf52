#ifndef __MAIN_H_
#define __MAIN_H_

#include "stdint.h"
#include "unix_time.h"



#define HARDWARE_VERSION                "V1.0.0"                                      /**< Hardware Version. Will be passed to Device Information Service. */
#define SOFTWARE_VERSION                "V1.0.8"                   						     /**< Software Version. Will be passed to Device Information Service. */



#ifdef __cplusplus
extern "C" {
#endif


void ble_send_string(uint8_t * pData,uint16_t len);
uint32_t ble_updateData(uint8_t * pData,uint16_t len);

extern uint8_t pBuffer[170];
#define DEVICE_LOG_INFO(fmt,args...) \
      do					\
			{						\
				sprintf((char*)pBuffer,fmt,##args); \
				uint8_t len = strlen((const char*)pBuffer); \
				ASSERT(len<150);							\
				ble_send_string(pBuffer,len);   \
			}while(0)


#if DEBUG_BY_BLE_ENABLED

#define BLE_LOG_INFO  DEVICE_LOG_INFO
     
#else
#define BLE_LOG_INFO(fmt,args...) ((void)0)
#endif


void advertising_start(void);
void advertising_stop(void);


#ifdef __cplusplus
}
#endif

#endif



#ifndef __CTRLUNITS_H
#define __CTRLUNITS_H

#include <stdint.h>
#include "bru_WaterSensor.h"

#define NORMAL_TIMEOUT    (50)
#define TASK_TICK_INTERVAL	10
#define KEY_PUSH_SHORT 			(50/TASK_TICK_INTERVAL)
#define KEY_PUSH_LONG 			(2000/TASK_TICK_INTERVAL)
#define BUTTON_L	BUTTON_1
#define BUTTON_R	BUTTON_2
#define BUTTON_E	BUTTON_3

typedef enum
{
	NO_EVENT = 0,
	EVENT_TICKS,
	EVENT_ALARM_TIMER,
	EVENT_LEFT_BUTTON_CLICK,
	EVENT_RIGHT_BUTTON_CLICK,
	EVENT_ENCODER_BUTTON_CLICK,
	EVENT_LEFT_BUTTON_RESUME,
	EVENT_RIGHT_BUTTON_RESUME,
	EVENT_ENCODER_TURN_LEFT,	
	EVENT_ENCODER_TURN_RIGHT,	
	EVENT_WATER_NO,
	EVENT_WATER_OK,
  EVENT_LID_OPENED,
  EVENT_LID_CLOSED,
	EVENT_CUP_ON_PLACE,
	EVENT_CUP_REMOVED,
	EVENT_PV_BEGIN,
	EVENT_PV_OPEN,
	EVENT_PV_CLOSE,
	EVENT_PV_ERROR,
	EVENT_CANCEL
} event_id_t;

typedef struct _bru_Sensor_Status
{
	bool	LeftButton;									// Button1 (digi)
	bool	RightButton;								// Button2 (digi)
	bool	EncoderButton;							// EncoderBtn (digi)
	bool	BrewingChamber;							// BrewingChamber (digi)
	bool	Water;											// Water (digi)
	bool	Lid;												// Lid (digi)
	bool	Cup;												// Cup status (digi)
	int16_t IR_vol;										// IR (analog)
	bool	TopSensorStatus;						// 0 - nc, 1 - connected
	float TopTemperature;							// ThermoTop (analog)
	bool	BotSensorStatus;						// 0 - nc, 1 - connected
	float BotTemperature;							// ThermoBottom (analog)	
} bru_Sensor_Status;

typedef struct _bru_Actuators_Status
{
	bool Heater;											// Heater status (0 - off, 1 - on)
	bool Pump;												// Pump status (0 - off, 1 - on)
} bru_Actuators_Status;

typedef struct _bru_Sensor_EventMap
{
	bool tick_gen_enb;								// tick generator (command_32hz_timer_evt)
	bool left_button_enb;							// button1_release_evt
	bool right_button_enb;						// button2_release_evt
	bool encoder_button_enb;					// encoder_release_evt
	bool encoder_turn_enb;						// encoder_turn_release_evt
	bool water_enb;										// water evt
	bool lid_open_enb;								// lid_open_evt
	bool lid_close_enb;								// lid_close_evt
	bool water_ok_enb;								// water_ok
	bool water_few_enb;								// water_few (reinit)
	bool water_nc_enb;								// water_sensor_nc (reinit)
	bool chamber_removed;							// brewing_chamber_removed (reinit)
	bool cup_on_place_enb;						// cup_on_place
	bool cup_removed_enb;							// cup_no (reinit)	
	bool pv_begin_enb;								// Begin process
	bool pv_open_enb;									// Open
	bool pv_close_enb;								// Close
	bool pv_error_enb;								// Error
	bool cancel_enb;									//
} bru_Sensor_EventMap;

typedef enum
{
	MenuSurfing,
	PV_all_events
	
} EventMapSet;

extern char TimeCaptureResult[];
extern void StartTickGenerator(uint32_t);
extern void AlarmTimerStart(int32_t);
extern void AlarmTimerPause(void);
extern void AlarmTimerResume(void);
extern void EventMapSetup(EventMapSet);
extern void TimeCapture_Init(void);
extern void TimeCapture_Begin(void);
extern void TimeCapture_End(void);
extern bool TimeCapture_Result(void);

#endif



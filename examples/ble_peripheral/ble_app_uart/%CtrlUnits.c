/*
 * 	%CtrlUnits.c
 *	Buttons, Encoder, Actuators,  ...
 *
 *  Created on: Nov 21, 2021
 *      Author: Andrew Kashuba
 */

#include <stdbool.h>
#include "nrf_gpio.h"
#include "dbprintf.h"
#include "CtrlUnits.h"
#include "BSP_KeyScan.h"
#include "BSP_menu_info.h"
#include "gpiote_drv_core.h"
#include "BSP_ADC.h"
#include "event_queue_c.h" 
#include "bru_Context.h"
#include "bru_PinchValve.h"
#include "pwm_pump_core.h"
#include "bru_version.h"

//----- static variables -----------------------------------------------------//
bool fCtrlUnitsActivity;
bool fDisplayStartSolution = false;
bru_Sensor_Status Sensors;
bru_Actuators_Status Actuators;
bru_Sensor_EventMap EventMap;
static uint16_t LButtonCounter = 0;
static uint16_t RButtonCounter = 0;
static uint16_t EButtonCounter = 0;
static uint32_t AlarmTimer;
static bool AlarmTimerEnable = false;

//------ New Defined Function ------------------------------------------------//
static void control_task_function (void * pvParameter);
static void btn_timer_check(uint32_t _tickCount);
static void bru_TickEvent(void);
static void bru_AlarmTimerLoop(void);
static void bru_SensorsScan(void);
void ClearSensorEventMap(void);


void bru_Relay_Init(void);
void encode_filter_timer_handler(void *timer);

// ##### FreeRTOS units ########################################################

#include "FreeRTOS.h"
#include "task.h"

TaskHandle_t  control_task_handle;   /**< Reference to LED0 toggling FreeRTOS task. */

bool CtrlUnits_Init(void){

	/* Create task for sensors and actuators with priority set to 2 */
	xTaskCreate(control_task_function, "ControlStream", configMINIMAL_STACK_SIZE + 200, NULL, 1, &control_task_handle);

return true;	
}

static void control_task_function (void * pvParameter)
{	
  UNUSED_PARAMETER(pvParameter);	
	static uint32_t _tickCount;
	extern gevent_t eventQueueBuffer[APP_EVENT_QUEUE_SIZE];
	extern bool evt_queue_Reinit(uint8_t queueSize, gevent_t* pEventBuffer);
	extern void bru_Water_Sensor_Init(void);

	BSP_ADC_Init();
	RGBB_PWM_Init();    								// rgb and backlight pwm initial
	BUZZER_PWM_Init();  								// buzzer pwm initial
	PUMP_PWM_Init();    								// pumb pwm inital	
	bru_Water_Sensor_Init();
	bru_Relay_Init();
	bru_Pinch_Valve_Init();
	ClearSensorEventMap();

	bru_LCD_GC9A01_Initial();        		// lcd initial

	BSP_gpiote_init(ENCODE_OUTA_PIN,encode_outA_int_handler,NULL);
	nrf_gpio_cfg_input(ENCODE_OUTB_PIN,NRF_GPIO_PIN_PULLUP);
	register_timer(ENCODE_FILTER_TIMER_ID,encode_filter_timer_handler);
	
  if (! evt_queue_Reinit(APP_EVENT_QUEUE_SIZE, eventQueueBuffer))
		DB_Printf("Event queue init failed!\n");
  else
    DB_Printf("Event queue init success!\n"); 

	fCtrlUnitsActivity = true;
  DB_Printf("Control Unit started!\n");
	
	//-------------------------------- Main Loop ------------------------------ //
  for (;;)
	{
		_tickCount++;
		if (_tickCount >= 500) _tickCount=0;      // tickCount Initialize
		btn_timer_check(_tickCount);		

		bru_TickEvent();
		bru_AlarmTimerLoop();
		bru_SensorsScan();
		bru_PV_Loop();

    vTaskDelay(TASK_TICK_INTERVAL);
  }
	//-------------------------------- End Main Loop -------------------------- //
}

void EventPost(event_id_t event)
{
	extern bool evt_queue_Post(gevent_t* pEvent);
	gevent_t evt;	
	evt.event = event;
	evt_queue_Post(&evt);	
}

// ##### Sensors ###############################################################

static void bru_SensorsScan(void)
{
	static uint8_t _adcScanCount;
	extern bool bru_Water_Status(void);
	
 	switch(_adcScanCount++ % 8) 
	{
		case 0:
				//start_multiplex_messurement(MULT_NTC_PROTECTION); // NC
				break;
		case 1:
				start_multiplex_messurement(MULT_NTC_TOP);
				break;
		case 2:
				start_multiplex_messurement(MULT_NTC_BOTTOM);
				break;
		case 3:
				start_multiplex_messurement(MULT_PINCH_VALVE_CURRENT);
				break;
		case 4:
				start_multiplex_messurement(MULT_IR_SENSOR);
				break;
		case 5:
				start_multiplex_messurement(MULT_LID_POSITION);
				break;
		case 6:
				start_multiplex_messurement(MULT_BREWING_CHAMBER);
				break;
		case 7:
				Sensors.Water = bru_Water_Status();
				break;
	}
}

// === Tick Generator ==========================================================

static uint32_t _TickCount = 0;
static uint32_t TickTime = 1000;

void StartTickGenerator(uint32_t Time)
{
	TickTime = Time < 100 ? 100 : Time;
	_TickCount = 0;
	EventMap.tick_gen_enb = true;
}
	
static void bru_TickEvent(void)
{
	_TickCount++;
	if (_TickCount >= TickTime / TASK_TICK_INTERVAL) {
		_TickCount=0;      // tickCount Initialize
		if(fDisplayStartSolution) set_backlight_value(BACKLIGHT_ON_VALUE); //ak!!!! temp solution - disable backlight_off
		if (EventMap.tick_gen_enb) EventPost(EVENT_TICKS);
	}	
}

// === Alarm Task Timer & Time Capture =========================================

typedef enum
{
	TC_Off,
	TC_Ready,
	TC_Active,
	TC_Done	
} TC_SM;

static uint32_t _OneSecTickCount = 0;
static uint32_t TimeCaptureTicks;
static TC_SM TimeCaptureSM = TC_Off;
char TimeCaptureResult[16] = {0};

void AlarmTimerStart(int32_t msec_tick)
{
	AlarmTimer = msec_tick / TASK_TICK_INTERVAL;
	AlarmTimerEnable = true;
	_OneSecTickCount = 0;
}

void AlarmTimerPause(void)
{
	AlarmTimerEnable = false;
}

void AlarmTimerResume(void)
{
	AlarmTimerEnable = true;	
}

static void bru_AlarmTimerLoop(void)
{
	if ((AlarmTimerEnable) && (AlarmTimer < _OneSecTickCount++) && (TimeCaptureSM != TC_Active)) 
	{
		AlarmTimerEnable = false;
		EventPost(EVENT_ALARM_TIMER);
	}
}

void TimeCapture_Init(void)
{
	TimeCaptureTicks = 0;
	TimeCaptureSM = TC_Ready;
}

void TimeCapture_Begin(void)
{
	if(TimeCaptureSM == TC_Ready)
		TimeCaptureSM = TC_Active;
}

void TimeCapture_End(void)
{
	if(TimeCaptureSM == TC_Active) {
		TimeCaptureTicks = _OneSecTickCount;
		AlarmTimer = 0;
		TimeCaptureSM = TC_Done;
	}
}

bool TimeCapture_Result(void)
{	
  sprintf(TimeCaptureResult, "%d.%02d", 
														(TimeCaptureTicks * TASK_TICK_INTERVAL) / 1000, 
														(TimeCaptureTicks * TASK_TICK_INTERVAL) % 1000);
	return TimeCaptureSM == TC_Done;
}

// === Buttons =================================================================

static void btn_timer_check(uint32_t tickCount)
{
	extern void put_key_event(uint8_t evt);
	static bool last_btnL_val;
	static bool last_btnR_val;
	static bool last_btnE_val;
	extern _ContextSM ContextSM;
	
	if (LButtonCounter == KEY_PUSH_SHORT)	{
			DB_Printf("Click Left btn\n");
			if(ContextSM == Suspend) put_key_event(button1_release_evt);		
			else if (EventMap.left_button_enb) 
				EventPost(EVENT_LEFT_BUTTON_CLICK);
		}

	if (RButtonCounter == KEY_PUSH_SHORT)	{
			DB_Printf("Click Right btn\n");
			if(ContextSM == Suspend) put_key_event(button2_release_evt);
			else if (EventMap.right_button_enb) 
				EventPost(EVENT_RIGHT_BUTTON_CLICK);
		}

	if (EButtonCounter == KEY_PUSH_SHORT)	{
			DB_Printf("Click Encoder btn\n");
			if(ContextSM == Suspend) put_key_event(encoder_button_release_evt);		
			else if (EventMap.encoder_button_enb) 
				EventPost(EVENT_ENCODER_BUTTON_CLICK);
		}

	if (EButtonCounter == KEY_PUSH_LONG)	{
			DB_Printf("Long click Encoder btn\n");
//			evt.event = EVENT_ENCODER_BUTTON_CLICK;
//			if (SensorEventMap.encoder_button_enb) evt_queue_Post(&evt);		
		}
		
	if(!last_btnL_val && !(nrf_gpio_pin_read(BUTTON_L))) {
		LButtonCounter++;
		Sensors.LeftButton = true;
	}
	else if(!last_btnL_val && nrf_gpio_pin_read(BUTTON_L)) {
		LButtonCounter = 0;
		Sensors.LeftButton = false;
		if (EventMap.left_button_enb) 
			EventPost(EVENT_LEFT_BUTTON_RESUME);
	}

	if(!last_btnR_val && !(nrf_gpio_pin_read(BUTTON_R))) {
		RButtonCounter++;
		Sensors.RightButton = true;
	}
	else if(!last_btnR_val && nrf_gpio_pin_read(BUTTON_R)) {
		RButtonCounter = 0;
		Sensors.RightButton = false;
		if (EventMap.right_button_enb) 
			EventPost(EVENT_RIGHT_BUTTON_RESUME);
	}

	if(!last_btnE_val && !(nrf_gpio_pin_read(BUTTON_E))) {
		EButtonCounter++;
		Sensors.EncoderButton = true;
	} else {
		EButtonCounter = 0;
		Sensors.EncoderButton = false;
	}

	last_btnL_val = nrf_gpio_pin_read(BUTTON_L);
	last_btnR_val = nrf_gpio_pin_read(BUTTON_R);
	last_btnE_val = nrf_gpio_pin_read(BUTTON_E);
}

// === Encoder =================================================================

void encode_filter_timer_handler(void *timer)
{
	gpiote_interrupt_switching(true,ENCODE_OUTA_PIN);
}

void encode_outA_int_handler(nrfx_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
	static uint32_t filter_flag;
	extern void put_key_event(uint8_t evt);
	extern _ContextSM ContextSM;
	
	gpiote_interrupt_switching(false,ENCODE_OUTA_PIN);
	if((action == NRF_GPIOTE_POLARITY_HITOLO) && (nrf_gpio_pin_read(ENCODE_OUTA_PIN) == 0))
	{
		nrf_delay_us(100);
		if(nrf_gpio_pin_read(ENCODE_OUTA_PIN) == 0)
		{
			if(nrf_gpio_pin_read(ENCODE_OUTB_PIN) == 0)	{ 
				if(ContextSM == Suspend) 
					put_key_event(BRU_ENCODER_OUT_A_RELEASE);		
				else if (EventMap.encoder_turn_enb) 
					EventPost(EVENT_ENCODER_TURN_RIGHT);
			}
			else {
			if(ContextSM == Suspend) 
				put_key_event(BRU_ENCODER_OUT_B_RELEASE);		
			else if (EventMap.encoder_turn_enb) 
				EventPost(EVENT_ENCODER_TURN_LEFT);	
			}
		}		
	}
	set_timer_value(ENCODE_FILTER_TIMER_ID,ENCODE_FILTER_TIMER_VALUE,(void*)&filter_flag);
}

// === Thermosensors ===========================================================
static const float Rb  = 15000;        //Ballast resistor from the button branch of NTC 42.2

static const uint32_t R2T[140] = {
	544435,516236,489694,464701,441157,418969,398049,378317,359698,342123,325526,
	309233,293865,279364,265676,252750,240539,228999,218089,207770,198008,188768,
	180020,171734,163883,156442,149387,142696,136347,130321,124600,119167,114005,
	109099,104435,100000,95667,91549,87634,83911,80369,76998,73789,70734,67824,
	65051,62409,59890,57487,55196,53010,50923,48932,47029,45213,43477,41818,40232,
	38715,37265,35877,34519,33221,31978,30789,29652,28563,27520,26521,25564,24647,
	23768,22926,22118,21343,20599,19886,19201,18544,17913,17307,16724,16165,15627,
	15110,14613,14135,13676,13234,12808,12398,12004,11625,11259,10907,10568,10233,
	9911,9600,9301,9013,8736,8468,8210,7961,7721,7489,7266,7050,6842,6641,6446,
	6257,6074,5898,5728,5563,5404,5251,5102,4959,4820,4686,4556,4431,1309,4192,
	4078,3968,3861,3758,3658,3561,3467,3377,3289,3202,3117,3036,2957
};

/**
* @brieaf calc temperature 
*/
/**
�� data/4096 = vol/Vcc  ---> vol = data * Vcc / 4096
�� (vol/4)/(Vcc/4) = Rb/(Rb+Rntc) --->vol =  Rb*Vcc/(Rb+Rntc)
��+�� ---> data * Vcc / 4096 = Rb*Vcc/(Rb+Rntc)
		---->Rntc = (4096/data-1)*Rb 
*/
float calc_temperature_value(void *data)
{
	float temperature = 0;
	int16_t adc_temp = *((int16_t *)(data));
	if(adc_temp < 271)  return -10.0f;
	uint32_t Rntc = ((float)4096/adc_temp-1)*Rb;
	uint8_t i;
	for(i=0; i<140; i++)
	{
		if(Rntc >= R2T[i]) break;
	}
	if(i == 0) temperature = -10.0f;
	else if(i == 140) temperature = 130.0f;
	else	temperature = (i-10)-(float)(Rntc-R2T[i])/(R2T[i-1]-R2T[i]);
	return temperature;
}

void get_ntc_top(void *data)
{
	Sensors.TopTemperature = calc_temperature_value(data);
	Sensors.TopSensorStatus = Sensors.TopTemperature > -5.0;
	
/*  static char buf[32];
	sprintf(buf,"%.1f",Sensors.TopTemperature);
	DB_Printf("top %s,%d\r\n",buf,*((int16_t *)data));
*/
}

void get_ntc_bottom(void *data)
{	
	Sensors.BotTemperature = calc_temperature_value(data);
	Sensors.BotSensorStatus = Sensors.BotTemperature > -5.0;

/*	static char buf[32];
	sprintf(buf,"%.1f",Sensors.BotTemperature);
	DB_Printf("bot %s,%d\r\n",buf,*((int16_t *)data));
*/	
}

// === IR Cup Sensor ===========================================================

//#if (PUBLIC_RELEASE)   
	#define CUP_LIMIT_VOLTAGE    						(1200)      								// 1.2V
//#else 
//	#define CUP_LIMIT_VOLTAGE    						(1200)      									// 2.3V
//#endif		
//#define CUP_LIMIT_ADC_VALUE(VOL_IN_MV)  (VOL_IN_MV/**4096/3250*/)

void get_ir_sensor(void *data)
{
	static bool last_cup_vol = false;	

	Sensors.IR_vol = *((int16_t *)(data)) * 3250 / 4096;
	
	// !!!! temp solution just for 6 Dec 2021
	bru_UI_Interface_Handle  hUserInterface = get_ui_interface();
	if(hUserInterface->hMachineSetup->buzzerState == MS_BUZZER_ON)
		Sensors.Cup = Sensors.IR_vol > CUP_LIMIT_VOLTAGE;
	else
		Sensors.Cup = true;
	
	//Sensors.IR_vol = Sensors.IR_vol * 3250/4096;
	//DB_Printf("ir sensor: %d\n",Sensors.IR_vol);
	
	if (EventMap.cup_on_place_enb)
		if (Sensors.Cup && !last_cup_vol) {
			EventPost(EVENT_CUP_ON_PLACE);
			DB_Printf("Cup on place!!!\n");
		}

	if (EventMap.cup_removed_enb)
		if (!Sensors.Cup && last_cup_vol) {
			EventPost(EVENT_CUP_REMOVED);
			DB_Printf("Cup removed!!!\n");
		}
		
	last_cup_vol = Sensors.Cup;
}

// === Lid Sensor ==============================================================

void get_lid_position(void *data)
{
	static bool last_lid_val = false;
	extern bool fFirstCup;

	Sensors.Lid = *((int16_t *)(data)) > 100;
	
	if (!Sensors.Lid && last_lid_val) {
		if (EventMap.lid_open_enb)
			EventPost(EVENT_LID_OPENED);
		fFirstCup = true;
		DB_Printf("Opened Lid!!!\n");
	}

	if (Sensors.Lid && !last_lid_val) {
		if (EventMap.lid_close_enb)
			EventPost(EVENT_LID_CLOSED);
		DB_Printf("Closed Lid!!!\n");
	}
		
	last_lid_val = Sensors.Lid;
}

// === Brewing Chamber Sensor ==================================================

void get_brewing_chamber(void *data)
{
	Sensors.BrewingChamber = *((int16_t *)(data)) > 100;
}

// === Sensors Map =============================================================
void EventMapSetup(EventMapSet set)
{
	switch(set)
	{
		case MenuSurfing:
			EventMap.encoder_turn_enb = true;
			EventMap.encoder_button_enb = true;
			EventMap.left_button_enb = true;
			EventMap.right_button_enb = true;
			break;
		
		case PV_all_events:
			EventMap.pv_begin_enb = true;
			EventMap.pv_open_enb = true;
			EventMap.pv_close_enb = true;
			EventMap.pv_error_enb = true;
			break;
	}
}

void ClearSensorEventMap(void)
{
	EventMap.tick_gen_enb = false;				//	tick_generator
	EventMap.left_button_enb = false;			//	button1_release_evt
	EventMap.right_button_enb = false;		//	button2_release_evt
	EventMap.encoder_button_enb = false;	//	encoder_release_evt
	EventMap.encoder_turn_enb = false;		//	encoder_turn_release_evt
	EventMap.lid_open_enb = false;				//	lid_open_evt
	EventMap.lid_close_enb = false;				//	lid_close_evt
	EventMap.water_ok_enb = false;				//	water_ok
	EventMap.water_few_enb = false;				//	water_few (reinit)
	EventMap.water_nc_enb = false;				//	water_sensor_nc (reinit)
	EventMap.chamber_removed = false;			//	brewing_chamber_removed (reinit)	
	EventMap.cup_on_place_enb = false;		//	cup_ok
	EventMap.cup_removed_enb = false;			//	cup_no (reinit)	
	EventMap.pv_begin_enb = false;				//	Begin process
	EventMap.pv_open_enb = false;					// 	PV Open
	EventMap.pv_close_enb = false;				// 	PV Close
	EventMap.pv_error_enb = false;				// 	PV Error	
}

// ##### Actuators #############################################################
//TODO: �hard code�
// 21	- pump never works if there is no water (maybe)
// 22 - pump never works if open lid
// 23 - heater never turn on if pump is not working
// 24 - heater never turn on if thermo sensor is not connecting
// 25 - PV only open when cup is there
		
// === Pump ====================================================================
#include "pwm_pump_core.h"
#define PUMP_TIMER_PERIOD   1000   // 100HZ

void bru_Pump_Raw_Set(uint32_t pump_tick)
{
	if (pump_tick > 100) pump_tick = 100;
	set_pump_raw_duty(PUMP_TIMER_PERIOD - pump_tick * PUMP_TIMER_PERIOD / 100);
}

bool bru_Pump_Start(void)
{
	if(!Sensors.Water) {
		DB_Printf("WARNING! Hard code error 21!\n");
		return true;
	}
	if(!Sensors.Lid) {
		DB_Printf("WARNING! Hard code error 22!\n");
		return true;
	}
	bru_Pump_Raw_Set(100);
	Actuators.Pump = true;
	DB_Printf("Pump Start\n");
	return false;
}

void bru_Pump_Stop(void)
{
	bru_Pump_Raw_Set(0);
	Actuators.Pump = false;
	DB_Printf("Pump Stop\n");
}


// === Relay ===================================================================
#include "bru_Relay.h"
#define BRU_RELAY_CONTROL_PIN				25
static bool Relay_Status;

void bru_Relay_Init(void)
{
	nrf_gpio_cfg_output(BRU_RELAY_CONTROL_PIN);
}

bool bru_Relay_ON(void)
{
	if(!Actuators.Pump) {
		DB_Printf("WARNING! Hard code error 23!\n");
		return true;		
	}
	if(!Sensors.TopSensorStatus || !Sensors.BotSensorStatus) {
		DB_Printf("WARNING! Hard code error 24!\n");
		return true;
	}
	nrf_gpio_pin_set(BRU_RELAY_CONTROL_PIN);
	if (!Relay_Status) DB_Printf("Heater On\n");
	Relay_Status = true;
	Actuators.Heater = true;
	return false;
}

void bru_Relay_OFF(void)
{
	nrf_gpio_pin_clear(BRU_RELAY_CONTROL_PIN);
	if (Relay_Status) DB_Printf("Heater Off\n");
	Relay_Status = false;
	Actuators.Heater = false;
}

// === End file  ===============================================================

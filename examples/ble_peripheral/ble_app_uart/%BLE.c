// BLE Core

#include "dbprintf.h"
#include "main.h"
//#include "BLE.h"

bool BT_inited;
bool BT_event;
bool fBLEUnitsActivity;

//------ New Defined Function ------------------------------------------------//
bool BLEUnits_Init(void);	
void btle_task_function (void * pvParameter);


// ##### FreeRTOS units ########################################################

#include "FreeRTOS.h"
#include "task.h"

TaskHandle_t  btle_task_handle;   /**< Reference to LED0 toggling FreeRTOS task. */

bool BLEUnits_Init(void){
	/* Create task for BLE with priority set to 2 */
	xTaskCreate(btle_task_function, "BLE", configMINIMAL_STACK_SIZE + 1000, NULL, 2, &btle_task_handle);
	return true;	
}

static void btle_task_function (void * pvParameter)
{	
		UNUSED_PARAMETER(pvParameter);	
		//extern bool fBLEShutdown;
		//extern bool BLEAdvertiseActivity;
	
		fBLEUnitsActivity = true;
		DB_Printf("BLE Unit started!\r\n");
	
		//-------------------------------- Main Loop -----------------------------//
    for (;;) 
		{
//  while (!fBLEShutdown){

    vTaskDelay(1000);
  }
	
//	fBLEUnitsActivity = true;
//  DB_Printf("BLE Unit shutdown!\r\n");
//	
//	vTaskDelete( NULL);	
}

// ##### Bru old BLECore #######################################################


// === End file  ========================================


@echo off
SET application-version=3
SET bootloader-version=2
SET bl-settings-version=1
SET hw-version=52


:start
echo ====================================================
echo      Script Programming NRF52 Tools
echo ====================================================

echo ====================================================
echo    1.Generate bootloader_settings
echo    2.Generate new private.pem and public_key.c 
echo   (ONLY FOR FIRST RELEASE) Replace key in dfu_public_key.c manually!
echo:
echo    3.Generate Package with application: application.zip
echo    4.Generate Package with bootloader: bootloader.zip
echo    5.Generate boot settings, merge, output BRU_final.hex
echo    6.RESET NRF52 IC (with J-Link only)
echo ====================================================
echo    7.Flash BRU_final.hex to NRF52 (with J-Link only)
echo ====================================================
echo    8. Exit

set /p menu=Select 
if %menu%==1  (goto :1)
if %menu%==2  (goto :2) 
if %menu%==3  (goto :3) 
if %menu%==4  (goto :4)
if %menu%==5  (goto :5)
if %menu%==6  (goto :6)
if %menu%==7  (goto :7)
if %menu%==8  (goto :exit)
::else (goto : start) 


:1
copy .\dfu\secure_bootloader\pca10040_s132_ble\arm5_no_packs\_build\dfu_bootloader.hex  .\
copy .\ble_peripheral\ble_app_uart\pca10040\s132\arm5_no_packs\_build\s132_ble_app.hex   .\

nrfutil.exe settings generate --family NRF52 --application s132_ble_app.hex --application-version %application-version% --bootloader-version %bootloader-version% --bl-settings-version %bl-settings-version% bootloader_settings.hex
echo Generate bootloader_setting file...
:pause>nul
goto start


:2
nrfutil.exe keys generate private.pem
nrfutil keys display --key pk --format code private.pem --out_file public_key.c
::nrfutil.exe keys generate private.pem Generated private key and stored in :key.pem
echo generate private.pem and public_key.c
:pause>nul
goto start


:3
copy .\dfu\secure_bootloader\pca10040_s132_ble\arm5_no_packs\_build\dfu_bootloader.hex  .\
copy .\ble_peripheral\ble_app_uart\pca10040\s132\arm5_no_packs\_build\s132_ble_app.hex   .\

nrfutil pkg generate --hw-version %hw-version% --sd-req 0x101 --application-version %application-version% --application s132_ble_app.hex --key-file private.pem application.zip
del s132_ble_app.hex
del dfu_bootloader.hex
echo ......
:pause>nul
goto start


:4
copy .\dfu\secure_bootloader\pca10040_s132_ble\arm5_no_packs\_build\dfu_bootloader.hex  .\
nrfutil pkg generate --hw-version %hw-version% --sd-req 0x101 --bootloader-version %bootloader-version% --bootloader dfu_bootloader.hex --key-file private.pem bootloader.zip
del dfu_bootloader.hex
echo ......
:pause>nul
goto start

:5
copy .\dfu\secure_bootloader\pca10040_s132_ble\arm5_no_packs\_build\dfu_bootloader.hex  .\
copy .\ble_peripheral\ble_app_uart\pca10040\s132\arm5_no_packs\_build\s132_ble_app.hex   .\

nrfutil.exe settings generate --family NRF52 --application s132_ble_app.hex --application-version %application-version% --bootloader-version %bootloader-version% --bl-settings-version %bl-settings-version% bootloader_settings.hex

mergehex.exe  --merge  dfu_bootloader.hex s132_nrf52_7.2.0_softdevice.hex --output production_final1.hex 
mergehex.exe  --merge  production_final1.hex s132_ble_app.hex --output production_final2.hex 
mergehex.exe  --merge  production_final2.hex   bootloader_settings.hex  --output BRU_final.hex 
del production_final1.hex
del production_final2.hex
del s132_ble_app.hex
del dfu_bootloader.hex
:pause>nul
goto start

:6
nrfjprog -f NRF52 --reset
:pause>nul
goto start

:7
::nrfjprog.exe  -f NRF52  --program "app_test.hex" --verify
::nrfjprog.exe  -f NRF52  --program "dfu_bootloader.hex" --verify
::nrfjprog.exe  -f NRF52  --program "bootloader_settings.hex" --verify
cls
nrfjprog -f NRF52 --eraseall
::nrfjprog --family NRF52 --recovers 
echo Programming "BRU_final.hex"
nrfjprog.exe  -f NRF52  --program  "BRU_final.hex" --verify
nrfjprog -f NRF52 --reset
echo ......
:pause>nul
goto start



:end
echo Error command
goto start

:exit

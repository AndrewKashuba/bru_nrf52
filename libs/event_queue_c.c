/**
  @attention The Reinit method has to called succesfull before any other later methods
*/

#include "event_queue_c.h"
#include "app_util_platform.h"

uint8_t sQueueTail, sQueueHead, sQueueSize;
gevent_t* spEventQueue;
gevent_t eventQueueBuffer[APP_EVENT_QUEUE_SIZE];
static bool evt_queue_IsEmpty(void);
static bool evt_queue_IsFull(void);
static uint8_t evt_queue_NexIndex(uint8_t curIndex);

//*******************************************************************************************
/**
  @brief Reinit the event queue data object
  @param[in] queueSize The number event can be stored in buffer
  @param[in] pEventBuffer Pointer to the buffer to storage events
  @return TRUE done success
*/
bool evt_queue_Reinit(uint8_t queueSize, gevent_t* pEventBuffer)
{
  if (! pEventBuffer){
    return false;
  }
  else{
    spEventQueue = pEventBuffer;
    sQueueSize = queueSize;
    sQueueTail = 0;
    sQueueHead = 0;
    return true;
  }
};


//*******************************************************************************************
/**
  @brief Clear the queue
  @return Always return TRUE with this implement
*/
bool evt_queue_Clear(void)
{
  sQueueTail = sQueueHead;
  return true;
}


//*******************************************************************************************
/**
  @brief Calculate the next index
  @param[in] curIndex Current index to calculate
  @return The next index
*/
static uint8_t evt_queue_NexIndex(uint8_t curIndex)
{
  return (curIndex < sQueueSize) ? (curIndex + 1) : 0;
}

//*******************************************************************************************
/**
  @brief Checking if the queue is empty
  @return TRUE the queue is empty
*/
static bool evt_queue_IsEmpty(void)
{
  return (sQueueTail == sQueueHead);
};


//*******************************************************************************************
/**
  @brief Checking if the queue is full
  @return TRUE the queue is full
*/
static bool evt_queue_IsFull(void)
{
  return (sQueueTail == evt_queue_NexIndex(sQueueHead));
};


//*******************************************************************************************
/**
  @brief Post an event to the queue
  @param[in] pEvent Pointer to the event to post. Event will be copied to the queue
  @return TRUE done success
*/
bool evt_queue_Post(gevent_t* pEvent)
{
  static bool stat_post;
	
  CRITICAL_REGION_ENTER();
  if (! evt_queue_IsFull()){
    spEventQueue[sQueueHead] = *pEvent;
    sQueueHead = evt_queue_NexIndex(sQueueHead);
    stat_post = true;
  }
  else{
    stat_post = false;
  }
  CRITICAL_REGION_EXIT();
	
  return stat_post;
};


//*******************************************************************************************
/**
  @brief Get an event from the queue
  @param[out] pRetEvent Pointer to output data event
  @return TRUE: the queue is not empty and an event is copied to *pRetEvent
*/
bool evt_queue_GetEvent(gevent_t* pRetEvent)
{
  static bool stat;
	
  CRITICAL_REGION_ENTER();
  if (! evt_queue_IsEmpty()){
    *pRetEvent = spEventQueue[sQueueTail];
    sQueueTail = evt_queue_NexIndex(sQueueTail);
    stat = true;
  }
  else{
    stat = false;
  }
  CRITICAL_REGION_EXIT();
	
  return stat;
};

/**
 @file systmr.h
 @version 1.0
 @date 2017-12-14
 @brief System timer API
 @attention The library use RTC2
*/
#ifndef __SYS_TMR_H
#define __SYS_TMR_H

    #ifdef __cplusplus
        extern "C" {
    #endif
            
        #include <stdint.h>
        #include "nrf_drv_rtc.h"
        #include "nrf_drv_clock.h"
        
        
        #define SYSTMR_RTC_INSTANCE_INDEX	2 // Use RTC2 to implement this timer
        #define SYSTMR_NUM_MSEC_PER_TICK    (125)
        #define SYSTMR_NUM_TICK_PER_SEC     (8)

        bool SYSTMR_Reinit(void);
        uint32_t SYSTMR_Second(void);
        uint32_t SYSTMR_GetNumTick(void);
    
    #ifdef __cplusplus
        }
    #endif
        
#endif /*  __SYS_TMR_H */

#ifndef __DBPRINTF_H
#define __DBPRINTF_H

#ifdef __cplusplus
extern "C"
{
#endif

#include "SEGGER_RTT.h"
  
#define DBPRINT_EN      1
#define INFOPRINT_EN    1
#define ERRPRINT_EN     1  
#define LOGPRINT_EN     1
  
  
#if (DBPRINT_EN == 1)  
  #define RTT_Reinit()        do{ SEGGER_RTT_Init(); } while(0)
  
  #define DB_Printf(...)      do{ SEGGER_RTT_printf(0, __VA_ARGS__); } while(0)
  #define DB_Puts(str)        do{ SEGGER_RTT_WriteString(0, (str)); } while(0)
  #define DB_Getline(str)     do{                                                     \
                                char c; char *pStr = (str);                           \
                                do{                                                   \
                                  while (! SEGGER_RTT_HasKey()){ /*wait a key */};   \
                                  c = SEGGER_RTT_GetKey();                            \
                                  if ((c != '\n') && (c != '\r')){ *pStr = c; pStr++; }                \
                                  else{ *pStr = '\0'; }                               \
                                } while ((c != '\n') && (c != '\r'));                                  \
                              } while(0)
#else
  #define RTT_Reinit()
  #define DB_Printf(...)
  #define DB_WriteStr(str)                           
#endif                              
  

#if (INFOPRINT_EN == 1)                            


  #define INFO_Printf(...)    do{ SEGGER_RTT_printf(0, __VA_ARGS__); } while(0)
  #define INFO_Puts(str)      do{ SEGGER_RTT_WriteString(0, (str)); } while(0)
#else
  #define INFO_Printf(...)
  #define INFO_WriteStr(str)  
#endif


#if (ERRPRINT_EN == 1)  
  #define ERR_Printf(...)     do{ SEGGER_RTT_printf(0, __VA_ARGS__); } while(0)
  #define ERR_Puts(str)       do{ SEGGER_RTT_WriteString(0, (str)); } while(0)
#else
  #define ERR_Printf(...)
  #define ERR_WriteStr(str)
#endif  


#if (LOGPRINT_EN == 1)  
  #define LOG_Printf(...)     do{ SEGGER_RTT_printf(0, __VA_ARGS__); } while(0)
  #define LOG_Puts(str)       do{ SEGGER_RTT_WriteString(0, (str));  } while(0)
  #define LOG_PutChar(c)      do{ SEGGER_RTT_PutChar(0, (c));        } while(0)
#else
  #define LOG_Printf(...)
  #define LOG_WriteStr(str)
  #define LOG_PutChar(c)
#endif    
  
#ifdef __cplusplus
}
#endif

#endif /* __DBPRINTF_H */
